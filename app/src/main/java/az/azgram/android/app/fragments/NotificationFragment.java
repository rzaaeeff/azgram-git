package az.azgram.android.app.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import az.azgram.android.app.R;
import az.azgram.android.app.activities.CoreActivity;
import az.azgram.android.app.adapters.notification.RecyclerAdapterNotification;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.notifications.FollowRequestListReceivedEvent;
import az.azgram.android.app.events.notifications.FollowRequestsClearedEvent;
import az.azgram.android.app.events.notifications.NotificationListReceivedEvent;
import az.azgram.android.app.events.notifications.NotificationReceivedEvent;
import az.azgram.android.app.events.notifications.NotificationsClearedEvent;
import az.azgram.android.app.events.notifications.UserActionListReceivedEvent;
import az.azgram.android.app.events.notifications.UserActionsClearedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.models.response.NotificationListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment implements View.OnClickListener {
    // Constants
    public static final byte CATEGORY_COUNT = 3;
    public static final byte CATEGORY_NOTIFICATIONS = 0;
    public static final byte CATEGORY_USER_ACTIONS = 1;
    public static final byte CATEGORY_FOLLOW_REQUESTS = 2;

    // Widgets
    private Button[] categoryButtons = new Button[CATEGORY_COUNT];
    private LinearLayout linearLayoutDelete;
    private SuperRecyclerView recyclerView;
    private TextView textViewError;

    // Other elements
    private Activity activity;
    private NotificationListModel[] data = new NotificationListModel[CATEGORY_COUNT];
    private RecyclerAdapterNotification[] adapter = new RecyclerAdapterNotification[CATEGORY_COUNT];
    private byte currentCategory = -1;
    private boolean[] isEmpty = new boolean[CATEGORY_COUNT];
    private boolean[] isInitialized = new boolean[CATEGORY_COUNT];
    private boolean[] isCompleted = new boolean[CATEGORY_COUNT];
    private boolean[] isLoading = new boolean[CATEGORY_COUNT];
    private CoreModel request;
    private NetworkOperations service;
    private static boolean isVisible;

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        activity = getActivity();

        init(view);
        initValues();
        setListeners();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        BusProvider.getInstance().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisibleToUser()) {
            doFirstTimeInitialization();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        isVisible = isVisibleToUser;

        if (isVisibleToUser && isResumed()) {
            doFirstTimeInitialization();
        }
    }

    @Override
    public void onClick(View v) {
        for (byte category = 0; category < categoryButtons.length; category++)
            if (v.getId() == categoryButtons[category].getId()) {

                if (category != currentCategory) {
                    categoryButtons[category].setBackgroundResource(R.drawable.notification_category_button_selected_background);
                    categoryButtons[category].setTextColor(Color.WHITE);
                    categoryButtons[currentCategory].setBackgroundResource(R.drawable.notification_category_button_background);
                    categoryButtons[currentCategory].setTextColor(Color.parseColor("#337ab7"));
                    currentCategory = category;
                    showCategory();
                    if (!isInitialized[currentCategory]) loadMore();
                    if (category == CATEGORY_NOTIFICATIONS) {
                        linearLayoutDelete.setVisibility(View.VISIBLE);
                        new Cache(activity).setUnseenNotificationCount(0);
                    } else {
                        linearLayoutDelete.setVisibility(View.GONE);
                    }
                }

                return;
            }

        if (v.getId() == linearLayoutDelete.getId())
            clear();
    }

    public static boolean isVisibleToUser() {
        return isVisible;
    }

    private void init(View view) {
        categoryButtons[CATEGORY_NOTIFICATIONS] = (Button) view.findViewById(R.id.buttonNotifications_NotificationFragment);
        categoryButtons[CATEGORY_USER_ACTIONS] = (Button) view.findViewById(R.id.buttonFollowing_NotificationFragment);
        categoryButtons[CATEGORY_FOLLOW_REQUESTS] = (Button) view.findViewById(R.id.buttonRequests_NotificationFragment);
        linearLayoutDelete = (LinearLayout) view.findViewById(R.id.linearLayoutDelete_NotificationFragment);
        recyclerView = (SuperRecyclerView) view.findViewById(R.id.recyclerView_NotificationFragment);
        textViewError = (TextView) view.findViewById(R.id.textViewError_NotificationFragment);

        setRetainInstance(true);
    }

    private void initValues() {
//         We assume no category will be empty
//         We assume no category is loading
//         We assume no category is initialized
//         We assume no category is completed
        for (int i = 0; i < CATEGORY_COUNT; i++) {
            isEmpty[i] = false;
            isLoading[i] = false;
            isInitialized[i] = false;
            isCompleted[i] = false;
        }

        textViewError.setText(R.string.NotificationFragmentEmptyText);

        request = new CoreModel();
        request.setToken(new Cache(activity).loadUser().getToken());
        request.setApiKey(Keys.API_KEY);

        // initializing data&adapter
        for (int i = 0; i < CATEGORY_COUNT; i++) {
            data[i] = new NotificationListModel();
            data[i].setNotifications(new ArrayList<NotificationModel>());
            adapter[i] = new RecyclerAdapterNotification(activity, data[i]);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter[CATEGORY_NOTIFICATIONS]);

        service = new NetworkOperations(activity, BusProvider.getInstance());
        service.getNotificationsEndpoint();
    }

    private void setListeners() {
        for (Button categoryButton : categoryButtons) {
            categoryButton.setOnClickListener(NotificationFragment.this);
        }

        recyclerView.setOnMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                loadMore();
            }
        });

        recyclerView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initValues();
                adapter[currentCategory].notifyDataSetChanged();
            }
        });

        linearLayoutDelete.setOnClickListener(this);
    }

    private void doFirstTimeInitialization() {
        // first load
        if (currentCategory == -1) {
            currentCategory = CATEGORY_NOTIFICATIONS;
            categoryButtons[currentCategory].setBackgroundResource(R.drawable.notification_category_button_selected_background);
            categoryButtons[currentCategory].setTextColor(Color.WHITE);
        }

        if (currentCategory == CATEGORY_NOTIFICATIONS) {
            new Cache(activity).setUnseenNotificationCount(0);
        }

        if (!isInitialized[currentCategory]) loadMore();
    }

    private void setLoading(boolean loading) {
        recyclerView.setLoadingMore(loading);
        isLoading[currentCategory] = loading;

        if (loading)
            recyclerView.getMoreProgressView().setVisibility(View.VISIBLE);
        else
            recyclerView.getMoreProgressView().setVisibility(View.GONE);
    }

    private void setRefreshing(boolean refreshing) {
        recyclerView.setRefreshing(refreshing);
    }

    private void loadMore() {
        if (/*isEmpty[currentCategory]
                || */isCompleted[currentCategory]
                || isLoading[currentCategory])
            return;

        if (data[currentCategory].getPage() != null)
            request.setPage(data[currentCategory].getPage() + 1);

        service.getNotificationsEndpoint().autoGet(request, currentCategory);

        setLoading(true);
    }

    private void clear() {
        // TODO: Remove following return
        if (true)
            return;

        if (isEmpty[currentCategory]
                || isCompleted[currentCategory]
                || recyclerView.isLoadingMore())
            return;

        service.getNotificationsEndpoint().clearNotifications(request);

        setLoading(true);
    }

    private void showCategory() {
        if (isEmpty[currentCategory]) {
            textViewError.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        } else {
            textViewError.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        recyclerView.setAdapter(adapter[currentCategory]);
    }

    private void onReceiveResponse(NotificationListModel response, byte category) {
        setLoading(false);
        setRefreshing(false);

        // TODO: 6/9/2017 Remove following code
        if (response == null)
            return;

        if (response.getMessageId() == null || response.getMessageId() == NetworkOperations.API_SUCCESS) {
            data[category].setPage(response.getPage());
            data[category].getNotifications().addAll(response.getNotifications());
            isInitialized[category] = true;
            isCompleted[category] = response.getPage() == null;
        } else if (response.getMessageId() == NetworkOperations.EMPTY) {
            isEmpty[category] = true;
            isCompleted[category] = true;
        }


        if (category == currentCategory) {
            adapter[category].notifyDataSetChanged();
        } else {
            showCategory();
        }
    }

    private void onCleared(CoreModel response, byte category) {
        setLoading(false);
        setRefreshing(false);

        if (response.getMessageId() == NetworkOperations.API_SUCCESS) {
            isEmpty[category] = true;
            data[category].getNotifications().clear();
            showCategory();
        }
    }

    @Subscribe
    @Produce
    public void onAzGramPushNotification(final NotificationReceivedEvent event) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                data[CATEGORY_NOTIFICATIONS].getNotifications().add(0, event.getModel());

                if (isEmpty[CATEGORY_NOTIFICATIONS])
                    isEmpty[CATEGORY_NOTIFICATIONS] = false;

                showCategory();
            }
        });
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(NotificationListReceivedEvent event) {
        onReceiveResponse(event.getModel(), CATEGORY_NOTIFICATIONS);
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(NotificationsClearedEvent event) {
        onCleared(event.getModel(), CATEGORY_NOTIFICATIONS);
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(UserActionListReceivedEvent event) {
        onReceiveResponse(event.getModel(), CATEGORY_USER_ACTIONS);
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(UserActionsClearedEvent event) {
        onCleared(event.getModel(), CATEGORY_USER_ACTIONS);
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(FollowRequestListReceivedEvent event) {
        onReceiveResponse(event.getModel(), CATEGORY_FOLLOW_REQUESTS);
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(FollowRequestsClearedEvent event) {
        onCleared(event.getModel(), CATEGORY_FOLLOW_REQUESTS);
    }
}