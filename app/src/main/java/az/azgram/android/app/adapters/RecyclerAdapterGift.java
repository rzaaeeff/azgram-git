package az.azgram.android.app.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;

import az.azgram.android.app.R;
import az.azgram.android.app.models.GiftModel;
import az.azgram.android.app.models.response.GiftListModel;

/**
 * Created by Rzaaeeff on 7/19/2017.
 */

public class RecyclerAdapterGift extends RecyclerView.Adapter<RecyclerAdapterGift.GiftHolder> {

    private Context context;
    private GiftListModel data;
    private String currency;

    public RecyclerAdapterGift(Context context, GiftListModel data) {
        this.context = context;
        this.data = data;

        currency = context.getString(R.string.currency);
    }

    @Override
    public GiftHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.recycler_item_gift,
                parent,
                false
        );

        return new GiftHolder(view);
    }

    @Override
    public void onBindViewHolder(GiftHolder holder, int position) {
        holder.bind(data.getGifts().get(position));
    }

    @Override
    public int getItemCount() {
        return data.getGifts().size();
    }

    public GiftModel getSelectedGift() {
        for (GiftModel gift: data.getGifts()) {
            if (gift.isSelected()) {
                return gift;
            }
        }

        return null;
    }

    public void clearSelection() {
        for (GiftModel gift : data.getGifts()) {
            gift.setSelected(false);
        }

        notifyDataSetChanged();
    }

    class GiftHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private SimpleDraweeView draweeView;
        private TextView textView;
        private ControllerListener controllerListener;

        public GiftHolder(View itemView) {
            super(itemView);

            draweeView = (SimpleDraweeView) itemView.findViewById(R.id.draweeView);
            textView = (TextView) itemView.findViewById(R.id.textView);

            itemView.setOnClickListener(this);

            controllerListener = new BaseControllerListener<ImageInfo>() {
                @Override
                public void onFailure(String id, Throwable throwable) {
                    draweeView.setImageResource(R.drawable.post_photo_placeholder);
                }
            };
        }

        public void bind(GiftModel gift) {
            if (gift.getPrice() > 0) {
                textView.setText(String.valueOf(gift.getPrice()) + " " + currency);
            } else {
                textView.setText(R.string.free);
            }

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setUri(gift.getUrl())
                    .setControllerListener(controllerListener)
                    .setAutoPlayAnimations(true)
                    .build();

            draweeView.setController(controller);

            if (gift.isSelected()) {
                itemView.setBackgroundColor(Color.parseColor("#22000000"));
            } else {
                itemView.setBackgroundColor(Color.TRANSPARENT);
            }
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                // unselect previous
                for (GiftModel gift: data.getGifts()) {
                    gift.setSelected(false);
                }

                GiftModel gift = data.getGifts().get(position);
                gift.setSelected(true);

                notifyDataSetChanged();
            }
        }
    }
}