package az.azgram.android.app.models.response;

import java.util.List;

import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.GiftModel;

/**
 * Created by Rzaaeeff on 1/12/2017.
 */

public class GiftListModel extends CoreModel {
    private List<GiftModel> gifts;

    public List<GiftModel> getGifts() {
        return gifts;
    }

    public void setGifts(List<GiftModel> gifts) {
        this.gifts = gifts;
    }
}
