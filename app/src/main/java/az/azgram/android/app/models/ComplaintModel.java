package az.azgram.android.app.models;

/**
 * Created by Rzaaeeff on 2/5/2017.
 */

public class ComplaintModel extends CoreModel {
    private PostModel target;
    private String text;

    public PostModel getTarget() {
        return target;
    }

    public void setTarget(PostModel target) {
        this.target = target;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
