package az.azgram.android.app.activities;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class UploadControllerActivity extends AppCompatActivity {

    // Constants
    public static final String EXTRA_CHOICE = "extra_choice";
    public static final int CHOICE_PHOTO = 0;
    public static final int CHOICE_VIDEO = 1;
    public static final int CHOICE_AD = 2;
    private static final int REQUEST_PICK_PHOTO = 90;
    private static final int REQUEST_PICK_VIDEO = 91;
    private static final int REQUEST_DESCRIBE_POST = 92;

    // Other elements
    private Uri uriTemporaryImage;
    private int choice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        choice = getIntent().getIntExtra(EXTRA_CHOICE, CHOICE_PHOTO);
        pick();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        deleteTemporaryImage();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_PICK_PHOTO:
                if (resultCode == RESULT_OK) {
                    uriTemporaryImage = Uri.parse(data.getStringExtra("data"));
                    Intent intent = new Intent(UploadControllerActivity.this,
                            choice == CHOICE_AD ? DescribeAdActivity.class : DescribePostActivity.class);
                    intent.putExtra("data", uriTemporaryImage);
                    intent.putExtra(EXTRA_CHOICE, CHOICE_PHOTO);
                    startActivityForResult(intent, REQUEST_DESCRIBE_POST);
                } else if (resultCode == RESULT_CANCELED) {
                    deleteTemporaryImage();
                    finish();
                }
                break;
            case REQUEST_PICK_VIDEO:
                if (resultCode == RESULT_OK) {
                    uriTemporaryImage = data.getData();
                    Intent intent = new Intent(UploadControllerActivity.this, DescribePostActivity.class);
                    intent.putExtra("data", uriTemporaryImage);
                    intent.putExtra(EXTRA_CHOICE, CHOICE_VIDEO);
                    startActivityForResult(intent, REQUEST_DESCRIBE_POST);
                } else if (resultCode == RESULT_CANCELED) {
                    finish();
                }
                break;
            case REQUEST_DESCRIBE_POST:
                if (resultCode == RESULT_OK) {
                    deleteTemporaryImage();
                    finish();
                } else if (resultCode == RESULT_CANCELED) {
                    deleteTemporaryImage();
                    pick();
                }
                break;
        }
    }

    private void pick() {
        Intent intent;
        switch (choice) {
            case CHOICE_AD:
            case CHOICE_PHOTO:
                intent = new Intent(UploadControllerActivity.this, PhotoPickerActivity.class);
                startActivityForResult(intent, REQUEST_PICK_PHOTO);
                break;
            case CHOICE_VIDEO:
//                intent = new Intent(Intent.ACTION_PICK);
//                intent.setType("video/*");
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, REQUEST_PICK_VIDEO);
                break;
        }
    }

    private void deleteTemporaryImage() {
        if (choice == CHOICE_PHOTO || choice == CHOICE_AD) {
            if (uriTemporaryImage != null) {
                getContentResolver().delete(uriTemporaryImage, null, null);
                uriTemporaryImage = null;
            }
        }
    }
}
