package az.azgram.android.app.events.balance;

import az.azgram.android.app.models.CoreModel;

/**
 * Created by Rzaaeeff on 7/19/2017.
 */

public class MoneyRequestedEvent {
    private CoreModel model;

    public MoneyRequestedEvent(CoreModel model) {
        this.model = model;
    }

    public CoreModel getModel() {
        return model;
    }

    public void setModel(CoreModel model) {
        this.model = model;
    }
}
