package az.azgram.android.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.app.AppSettingsReceivedEvent;
import az.azgram.android.app.events.app.AppSettingsSavedEvent;
import az.azgram.android.app.events.users.LoggedOutEvent;
import az.azgram.android.app.events.users.ProfileDeletedEvent;
import az.azgram.android.app.models.AppSettingsModel;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;
import cn.refactor.lib.colordialog.ColorDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    // Constants

    // Widgets
    private AppCompatSpinner spinnerLanguage;
    private SwitchCompat switchPrivateProfile, switchNotifyAboutSubscription, switchAcceptQuestion,
            switchAcceptAnonymousQuestion, switchNotifyAboutOtherDevices, switchPushNotification,
            switchShowInGuestList;
    private AppCompatCheckBox checkBoxOnline, checkBoxDefault, checkBoxOffline;
    private Button buttonBlocked, buttonBlocker, buttonNotFollower, buttonPrivate, buttonLiked,
            buttonDisliked, buttonSubscribed, buttonPrivacyPolicy, buttonTerms, buttonFAQ, buttonDelete,
            buttonLogOutAll, buttonLogOut;

    // Other elements
    private NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
    private UserModel request;
    private boolean isInitialized;
    private AppSettingsModel cachedSettings;
    private Cache cache;

    // see usage for details
    private int onItemSelectedListenerCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        cache = new Cache(this);

        init();
        initValues();
        setListeners();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);

        if (!isInitialized) {
            UserModel cachedUser = cache.loadUser();

            UserModel request = new UserModel();
            request.setToken(cachedUser.getToken());
            request.setUsername(cachedUser.getUsername());
            request.setApiKey(Keys.API_KEY);

            service.getAppEndpoint().getSettings(request);
        }
    }

    @Override
    protected void onPause() {
        BusProvider.getInstance().unregister(this);

        super.onPause();
    }

    public void openAccount(View view) {
        Intent intent = new Intent(this, AccountActivity.class);
        startActivity(intent);
    }

    public void openProfileSettings(View view) {
        Intent intent = new Intent(this, ProfileSettingsActivity.class);
        startActivity(intent);
    }

    public void init() {
        spinnerLanguage = (AppCompatSpinner) findViewById(R.id.spinnerLanguage);

        switchPrivateProfile = (SwitchCompat) findViewById(R.id.switchPrivateProfile);
        switchNotifyAboutSubscription = (SwitchCompat) findViewById(R.id.switchNotifyAboutSubscription);
        switchAcceptQuestion = (SwitchCompat) findViewById(R.id.switchAcceptQuestion);
        switchAcceptAnonymousQuestion = (SwitchCompat) findViewById(R.id.switchAcceptAnonymousQuestion);
        switchNotifyAboutOtherDevices = (SwitchCompat) findViewById(R.id.switchNotifyAboutOtherDevices);
        switchPushNotification = (SwitchCompat) findViewById(R.id.switchPushNotification);
        switchShowInGuestList = (SwitchCompat) findViewById(R.id.switchShowInGuestList);

        checkBoxOnline = (AppCompatCheckBox) findViewById(R.id.checkBoxOnline);
        checkBoxDefault = (AppCompatCheckBox) findViewById(R.id.checkBoxDefault);
        checkBoxOffline = (AppCompatCheckBox) findViewById(R.id.checkBoxOffline);

        buttonBlocked = (Button) findViewById(R.id.buttonBlocked);
        buttonBlocker = (Button) findViewById(R.id.buttonBlocker);
        buttonNotFollower = (Button) findViewById(R.id.buttonNotFollower);
        buttonPrivate = (Button) findViewById(R.id.buttonPrivate);
        buttonLiked = (Button) findViewById(R.id.buttonLiked);
        buttonDisliked = (Button) findViewById(R.id.buttonDisliked);
        buttonSubscribed = (Button) findViewById(R.id.buttonSubscribed);
        buttonPrivacyPolicy = (Button) findViewById(R.id.buttonPrivacyPolicy);
        buttonTerms = (Button) findViewById(R.id.buttonTerms);
        buttonFAQ = (Button) findViewById(R.id.buttonFAQ);
        buttonDelete = (Button) findViewById(R.id.buttonDelete);
        buttonLogOutAll = (Button) findViewById(R.id.buttonLogOutAll);
        buttonLogOut = (Button) findViewById(R.id.buttonLogOut);
    }

    public void initValues() {
        UserModel cachedUser = cache.loadUser();
        request = new UserModel();
        request.setUsername(cachedUser.getUsername());
        request.setToken(cachedUser.getToken());
        request.setApiKey(Keys.API_KEY);

        // init service
        service.getAppEndpoint();

        cachedSettings = cache.loadSettings();
        applySettings();
    }

    public void setListeners() {
        spinnerLanguage.setOnItemSelectedListener(this);

        switchPrivateProfile.setOnCheckedChangeListener(this);
        switchNotifyAboutSubscription.setOnCheckedChangeListener(this);
        switchAcceptQuestion.setOnCheckedChangeListener(this);
        switchAcceptAnonymousQuestion.setOnCheckedChangeListener(this);
        switchNotifyAboutOtherDevices.setOnCheckedChangeListener(this);
        switchPushNotification.setOnCheckedChangeListener(this);
        switchShowInGuestList.setOnCheckedChangeListener(this);

        checkBoxOnline.setOnCheckedChangeListener(this);
        checkBoxDefault.setOnCheckedChangeListener(this);
        checkBoxOffline.setOnCheckedChangeListener(this);

        buttonBlocked.setOnClickListener(this);
        buttonBlocker.setOnClickListener(this);
        buttonNotFollower.setOnClickListener(this);
        buttonPrivate.setOnClickListener(this);
        buttonLiked.setOnClickListener(this);
        buttonDisliked.setOnClickListener(this);
        buttonSubscribed.setOnClickListener(this);
        buttonPrivacyPolicy.setOnClickListener(this);
        buttonTerms.setOnClickListener(this);
        buttonFAQ.setOnClickListener(this);
        buttonDelete.setOnClickListener(this);
        buttonLogOutAll.setOnClickListener(this);
        buttonLogOut.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // this method is called when user selects language from spinner
        // but it's also called during initialization of spinner
        // so wee keep counter for that
        if (onItemSelectedListenerCounter++ > 0) {
            AppSettingsModel settings = new AppSettingsModel();
            settings.setChange("language");
            settings.setLanguage((byte) position);
            settings.setToken(request.getToken());
            settings.setApiKey(request.getApiKey());

            service.getAppEndpoint().saveSettings(settings);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView instanceof CheckBox) {
            byte status;
            checkBoxOnline.setOnCheckedChangeListener(null);
            checkBoxDefault.setOnCheckedChangeListener(null);
            checkBoxOffline.setOnCheckedChangeListener(null);

            checkBoxOnline.setChecked(false);
            checkBoxDefault.setChecked(false);
            checkBoxOffline.setChecked(false);
            buttonView.setChecked(true);

            checkBoxOnline.setOnCheckedChangeListener(this);
            checkBoxDefault.setOnCheckedChangeListener(this);
            checkBoxOffline.setOnCheckedChangeListener(this);

            switch (buttonView.getId()) {
                case R.id.checkBoxDefault:
                    status = AppSettingsModel.STATUS.STANDARD;
                    break;
                case R.id.checkBoxOffline:
                    status = AppSettingsModel.STATUS.OFFLINE;
                    break;
                default:
                    status = AppSettingsModel.STATUS.ONLINE;
                    break;
            }

            AppSettingsModel settings = new AppSettingsModel();
            settings.setToken(request.getToken());
            settings.setApiKey(request.getApiKey());
            settings.setChange("status");
            settings.setStatus(status);

            service.getAppEndpoint().saveSettings(settings);
        } else {
            AppSettingsModel settings = new AppSettingsModel();

            switch (buttonView.getId()) {
                case R.id.switchPrivateProfile:
                    settings.setChange("privateProfile");
                    settings.setPrivateProfile(isChecked);
                    break;
                case R.id.switchNotifyAboutSubscription:
                    settings.setChange("notifyAboutSubscription");
                    settings.setNotifyAboutSubscription(isChecked);
                    break;
                case R.id.switchAcceptQuestion:
                    settings.setChange("acceptQuestion");
                    settings.setAcceptQuestion(isChecked);
                    break;
                case R.id.switchAcceptAnonymousQuestion:
                    settings.setChange("acceptAnonymousQuestion");
                    settings.setAcceptAnonymousQuestion(isChecked);
                    break;
                case R.id.switchNotifyAboutOtherDevices:
                    settings.setChange("notifyAboutOtherDevices");
                    settings.setNotifyAboutOtherDevices(isChecked);
                    break;
                case R.id.switchPushNotification:
                    settings.setChange("pushNotification");
                    settings.setPushNotification(isChecked);
                    break;
                case R.id.switchShowInGuestList:
                    settings.setChange("showInGuestList");
                    settings.setShowInGuestList(isChecked);
                    break;
            }

            settings.setToken(request.getToken());
            settings.setApiKey(request.getApiKey());

            settings.setControlView(buttonView);
            setEnabled(buttonView, false);

            service.getAppEndpoint().saveSettings(settings);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonBlocked:
                openUserList(UserListActivity.Type.Blocked);
                break;
            case R.id.buttonBlocker:
                openUserList(UserListActivity.Type.Blocker);
                break;
            case R.id.buttonNotFollower:
                openUserList(UserListActivity.Type.NotFollower);
                break;
            case R.id.buttonPrivate:
                openPostList(PostListActivity.Type.Private);
                break;
            case R.id.buttonLiked:
                openPostList(PostListActivity.Type.Liked);
                break;
            case R.id.buttonDisliked:
                openPostList(PostListActivity.Type.Disliked);
                break;
            case R.id.buttonSubscribed:
                openPostList(PostListActivity.Type.Subscribed);
                break;
            case R.id.buttonPrivacyPolicy:
                openInternalBrowser("http://azgram.az/privacy");
                break;
            case R.id.buttonTerms:
                openInternalBrowser("http://azgram.az/terms");
                break;
            case R.id.buttonFAQ:
                openInternalBrowser("http://azgram.az/faq");
                break;
            case R.id.buttonDelete:
                Interaction.getInstance(this)
                        .showPromptDialog(getString(R.string.irreversible_are_you_sure))
                        .setPositiveListener(R.string.Yes, new ColorDialog.OnPositiveListener() {
                            @Override
                            public void onClick(ColorDialog colorDialog) {
                                service.getUsersEndpoint().delete(request);
                            }
                        })
                        .setNegativeListener(R.string.No, new ColorDialog.OnNegativeListener() {
                            @Override
                            public void onClick(ColorDialog colorDialog) {
                                colorDialog.dismiss();
                            }
                        })
                        .show();
                break;
            case R.id.buttonLogOut:
                Interaction.getInstance(this)
                        .showPromptDialog(getString(R.string.are_you_sure))
                        .setPositiveListener(R.string.Yes, new ColorDialog.OnPositiveListener() {
                            @Override
                            public void onClick(ColorDialog colorDialog) {
                                service.getUsersEndpoint().logout(request);
                            }
                        })
                        .setNegativeListener(R.string.No, new ColorDialog.OnNegativeListener() {
                            @Override
                            public void onClick(ColorDialog colorDialog) {
                                colorDialog.dismiss();
                            }
                        })
                        .show();
                break;
            case R.id.buttonLogOutAll:
                Interaction.getInstance(this)
                        .showPromptDialog(getString(R.string.are_you_sure))
                        .setPositiveListener(R.string.Yes, new ColorDialog.OnPositiveListener() {
                            @Override
                            public void onClick(ColorDialog colorDialog) {
                                service.getUsersEndpoint().logoutAll(request);
                            }
                        })
                        .setNegativeListener(R.string.No, new ColorDialog.OnNegativeListener() {
                            @Override
                            public void onClick(ColorDialog colorDialog) {
                                colorDialog.dismiss();
                            }
                        })
                        .show();
                break;
        }
    }

    private void openUserList(byte type) {
        Intent intent = new Intent(this, UserListActivity.class);
        intent.putExtra(UserListActivity.EXTRA_TYPE, type);
        UserModel user = new UserModel();
        user.setUsername(request.getUsername());
        intent.putExtra(UserModel.UID, user);
        startActivity(intent);
    }

    private void openPostList(byte type) {
        Intent intent = new Intent(this, PostListActivity.class);
        intent.putExtra(PostListActivity.EXTRA_TYPE, type);
        startActivity(intent);
    }

    private void openInternalBrowser(String url) {
        Intent intent = new Intent(this, BrowserActivity.class);
        intent.putExtra(BrowserActivity.EXTRA_URL, url);
        startActivity(intent);
    }

    private void setEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        view.setAlpha(enabled ? Constants.ENABLED_STATE_ALPHA : Constants.DISABLED_STATE_ALPHA);
    }

    private void applySettings() {
        if (cachedSettings.getLanguage() != null) {
            spinnerLanguage.setSelection(cachedSettings.getLanguage());
        }

        if (cachedSettings.getPrivateProfile() != null) {
            switchPrivateProfile.setChecked(cachedSettings.getPrivateProfile());
        }

        if (cachedSettings.getNotifyAboutSubscription() != null) {
            switchNotifyAboutSubscription.setChecked(cachedSettings.getNotifyAboutSubscription());
        }

        if (cachedSettings.getAcceptQuestion() != null) {
            switchAcceptQuestion.setChecked(cachedSettings.getAcceptQuestion());
        }

        if (cachedSettings.getAcceptAnonymousQuestion() != null) {
            switchAcceptAnonymousQuestion.setChecked(cachedSettings.getAcceptAnonymousQuestion());
        }

        if (cachedSettings.getNotifyAboutOtherDevices() != null) {
            switchNotifyAboutOtherDevices.setChecked(cachedSettings.getNotifyAboutOtherDevices());
        }

        if (cachedSettings.getPushNotification() != null) {
            switchPushNotification.setChecked(cachedSettings.getPushNotification());
        }

        if (cachedSettings.getShowInGuestList() != null) {
            switchShowInGuestList.setChecked(cachedSettings.getShowInGuestList());
        }

        if (cachedSettings.getStatus() != null) {
            switch (cachedSettings.getStatus()) {
                case AppSettingsModel.STATUS.OFFLINE:
                    checkBoxOffline.setChecked(true);
                    break;
                case AppSettingsModel.STATUS.ONLINE:
                    checkBoxOnline.setChecked(true);
                    break;
                case AppSettingsModel.STATUS.STANDARD:
                    checkBoxDefault.setChecked(true);
                    break;
            }
        }
    }

    private void updateSettings() {
        cachedSettings.setLanguage((byte) spinnerLanguage.getSelectedItemPosition());

        cachedSettings.setPrivateProfile(switchPrivateProfile.isChecked());

        cachedSettings.setNotifyAboutSubscription(switchNotifyAboutSubscription.isChecked());

        cachedSettings.setAcceptQuestion(switchAcceptQuestion.isChecked());

        cachedSettings.setAcceptAnonymousQuestion(switchAcceptAnonymousQuestion.isChecked());

        cachedSettings.setNotifyAboutOtherDevices(switchNotifyAboutOtherDevices.isChecked());

        cachedSettings.setPushNotification(switchPushNotification.isChecked());

        cachedSettings.setShowInGuestList(switchShowInGuestList.isChecked());

        if (checkBoxOffline.isChecked()) {
            cachedSettings.setStatus(AppSettingsModel.STATUS.OFFLINE);
        } else if (checkBoxOnline.isChecked()) {
            cachedSettings.setStatus(AppSettingsModel.STATUS.ONLINE);
        } else {
            cachedSettings.setStatus(AppSettingsModel.STATUS.STANDARD);
        }

        cache.saveSettings(cachedSettings);
    }

    @Subscribe
    public void onAzGramServiceResponse(AppSettingsSavedEvent event) {
        AppSettingsModel settings = event.getModel();

        // if it's a switch, we've disabled it
        if (settings.getControlView() instanceof SwitchCompat) {
            setEnabled(settings.getControlView(), true);
        }

        if (settings.getMessageId() == NetworkOperations.API_SUCCESS) {
            updateSettings();
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();

            // reverting
            if (settings.getControlView() instanceof SwitchCompat) {
                SwitchCompat switchCompat = (SwitchCompat) settings.getControlView();
                switchCompat.setOnCheckedChangeListener(null);
                switchCompat.setChecked(
                        !switchCompat.isChecked()
                );
                switchCompat.setOnCheckedChangeListener(null);
            } else if (settings.getControlView() instanceof AppCompatCheckBox) {
                if (cachedSettings.getStatus() != null) {
                    switch (cachedSettings.getStatus()) {
                        case AppSettingsModel.STATUS.OFFLINE:
                            checkBoxOffline.setChecked(true);
                            break;
                        case AppSettingsModel.STATUS.ONLINE:
                            checkBoxOnline.setChecked(true);
                            break;
                        case AppSettingsModel.STATUS.STANDARD:
                            checkBoxDefault.setChecked(true);
                            break;
                    }
                }
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(ProfileDeletedEvent event) {
        onLogOutEvent(event.getModel());
    }

    @Subscribe
    public void onAzGramServiceResponse(LoggedOutEvent event) {
        onLogOutEvent(event.getModel());
    }

    private void onLogOutEvent(CoreModel response) {
        if (response.getMessageId() == NetworkOperations.API_SUCCESS) {
            // clear cache
            new Cache(this).clearUser();

            // go to login
            Intent intent = new Intent(getApplicationContext(), StarterActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(AppSettingsReceivedEvent event) {
        isInitialized = true;
        AppSettingsModel settings = event.getModel();

        if (settings.getMessageId() == null ||
                settings.getMessageId() == NetworkOperations.API_SUCCESS) {
            cachedSettings = settings;
            applySettings();
            cache.saveSettings(settings);
        }
    }
}