package az.azgram.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import az.azgram.android.app.R;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 2/11/2017.
 */

public class RecyclerAdapterMinimalUser extends RecyclerView.Adapter<RecyclerAdapterMinimalUser.UserHolder> {
    private Context context;
    private UserListModel data;
    private Controller controller;

    public RecyclerAdapterMinimalUser(Context context, UserListModel data, Controller controller) {
        this.context = context;
        this.data = data;
        this.controller = controller;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.recycler_item_minimal_user,
                parent,
                false
        );

        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        holder.bind(data.getUsers().get(position));
    }

    @Override
    public int getItemCount() {
        return data.getUsers().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageViewProfilePhoto, imageViewUserBadge;
        private TextView textView;
        private LinearLayout rootLayout;

        public UserHolder(View itemView) {
            super(itemView);

            imageViewProfilePhoto = (ImageView) itemView.findViewById(R.id.imageViewProfilePhotoMinimalUserItem);
            imageViewUserBadge = (ImageView) itemView.findViewById(R.id.imageViewUserBadgeMinimalUserItem);
            textView = (TextView) itemView.findViewById(R.id.textViewMinimalUserItem);
            rootLayout = (LinearLayout) itemView.findViewById(R.id.rootLayoutMinimalUserItem);

            rootLayout.setOnClickListener(this);
        }

        public void bind(UserModel user) {
            Utility.loadWithPicasso(
                    context,
                    user.getProfilePhoto().getSmallestPossible(),
                    imageViewProfilePhoto,
                    R.drawable.profile_photo_placeholder
            );

            imageViewUserBadge.setImageResource(
                    user.isOnline() ? R.drawable.user_online_badge :
                            R.drawable.user_offline_badge
            );

            if (user.getFullName() != null) {
                if (user.getFullName().length() > 15)
                    textView.setText(user.getUsername());
                else
                    textView.setText(user.getFullName());
            } else {
                textView.setText(user.getUsername());
            }

         }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                controller.onSearchResultClicked(data.getUsers().get(position));
            }
        }
    }

    public interface Controller {
        void onSearchResultClicked(CoreModel clickedItem);
    }
}