package az.azgram.android.app.models;

/**
 * Created by Rzaaeeff on 2/9/2017.
 */

public class SearchModel extends CoreModel {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
