package az.azgram.android.app.models;

import java.io.Serializable;

/**
 * Created by Rzaaeeff on 11/30/2016.
 */

public class CommentModel extends CoreModel implements Serializable {

    // Fields
    private UserModel author;
    private String text;
    private PhotoModel photo;

    public UserModel getAuthor() {
        return author;
    }

    public void setAuthor(UserModel author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public PhotoModel getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoModel photo) {
        this.photo = photo;
    }
}
