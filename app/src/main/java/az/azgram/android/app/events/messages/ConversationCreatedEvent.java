package az.azgram.android.app.events.messages;

import az.azgram.android.app.models.response.ConversationWrapperModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class ConversationCreatedEvent {
    private ConversationWrapperModel model;

    public ConversationCreatedEvent(ConversationWrapperModel model) {
        this.model = model;
    }

    public ConversationWrapperModel getModel() {
        return model;
    }

    public void setModel(ConversationWrapperModel model) {
        this.model = model;
    }
}
