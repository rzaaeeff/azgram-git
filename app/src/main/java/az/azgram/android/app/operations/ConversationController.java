package az.azgram.android.app.operations;

import az.azgram.android.app.models.ConversationModel;

/**
 * Created by Rzaaeeff on 4/26/2017.
 */

public interface ConversationController {
    void onConversationClicked(ConversationModel conversation);
    void onConversationDeleteClicked(ConversationModel conversation);
    void onConversationAcceptClicked(ConversationModel conversation);
    void onConversationRejectClicked(ConversationModel conversation);
    void onConversationCreateClicked();
}
