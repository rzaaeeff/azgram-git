package az.azgram.android.app.events.search;

import az.azgram.android.app.models.response.FeedModel;

/**
 * Created by Rzaaeeff on 2/11/2017.
 */

public class PostSearchResultReceivedEvent {
    private FeedModel model;

    public PostSearchResultReceivedEvent(FeedModel model) {
        this.model = model;
    }

    public FeedModel getModel() {
        return model;
    }

    public void setModel(FeedModel model) {
        this.model = model;
    }
}
