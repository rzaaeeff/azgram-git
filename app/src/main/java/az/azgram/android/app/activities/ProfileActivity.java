package az.azgram.android.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Bus;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.messages.MessageSentEvent;
import az.azgram.android.app.events.posts.PostDeletedEvent;
import az.azgram.android.app.events.users.BlockedUserEvent;
import az.azgram.android.app.events.users.UserReceivedEvent;
import az.azgram.android.app.fragments.AnotherUserPrivateProfileFragment;
import az.azgram.android.app.fragments.AnotherUserProfileFragment;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.UserWrapperModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.ProfileOptionsListener;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileActivity extends AppCompatActivity implements ProfileOptionsListener {

    private static final int REQUEST_PICK_USER_AS_RECIPIENT = 133;

    // Widgets
    private Toolbar toolbar;
    private FrameLayout rootLayout;
    private LinearLayout errorLayout, hintLayout;
    private ProgressBar progressBarErrorLayout;
    private ImageView imageViewErrorLayout;
    private TextView textViewErrorLayout, textViewHintLayout;
    private Button buttonUnblock;

    // Other Elements
    public static UserModel request;
    private UserModel user;
    private String username;
    private static boolean isAlive = false;
    private ProgressDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        if (getIntent().getExtras().containsKey(UserModel.UID)) {
            init();
            username = ((UserModel) getIntent().getSerializableExtra(UserModel.UID)).getUsername();
            getUser();
        } else {
            // error
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);
        isAlive = true;
    }

    @Override
    protected void onPause() {
        BusProvider.getInstance().unregister(this);
        isAlive = false;

        super.onPause();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void restart() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public void onShareProfileAsMessageClicked(UserModel user) {
        startActivityForResult(
                new Intent(this, UserPickerActivity.class),
                REQUEST_PICK_USER_AS_RECIPIENT
        );
    }

    @Override
    public void onSubscribeClicked(UserModel user) {

    }

    @Override
    public void onSendGiftClicked(UserModel user) {

    }

    @Override
    public void onReportClicked(UserModel user) {

    }

    @Override
    public void onBlockClicked(UserModel user) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PICK_USER_AS_RECIPIENT) {
            if (resultCode == Activity.RESULT_OK) {
                UserModel recipient = new UserModel();
                recipient.setUsername(((UserModel) data.getSerializableExtra(UserModel.UID)).getUsername());

                MessageModel message = new MessageModel();

                message.setRecipients(new ArrayList<CoreModel>());
                message.getRecipients().add(recipient);

                message.setAuthor(new UserModel());
                message.getAuthor().setToken(request.getToken());

                message.setUser(new UserModel());
                message.getUser().setUsername(username);

                message.setType(MessageModel.Type.USER);
                message.setApiKey(Keys.API_KEY);

                NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
                service.getMessagesEndpoint().send(message);
            }
        }
    }

    /**
     * OnClick function for button {follow}
     * Referenced from XML
     *
     * @param view
     */
    public void followButtonClicked(View view) {
        // TODO: call follow function
    }

    /**
     * Assigns IDs and does some other initialization
     */
    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbarProfileActivity);
        rootLayout = (FrameLayout) findViewById(R.id.rootLayoutProfileActivity);
        errorLayout = (LinearLayout) findViewById(R.id.errorLayoutProfileActivity);
        hintLayout = (LinearLayout) findViewById(R.id.hintLayoutErrorLayoutProfileActivity);
        progressBarErrorLayout = (ProgressBar) findViewById(R.id.progressBarErrorLayoutProfileActivity);
        imageViewErrorLayout = (ImageView) findViewById(R.id.imageViewErrorLayoutProfileActivity);
        textViewErrorLayout = (TextView) findViewById(R.id.textViewErrorLayoutProfileActivity);
        textViewHintLayout = (TextView) findViewById(R.id.textViewHintErrorLayoutProfileActivity);
        buttonUnblock = (Button) findViewById(R.id.buttonUnblock);
    }

    private void getUser() {
        // Getting User from Cache
        Cache cache = new Cache(this);
        UserModel cachedUser = cache.loadUser();

        // setting properties of request
        request = new UserModel();
        request.setToken(cachedUser.getToken());
        request.setUsername(username);
        request.setApiKey(Keys.API_KEY);

        // calling service
        NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
        service.getUsersEndpoint().getUser(request);
    }

    /**
     * When opening another profile check
     * if requested user is same
     *
     * @param username
     * @return
     */
    public static boolean isSameUser(String username) {
        if (isAlive) {
            return request.getUsername().equals(username);
        }

        return false;
    }

    private void openUserProfile(UserModel user) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        AnotherUserProfileFragment fragment = new AnotherUserProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(UserModel.UID, user);
        fragment.setArguments(bundle);
        fragment.addProfileOptionListener(this);

        ft.add(rootLayout.getId(), fragment).commit();
    }

    private void openPrivateUserProfile(UserModel user) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        AnotherUserPrivateProfileFragment fragment = new AnotherUserPrivateProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(UserModel.UID, user);
        fragment.setArguments(bundle);
        fragment.addProfileOptionListener(this);

        ft.add(rootLayout.getId(), fragment).commit();
    }

    @Subscribe
    public void onAzGramServiceResponse(UserReceivedEvent event) {
        if (event.getModel().getMessageId() == null ||
                event.getModel().getMessageId() == NetworkOperations.Users.PRIVATE_PROFILE) {

            user = event.getModel();
            errorLayout.setVisibility(View.GONE);
            toolbar.setVisibility(View.GONE);

            // public profile
            if (user.getMessageId() == null) {
                openUserProfile(user);
                return;
            }

            // private profile
            if (user.getMessageId() == NetworkOperations.Users.PRIVATE_PROFILE) {
                if (user.getFollowStatus() == UserModel.FollowStatus.FOLLOWING)
                    openUserProfile(user);
                else
                    openPrivateUserProfile(user);
            }

        } else {
            if (event.getModel().getMessageId() == NetworkOperations.Users.BLOCKED) {
                progressBarErrorLayout.setVisibility(View.INVISIBLE);
                imageViewErrorLayout.setVisibility(View.VISIBLE);
                textViewErrorLayout.setVisibility(View.VISIBLE);

                textViewErrorLayout.setText(
                        getString(R.string.ProfileActivityErrorBlocked)
                );

                buttonUnblock.setVisibility(View.VISIBLE);
                buttonUnblock.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UserWrapperModel block = new UserWrapperModel();

                        block.setTarget(new UserModel());
                        block.getTarget().setUsername(username);

                        block.setToken(request.getToken());
                        block.setApiKey(request.getApiKey());

                        NetworkOperations service = new NetworkOperations(ProfileActivity.this, BusProvider.getInstance());
                        service.getUsersEndpoint().block(block);
                        dialogLoading = Interaction.getInstance(ProfileActivity.this).showLoadingDialog(
                                getString(R.string.PleaseWait)
                        );
                    }
                });
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(MessageSentEvent event) {
        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, R.string.ProfileActivityUserSent, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(BlockedUserEvent event) {
        if (dialogLoading != null) {
            dialogLoading.hide();
        }

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            ProfileActivity.this.restart();
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }
}