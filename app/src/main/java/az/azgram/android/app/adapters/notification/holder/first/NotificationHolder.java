package az.azgram.android.app.adapters.notification.holder.first;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import az.azgram.android.app.R;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 5/19/2017.
 */

class NotificationHolder extends RecyclerView.ViewHolder {
    // Widgets
    private ImageView imageViewProfilePhoto, imageViewUserBadge;
    private TextView textView;
    private ViewGroup rootLayout;

    // Other elements
    protected Context context;
    protected NotificationModel notification;
    protected String TEXT_FORMAT;
    private View.OnClickListener rootListener;

    public NotificationHolder(View itemView, Context context) {
        super(itemView);

        this.context = context;

        imageViewProfilePhoto = (ImageView) itemView.findViewById(R.id.imageViewProfilePhoto_NotificationItem);
        imageViewUserBadge = (ImageView) itemView.findViewById(R.id.imageViewUserBadge_NotificationItem);
        textView = (TextView) itemView.findViewById(R.id.textView_NotificationItem);
        rootLayout = (ViewGroup) itemView.findViewById(R.id.rootLayout_NotificationItem);

        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void bind(final NotificationModel notification, CharSequence text) {
        this.notification = notification;

        if (notification.getUsers().get(0).getProfilePhoto() != null) {
            Utility.loadWithPicasso(
                    context,
                    notification.getUsers().get(0).getProfilePhoto().getSmallestPossible(),
                    imageViewProfilePhoto,
                    R.drawable.profile_photo_placeholder
            );

            imageViewProfilePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UniversalListenerMethods.onClickUser(
                            notification.getUsers().get(0).getUsername(),
                            context);
                }
            });
        }

        imageViewUserBadge.setImageResource(
                notification.getUsers().get(0).isOnline() == null ||
                        !notification.getUsers().get(0).isOnline() ?
                        R.drawable.user_offline_badge :
                        R.drawable.user_online_badge
        );

        textView.setText(text);
    }

    public void setRootListener(View.OnClickListener listener) {
        rootLayout.setOnClickListener(listener);
        textView.setOnClickListener(listener);
        rootListener = listener;
    }

    public View.OnClickListener getRootListener() {
        return rootListener;
    }

    public TextView getTextView() {
        return textView;
    }

    public ImageView getImageViewProfilePhoto() {
        return imageViewProfilePhoto;
    }
}