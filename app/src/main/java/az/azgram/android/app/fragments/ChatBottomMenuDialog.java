package az.azgram.android.app.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import az.azgram.android.app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatBottomMenuDialog extends BottomSheetDialogFragment implements View.OnClickListener {
    // Widgets
    private TextView textViewMark, textViewClear;
    private Button buttonCancel;

    // Other Elements
    private List<Controller> controllers = new ArrayList<>();

    public ChatBottomMenuDialog() {
        // Required empty public constructor
    }

    public void addController(Controller controller) {
        controllers.add(controller);
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.dialog_fragment_chat_bottom_menu, null);
        dialog.setContentView(contentView);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent())
                .getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_chat_bottom_menu, container, false);

        init(view);

        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getActivity(), getTheme());
    }

    private void init(View view) {
        textViewMark = (TextView) view.findViewById(R.id.textViewMark_ChatBottomMenuDialog);
        textViewClear = (TextView) view.findViewById(R.id.textViewClear_ChatBottomMenuDialog);
        buttonCancel = (Button) view.findViewById(R.id.buttonCancel_ChatBottomMenuDialog);

        textViewMark.setOnClickListener(this);
        textViewClear.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textViewMark_ChatBottomMenuDialog:
                for (Controller controller : controllers) {
                    controller.markAsUnreadClicked();
                }
                dismiss();
                break;
            case R.id.textViewClear_ChatBottomMenuDialog:
                for (Controller controller : controllers) {
                    controller.clearClicked();
                }
                dismiss();
                break;
            case R.id.buttonCancel_ChatBottomMenuDialog:
                dismiss();
                break;
        }
    }

    public interface Controller {
        void markAsUnreadClicked();

        void clearClicked();
    }
}