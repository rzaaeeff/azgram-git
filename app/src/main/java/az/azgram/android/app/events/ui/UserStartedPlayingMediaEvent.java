package az.azgram.android.app.events.ui;

/**
 * Created by Rzaaeeff on 7/3/2017.
 */

public class UserStartedPlayingMediaEvent {
    private String url;

    public UserStartedPlayingMediaEvent(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
