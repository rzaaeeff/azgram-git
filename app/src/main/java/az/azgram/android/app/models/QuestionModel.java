package az.azgram.android.app.models;

import az.azgram.android.app.BuildConfig;

/**
 * Created by Rzaaeeff on 12/28/2016.
 */

public class QuestionModel extends CoreModel {
    public static final String UID = BuildConfig.APPLICATION_ID +
            ".QuestionModel";

    private UserModel author;
    private UserModel target;
    private String text;
    private String answer;
    private Boolean isAnswered;
    private Boolean isAnonymous;
    private PhotoModel backgroundPhoto;

    public UserModel getAuthor() {
        return author;
    }

    public void setAuthor(UserModel author) {
        this.author = author;
    }

    public UserModel getTarget() {
        return target;
    }

    public void setTarget(UserModel target) {
        this.target = target;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Boolean isAnswered() {
        return isAnswered;
    }

    public void setAnswered(Boolean answered) {
        isAnswered = answered;
    }

    public Boolean isAnonymous() {
        return isAnonymous;
    }

    public void setAnonymous(Boolean anonymous) {
        isAnonymous = anonymous;
    }

    public PhotoModel getBackgroundPhoto() {
        return backgroundPhoto;
    }

    public void setBackgroundPhoto(PhotoModel backgroundPhoto) {
        this.backgroundPhoto = backgroundPhoto;
    }
}
