package az.azgram.android.app.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import az.azgram.android.app.AzGram;
import az.azgram.android.app.R;
import az.azgram.android.app.activities.AccountActivity;
import az.azgram.android.app.activities.AskQuestionActivity;
import az.azgram.android.app.activities.MessagingActivity;
import az.azgram.android.app.activities.ProfileActivity;
import az.azgram.android.app.adapters.RecyclerAdapterGifts;
import az.azgram.android.app.adapters.RecyclerAdapterPostsCollection;
import az.azgram.android.app.adapters.RecyclerAdapterQuestions;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.custom.OnSwipeTouchListener;
import az.azgram.android.app.custom.SpacesItemDecoration;
import az.azgram.android.app.events.posts.GiftsFeedReceivedEvent;
import az.azgram.android.app.events.posts.QuestionReceivedEvent;
import az.azgram.android.app.events.users.FollowEvent;
import az.azgram.android.app.events.users.FollowRequestEvent;
import az.azgram.android.app.events.users.ReportedUserEvent;
import az.azgram.android.app.events.users.SubscribedToUserEvent;
import az.azgram.android.app.models.GiftModel;
import az.azgram.android.app.models.QuestionModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.UserWrapperModel;
import az.azgram.android.app.models.response.GiftFeedModel;
import az.azgram.android.app.models.response.QuestionFeedModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.ProfileOptionsListener;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;
import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.Toro;

public class AnotherUserPrivateProfileFragment extends Fragment implements View.OnClickListener {

    // Constants
    private static final byte CONTENT_PAGE_COUNT = 5;
    private static final byte POSTS_COLLECTION_PAGE = 0;
    private static final byte POSTS_LIST_PAGE = 1;
    private static final byte TAGS_PAGE = 2;
    private static final byte GIFTS_PAGE = 3;
    private static final byte QUESTIONS_PAGE = 4;
    private static final int[] THEMES = {
            R.style.ProfileFragmentColorOption1,
            R.style.ProfileFragmentColorOption2,
            R.style.ProfileFragmentColorOption3,
    };
    private static final int DEFAULT_THEME_INDEX = UserModel.ProfileDesign.OPTION3;

    // Widgets
    private RecyclerView recyclerViewContent;
    private ImageButton[] imageButtons;
    private TextView textViewFullName, textViewBio, textViewPostCount, textViewEmpty,
            textViewFollowerCount, textViewFollowingCount, textViewQuestionCount, textViewPrivate;
    private ImageView imageViewUserBadge, imageViewCover;
    private CircleImageView imageViewProfilePhoto;
    private ProgressBar progressBarLoadMore, progressBarFollow;
    private NestedScrollView scrollView;
    private ImageButton imageButtonFollow, imageButtonOptions,
            imageButtonAskQuestion, imageButtonSendMessage;
    private LinearLayout layoutFollowRequest;

    // Other elements
    private List<ProfileOptionsListener> profileOptionListeners = new ArrayList<>();
    private ProgressDialog dialogLoading;
    private RecyclerAdapterGifts adapterGifts;
    private RecyclerAdapterQuestions adapterQuestions;
    private GiftFeedModel feedGift = null;
    private QuestionFeedModel feedQuestion = null;
    private RecyclerView.ItemDecoration itemDecoration =
            new SpacesItemDecoration(RecyclerAdapterPostsCollection.SPACING);
    private byte currentPage = -1;
    private boolean[] isEmpty = new boolean[CONTENT_PAGE_COUNT];
    private boolean[] isInitialized = new boolean[CONTENT_PAGE_COUNT];
    private boolean isLoading = false;
    private String[] emptyText;

    private Context context;
    private UserModel request;
    private UserModel receivedUser;

    public AnotherUserPrivateProfileFragment() {
        // Required empty public constructor
    }

    public void addProfileOptionListener(ProfileOptionsListener listener) {
        profileOptionListeners.add(listener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        context = getContext();

        UserModel user;
        if (getArguments().containsKey(UserModel.UID)) {
            user = (UserModel) getArguments().getSerializable(UserModel.UID);
        } else {
            // TODO: handle error
            return null;
        }

        // Setting theme
        final int preferredTheme;
        if (user.getProfileDesign() != null) {
            preferredTheme = user.getProfileDesign();
        } else {
            preferredTheme = DEFAULT_THEME_INDEX;
        }

        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), THEMES[preferredTheme]);
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);

        // Inflate the layout for this fragment
        View view = localInflater.inflate(R.layout.fragment_another_user_private_profile, container, false);

        init(view);
        initUser(user);
        initValues(user);
        setListeners();
        setFollowButtonListener();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);
        Toro.register(recyclerViewContent);

        // first load
        if (currentPage == -1) {
            currentPage = QUESTIONS_PAGE;
        }

        if (!isInitialized[currentPage]) {
            loadMore(currentPage);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        BusProvider.getInstance().unregister(this);
        Toro.unregister(recyclerViewContent);
    }

    /**
     * Assigns IDs and does initialization...
     */
    private void init(View view) {
        // Standard initialization
        // TextViews
        textViewFullName = (TextView) view.findViewById(R.id.textViewFullNameProfileFragment);
        textViewBio = (TextView) view.findViewById(R.id.textViewBioProfileFragment);
        textViewPostCount = (TextView) view.findViewById(R.id.textViewPostCountProfileFragment);
        textViewEmpty = (TextView) view.findViewById(R.id.textViewEmptyProfileFragment);
        textViewFollowerCount = (TextView) view.findViewById(R.id.textViewFollowerCountProfileFragment);
        textViewFollowingCount = (TextView) view.findViewById(R.id.textViewFollowingCountProfileFragment);
        textViewQuestionCount = (TextView) view.findViewById(R.id.textViewQuestionCountProfileFragment);
        textViewPrivate = (TextView) view.findViewById(R.id.textViewPrivateProfileFragment);

        imageViewUserBadge = (ImageView) view.findViewById(R.id.imageViewUserBadgeProfileFragment);
        imageViewProfilePhoto = (CircleImageView) view.findViewById(R.id.imageViewProfilePhotoProfileFragment);
        imageViewCover = (ImageView) view.findViewById(R.id.main_backdrop_ProfileFragment);

        imageButtonOptions = (ImageButton) view.findViewById(R.id.buttonSettingAnotherUserPrivateProfileFragment);
        imageButtonFollow = (ImageButton) view.findViewById(R.id.imageButtonFollowAnotherUserProfileFragment);
        imageButtonAskQuestion = (ImageButton) view.findViewById(R.id.imageButtonAskQuestionAnotherUserPrivateProfileFragment);
        imageButtonSendMessage = (ImageButton) view.findViewById(R.id.imageButtonSendMessageAnotherUserPrivateProfileFragment);

        layoutFollowRequest = (LinearLayout) view.findViewById(R.id.rootLayoutFollowRequest);

        recyclerViewContent = (RecyclerView) view.findViewById(R.id.recyclerViewContentProfileFragment);
        recyclerViewContent.setNestedScrollingEnabled(false);
        recyclerViewContent.setHasFixedSize(true);
        recyclerViewContent.setItemViewCacheSize(30);
        recyclerViewContent.setDrawingCacheEnabled(true);
        recyclerViewContent.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // Navigation Buttons
        imageButtons = new ImageButton[CONTENT_PAGE_COUNT];
        imageButtons = new ImageButton[CONTENT_PAGE_COUNT];
        imageButtons[POSTS_COLLECTION_PAGE] = (ImageButton) view.findViewById(R.id.imageButtonPostsCollectionProfileFragment);
        imageButtons[POSTS_LIST_PAGE] = (ImageButton) view.findViewById(R.id.imageButtonPostsListProfileFragment);
        imageButtons[TAGS_PAGE] = (ImageButton) view.findViewById(R.id.imageButtonTagsProfileFragment);
        imageButtons[GIFTS_PAGE] = (ImageButton) view.findViewById(R.id.imageButtonGiftsProfileFragment);
        imageButtons[QUESTIONS_PAGE] = (ImageButton) view.findViewById(R.id.imageButtonQuestionsProfileFragment);

        progressBarLoadMore = (ProgressBar) view.findViewById(R.id.progressBarProfileFragment);
        progressBarFollow = (ProgressBar) view.findViewById(R.id.progressBarFollowAnotherUserProfileFragment);
        scrollView = (NestedScrollView) view.findViewById(R.id.scrollViewProfileFragment);

        // Retaing fragment instance
        setRetainInstance(true);

        // Showing default page
        //changePage(POSTS_COLLECTION_PAGE);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == textViewFollowerCount.getId()) {
            UniversalListenerMethods.onClickFollower(request.getUsername(), context);
        } else if (v.getId() == textViewFollowingCount.getId()) {
            UniversalListenerMethods.onClickFollowing(request.getUsername(), context);
        } else if (v.getId() == imageButtonOptions.getId()) {
            final NetworkOperations service = new NetworkOperations(context, BusProvider.getInstance());
            PopupMenu popup = new PopupMenu(context, imageButtonOptions);
            popup.inflate(R.menu.menu_profile_options);
            if (receivedUser.isSubscribed() != null && receivedUser.isSubscribed()) {
                popup.getMenu().findItem(R.id.profile_option_subscribe).setTitle(R.string.unsubscribe);
            }
            if (receivedUser.isBlocked() != null && receivedUser.isBlocked()) {
                popup.getMenu().findItem(R.id.profile_option_block).setTitle(R.string.unblock);
            }
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    UserModel user = request;

                    switch (item.getItemId()) {
                        case R.id.profile_option_share:
                            for (ProfileOptionsListener listener : profileOptionListeners) {
                                listener.onShareProfileAsMessageClicked(user);
                            }
                            break;
                        case R.id.profile_option_subscribe:
                            for (ProfileOptionsListener listener : profileOptionListeners) {
                                listener.onSubscribeClicked(user);
                            }

                            UserWrapperModel subscription = new UserWrapperModel();

                            subscription.setTarget(new UserModel());
                            subscription.getTarget().setUsername(receivedUser.getUsername());

                            subscription.setAuthor(new UserModel());
                            subscription.getAuthor().setToken(request.getToken());
                            subscription.getAuthor().setUsername(request.getUsername());

                            subscription.setApiKey(request.getApiKey());

                            service.getUsersEndpoint().subscribe(subscription);
                            dialogLoading.show();
                            break;
                        case R.id.profile_option_send:
                            for (ProfileOptionsListener listener : profileOptionListeners) {
                                listener.onSendGiftClicked(user);
                            }

                            final UserModel receiverOfGift = new UserModel();
                            receiverOfGift.setUsername(user.getUsername());
                            Intent intent = new Intent(context, AccountActivity.class);
                            intent.putExtra(UserModel.UID, user);
                            context.startActivity(intent);
                            break;
                        case R.id.profile_option_report:
                            for (ProfileOptionsListener listener : profileOptionListeners) {
                                listener.onReportClicked(user);
                            }

                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(getString(R.string.CoreActivityBottomReportButtonText));

                            // Set up input
                            final AppCompatEditText editText = new AppCompatEditText(context);
                            editText.setHint(getString(R.string.CoreActivityReportDialogHintText));
                            // Specify the type of input expected;
                            editText.setInputType(InputType.TYPE_CLASS_TEXT);
                            builder.setView(editText);

                            // Set up buttons
                            builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (editText.getText().toString().trim().length() > 0) {
                                        UserWrapperModel complaint = new UserWrapperModel();

                                        complaint.setTarget(new UserModel());
                                        complaint.getTarget().setUsername(receivedUser.getUsername());

                                        complaint.setAuthor(new UserModel());
                                        complaint.getAuthor().setToken(request.getToken());
                                        complaint.getAuthor().setUsername(request.getUsername());

                                        complaint.setApiKey(request.getApiKey());
                                        complaint.setText(editText.getText().toString());

                                        service.getUsersEndpoint().report(complaint);
                                        dialogLoading.show();
                                    } else {
                                        Toast.makeText(context, getString(R.string.CoreActivityMessageFillEmptyField), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                            builder.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });

                            builder.show();
                            break;
                        case R.id.profile_option_block:
                            for (ProfileOptionsListener listener : profileOptionListeners) {
                                listener.onBlockClicked(user);
                            }

                            UserWrapperModel block = new UserWrapperModel();

                            block.setTarget(new UserModel());
                            block.getTarget().setUsername(receivedUser.getUsername());

                            block.setToken(request.getToken());
                            block.setApiKey(request.getApiKey());

                            service.getUsersEndpoint().block(block);
                            dialogLoading.show();
                            break;
                    }

                    return true;
                }

            });

            popup.show();
        } else {
            for (byte position = 0; position < imageButtons.length; position++) {
                if (v.getId() == imageButtons[position].getId())
                    if (!isLoading) {
                        if (position != GIFTS_PAGE && position != QUESTIONS_PAGE) {
                            showLock();
                            return;
                        }

                        if (isEmpty[position]) {
                            showPage(position);
                            return;
                        }

                        if (!isInitialized[position]) {
                            loadMore(position);

                            recyclerViewContent.setVisibility(View.GONE);
                            textViewEmpty.setVisibility(View.GONE);
                        } else {
                            currentPage = position;
                            showPage(position);
                        }
                    }
            }
        }
    }

    private void initUser(UserModel user) {
        receivedUser = user;

        if (receivedUser.isSubscribed() == null) {
            receivedUser.setSubscribed(false);
        }

        if (receivedUser.isBlocked() == null) {
            receivedUser.setBlocked(false);
        }

        // Setting data
        textViewFullName.setText(user.getFullName());
        textViewBio.setText(user.getInfo());
        textViewPostCount.setText(String.format(getString(R.string.ProfileFragmentPostCountText), user.getPostCount()));
        textViewFollowerCount.setText(String.format(getString(R.string.ProfileFragmentFollowerCountText), user.getFollowerCount()));
        textViewFollowingCount.setText(String.format(getString(R.string.ProfileFragmentFollowingCountText), user.getFollowingCount()));
        textViewQuestionCount.setText(String.format(getString(R.string.ProfileFragmentQuestionCountText), user.getQuestionCount()));

        if (user.getProfilePhoto() != null) {
            String photoUrl = user.getProfilePhoto().getLargestPossible();

            // Profile Photo
            Utility.loadWithPicasso(
                    context,
                    photoUrl,
                    imageViewProfilePhoto,
                    R.drawable.profile_photo_placeholder
            );

            // Cover Photo
            Utility.loadWithPicasso(
                    context,
                    photoUrl,
                    imageViewCover,
                    R.drawable.profile_photo_placeholder
            );

        }

        updateFollowStatus(user);

//        imageViewUserBadge.setImageResource(
//                request.isOnline() ?
//                        R.drawable.user_online_badge :
//                        R.drawable.user_offline_badge
//        );
    }

    private void initValues(UserModel user) {
        // We assume no page will be empty
        // We assume no page is initialized
        for (int i = 0; i < isEmpty.length; i++) {
            isEmpty[i] = false;
            isInitialized[i] = false;
        }

        // Exception for Private Profile
        isInitialized[POSTS_COLLECTION_PAGE] = true;
        isInitialized[POSTS_LIST_PAGE] = true;
        isInitialized[TAGS_PAGE] = true;

        // Getting error messages for empty page
        emptyText = context.getResources().getStringArray(R.array.ProfileFragmentEmptyTextArray);

        // Creating request
        request = new UserModel();
        request.setUsername(user.getUsername());
        request.setApiKey(Keys.API_KEY);
        request.setToken(ProfileActivity.request.getToken());

        // Feed
        int defaultPage = 0;

        feedQuestion = new QuestionFeedModel();
        List<QuestionModel> questions = new ArrayList<>();
        feedQuestion.setQuestions(questions);
        feedQuestion.setPage(defaultPage);

        feedGift = new GiftFeedModel();
        List<GiftModel> gifts = new ArrayList<>();
        feedGift.setGifts(gifts);
        feedGift.setPage(defaultPage);

        dialogLoading = Interaction.getInstance(context).showLoadingDialog(
                getString(R.string.PleaseWait)
        );
        dialogLoading.hide();

        if (user.wantsToFollowYou() != null) {
            if (user.wantsToFollowYou()) {
                layoutFollowRequest.setVisibility(View.VISIBLE);

                Button buttonYes = (Button) layoutFollowRequest.findViewById(R.id.buttonYesFollowRequest);
                Button buttonNo = (Button) layoutFollowRequest.findViewById(R.id.buttonNoFollowRequest);

                buttonYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        NetworkOperations service = new NetworkOperations(context, BusProvider.getInstance());
                        service.getUsersEndpoint().acceptFollowRequest(request);
                        dialogLoading.show();
                    }
                });

                buttonNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        NetworkOperations service = new NetworkOperations(context, BusProvider.getInstance());
                        service.getUsersEndpoint().rejectFollowRequest(request);
                        dialogLoading.show();
                    }
                });
            }
        }
    }

    private void setListeners() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if (isLoading)
                        return;

                    loadMore(currentPage);
                    Log.d(AzGram.TAG, "onScrollChange: " + currentPage);
                }
            }
        });

        // Navigation listeners
        for (ImageButton imageButton : imageButtons) {
            imageButton.setOnClickListener(this);
        }

        imageButtonAskQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AskQuestionActivity.class);
                UserModel user = new UserModel();
                user.setUsername(request.getUsername());
                intent.putExtra(UserModel.UID, user);
                context.startActivity(intent);
            }
        });

        imageButtonSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessagingActivity.class);
                intent.putExtra(MessagingActivity.EXTRA_MODE, MessagingActivity.MODE_OPEN_CONVERSATION_WITH_USER);
                UserModel user = new UserModel();
                user.setUsername(request.getUsername());
                intent.putExtra(UserModel.UID, user);
                context.startActivity(intent);
            }
        });

        textViewFollowerCount.setOnClickListener(this);
        textViewFollowingCount.setOnClickListener(this);

        imageButtonOptions.setOnClickListener(this);

        OnSwipeTouchListener swipeTouchListener = new OnSwipeTouchListener(context) {
            @Override
            public void onSwipeTop() {
                layoutFollowRequest.setVisibility(View.GONE);
            }
        };

        layoutFollowRequest.setOnTouchListener(swipeTouchListener);
        layoutFollowRequest.findViewById(R.id.imageViewDismiss).setOnTouchListener(swipeTouchListener);
    }

    private void startLoading() {
        isLoading = true;
        progressBarLoadMore.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        progressBarLoadMore.setVisibility(View.GONE);
    }

    private void loadMore(byte position) {
        if (position != GIFTS_PAGE && position != QUESTIONS_PAGE) {
            showLock();
            return;
        }

        if (isEmpty[position]) {
            showPage(position);
            return;
        }

        dismissLock();

        currentPage = position;
        NetworkOperations service = new NetworkOperations(context, BusProvider.getInstance());

        switch (position) {
            case GIFTS_PAGE:
                if (feedGift.getPage() == null)
                    return;

                request.setPage(feedGift.getPage() + 1);
                service.getPostsEndpoint().getGifts(request);
                startLoading();
                break;
            case QUESTIONS_PAGE:
                if (feedQuestion.getPage() == null)
                    return;

                request.setPage(feedQuestion.getPage() + 1);
                service.getPostsEndpoint().getQuestions(request);
                startLoading();
                break;
        }
    }

    private void showPage(byte position) {
        dismissLock();
        // if page is empty just skip process, show message
        if (isEmpty[position]) {
            recyclerViewContent.setAdapter(null);
            textViewEmpty.setVisibility(View.VISIBLE);
            textViewEmpty.setText(emptyText[position]);
            return;
        } else {
            recyclerViewContent.setVisibility(View.VISIBLE);
            textViewEmpty.setVisibility(View.GONE);
        }

        RecyclerView.LayoutManager layoutManager;
        recyclerViewContent.removeItemDecoration(itemDecoration);

        switch (position) {
            case GIFTS_PAGE:
                if (adapterGifts == null) {
                    adapterGifts = new RecyclerAdapterGifts(context, feedGift);
                    adapterGifts.setHasStableIds(true);
                }

                recyclerViewContent.setAdapter(adapterGifts);

                layoutManager = new GridLayoutManager(getActivity(),
                        RecyclerAdapterPostsCollection.COLUMN_COUNT);
                layoutManager.setAutoMeasureEnabled(true);
                recyclerViewContent.setLayoutManager(layoutManager);

                recyclerViewContent.addItemDecoration(itemDecoration);

                break;

            case QUESTIONS_PAGE:
                if (adapterQuestions == null) {
                    adapterQuestions = new RecyclerAdapterQuestions(context, feedQuestion, request);
                    adapterQuestions.setHasStableIds(true);
                }

                recyclerViewContent.setAdapter(adapterQuestions);

                layoutManager = new LinearLayoutManager(context);
                layoutManager.setAutoMeasureEnabled(true);
                recyclerViewContent.setLayoutManager(layoutManager);

                break;
            default:
                showLock();
        }
    }

    private void showLock() {
        stopLoading();
        recyclerViewContent.setVisibility(View.GONE);
        progressBarLoadMore.setVisibility(View.GONE);
        textViewEmpty.setVisibility(View.GONE);
        textViewPrivate.setVisibility(View.VISIBLE);
    }

    private void dismissLock() {
        textViewPrivate.setVisibility(View.GONE);
    }

    private void updateFollowStatus(UserModel user) {
        int resId = R.drawable.ic_add_user_not_following;

        switch (user.getFollowStatus()) {
            case UserModel.FollowStatus.FOLLOWING:
                resId = R.drawable.ic_add_user_following;
                break;
            case UserModel.FollowStatus.PENDING:
                resId = R.drawable.ic_add_user_pending;
        }

        imageButtonFollow.setImageResource(resId);
    }

    private void setFollowButtonListener() {
        imageButtonFollow.setVisibility(View.VISIBLE);
        progressBarFollow.setVisibility(View.GONE);
        imageButtonFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                follow();
            }
        });
    }

    /**
     * Show progressBar
     * Don't resend request, just inform the user
     */
    private void cancelFollowButtonListener() {
        imageButtonFollow.setVisibility(View.GONE);
        progressBarFollow.setVisibility(View.VISIBLE);
        progressBarFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, getString(R.string.PleaseWait), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void follow() {
        cancelFollowButtonListener();

        UserModel userToFollow = new UserModel();
        userToFollow.setUsername(request.getUsername());
        userToFollow.setApiKey(Keys.API_KEY);
        userToFollow.setToken(request.getToken());

        NetworkOperations service = new NetworkOperations(context, BusProvider.getInstance());
        service.getUsersEndpoint().follow(userToFollow);
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(FollowEvent event) {
        setFollowButtonListener();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS ||
                event.getModel().getMessageId() == NetworkOperations.Users.PENDING) {
            ((ProfileActivity) context).restart();
        } else {
            Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    @Produce
    public void onResponse(GiftsFeedReceivedEvent event) {
        stopLoading();
        isInitialized[GIFTS_PAGE] = true;

        if (event.getModel() != null) {
            if (event.getModel().getGifts() != null) {
                feedGift.getGifts().addAll(event.getModel().getGifts());
                feedGift.setPage(event.getModel().getPage());

                if (currentPage == GIFTS_PAGE)
                    showPage(GIFTS_PAGE);
            } else if (event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                isEmpty[GIFTS_PAGE] = true;
                showPage(GIFTS_PAGE);
            }
        }
    }

    @Subscribe
    @Produce
    public void onResponse(QuestionReceivedEvent event) {
        stopLoading();
        isInitialized[QUESTIONS_PAGE] = true;

        if (event.getModel() != null) {
            if (event.getModel().getQuestions() != null) {
                feedQuestion.getQuestions().addAll(event.getModel().getQuestions());
                feedQuestion.setPage(event.getModel().getPage());

                if (currentPage == QUESTIONS_PAGE)
                    showPage(QUESTIONS_PAGE);
            } else if (event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                isEmpty[QUESTIONS_PAGE] = true;
                showPage(QUESTIONS_PAGE);
            }
        }
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(FollowRequestEvent event) {
        if (dialogLoading != null) {
            dialogLoading.dismiss();
            dialogLoading = null;
        }

        if (layoutFollowRequest != null) {
            layoutFollowRequest.setVisibility(View.GONE);
            layoutFollowRequest = null;
        }

        if (event.getModel().getMessageId() != NetworkOperations.API_SUCCESS) {
            Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(SubscribedToUserEvent event) {
        dialogLoading.hide();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(context, R.string.Success, Toast.LENGTH_SHORT).show();
            receivedUser.setSubscribed(!receivedUser.isSubscribed());
        } else {
            Toast.makeText(context, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(ReportedUserEvent event) {
        dialogLoading.hide();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(context, R.string.report_success, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }
}