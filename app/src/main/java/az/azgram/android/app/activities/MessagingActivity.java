package az.azgram.android.app.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterUserMini;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.messages.ConversationCreatedEvent;
import az.azgram.android.app.fragments.ChatFragment;
import az.azgram.android.app.fragments.ConversationListFragment;
import az.azgram.android.app.fragments.UserPickerFragment;
import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.ConversationController;

import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MessagingActivity extends AppCompatActivity implements View.OnClickListener, ConversationController {

    // Constants
    public static final String EXTRA_MODE = "extra_mode";
    public static final byte MODE_DEFAULT = 0;
    public static final byte MODE_OPEN_CONVERSATION_WITH_USER = 1;
    public static final byte MODE_OPEN_CONVERSATION = 2;

    // Widgets
    private Toolbar toolbar;
    private ImageButton imageButtonBack;
    private FrameLayout frameLayout;

    // Other elements
    private NetworkOperations service;
    private UserModel request;
    private ProgressDialog dialogLoading;
    private static boolean isVisibleToUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging);

        new Cache(this).setUnseenMessageCount(0);

        init();
        initValues();
        setListeners();
        openFragments();
    }

    @Override
    protected void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
        isVisibleToUser = true;
    }

    @Override
    protected void onStop() {
        isVisibleToUser = false;
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    public static boolean isVisibleToUser() {
        return isVisibleToUser;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_MessagingActivity);
        imageButtonBack = (ImageButton) findViewById(R.id.imageButtonBack_MessagingActivity);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout_MessagingActivity);
    }

    private void initValues() {
        setSupportActionBar(toolbar);
        setTitle("");

        UserModel user = new Cache(this).loadUser();
        request = new UserModel();
        request.setUsername(user.getUsername());
        request.setToken(user.getToken());
        request.setApiKey(Keys.API_KEY);

        dialogLoading = Interaction.getInstance(this).showLoadingDialog(getString(R.string.PleaseWait));
        dialogLoading.hide();

        service = new NetworkOperations(this, BusProvider.getInstance());
        service.getMessagesEndpoint();
    }

    private void setListeners() {
        imageButtonBack.setOnClickListener(this);
    }

    private void openFragments() {
        int mode = getIntent().getByteExtra(EXTRA_MODE, MODE_DEFAULT);

        switch (mode) {
            case MODE_DEFAULT:
                openConversationList();
                break;
            case MODE_OPEN_CONVERSATION_WITH_USER:
                // just to create back stack
                openConversationList();

                UserModel user = (UserModel) getIntent().getSerializableExtra(UserModel.UID);

                if (user != null) {
                    createConversation(user);
                }
                break;
            case MODE_OPEN_CONVERSATION:
                // just to create back stack
                openConversationList();

                ConversationModel conversation = (ConversationModel) getIntent().getSerializableExtra(ConversationModel.UID);

                if (conversation != null) {
                    openConversation(conversation);
                }
                break;
        }
    }

    private void setLoading(boolean loading) {
        if (loading) {
            dialogLoading.show();
        } else {
            dialogLoading.hide();
        }
    }

    /**
     * Open conversation with user.
     * Send request to do so.
     *
     * @param user recipient
     */
    private void createConversation(UserModel user) {
        UserModel userMini = new UserModel();
        userMini.setUsername(user.getUsername());

        ConversationModel conversation = new ConversationModel();
        conversation.setRecipients(new ArrayList<CoreModel>());
        conversation.getRecipients().add(userMini);
        conversation.setAuthor(new UserModel());
        conversation.getAuthor().setToken(request.getToken());
        conversation.getAuthor().setUsername(request.getUsername());
        conversation.setApiKey(request.getApiKey());

        service.getMessagesEndpoint().createConversation(conversation);
        setLoading(true);
    }

    private void openConversationList() {
        ConversationListFragment fragment = new ConversationListFragment();
        fragment.addController(MessagingActivity.this);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameLayout_MessagingActivity, fragment)
                .commit();
    }

    private void openConversation(ConversationModel conversation) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putLong(ChatFragment.ARG_CONVERSATION_ID, conversation.getId());
        fragment.setArguments(args);
        // TODO: fragment.addController(this);
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.open_from_right_300, R.anim.close_to_left_300)
                .replace(R.id.frameLayout_MessagingActivity, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButtonBack_MessagingActivity:
                super.onBackPressed();
                break;
        }
    }

    @Override
    public void onConversationClicked(ConversationModel conversation) {
        openConversation(conversation);
    }

    @Override
    public void onConversationDeleteClicked(ConversationModel conversation) {

    }

    @Override
    public void onConversationAcceptClicked(ConversationModel conversation) {

    }

    @Override
    public void onConversationRejectClicked(ConversationModel conversation) {

    }

    @Override
    public void onConversationCreateClicked() {
        UserPickerFragment fragment = new UserPickerFragment();

        fragment.addController(new RecyclerAdapterUserMini.Controller() {
            @Override
            public void onUserClicked(UserModel user) {
                createConversation(user);
                MessagingActivity.super.onBackPressed();
            }
        });

        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.open_from_right_300, R.anim.close_to_left_300)
                .replace(R.id.frameLayout_MessagingActivity, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Subscribe
    public void onAzGramServiceResponse(ConversationCreatedEvent event) {
        dialogLoading.hide();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            openConversation(event.getModel().getConversation());
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }
}