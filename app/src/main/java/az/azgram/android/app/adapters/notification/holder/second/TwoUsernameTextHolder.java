package az.azgram.android.app.adapters.notification.holder.second;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.adapters.notification.holder.first.PhotoNotificationHolder;
import az.azgram.android.app.adapters.notification.holder.first.TextNotificationHolder;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.SpanFormatter;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class TwoUsernameTextHolder extends TextNotificationHolder {
    public TwoUsernameTextHolder(View itemView, final Context context) {
        super(itemView, context);

        setRootListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UniversalListenerMethods.onClickUser(
                        notification.getUsers().get(0).getUsername(),
                        context
                );
            }
        });
    }

    public void bind(NotificationModel notification) {
        super.bind(notification, SpanFormatter.format(TEXT_FORMAT,
                UniversalListenerMethods.getHighlightedAndClickableUsername(
                        notification.getUsers().get(0),
                        context
                ),
                UniversalListenerMethods.getHighlightedAndClickableUsername(
                        notification.getUsers().get(1),
                        context
                ))
        );
    }
}
