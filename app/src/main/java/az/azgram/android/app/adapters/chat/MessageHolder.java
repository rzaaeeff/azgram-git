package az.azgram.android.app.adapters.chat;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.events.ui.MessageRetryClickedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 5/3/2017.
 */

class MessageHolder extends RecyclerView.ViewHolder {
    private ImageView imageViewProfilePhoto;
    private TextView textViewTime;
    private ViewGroup rootLayout;
    MessageModel message;
    Context context;

    MessageHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;

        imageViewProfilePhoto = (ImageView) itemView.findViewById(R.id.imageViewProfilePhoto_MessageItem);
        textViewTime = (TextView) itemView.findViewById(R.id.textViewTime_MessageItem);
        rootLayout = (ViewGroup) itemView.findViewById(R.id.rootLayoutMessage_MessageItem);
    }

    public void bind(MessageModel message, boolean shouldShowDetails) {
        this.message = message;

        if (shouldShowDetails) {
            imageViewProfilePhoto.setVisibility(View.VISIBLE);
            textViewTime.setVisibility(View.VISIBLE);

            if (message.getAuthor().getProfilePhoto() != null) {
                Utility.loadWithPicasso(
                        context,
                        message.getAuthor().getProfilePhoto().getSmallestPossible(),
                        imageViewProfilePhoto,
                        R.drawable.profile_photo_placeholder
                );
            }

            try {
                Date date = new SimpleDateFormat(CoreModel.DATE_TIME_FORMAT).parse(message.getDate());

                if (DateUtils.isToday(date)) {
                    // Today
                    SimpleDateFormat targetFormat = new SimpleDateFormat("HH:mm");

                    textViewTime.setText(targetFormat.format(date));
                } else if (DateUtils.isCurrentYear(date)) {
                    // Current year
                    SimpleDateFormat targetFormat = new SimpleDateFormat("HH:mm\ndd MMM");

                    textViewTime.setText(targetFormat.format(date));
                } else {
                    // Not current year
                    SimpleDateFormat targetFormat = new SimpleDateFormat("HH:mm\ndd MMM\nyyyy");

                    textViewTime.setText(targetFormat.format(date));
                }
            } catch (Exception exc) {
                // Couldn't parse
            }
        } else {
            imageViewProfilePhoto.setVisibility(View.INVISIBLE);
            textViewTime.setVisibility(View.GONE);
        }

        updateStatus(message.getStatus());
    }

    public void updateStatus(byte status) {
        switch (status) {
            case MessageModel.Status.SENDING:
                itemView.setAlpha(Constants.DISABLED_STATE_ALPHA);
                itemView.setEnabled(false);
                break;
            case MessageModel.Status.NOT_SENT:
                final ImageButton imageButton = new ImageButton(context);
                imageButton.setImageResource(R.drawable.ic_retry);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
                imageButton.setLayoutParams(params);
                ((ViewGroup) itemView).addView(imageButton);
                imageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BusProvider.getInstance().post(new MessageRetryClickedEvent(getAdapterPosition()));
                        ((ViewGroup) itemView).removeView(imageButton);
                        imageButton.setOnClickListener(null);
                    }
                });
                break;
            case MessageModel.Status.SENT:
                itemView.setAlpha(Constants.ENABLED_STATE_ALPHA);
                itemView.setEnabled(true);
                break;
        }
    }

    public void setRootListener(View.OnClickListener listener) {
        rootLayout.setOnClickListener(listener);
    }
}