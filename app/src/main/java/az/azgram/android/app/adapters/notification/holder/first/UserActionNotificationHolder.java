package az.azgram.android.app.adapters.notification.holder.first;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.RecyclerAdapterNotification;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 5/20/2017.
 */

public class UserActionNotificationHolder extends TextNotificationHolder implements View.OnClickListener {
    public static final int LAYOUT_RES = R.layout.recycler_item_notification_user;

    // Widgets
    private Button buttonNotFollowing, buttonFollowing, buttonPending;

    public UserActionNotificationHolder(View itemView, final Context context) {
        super(itemView, context);

        buttonNotFollowing = (Button) itemView.findViewById(R.id.buttonNotFollowing_NotificationItem);
        buttonFollowing = (Button) itemView.findViewById(R.id.buttonFollowing_NotificationItem);
        buttonPending = (Button) itemView.findViewById(R.id.buttonPending_NotificationItem);

        buttonNotFollowing.setOnClickListener(this);
        buttonFollowing.setOnClickListener(this);
        buttonPending.setOnClickListener(this);

        setRootListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UniversalListenerMethods.onClickUser(
                        notification.getUsers().get(0).getUsername(),
                        context
                );
            }
        });
    }

    public void bind(NotificationModel notification) {
        bind(notification, TextUtils.concat(
                UniversalListenerMethods.getHighlightedAndClickableUsername(
                        notification.getUsers().get(0),
                        context
                ), " ", TEXT_FORMAT
        ));
    }

    public void bind(NotificationModel notification, CharSequence text) {
        super.bind(notification, text);
        updateFollowStatus(notification.getUsers().get(0));
    }

    private void updateFollowStatus(UserModel user) {
        buttonNotFollowing.setVisibility(View.GONE);
        buttonFollowing.setVisibility(View.GONE);
        buttonPending.setVisibility(View.GONE);

        switch (user.getFollowStatus()) {
            case UserModel.FollowStatus.NOT_FOLLOWING:
                buttonNotFollowing.setVisibility(View.VISIBLE);
                break;
            case UserModel.FollowStatus.FOLLOWING:
                buttonFollowing.setVisibility(View.VISIBLE);
                break;
            case UserModel.FollowStatus.PENDING:
                buttonPending.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.rootLayoutUserItem:
//                UniversalListenerMethods.onClickUser(
//                        "@" + notification.getUsers().getus.getUsername(), context);
//                break;
            case R.id.buttonFollowing_NotificationItem:
            case R.id.buttonNotFollowing_NotificationItem:
            case R.id.buttonPending_NotificationItem:
                RecyclerAdapterNotification.request.setUsername(
                        notification.getUsers().get(0).getUsername()
                );
                RecyclerAdapterNotification.service.getUsersEndpoint().follow(
                        RecyclerAdapterNotification.request
                );
                break;
        }
    }
}