package az.azgram.android.app.services;

import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

import az.azgram.android.app.operations.FirebaseTokenHandler;

/**
 * Created by Rzaaeeff on 6/9/2017.
 */

public class FirebaseTokenReceiverService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        FirebaseTokenHandler tokenHandler = new FirebaseTokenHandler(this);
        tokenHandler.setTokenSent(false);
        tokenHandler.sendToServer();

        FirebaseMessaging.getInstance().subscribeToTopic("devices.all");
        FirebaseMessaging.getInstance().subscribeToTopic("devices.android");
    }
}
