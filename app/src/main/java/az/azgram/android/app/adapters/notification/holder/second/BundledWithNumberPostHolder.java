package az.azgram.android.app.adapters.notification.holder.second;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import az.azgram.android.app.adapters.notification.holder.first.PhotoNotificationBundledHolder;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class BundledWithNumberPostHolder extends PhotoNotificationBundledHolder {
    public BundledWithNumberPostHolder(View itemView, Context context) {
        super(itemView, context);
    }

    public void bind(NotificationModel notification) {
        super.bind(notification, TextUtils.concat(
                UniversalListenerMethods.getHighlightedAndClickableUsername(
                        notification.getUsers().get(0), context
                ), " ",
                String.format(TEXT_FORMAT, notification.getPosts().size())
        ));
    }
}
