package az.azgram.android.app.events.users;

import az.azgram.android.app.models.UserModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class SignUpEvent {
    private UserModel model;

    public SignUpEvent(UserModel model) {
        this.model = model;
    }

    public UserModel getModel() {
        return model;
    }

    public void setModel(UserModel model) {
        this.model = model;
    }
}
