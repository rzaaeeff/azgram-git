package az.azgram.android.app.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterGift;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.balance.BalanceReceivedEvent;
import az.azgram.android.app.events.balance.MoneyRequestedEvent;
import az.azgram.android.app.events.balance.MoneySentEvent;
import az.azgram.android.app.events.gifts.GiftListReceivedEvent;
import az.azgram.android.app.events.gifts.GiftSentEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.GiftModel;
import az.azgram.android.app.models.MoneyWrapperModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.GiftListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;

public class AccountActivity extends AppCompatActivity implements View.OnClickListener {

    // Constants
    private final static int REQUEST_CHOOSE_USER_FOR_SEND_GIFT = 200;
    private final static int REQUEST_CHOOSE_USER_FOR_SEND_MONEY = 201;
    private final static int REQUEST_CHOOSE_USER_FOR_REQUEST_MONEY = 202;

    // Widgets
    private TextView textViewBalance, textViewCreateAd, textViewTopUp, textViewSendGift,
            textViewUserSendGift, textViewSendMoney, textViewUserSendMoney, textViewRequestMoney,
            textViewUserRequestMoney;
    private EditText editTextSendGift, editTextSendMoney, editTextRequestMoney;
    private ExpandableLayout expandableLayoutSendGift, expandableLayoutSendMoney,
            expandableLayoutRequestMoney;
    private RecyclerView recyclerViewSendGift;
    private AppCompatButton buttonSendGift, buttonSendMoney, buttonRequestMoney;

    // Other elements
    private NetworkOperations service;
    private UserModel request;
    private boolean isGiftListInitialized;
    private boolean isGiftListLoading;
    private GiftListModel dataGift;
    private RecyclerAdapterGift adapterGift;
    private ProgressDialog dialogLoading;

    private UserModel receiverOfSendGift, receiverOfSendMoney, receiverOfRequestMoney;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        init();
        initValues();
        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);
        showBalance();

        if (!isGiftListInitialized) {
            loadMoreGift();
        }
    }

    @Override
    protected void onPause() {
        BusProvider.getInstance().unregister(this);

        super.onPause();
    }

    public void finish(View view) {
        finish();
    }

    private void init() {
        textViewBalance = (TextView) findViewById(R.id.textViewBalance);
        textViewCreateAd = (TextView) findViewById(R.id.textViewCreateAd);
        textViewTopUp = (TextView) findViewById(R.id.textViewTopUp);
        textViewSendGift = (TextView) findViewById(R.id.textViewSendGift);
        textViewUserSendGift = (TextView) findViewById(R.id.textViewUserSendGift);
        textViewSendMoney = (TextView) findViewById(R.id.textViewSendMoney);
        textViewUserSendMoney = (TextView) findViewById(R.id.textViewUserSendMoney);
        textViewRequestMoney = (TextView) findViewById(R.id.textViewRequestMoney);
        textViewUserRequestMoney = (TextView) findViewById(R.id.textViewUserRequestMoney);

        editTextSendGift = (EditText) findViewById(R.id.editTextSendGift);
        editTextSendMoney = (EditText) findViewById(R.id.editTextSendMoney);
        editTextRequestMoney = (EditText) findViewById(R.id.editTextRequestMoney);

        expandableLayoutSendGift = (ExpandableLayout) findViewById(R.id.expandableLayoutSendGift);
        expandableLayoutSendMoney = (ExpandableLayout) findViewById(R.id.expandableLayoutSendMoney);
        expandableLayoutRequestMoney = (ExpandableLayout) findViewById(R.id.expandableLayoutRequestMoney);

        recyclerViewSendGift = (RecyclerView) findViewById(R.id.recyclerViewSendGift);

        buttonSendGift = (AppCompatButton) findViewById(R.id.buttonSendGift);
        buttonSendMoney = (AppCompatButton) findViewById(R.id.buttonSendMoney);
        buttonRequestMoney = (AppCompatButton) findViewById(R.id.buttonRequestMoney);
    }

    private void initValues() {
        textViewBalance.setText(String.format(
                getString(R.string.AccountActivityBalanceFormat),
                0
        ));

        dialogLoading = Interaction.getInstance(this).showLoadingDialog(
                getString(R.string.PleaseWait)
        );
        dialogLoading.hide();

        UserModel cachedUser = new Cache(this).loadUser();

        request = new UserModel();
        request.setApiKey(Keys.API_KEY);
        request.setToken(cachedUser.getToken());
        request.setUsername(cachedUser.getUsername());
        request.setPage(0);

        LinearLayoutManager layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.HORIZONTAL,
                false
        );
        layoutManager.setAutoMeasureEnabled(true);
        recyclerViewSendGift.setLayoutManager(layoutManager);

        dataGift = new GiftListModel();
        dataGift.setPage(0);
        dataGift.setGifts(new ArrayList<GiftModel>());
        adapterGift = new RecyclerAdapterGift(this, dataGift);
        recyclerViewSendGift.setAdapter(adapterGift);

        service = new NetworkOperations(this, BusProvider.getInstance());
        service.getBalanceEndpoint();
        service.getGiftsEndpoint();

        if (getIntent().hasExtra(UserModel.UID)) {
            receiverOfSendGift = (UserModel) getIntent().getSerializableExtra(UserModel.UID);
            textViewUserSendGift.setText(receiverOfSendGift.getUsername());
            expandableLayoutSendGift.expand();
        }
    }

    private void setListeners() {
        textViewCreateAd.setOnClickListener(this);
        textViewSendGift.setOnClickListener(this);
        expandableLayoutSendGift.setOnClickListener(this);
        textViewSendMoney.setOnClickListener(this);
        expandableLayoutSendMoney.setOnClickListener(this);
        textViewRequestMoney.setOnClickListener(this);
        expandableLayoutRequestMoney.setOnClickListener(this);
        textViewTopUp.setOnClickListener(this);
        textViewUserSendGift.setOnClickListener(this);
        textViewUserSendMoney.setOnClickListener(this);
        textViewUserRequestMoney.setOnClickListener(this);
        buttonSendGift.setOnClickListener(this);
        buttonSendMoney.setOnClickListener(this);
        buttonRequestMoney.setOnClickListener(this);

        final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerViewSendGift.getLayoutManager();

        recyclerViewSendGift.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dx > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    int lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                    if (pastVisiblesItems + visibleItemCount >= totalItemCount) {
                        loadMoreGift();
                    }
                }
            }
        });

        editTextSendMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean enabled = s.toString().trim().length() > 0;
                buttonSendMoney.setEnabled(enabled);
                buttonSendMoney.setAlpha(enabled ? Constants.ENABLED_STATE_ALPHA : Constants.DISABLED_STATE_ALPHA);
            }
        });

        editTextRequestMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean enabled = s.toString().trim().length() > 0;
                buttonRequestMoney.setEnabled(enabled);
                buttonRequestMoney.setAlpha(enabled ? Constants.ENABLED_STATE_ALPHA : Constants.DISABLED_STATE_ALPHA);
            }
        });

        // to disable buttons on first
        editTextSendMoney.setText("");
        editTextRequestMoney.setText("");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.expandableLayoutSendGift:
            case R.id.textViewSendGift:
                expandableLayoutSendGift.toggle();
                break;
            case R.id.expandableLayoutSendMoney:
            case R.id.textViewSendMoney:
                expandableLayoutSendMoney.toggle();
                break;
            case R.id.expandableLayoutRequestMoney:
            case R.id.textViewRequestMoney:
                expandableLayoutRequestMoney.toggle();
                break;
            case R.id.textViewCreateAd:
                Intent adIntent = new Intent(
                        AccountActivity.this,
                        UploadControllerActivity.class
                );

                adIntent.putExtra(UploadControllerActivity.EXTRA_CHOICE, UploadControllerActivity.CHOICE_AD);
                startActivity(adIntent);
                break;
            case R.id.textViewTopUp:
                Intent urlIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://azgram.az/payment")
                );
                startActivity(urlIntent);
                break;
            case R.id.textViewUserSendGift:
                chooseUser(REQUEST_CHOOSE_USER_FOR_SEND_GIFT);
                break;
            case R.id.textViewUserSendMoney:
                chooseUser(REQUEST_CHOOSE_USER_FOR_SEND_MONEY);
                break;
            case R.id.textViewUserRequestMoney:
                chooseUser(REQUEST_CHOOSE_USER_FOR_REQUEST_MONEY);
                break;
            case R.id.buttonSendGift:
                sendGift();
                break;
            case R.id.buttonSendMoney:
                sendMoney();
                break;
            case R.id.buttonRequestMoney:
                requestMoney();
                break;
        }
    }

    private void showBalance() {
        UserModel requestBalance = new UserModel();
        requestBalance.setToken(request.getToken());
        requestBalance.setUsername(request.getUsername());
        requestBalance.setApiKey(request.getApiKey());
        service.getBalanceEndpoint().get(requestBalance);
    }

    private void loadMoreGift() {
        if (isGiftListLoading)
            return;

        if (dataGift.getPage() == null)
            return;

        setGiftListLoading(true);
        request.setPage(dataGift.getPage() + 1);
        service.getGiftsEndpoint().list(request);
    }

    private void setGiftListLoading(boolean loading) {
        isGiftListLoading = loading;
    }

    private void chooseUser(int request) {
        Intent intent = new Intent(this, UserPickerActivity.class);
        startActivityForResult(intent, request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            UserModel user = (UserModel) data.getSerializableExtra(UserModel.UID);

            switch (requestCode) {
                case REQUEST_CHOOSE_USER_FOR_SEND_GIFT:
                    receiverOfSendGift = user;
                    textViewUserSendGift.setText(user.getUsername());
                    break;
                case REQUEST_CHOOSE_USER_FOR_SEND_MONEY:
                    receiverOfSendMoney = user;
                    textViewUserSendMoney.setText(user.getUsername());
                    break;
                case REQUEST_CHOOSE_USER_FOR_REQUEST_MONEY:
                    receiverOfRequestMoney = user;
                    textViewUserRequestMoney.setText(user.getUsername());
                    break;
            }
        }
    }

    private void sendGift() {
        GiftModel selectedGift = adapterGift.getSelectedGift();

        if (selectedGift == null) {
            Interaction.getInstance(this).showErrorDialog(
                    getString(R.string.AccountActivityErrorNoGiftSelected)
            );
            return;
        }

        if (receiverOfSendGift == null) {
            Interaction.getInstance(this).showErrorDialog(
                    getString(R.string.AccountActivityErrorNoUserSelected)
            );
            return;
        }

        GiftModel gift = new GiftModel();
        gift.setId(selectedGift.getId());
        gift.setApiKey(request.getApiKey());
        gift.setText(editTextSendGift.getText().toString());
        gift.setAuthor(new UserModel());
        gift.getAuthor().setToken(request.getToken());
        gift.getAuthor().setUsername(request.getUsername());
        gift.setReceiver(new UserModel());
        gift.getReceiver().setUsername(receiverOfSendGift.getUsername());

        dialogLoading.show();
        service.getGiftsEndpoint().send(gift);
    }

    private void sendMoney() {
        if (receiverOfSendMoney == null) {
            Interaction.getInstance(this).showErrorDialog(
                    getString(R.string.AccountActivityErrorNoUserSelected)
            );
            return;
        }

        MoneyWrapperModel money = new MoneyWrapperModel();
        money.setApiKey(request.getApiKey());
        money.setAuthor(new UserModel());
        money.getAuthor().setToken(request.getToken());
        money.getAuthor().setUsername(request.getUsername());
        money.setAmount(Long.parseLong(editTextSendMoney.getText().toString()));
        money.setUser(new UserModel());
        money.getUser().setUsername(receiverOfSendMoney.getUsername());

        dialogLoading.show();
        service.getBalanceEndpoint().send(money);
    }

    private void requestMoney() {
        if (receiverOfRequestMoney == null) {
            Interaction.getInstance(this).showErrorDialog(
                    getString(R.string.AccountActivityErrorNoUserSelected)
            );
            return;
        }

        MoneyWrapperModel money = new MoneyWrapperModel();
        money.setApiKey(request.getApiKey());
        money.setAuthor(new UserModel());
        money.getAuthor().setToken(request.getToken());
        money.getAuthor().setUsername(request.getUsername());
        money.setAmount(Long.parseLong(editTextRequestMoney.getText().toString()));
        money.setUser(new UserModel());
        money.getUser().setUsername(receiverOfRequestMoney.getUsername());

        dialogLoading.show();
        service.getBalanceEndpoint().request(money);
    }

    @Subscribe
    public void onAzGramServiceResponse(BalanceReceivedEvent event) {
        UserModel model = event.getModel();

        if (model.getMessageId() == NetworkOperations.API_SUCCESS) {
            textViewBalance.setText(String.format(
                    getString(R.string.AccountActivityBalanceFormat),
                    model.getBalance()
            ));
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(GiftListReceivedEvent event) {
        setGiftListLoading(false);
        isGiftListInitialized = true;
        GiftListModel model = event.getModel();

        if (model.getGifts() != null && model.getGifts().size() > 0) {
            dataGift.setPage(model.getPage());
            dataGift.getGifts().addAll(model.getGifts());
            adapterGift.notifyDataSetChanged();
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(GiftSentEvent event) {
        CoreModel model = event.getModel();
        dialogLoading.hide();

        if (model.getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, R.string.AccountActivityInfoGiftSent, Toast.LENGTH_SHORT).show();
            showBalance();

            receiverOfSendGift = null;
            textViewUserSendGift.setText(R.string.AccountActivitySectionChooseUser);
            adapterGift.clearSelection();
            editTextSendGift.setText("");
        } else if (model.getMessageId() == NetworkOperations.Balance.NOT_ENOUGH_BALANCE) {
            Interaction.getInstance(this).showErrorDialog(
                    getString(R.string.AccountActivityErrorNotEnoughBalance)
            );
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(MoneySentEvent event) {
        CoreModel model = event.getModel();
        dialogLoading.hide();

        if (model.getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, R.string.AccountActivityInfoMoneySent, Toast.LENGTH_SHORT).show();
            showBalance();

            receiverOfSendMoney = null;
            textViewUserSendMoney.setText(R.string.AccountActivitySectionChooseUser);
            editTextSendMoney.setText("");
        } else if (model.getMessageId() == NetworkOperations.Balance.NOT_ENOUGH_BALANCE) {
            Interaction.getInstance(this).showErrorDialog(
                    getString(R.string.AccountActivityErrorNotEnoughBalance)
            );
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(MoneyRequestedEvent event) {
        CoreModel model = event.getModel();
        dialogLoading.hide();

        if (model.getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, R.string.AccountActivityInfoMoneyRequested, Toast.LENGTH_SHORT).show();

            receiverOfRequestMoney = null;
            textViewUserRequestMoney.setText(R.string.AccountActivitySectionChooseUser);
            editTextRequestMoney.setText("");
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }
}