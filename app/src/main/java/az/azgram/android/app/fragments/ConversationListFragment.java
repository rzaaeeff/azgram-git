package az.azgram.android.app.fragments;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterConversation;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.messages.ConversationAcceptedEvent;
import az.azgram.android.app.events.messages.ConversationDeletedEvent;
import az.azgram.android.app.events.messages.ConversationListReceivedEvent;
import az.azgram.android.app.events.messages.ConversationRejectedEvent;
import az.azgram.android.app.events.messages.MessageReceivedEvent;
import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.models.response.ConversationListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.ConversationController;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.MessagingUtils;
import az.azgram.android.app.operations.NetworkOperations;

import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import cn.refactor.lib.colordialog.ColorDialog;

public class ConversationListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ConversationController {

    // Constants

    // Widgets
    private SuperRecyclerView recyclerView;
    private TextView textViewError;

    // Other elements
    private Activity activity;
    private ConversationListModel data;
    private RecyclerAdapterConversation adapter;
    private CoreModel request;
    private boolean isRefreshing, isLoading;
    private List<ConversationController> controllers = new ArrayList<>();
    private NetworkOperations service;

    public ConversationListFragment() {
        // Required empty public constructor
    }

    public void addController(ConversationController controller) {
        controllers.add(controller);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_conversation_list, container, false);

        activity = getActivity();

        init(view);
        initValues();
        setListeners();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_conversation_list_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_ConversationListFragmentMenu:
                for (ConversationController controller : controllers) {
                    controller.onConversationCreateClicked();
                }
                break;
        }

        return true;
    }

    private void init(View view) {
        recyclerView = (SuperRecyclerView) view.findViewById(R.id.recyclerView_ConversationListFragment);
        textViewError = (TextView) view.findViewById(R.id.textViewError_ConversationListFragment);
    }

    private void initValues() {
        setHasOptionsMenu(true);

        isLoading = false;
        isRefreshing = false;

        request = new CoreModel();
        request.setToken(new Cache(activity).loadUser().getToken());
        request.setApiKey(Keys.API_KEY);

        recyclerView.setRefreshingColor(
                Color.parseColor("#337ab7"),
                Color.parseColor("#3258b7"),
                Color.parseColor("#3732b7"),
                Color.parseColor("#7032b7")
        );

        recyclerView.getRecyclerView().setHasFixedSize(true);

        data = new ConversationListModel();
        data.setConversations(new ArrayList<ConversationModel>());
        data.setPage(0);

        adapter = new RecyclerAdapterConversation(activity, data, this);
        adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(layoutManager);

        service = new NetworkOperations(activity, BusProvider.getInstance());
        service.getMessagesEndpoint();

        // init
        loadMore();
    }

    private void setListeners() {
        recyclerView.setRefreshListener(this);
        recyclerView.setOnMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                loadMore();
            }
        });
    }

    private void setLoading(boolean loading) {
        isLoading = loading;

        if (loading)
            recyclerView.getMoreProgressView().setVisibility(View.VISIBLE);
        else
            recyclerView.getMoreProgressView().setVisibility(View.GONE);
    }

    private void setRefreshing(boolean refreshing) {
        isRefreshing = refreshing;
        recyclerView.getSwipeToRefresh().setRefreshing(refreshing);
    }

    @Override
    public void onRefresh() {
        if (isLoading || isRefreshing) {
            recyclerView.getSwipeToRefresh().setRefreshing(false);
            isRefreshing = false;
            return;
        }

        setRefreshing(true);

        request.setPage(0);

        service.getMessagesEndpoint().getConversations(request);
    }

    private void loadMore() {
        if (isRefreshing || isLoading)
            return;

        setLoading(true);

        if (data.getPage() != null) {
            request.setPage(data.getPage() + 1);
        }

        service.getMessagesEndpoint().getConversations(request);
        setLoading(true);
    }

    @Override
    public void onConversationClicked(ConversationModel conversation) {
        for (ConversationController controller : controllers)
            controller.onConversationClicked(conversation);
    }

    @Override
    public void onConversationDeleteClicked(final ConversationModel conversation) {
        for (ConversationController controller : controllers)
            controller.onConversationDeleteClicked(conversation);

        Interaction.getInstance(activity).showPromptDialog(
                getString(R.string.ConversationListFragmentAreYouSureToDelete)
        ).setPositiveListener(R.string.Yes, new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog colorDialog) {
                request.setId(conversation.getId());
                service.getMessagesEndpoint().deleteConversation(request);
                colorDialog.dismiss();
            }
        }).setNegativeListener(R.string.No, new ColorDialog.OnNegativeListener() {
            @Override
            public void onClick(ColorDialog colorDialog) {
                colorDialog.dismiss();
            }
        }).show();
    }

    @Override
    public void onConversationCreateClicked() {

    }

    @Override
    public void onConversationAcceptClicked(final ConversationModel conversation) {
        for (ConversationController controller : controllers)
            controller.onConversationAcceptClicked(conversation);

        request.setId(conversation.getId());
        service.getMessagesEndpoint().acceptConversation(request);
    }

    @Override
    public void onConversationRejectClicked(ConversationModel conversation) {
        for (ConversationController controller : controllers)
            controller.onConversationRejectClicked(conversation);

        request.setId(conversation.getId());
        service.getMessagesEndpoint().rejectConversation(request);
    }

    @Produce
    @Subscribe
    public void onAzGramServiceResponse(ConversationListReceivedEvent event) {
        setLoading(false);
        boolean tempRefreshing = isRefreshing;
        setRefreshing(false);

        if (event.getModel().getMessageId() == null ||
                event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            if (tempRefreshing) {
                data.getConversations().clear();
            }

            data.getConversations().addAll(event.getModel().getConversations());
            data.setPage(event.getModel().getPage());
            adapter.notifyDataSetChanged();

            textViewError.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            return;
        }

        if (event.getModel().getMessageId() == NetworkOperations.Messages.CONVERSATION_NOT_FOUND) {
            textViewError.setVisibility(View.VISIBLE);
            textViewError.setText(R.string.ConversationListFragmentNotFound);
            return;
        }
    }

    @Produce
    @Subscribe
    public void onAzGramServiceResponse(ConversationDeletedEvent event) {
        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            for (int i = 0; i < data.getConversations().size(); i++) {
                if (data.getConversations().get(i).getId().equals(event.getModel().getId())) {
                    data.getConversations().remove(i);
                    adapter.notifyDataSetChanged();
                    return;
                }
            }
        }

        Toast.makeText(activity, R.string.UnknownError, Toast.LENGTH_SHORT).show();
    }

    @Produce
    @Subscribe
    public void onAzGramServiceResponse(ConversationAcceptedEvent event) {
        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            for (int i = 0; i < data.getConversations().size(); i++) {
                if (data.getConversations().get(i).getId().equals(event.getModel().getId())) {
                    data.getConversations().get(i).setPending(false);
                    adapter.notifyDataSetChanged();
                    return;
                }
            }
        }

        Toast.makeText(activity, R.string.UnknownError, Toast.LENGTH_SHORT).show();
    }

    @Produce
    @Subscribe
    public void onAzGramServiceResponse(ConversationRejectedEvent event) {
        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            for (int i = 0; i < data.getConversations().size(); i++) {
                if (data.getConversations().get(i).getId().equals(event.getModel().getId())) {
                    data.getConversations().remove(i);
                    adapter.notifyDataSetChanged();
                    return;
                }
            }
        }

        Toast.makeText(activity, R.string.UnknownError, Toast.LENGTH_SHORT).show();
    }

    @Produce
    @Subscribe
    public void onAzGramPushNotification(MessageReceivedEvent event) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
        MessageModel message = event.getModel();

        for (int i = 0; i < data.getConversations().size(); i++) {
            ConversationModel conversation = data.getConversations().get(i);

            if (message.getConversation().getId().equals(conversation.getId())) {
                conversation.setSeen(false);
                conversation.setPreviewType(message.getType());
                conversation.setPreview(message.getContent());
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
                return;
            }
        }

        MessagingUtils.showNotification(activity, message);
    }
}