package az.azgram.android.app.adapters.chat;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import az.azgram.android.app.R;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 5/3/2017.
 */

class UserHolder extends MessageHolder implements View.OnClickListener {
    static final int LAYOUT_RES_IN = R.layout.recycler_item_message_user_incoming;
    static final int LAYOUT_RES_OUT = R.layout.recycler_item_message_user_outgoing;

    private ImageView imageViewUserProfilePhoto, imageViewUserBadge;
    private TextView textViewUsername, textViewStatus,
            textViewPostCount, textViewFollowerCount,
            textViewFollowingCount, textViewQuestionCount;
    private Button button;
    private RelativeLayout relativeLayout;

    // Other elements
    private String username;
    private String errorPrivate;

    public UserHolder(View itemView, Context context) {
        super(itemView, context);

        imageViewUserProfilePhoto = (ImageView) itemView.findViewById(R.id.imageViewProfilePhoto_User_MessageItem);
        imageViewUserBadge = (ImageView) itemView.findViewById(R.id.imageViewBadge_User_MessageItem);
        textViewUsername = (TextView) itemView.findViewById(R.id.textViewUsername_User_MessageItem);
        textViewStatus = (TextView) itemView.findViewById(R.id.textViewStatus_User_MessageItem);
        textViewPostCount = (TextView) itemView.findViewById(R.id.textViewPostCount_User_MessageItem);
        textViewFollowerCount = (TextView) itemView.findViewById(R.id.textViewFollowerCount_User_MessageItem);
        textViewFollowingCount = (TextView) itemView.findViewById(R.id.textViewFollowingCount_User_MessageItem);
        textViewQuestionCount = (TextView) itemView.findViewById(R.id.textViewQuestionCount_User_MessageItem);
        button = (Button) itemView.findViewById(R.id.button_User_MessageItem);
        relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayoutMessage_MessageItem);

        errorPrivate = context.getString(R.string.MessageItemPrivateUserText);

        button.setOnClickListener(this);
    }

    public void bind(MessageModel message, boolean shouldShowDetails) {
        super.bind(message, shouldShowDetails);

        UserModel user = message.getUser();

        if (user != null) {
            username = user.getUsername();

            Utility.loadWithPicasso(
                    context,
                    user.getProfilePhoto().getSmallestPossible(),
                    imageViewUserProfilePhoto,
                    R.drawable.profile_photo_placeholder
            );

            textViewUsername.setText(username);

            imageViewUserBadge.setImageResource(
                    user.isOnline() == null ||
                            !user.isOnline() ?
                            R.drawable.user_offline_badge :
                            R.drawable.user_online_badge
            );

            if (user.getInfo() != null) {
                textViewStatus.setText(user.getInfo());
            } else {
                textViewStatus.setVisibility(View.INVISIBLE);
            }

            textViewPostCount.setText(String.format(
                    context.getString(R.string.ProfileFragmentPostCountText),
                    user.getPostCount())
            );
            textViewFollowerCount.setText(String.format(
                    context.getString(R.string.ProfileFragmentFollowerCountText),
                    user.getFollowerCount())
            );
            textViewFollowingCount.setText(String.format(
                    context.getString(R.string.ProfileFragmentFollowingCountText),
                    user.getFollowingCount())
            );
            textViewQuestionCount.setText(String.format(
                    context.getString(R.string.ProfileFragmentQuestionCountText),
                    user.getQuestionCount())
            );
        } else {
            // User is private
            button.setOnClickListener(null);

            for (int i = 0; i < relativeLayout.getChildCount(); i++) {
                View view = relativeLayout.getChildAt(i);
                view.setVisibility(View.GONE);
            }

            TextView textView = new TextView(context);
            textView.setText(errorPrivate);
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lock, 0, 0, 0);
            textView.setGravity(Gravity.CENTER);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            textView.setLayoutParams(params);

            relativeLayout.addView(textView);
            relativeLayout.setLayoutParams(new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            ));
        }
    }

    @Override
    public void onClick(View v) {
        UniversalListenerMethods.onClickUser("@" + username, context);
    }
}