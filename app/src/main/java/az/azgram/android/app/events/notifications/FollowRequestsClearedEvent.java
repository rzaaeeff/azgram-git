package az.azgram.android.app.events.notifications;

import az.azgram.android.app.models.CoreModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class FollowRequestsClearedEvent {
    private CoreModel model;

    public FollowRequestsClearedEvent(CoreModel model) {
        this.model = model;
    }

    public CoreModel getModel() {
        return model;
    }

    public void setModel(CoreModel model) {
        this.model = model;
    }
}
