package az.azgram.android.app.adapters.chat;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.otto.Subscribe;

import java.text.DecimalFormat;

import az.azgram.android.app.R;
import az.azgram.android.app.custom.RzPlayer;
import az.azgram.android.app.events.ui.UserStartedPlayingMediaEvent;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.operations.BusProvider;

/**
 * Created by Rzaaeeff on 5/3/2017.
 */

class AudioHolder extends MessageHolder implements View.OnClickListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {
    static final int LAYOUT_RES_IN = R.layout.recycler_item_message_audio_incoming;
    static final int LAYOUT_RES_OUT = R.layout.recycler_item_message_audio_outgoing;

    private ToggleButton toggleButtonPlay;
    private ProgressBar progressBarDuration;
    private TextView textViewDuration;

    private RzPlayer player;
    private String audio;
    private Handler handlerProgress = new Handler();
    private Runnable runnableProgress = new Runnable() {
        @Override
        public void run() {
            setProgress(player.getCurrentPosition());

            handlerProgress.postDelayed(this, 10);
        }
    };

    public AudioHolder(View itemView, Context context) {
        super(itemView, context);

        toggleButtonPlay = (ToggleButton) itemView.findViewById(R.id.toggleButtonPlay_MessageItem);
        progressBarDuration = (ProgressBar) itemView.findViewById(R.id.progressbarDuration_MessageItem);
        textViewDuration = (TextView) itemView.findViewById(R.id.textViewDuration_MessageItem);

        toggleButtonPlay.setOnClickListener(this);

        initPlayer(true);
    }

    public void bind(MessageModel message, boolean shouldShowDetails) {
        super.bind(message, shouldShowDetails);

        audio = message.getAudio();

        initPlayer(true);
    }

    private void initPlayer(boolean prepareAsync) {
        try {
            if (player == null || player.getState() == RzPlayer.State.ENDED
                    || player.getState() == RzPlayer.State.ERROR) {
                player = new RzPlayer();
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                player.addOnPreparedListener(this);
                player.addOnCompletionListener(this);
            }

            if (player.getState() == RzPlayer.State.IDLE)
                player.setDataSource(audio);

            if (player.getState() == RzPlayer.State.INITIALIZED)
                if (prepareAsync)
                    player.prepareAsync();
                else
                    player.prepare();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toggleButtonPlay_MessageItem:
                if (toggleButtonPlay.isChecked()) {
                    start();
                } else {
                    pause();
                }
                break;
        }
    }

    void start() {
        BusProvider.getInstance().register(this);

        initPlayer(false);

        player.start();
        handlerProgress.postDelayed(runnableProgress, 10);

        toggleButtonPlay.setChecked(true);

        int position = getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            BusProvider.getInstance().post(new UserStartedPlayingMediaEvent(audio));
        }
    }

    void pause() {
        BusProvider.getInstance().unregister(this);

        if (player.getState() == RzPlayer.State.STARTED) {
            player.pause();
            handlerProgress.removeCallbacks(runnableProgress);
        }

        toggleButtonPlay.setChecked(false);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        // MediaPlayer finished playing
        BusProvider.getInstance().unregister(this);

        toggleButtonPlay.setChecked(false);
        setProgress(0);

        player.reset();

        handlerProgress.removeCallbacks(runnableProgress);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        toggleButtonPlay.setVisibility(View.VISIBLE);
        progressBarDuration.setMax(mp.getDuration());
        setProgress(mp.getDuration());
        progressBarDuration.setProgress(0);
    }

    private void setProgress(int progress) {
        int seconds = progress / 1000;
        int minutes = seconds / 60;
        seconds = seconds % 60;

        progressBarDuration.setProgress(progress + 1);

        DecimalFormat formatter = new DecimalFormat("00");
        textViewDuration.setText(
                formatter.format(minutes) + ":" +
                        formatter.format(seconds)
        );
    }

    @Subscribe
    public void onUserStartedPlayingMedia(UserStartedPlayingMediaEvent event) {
        // if user start playing another media
        if (!event.getUrl().equals(audio)) {
            pause();
        }
    }
}