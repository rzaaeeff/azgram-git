package az.azgram.android.app.models.response;

import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.CoreModel;

import java.util.List;

/**
 * Created by Rzaaeeff on 4/24/2017.
 */

public class ConversationListModel extends CoreModel {
    private List<ConversationModel> conversations;

    public List<ConversationModel> getConversations() {
        return conversations;
    }

    public void setConversations(List<ConversationModel> conversations) {
        this.conversations = conversations;
    }
}
