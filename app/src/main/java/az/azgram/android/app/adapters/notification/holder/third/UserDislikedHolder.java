package az.azgram.android.app.adapters.notification.holder.third;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.holder.second.TwoUsernamePostHolder;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class UserDislikedHolder extends TwoUsernamePostHolder {
    public UserDislikedHolder(View itemView, Context context) {
        super(itemView, context);

        TEXT_FORMAT = context.getString(R.string.notification_user_disliked);
    }
}
