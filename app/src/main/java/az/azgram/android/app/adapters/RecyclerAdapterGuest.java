package az.azgram.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import az.azgram.android.app.R;
import az.azgram.android.app.events.ui.GuestItemClickedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 2/11/2017.
 */

public class RecyclerAdapterGuest extends RecyclerView.Adapter<RecyclerAdapterGuest.GuestHolder> {
    // Constant Fields
    public static final int COLUMN_COUNT = 3;

    private Context context;
    private UserListModel data;

    public RecyclerAdapterGuest(Context context, UserListModel data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public GuestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.recycler_item_guest,
                parent,
                false
        );

        return new GuestHolder(view);
    }

    @Override
    public void onBindViewHolder(GuestHolder holder, int position) {
        holder.bind(data.getUsers().get(position));
    }

    @Override
    public int getItemCount() {
        return data.getUsers().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class GuestHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageViewProfilePhoto, imageViewUserBadge;
        private TextView textViewName, textViewTime;
        private LinearLayout rootLayout;

        public GuestHolder(View itemView) {
            super(itemView);

            imageViewProfilePhoto = (ImageView) itemView.findViewById(R.id.imageViewProfilePhotoGuestItem);
            imageViewUserBadge = (ImageView) itemView.findViewById(R.id.imageViewUserBadgeGuestItem);
            textViewName = (TextView) itemView.findViewById(R.id.textViewNameGuestItem);
            textViewTime = (TextView) itemView.findViewById(R.id.textViewTimeGuestItem);
            rootLayout = (LinearLayout) itemView.findViewById(R.id.rootLayoutGuestItem);

            rootLayout.setOnClickListener(this);
        }

        public void bind(UserModel user) {
            if (user.getProfilePhoto() != null && !user.getUsername().equals("Anonim")) {
                Utility.loadWithPicasso(
                        context,
                        user.getProfilePhoto().getSmallestPossible(),
                        imageViewProfilePhoto,
                        R.drawable.profile_photo_placeholder
                );
            } else {
                imageViewProfilePhoto.setImageResource(R.drawable.profile_photo_placeholder);
            }

            if (user.isOnline() != null) {
                imageViewUserBadge.setImageResource(
                        user.isOnline() ? R.drawable.user_online_badge :
                                R.drawable.user_offline_badge
                );
            } else {
                imageViewUserBadge.setVisibility(View.GONE);
            }

            if (user.getFullName() != null) {
                if (user.getFullName().length() > 12) {
                    textViewName.setWidth(150);
                    textViewName.setText(user.getUsername());
                } else
                    textViewName.setText(user.getFullName());
            } else {
                textViewName.setText(user.getUsername());
            }

            setTimeAgo(user.getDate());
        }

        public int pxToDp(int px) {
            DisplayMetrics displayMetrics = textViewName.getContext().getResources().getDisplayMetrics();
            return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        }

        private void setTimeAgo(String dateStr) {
            try {
                SimpleDateFormat format = new SimpleDateFormat(CoreModel.DATE_TIME_FORMAT);
                Date date = format.parse(dateStr);
                textViewTime.setText(DateUtils.getTimeAgo(date, context));
            } catch (Exception exc) {
                textViewTime.setText(dateStr);
            }
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == rootLayout.getId()) {
                int position = getAdapterPosition();
                UserModel user = data.getUsers().get(position);

                if (user.getUsername() != null) {
                    BusProvider.getInstance().post(new GuestItemClickedEvent(user));
                } else {
                    Toast.makeText(context, context.getString(R.string.GuestItemAnonymousText), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}