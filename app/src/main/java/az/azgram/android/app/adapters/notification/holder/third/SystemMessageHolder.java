package az.azgram.android.app.adapters.notification.holder.third;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.adapters.notification.holder.first.TextNotificationHolder;
import az.azgram.android.app.models.NotificationModel;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class SystemMessageHolder extends TextNotificationHolder {
    public SystemMessageHolder(View itemView, Context context) {
        super(itemView, context);

        setRootListener(null);
    }

    @Override
    public void bind(NotificationModel notification) {
        super.bind(notification, notification.getSystemMessage());
    }
}
