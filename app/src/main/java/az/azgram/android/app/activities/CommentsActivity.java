package az.azgram.android.app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.apradanas.simplelinkabletext.LinkableEditText;
import com.google.gson.Gson;
import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterExtendedComment;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.CommentDeletedEvent;
import az.azgram.android.app.events.posts.CommentPostedEvent;
import az.azgram.android.app.events.posts.CommentsReceivedEvent;
import az.azgram.android.app.models.CommentModel;
import az.azgram.android.app.models.PhotoModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.CommentFeedModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.ImageUtils;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.LinkMechanism;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Utility;
import com.squareup.otto.Subscribe;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CommentsActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 123;
    // Widgets
    private RecyclerView recyclerView;
    private LinkableEditText editText;
    private ProgressBar progressBar;
    private ImageButton imageButtonSend, imageButtonPick;

    // Other elements
    private RecyclerView.OnScrollListener onScrollListener;
    private CommentFeedModel feed;
    private UserModel request;
    private RecyclerAdapterExtendedComment adapter;
    private boolean isLoading;
    private boolean shouldOpenInputField = false;
    private int commentCount;
    private String commentText;
    private Bitmap image = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        Intent intent = getIntent();
        if (intent.getExtras().containsKey(PostModel.UID)) {
            PostModel post = (PostModel) intent.getSerializableExtra(PostModel.UID);

            Cache cache = new Cache(this);
            UserModel user = cache.loadUser();
            request = new UserModel();
            request.setApiKey(Keys.API_KEY);
            request.setUsername(post.getAuthor().getUsername());
            request.setToken(user.getToken());
            request.setId(post.getId());
            request.setPage(0);

            commentCount = post.getCommentCount();

            if (intent.getExtras().containsKey(Constants.SHOULD_OPEN_INPUT_FIELD)) {
                shouldOpenInputField = intent.getBooleanExtra(Constants.SHOULD_OPEN_INPUT_FIELD, false);
            }

            init();
            stopLoading();

            loadMore();
        } else {
            // We didn't receive anything
            // Let's close activity
            finish();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        BusProvider.getInstance().unregister(this);

        super.onPause();
    }

    private void startLoading() {
        isLoading = true;

        final Context context = this;
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,
                        getString(R.string.ProcessOngoing) + "\n" + getString(R.string.PleaseWait)
                        , Toast.LENGTH_SHORT).show();
            }
        };

        imageButtonSend.setOnClickListener(onClickListener);
        imageButtonPick.setOnClickListener(onClickListener);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        progressBar.setVisibility(View.GONE);

        imageButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSendButtonClicked();
            }
        });

        imageButtonPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.PickAnImage)), PICK_IMAGE);
            }
        });
    }

    private void init() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewCommentsActivity);
        progressBar = (ProgressBar) findViewById(R.id.progressBarCommentsActivity);
        editText = (LinkableEditText) findViewById(R.id.editTextCommentsActivity);
        imageButtonSend = (ImageButton) findViewById(R.id.imageButtonSendCommentsActivity);
        imageButtonPick = (ImageButton) findViewById(R.id.imageButtonPickCommentsActivity);

        // EditText
        if (shouldOpenInputField) {
            editText.requestFocus();
            editText.setTextIsSelectable(true);
        }

        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        // Hashtag and Mention
        List<View> views = new ArrayList<>();
        views.add(editText);
        LinkMechanism.set(views, this);

        // RecyclerView
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(30);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        feed = new CommentFeedModel();
        feed.setComments(new ArrayList<CommentModel>());
        adapter = new RecyclerAdapterExtendedComment(this, feed);
        adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);

        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (isLoading)
                    return;

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                    loadMore();
                }
            }
        };

        recyclerView.addOnScrollListener(onScrollListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Toast.makeText(this, getString(R.string.UnknownError), Toast.LENGTH_SHORT).show();
                image = null;
                return;
            }

            try {
                InputStream inputStreamImage = getContentResolver().openInputStream(data.getData());
                image = ImageUtils.inputStreamToBitmap(inputStreamImage);
            } catch (Exception exc) {
                image = null;
            }
        }
    }

    private void loadMore() {
        if (isLoading)
            return;

        if (request.getPage() != null) {
            request.setPage(request.getPage() + 1);
            startLoading();

            NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
            service.getPostsEndpoint().getComments(request);
        }
    }

    /**
     * Referenced from XML
     * OnClick method
     *
     * @param view
     */
    public void finishCommentsActivity(View view) {
        finish();
    }

    private void onSendButtonClicked() {
        if (!editText.getText().toString().trim().isEmpty() &&
                editText.getText().toString().trim().length() > 0) {
            commentText = editText.getText().toString();
            postNewComment();
        }
    }

    private void postNewComment() {
        Cache cache = new Cache(this);
        UserModel user = cache.loadUser();

        CommentModel comment = new CommentModel();

        comment.setText(commentText);
        comment.setApiKey(Keys.API_KEY);
        comment.setId(request.getId());
        comment.setAuthor(new UserModel());
        comment.getAuthor().setUsername(request.getUsername());
        comment.setToken(user.getToken());

        if (image != null) {
            try {
                String background = ImageUtils.encodeBitmapToBase64(image);
                comment.setPhoto(new PhotoModel());
                comment.getPhoto().setLarge(background);
            } catch (Exception exc) {
                comment.setPhoto(null);
            }
        }

        String json = new Gson().toJson(comment);

        NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
        service.getPostsEndpoint().comment(comment);

        editText.setText("");
        Utility.hideSoftKeyboard(editText, this);
        startLoading();
    }

    public void reply(String username) {
        editText.setText("@" + username + " ");
        editText.setSelection(editText.getText().length());
        if (!editText.isFocused())
            editText.requestFocus();

    }

    public void delete(CommentModel comment) {
        try {
            PostModel post = new PostModel();
            post.setId(request.getId());
            post.setAuthor(new UserModel());
            post.getAuthor().setUsername(request.getUsername());
            post.setToken(request.getToken());
            post.setApiKey(Keys.API_KEY);

            post.setComments(new ArrayList<CommentModel>());
            post.getComments().add(new CommentModel());
            post.getComments().get(0).setId(comment.getId());

            NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
            service.getPostsEndpoint().deleteComment(post);
            startLoading();
            init();
        } catch (Exception exc) {
            stopLoading();
            Toast.makeText(this, getString(R.string.UnknownError), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(CommentDeletedEvent event) {
        stopLoading();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            feed.getComments().clear();
            request.setPage(0);
            loadMore();
        } else {
            Toast.makeText(this, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(CommentPostedEvent event) {
        stopLoading();
        image = null;

        switch (event.getModel().getMessageId()) {
            case NetworkOperations.API_SUCCESS:
                feed.getComments().clear();
                request.setPage(0);
                loadMore();
                break;
            case NetworkOperations.Posts.SAME_COMMENT:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.CommentsActivityErrorSameComment));
                break;
            default:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.UnknownError) + "\n"
                                + getString(R.string.TryAgain));
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(CommentsReceivedEvent event) {
        stopLoading();

        if (event.getModel() != null)
            if (event.getModel().getComments() != null) {
                feed.getComments().addAll(event.getModel().getComments());
                adapter.notifyDataSetChanged();

                if (event.getModel().getPage() == null)
                    recyclerView.removeOnScrollListener(onScrollListener);

                request.setPage(event.getModel().getPage());
            } else if (event.getModel().getMessageId() == NetworkOperations.Posts.COMMENTS_NOT_FOUND) {
                // do nothing; there was no comment to load
            } else {
                Toast.makeText(this, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
            }
    }
}