package az.azgram.android.app.events.posts;

import az.azgram.android.app.models.PostModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class PostReceivedEvent {
    private PostModel model;

    public PostReceivedEvent(PostModel model) {
        this.model = model;
    }

    public PostModel getModel() {
        return model;
    }

    public void setModel(PostModel model) {
        this.model = model;
    }
}
