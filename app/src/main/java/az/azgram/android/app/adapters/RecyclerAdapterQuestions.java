package az.azgram.android.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import az.azgram.android.app.R;
import az.azgram.android.app.activities.AnswerQuestionActivity;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.custom.RoundedCornerLayout;
import az.azgram.android.app.events.posts.QuestionDeletedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.QuestionModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.QuestionFeedModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Utility;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rzaaeeff on 12/12/2016.
 */

public class RecyclerAdapterQuestions extends RecyclerView.Adapter<RecyclerAdapterQuestions.QuestionViewHolder>{
    private boolean isAnotherUser = false;
    private QuestionFeedModel feed;
    private Context context;
    private UserModel user;

    public RecyclerAdapterQuestions(Context context, QuestionFeedModel feed, UserModel user) {
        this.context = context;
        this.feed = feed;
        this.user = user;

        isAnotherUser = !(new Cache(context).loadUser().getUsername().equals(user.getUsername()));
        BusProvider.getInstance().register(this);
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_question, parent, false);

        return new QuestionViewHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {
        final QuestionModel question = feed.getQuestions().get(position);

        if (!question.isAnonymous())
            holder.textViewAuthor.setText(question.getAuthor().getUsername());

        holder.textViewQuestion.setText(question.getText());

        if (question.isAnswered()) {
            holder.setAnswered(true);
            holder.textViewAnswer.setText(question.getAnswer());
        } else {
            holder.setAnswered(false);
        }

        if (isAnotherUser)
            holder.hideControls();
        else {
            final QuestionModel minQuestion = new QuestionModel();
            minQuestion.setAuthor(new UserModel());
            minQuestion.getAuthor().setUsername(question.getAuthor().getUsername());
            minQuestion.setId(question.getId());
            minQuestion.setApiKey(Keys.API_KEY);
            minQuestion.setToken(new Cache(context).loadUser().getToken());
            String json = new Gson().toJson(minQuestion);

            holder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NetworkOperations service = new NetworkOperations(context, BusProvider.getInstance());
                    service.getPostsEndpoint().deleteQuestion(minQuestion);
                }
            });

            if (!question.isAnswered()) {
                holder.buttonAnswer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        minQuestion.setText(question.getText());

                        Intent intent = new Intent(context, AnswerQuestionActivity.class);
                        intent.putExtra(QuestionModel.UID, minQuestion);
                        context.startActivity(intent);
                    }
                });
            }
        }

        holder.setTimeAgo(question.getDate());

        if (question.getBackgroundPhoto() != null) {
            Utility.loadWithPicasso(
                    context,
                    question.getBackgroundPhoto().getLarge(),
                    holder.imageViewBackground
            );
        } else {
            holder.imageViewBackground.setImageBitmap(null);
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(QuestionDeletedEvent event) {
        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            for (int i = 0; i < feed.getQuestions().size(); i++) {
                if (feed.getQuestions().get(i).getId() == event.getModel().getId()) {
                    feed.getQuestions().remove(i);
                    notifyItemRemoved(i);
                }
            }
        } else {
            Toast.makeText(context, context.getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return feed.getQuestions().size();
    }

    public class QuestionViewHolder extends RecyclerView.ViewHolder {
        RoundedCornerLayout root;
        ImageView imageViewBackground, imageViewDelete;
        TextView textViewAuthor, textViewQuestion, textViewAnswer, textViewTime;
        LinearLayout buttonAnswer;

        public QuestionViewHolder(View itemView) {
            super(itemView);

            imageViewBackground = (ImageView) itemView.findViewById(R.id.imageViewBackgroundQuestionItem);
            textViewAuthor = (TextView) itemView.findViewById(R.id.textViewAuthorQuestionItem);
            textViewQuestion = (TextView) itemView.findViewById(R.id.textViewQuestionQuestionItem);
            textViewAnswer = (TextView) itemView.findViewById(R.id.textViewAnswerQuestionItem);
            textViewTime = (TextView) itemView.findViewById(R.id.textViewTimeQuestionItem);
            buttonAnswer = (LinearLayout) itemView.findViewById(R.id.buttonAnswerQuestionItem);
            imageViewDelete = (ImageView) itemView.findViewById(R.id.imageViewDeleteQuestionItem);

            root = (RoundedCornerLayout) itemView.findViewById(R.id.roundedLayoutRecyclerItemQuestionsFragment);
            root.setCornerRadius(context.getResources().getDimension(R.dimen.QuestionItemCornerRadius));
        }

        public void setAnswered(boolean answered) {
            if (answered) {
                buttonAnswer.setVisibility(View.GONE);
                textViewAnswer.setVisibility(View.VISIBLE);
            } else {
                buttonAnswer.setVisibility(View.VISIBLE);
                textViewAnswer.setVisibility(View.GONE);
            }
        }

        public void hideControls() {
            buttonAnswer.setVisibility(View.GONE);
            imageViewDelete.setVisibility(View.GONE);
        }

        public void setTimeAgo(String dateStr) {
            try {
                SimpleDateFormat df = new SimpleDateFormat(CoreModel.DATE_TIME_FORMAT);
                Date date = df.parse(dateStr.trim());

                String agoStr = DateUtils.getTimeAgo(date, context);
                textViewTime.setText(agoStr);
            } catch (Exception exc) {
                // We couldn't parse it
                textViewTime.setText(dateStr);
            }
        }
    }
}
