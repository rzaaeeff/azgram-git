package az.azgram.android.app.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alexvasilkov.gestures.views.GestureImageView;

import az.azgram.android.app.R;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 6/30/2017.
 */

public class PhotoDialog extends DialogFragment {

    public static final String ARG_URL = "arg_url";

    // Widgets
    private GestureImageView imageView;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.dialog_photo, null);
        dialog.setContentView(contentView);

        ((View) contentView.getParent()).setBackgroundColor(Color.TRANSPARENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_photo, container, false);

        imageView = (GestureImageView) view.findViewById(R.id.imageView_PhotoDialog);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
    }

    @Override
    public void onResume() {
        super.onResume();

        Utility.loadWithPicasso(
                getContext(),
                getArguments().getString(ARG_URL, ""),
                imageView,
                R.drawable.post_photo_placeholder
        );
    }
}