package az.azgram.android.app.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import az.azgram.android.app.R;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.QuestionAnsweredEvent;
import az.azgram.android.app.models.QuestionModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.LinkMechanism;
import az.azgram.android.app.operations.NetworkOperations;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import cn.refactor.lib.colordialog.ColorDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AnswerQuestionActivity extends AppCompatActivity {

    // Widgets
    private EditText editText;
    private TextView textView;
    private ImageButton imageButton;
    private ProgressBar progressBar;

    // Other Elements
    private QuestionModel request;
    private QuestionModel question;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_question);

        if (getIntent().hasExtra(QuestionModel.UID))
            question = (QuestionModel) getIntent().getSerializableExtra(QuestionModel.UID);
        else
            finish();

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        BusProvider.getInstance().unregister(this);

        super.onPause();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * OnClick method for back button
     * Referenced from XML
     */
    public void finishAnswerQuestionActivity(View view) {
        final ColorDialog dialog;
        if (isLoading) {
            dialog = Interaction.getInstance(this).showPromptDialog(
                    getString(R.string.ProcessOngoing) + "\n" + getString(R.string.DoYouWantCancel));

            dialog.setPositiveListener(getString(R.string.Yes), new ColorDialog.OnPositiveListener() {
                @Override
                public void onClick(ColorDialog colorDialog) {
                    finish();
                }
            })
                    .setNegativeListener(getString(R.string.No), new ColorDialog.OnNegativeListener() {
                        @Override
                        public void onClick(ColorDialog colorDialog) {
                            dialog.dismiss();
                        }
                    });

            dialog.show();
        } else {
            finish();
        }
    }

    /**
     * Assigns IDs and does some other initialization
     */
    private void init() {
        editText = (EditText) findViewById(R.id.editTextAnswerQuestionActivity);
        textView = (TextView) findViewById(R.id.textViewAnswerQuestionActivity);
        imageButton = (ImageButton) findViewById(R.id.imageButtonSendAnswerQuestionActivity);
        progressBar = (ProgressBar) findViewById(R.id.progressBarAnswerQuestionActivity);

        editText.setTextIsSelectable(true);
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        textView.setText(question.getText());

        // Hashtag and Mention
        List<View> views = new ArrayList<>();
        views.add(editText);
        LinkMechanism.set(views, this);

        // Initializing request
        request = new QuestionModel();

        if (question.getAuthor() != null) {
            request.setTarget(new UserModel());
            request.getTarget().setUsername(question.getAuthor().getUsername());
        }

        request.setApiKey(Keys.API_KEY);
        request.setToken(new Cache(this).loadUser().getToken());
        request.setId(question.getId());

        stopLoading();
    }

    private void startLoading() {
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);

        final Context context = this;
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,
                        getString(R.string.ProcessOngoing) + "\n" + getString(R.string.PleaseWait),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void stopLoading() {
        isLoading = false;
        progressBar.setVisibility(View.GONE);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answer();
            }
        });
    }

    private void answer() {
        if (!isLoading) {
            if (editText.getText() != null && editText.getText().toString().trim().length() > 0) {
                request.setAnswer(editText.getText().toString());

                String json = new Gson().toJson(request);

                NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
                service.getPostsEndpoint().answerQuestion(request);
                startLoading();
            }
        } else
            Toast.makeText(this,
                    getString(R.string.ProcessOngoing) + "\n" + getString(R.string.PleaseWait),
                    Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onAzGramServiceResponse(QuestionAnsweredEvent event) {
        stopLoading();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, getString(R.string.AnswerQuestionActivitySuccessMessage), Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }
}
