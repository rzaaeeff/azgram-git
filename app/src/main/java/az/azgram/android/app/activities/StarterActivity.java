package az.azgram.android.app.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

import java.util.Locale;

import az.azgram.android.app.models.AppSettingsModel;
import az.azgram.android.app.operations.Cache;

public class StarterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Cache cache = new Cache(this);

        if (cache.isUpdateNeeded()) {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        } else {
            AppSettingsModel settings = cache.loadSettings();
            if (settings.getLanguage() != null) {
                switch (settings.getLanguage()) {
                    case AppSettingsModel.LANGUAGE.AZ:
                        changeLanguage("az");
                        break;
                     case AppSettingsModel.LANGUAGE.EN:
                        changeLanguage("en");
                        break;
                    case AppSettingsModel.LANGUAGE.RU:
                        changeLanguage("ru");
                    break;
                    case AppSettingsModel.LANGUAGE.TR:
                        changeLanguage("tr");
                        break;
                }
            }

            Intent intent;

            if (cache.hasUser()) {
                intent = new Intent(this, CoreActivity.class);
            } else {
                intent = new Intent(this, LoginActivity.class);
            }

            startActivity(intent);
        }

        finish();
    }

    private void changeLanguage(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}