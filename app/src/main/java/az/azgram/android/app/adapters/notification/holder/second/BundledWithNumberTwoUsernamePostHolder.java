package az.azgram.android.app.adapters.notification.holder.second;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import az.azgram.android.app.adapters.notification.holder.first.PhotoNotificationBundledHolder;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.SpanFormatter;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class BundledWithNumberTwoUsernamePostHolder extends PhotoNotificationBundledHolder {
    public BundledWithNumberTwoUsernamePostHolder(View itemView, Context context) {
        super(itemView, context);
    }

    public void bind(NotificationModel notification) {
        super.bind(notification, SpanFormatter.format(TEXT_FORMAT,
                UniversalListenerMethods.getHighlightedAndClickableUsername(
                        notification.getUsers().get(0), context
                ),
                notification.getPosts().size(),
                UniversalListenerMethods.getHighlightedAndClickableUsername(
                        notification.getUsers().get(1), context
                )
        ));
    }
}