package az.azgram.android.app.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.users.ProfilePhotoChangedEvent;
import az.azgram.android.app.events.users.SettingChangedEvent;
import az.azgram.android.app.events.users.UserReceivedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.PhotoModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.UserSettingsModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Security;
import az.azgram.android.app.operations.Utility;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.refactor.lib.colordialog.ColorDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    // Constants
    private final static byte REQUEST_CHOOSE_PHOTO = 90;

    // Widgets
    private TextView textViewName, textViewEmail, textViewPhone,
            textViewUsername, textViewPassword, textViewGender,
            textViewBirthDate, textViewCountry, textViewInfo;
    private Button buttonName, buttonEmail, buttonPhone,
            buttonUsername, buttonPassword, buttonGender,
            buttonBirthDate, buttonCountry, buttonInfo,
            buttonDesign1, buttonDesign2, buttonDesign3;
    private ImageView imageViewPhoto;

    // Other Elements
    private NetworkOperations service;
    private UserSettingsModel request;
    private UserModel cachedUser;
    private ProgressDialog dialogLoading;
    private String[] stringArrayGender, stringArrayCountry;
    private boolean isLoading;
    private Cache cache;
    private Uri photoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);

        init();

        cache = new Cache(this);
        cachedUser = cache.loadUser();

        initValues();
        setListeners();
        notifyDataChanged();
    }

    // To support fonts
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onStop() {
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    public void finishProfileSettingsActivity(View view) {
        finish();
    }

    private void init() {
        textViewName = (TextView) findViewById(R.id.textViewName_ProfileSettingsActivity);
        buttonName = (Button) findViewById(R.id.buttonName_ProfileSettingsActivity);
        textViewEmail = (TextView) findViewById(R.id.textViewEmail_ProfileSettingsActivity);
        buttonEmail = (Button) findViewById(R.id.buttonEmail_ProfileSettingsActivity);
        textViewPhone = (TextView) findViewById(R.id.textViewPhone_ProfileSettingsActivity);
        buttonPhone = (Button) findViewById(R.id.buttonPhone_ProfileSettingsActivity);
        textViewUsername = (TextView) findViewById(R.id.textViewUsername_ProfileSettingsActivity);
        buttonUsername = (Button) findViewById(R.id.buttonUsername_ProfileSettingsActivity);
        textViewPassword = (TextView) findViewById(R.id.textViewPassword_ProfileSettingsActivity);
        buttonPassword = (Button) findViewById(R.id.buttonPassword_ProfileSettingsActivity);
        textViewGender = (TextView) findViewById(R.id.textViewGender_ProfileSettingsActivity);
        buttonGender = (Button) findViewById(R.id.buttonGender_ProfileSettingsActivity);
        textViewBirthDate = (TextView) findViewById(R.id.textViewBirthDate_ProfileSettingsActivity);
        buttonBirthDate = (Button) findViewById(R.id.buttonBirthDate_ProfileSettingsActivity);
        textViewCountry = (TextView) findViewById(R.id.textViewCountry_ProfileSettingsActivity);
        buttonCountry = (Button) findViewById(R.id.buttonCountry_ProfileSettingsActivity);
        textViewInfo = (TextView) findViewById(R.id.textViewInfo_ProfileSettingsActivity);
        buttonInfo = (Button) findViewById(R.id.buttonInfo_ProfileSettingsActivity);

        buttonDesign1 = (Button) findViewById(R.id.buttonDesign1_ProfileSettingsActivity);
        buttonDesign2 = (Button) findViewById(R.id.buttonDesign2_ProfileSettingsActivity);
        buttonDesign3 = (Button) findViewById(R.id.buttonDesign3_ProfileSettingsActivity);

        imageViewPhoto = (ImageView) findViewById(R.id.imageViewPhoto_ProfileSettingsActivity);

        stringArrayGender = getResources().getStringArray(R.array.GenderArray);
        stringArrayCountry = getResources().getStringArray(R.array.CountryArray);
    }

    private void notifyDataChanged() {
        if (cachedUser.getFullName() != null) {
            textViewName.setText(cachedUser.getFullName());
        }

        if (cachedUser.getEmail() != null) {
            textViewEmail.setText(cachedUser.getEmail());
        }

        if (cachedUser.getPhoneNumber() != null) {
            textViewPhone.setText(cachedUser.getPhoneNumber());
        }

        if (cachedUser.getUsername() != null) {
            textViewUsername.setText(cachedUser.getUsername());
        }

        // Skipping Password

        if (cachedUser.getGender() != null) {
            textViewGender.setText(stringArrayGender[cachedUser.getGender()]);
        }

        if (cachedUser.getBirthDate() != null) {
            String formattedDate = DateUtils.getFormattedDate(
                    cachedUser.getBirthDate(),
                    CoreModel.DATE_FORMAT,
                    "dd.MM.yyyy"
            );

            if (formattedDate != null) {
                textViewBirthDate.setText(formattedDate);
            }
        }

        if (cachedUser.getCountry() != null) {
            textViewCountry.setText(stringArrayCountry[cachedUser.getCountry()]);
        }

        if (cachedUser.getInfo() != null) {
            textViewInfo.setText(cachedUser.getInfo());
        }

        if (cachedUser.getProfilePhoto() != null &&
                cachedUser.getProfilePhoto().getSmallestPossible() != null) {
            Utility.loadWithPicasso(
                    this,
                    cachedUser.getProfilePhoto().getSmallestPossible(),
                    imageViewPhoto,
                    R.drawable.profile_photo_placeholder
            );
        }
    }

    private void initValues() {
        dialogLoading = Interaction.getInstance(this).showLoadingDialog(
                getString(R.string.PleaseWait)
        );
        dialogLoading.hide();

        request = new UserSettingsModel();
        request.setApiKey(Keys.API_KEY);
        request.setToken(cachedUser.getToken());

        service = new NetworkOperations(this, BusProvider.getInstance());
        service.getUsersEndpoint();
    }

    private void setListeners() {
        textViewName.setOnClickListener(this);
        buttonName.setOnClickListener(this);
        textViewEmail.setOnClickListener(this);
        buttonEmail.setOnClickListener(this);
        textViewPhone.setOnClickListener(this);
        buttonPhone.setOnClickListener(this);
        textViewUsername.setOnClickListener(this);
        buttonUsername.setOnClickListener(this);
        textViewPassword.setOnClickListener(this);
        buttonPassword.setOnClickListener(this);
        textViewGender.setOnClickListener(this);
        buttonGender.setOnClickListener(this);
        textViewBirthDate.setOnClickListener(this);
        buttonBirthDate.setOnClickListener(this);
        textViewCountry.setOnClickListener(this);
        buttonCountry.setOnClickListener(this);
        textViewInfo.setOnClickListener(this);
        buttonInfo.setOnClickListener(this);

        buttonDesign1.setOnClickListener(this);
        buttonDesign2.setOnClickListener(this);
        buttonDesign3.setOnClickListener(this);

        imageViewPhoto.setOnClickListener(this);
    }

    private void startLoading() {
        isLoading = true;
        dialogLoading.show();
    }

    private void stopLoading() {
        isLoading = false;
        dialogLoading.hide();
    }

    private void getUserFromServer() {
        UserModel userToGetFromServer = new UserModel();
        userToGetFromServer.setUsername(cachedUser.getUsername());
        userToGetFromServer.setToken(cachedUser.getToken());
        userToGetFromServer.setApiKey(request.getApiKey());
        service.getUsersEndpoint().getUser(userToGetFromServer);

        startLoading();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == textViewName.getId() || i == buttonName.getId()) {
            changeName();
        } else if (i == textViewEmail.getId() || i == buttonEmail.getId()) {
            changeEmail();
        } else if (i == textViewPhone.getId() || i == buttonPhone.getId()) {
            changePhone();
        } else if (i == textViewUsername.getId() || i == buttonUsername.getId()) {
            changeUsername();
        } else if (i == textViewPassword.getId() || i == buttonPassword.getId()) {
            changePassword();
        } else if (i == textViewGender.getId() || i == buttonGender.getId()) {
            changeGender();
        } else if (i == textViewBirthDate.getId() || i == buttonBirthDate.getId()) {
            changeBirthDate();
        } else if (i == textViewCountry.getId() || i == buttonCountry.getId()) {
            changeCountry();
        } else if (i == textViewInfo.getId() || i == buttonInfo.getId()) {
            changeInfo();
        } else if (i == buttonDesign1.getId()) {
            changeProfileDesign(UserModel.ProfileDesign.OPTION1);
        } else if (i == buttonDesign2.getId()) {
            changeProfileDesign(UserModel.ProfileDesign.OPTION2);
        } else if (i == buttonDesign3.getId()) {
            changeProfileDesign(UserModel.ProfileDesign.OPTION3);
        } else if (i == imageViewPhoto.getId()) {
            Intent intent = new Intent(this, PhotoPickerActivity.class);
            startActivityForResult(intent, REQUEST_CHOOSE_PHOTO);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHOOSE_PHOTO) {
            if (resultCode == RESULT_OK) {
                try {
                    CoreModel photoRequest = new CoreModel();
                    photoRequest.setToken(request.getToken());
                    photoRequest.setApiKey(request.getApiKey());
                    photoUri = Uri.parse(data.getStringExtra("data"));
                    service.getUsersEndpoint().savePP(photoRequest, photoUri);
                    startLoading();
                } catch (Exception exc) {
                    Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void changeName() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_input,
                (ViewGroup) findViewById(android.R.id.content), false);
        final TextInputLayout inputLayout = (TextInputLayout) viewInflated.findViewById(R.id.inputLayout);
        final TextInputEditText input = (TextInputEditText) inputLayout.getEditText();
        inputLayout.setHint(getString(
                R.string.ProfileSettingsFragmentNameSurnameText));
        builder.setTitle(R.string.ProfileSettingsFragmentNameSurnameText);
        builder.setView(viewInflated);

        if (cachedUser.getFullName() != null) {
            input.setText(cachedUser.getFullName());
            input.setSelection(input.getText().length());
        }

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (input.getText().toString().trim().equals(textViewName.getText()))
                    return;

                request.setChange("fullName");
                request.setFullName(input.getText().toString().trim());

                service.getUsersEndpoint().save(request);
                startLoading();
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final Button button = builder.show().getButton(DialogInterface.BUTTON_POSITIVE);

        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() > 0) {
                    button.setEnabled(true);
                    button.setAlpha(Constants.ENABLED_STATE_ALPHA);

                    inputLayout.setErrorEnabled(false);
                } else {
                    button.setEnabled(false);
                    button.setAlpha(Constants.DISABLED_STATE_ALPHA);

                    inputLayout.setErrorEnabled(true);
                    inputLayout.setError(getString(R.string.CoreActivityMessageFillEmptyField));
                }
            }
        });
    }

    private void changeEmail() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_input,
                (ViewGroup) findViewById(android.R.id.content), false);
        final TextInputLayout inputLayout = (TextInputLayout) viewInflated.findViewById(R.id.inputLayout);
        final TextInputEditText input = (TextInputEditText) inputLayout.getEditText();
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        inputLayout.setHint(getString(
                R.string.ProfileSettingsFragmentEmailText));
        builder.setTitle(R.string.ProfileSettingsFragmentEmailText);
        builder.setView(viewInflated);

        if (cachedUser.getEmail() != null) {
            input.setText(cachedUser.getEmail());
            input.setSelection(input.getText().length());
        }

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (input.getText().toString().trim().equals(textViewEmail.getText()))
                    return;

                if (Utility.isValidEmailAddress(input.getText().toString().trim())) {
                    request.setChange("email");
                    request.setEmail(input.getText().toString().trim());

                    service.getUsersEndpoint().save(request);
                    startLoading();
                } else {
                    Toast.makeText(ProfileSettingsActivity.this,
                            getString(R.string.RegistrationActivityMessageInvalidEmail), Toast.LENGTH_SHORT).show();
                }
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final Button button = builder.show().getButton(DialogInterface.BUTTON_POSITIVE);

        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() > 0) {
                    button.setEnabled(true);
                    button.setAlpha(Constants.ENABLED_STATE_ALPHA);

                    inputLayout.setErrorEnabled(false);
                } else {
                    button.setEnabled(false);
                    button.setAlpha(Constants.DISABLED_STATE_ALPHA);

                    inputLayout.setErrorEnabled(true);
                    inputLayout.setError(getString(R.string.CoreActivityMessageFillEmptyField));
                }
            }
        });
    }

    private void changePhone() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_input,
                (ViewGroup) findViewById(android.R.id.content), false);
        final TextInputLayout inputLayout = (TextInputLayout) viewInflated.findViewById(R.id.inputLayout);
        final TextInputEditText input = (TextInputEditText) inputLayout.getEditText();
        input.setInputType(InputType.TYPE_CLASS_PHONE);
        inputLayout.setHint(getString(
                R.string.ProfileSettingsFragmentPhoneText));
        builder.setTitle(R.string.ProfileSettingsFragmentPhoneText);
        builder.setView(viewInflated);

        if (cachedUser.getPhoneNumber() != null) {
            input.setText(cachedUser.getPhoneNumber());
            input.setSelection(input.getText().length());
        }

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (input.getText().toString().trim().equals(textViewPhone.getText()))
                    return;

                request.setChange("phoneNumber");
                request.setPhoneNumber(input.getText().toString().trim());

                service.getUsersEndpoint().save(request);
                startLoading();
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final Button button = builder.show().getButton(DialogInterface.BUTTON_POSITIVE);

        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() > 0) {
                    button.setEnabled(true);
                    button.setAlpha(Constants.ENABLED_STATE_ALPHA);

                    inputLayout.setErrorEnabled(false);
                } else {
                    button.setEnabled(false);
                    button.setAlpha(Constants.DISABLED_STATE_ALPHA);

                    inputLayout.setErrorEnabled(true);
                    inputLayout.setError(getString(R.string.CoreActivityMessageFillEmptyField));
                }
            }
        });
    }

    private void changeUsername() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_input,
                (ViewGroup) findViewById(android.R.id.content), false);
        final TextInputLayout inputLayout = (TextInputLayout) viewInflated.findViewById(R.id.inputLayout);
        final TextInputEditText input = (TextInputEditText) inputLayout.getEditText();
        inputLayout.setHint(getString(
                R.string.ProfileSettingsFragmentUsernameText));
        builder.setTitle(R.string.ProfileSettingsFragmentUsernameText);
        builder.setView(viewInflated);

        if (cachedUser.getUsername() != null) {
            input.setText(cachedUser.getUsername());
            input.setSelection(input.getText().length());
        }

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (input.getText().toString().trim().equals(textViewUsername.getText()))
                    return;

                if (Utility.isValidUsername(input.getText().toString().trim())) {
                    request.setChange("username");
                    request.setUsername(input.getText().toString().trim());

                    service.getUsersEndpoint().save(request);
                    startLoading();
                } else {
                    Toast.makeText(ProfileSettingsActivity.this,
                            getString(R.string.RegistrationActivityMessageInvalidUsername), Toast.LENGTH_SHORT).show();
                }
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final Button button = builder.show().getButton(DialogInterface.BUTTON_POSITIVE);

        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() > 0) {
                    button.setEnabled(true);
                    button.setAlpha(Constants.ENABLED_STATE_ALPHA);

                    inputLayout.setErrorEnabled(false);
                } else {
                    button.setEnabled(false);
                    button.setAlpha(Constants.DISABLED_STATE_ALPHA);

                    inputLayout.setErrorEnabled(true);
                    inputLayout.setError(getString(R.string.CoreActivityMessageFillEmptyField));
                }
            }
        });
    }

    private void changePassword() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_input_password,
                (ViewGroup) findViewById(android.R.id.content), false);
        final TextInputLayout inputLayout1 = (TextInputLayout) viewInflated.findViewById(R.id.inputLayout1);
        final TextInputLayout inputLayout2 = (TextInputLayout) viewInflated.findViewById(R.id.inputLayout2);
        final TextInputLayout inputLayout3 = (TextInputLayout) viewInflated.findViewById(R.id.inputLayout3);
        final TextInputEditText input1 = (TextInputEditText) inputLayout1.getEditText();
        final TextInputEditText input2 = (TextInputEditText) inputLayout2.getEditText();
        final TextInputEditText input3 = (TextInputEditText) inputLayout3.getEditText();
        final String wrongPassword = getString(R.string.ProfileSettingsFragmentPasswordError2);
        builder.setTitle(R.string.ProfileSettingsFragmentPasswordText);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (cachedUser.getPasswordHash() != null) {
                    if (cachedUser.getPasswordHash().equals(Security.getHash(
                            input1.getText().toString().trim(),
                            Security.MD5
                    ))) {
                        request.setChange("passwordHash");
                        request.setPasswordHash(Security.getHash(
                                input3.getText().toString().trim(),
                                Security.MD5
                        ));

                        service.getUsersEndpoint().save(request);
                        startLoading();
                    } else {
                        Interaction.getInstance(ProfileSettingsActivity.this).showErrorDialog(
                                wrongPassword
                        );
                    }
                } else {
                    // We cannot find passwordHash in cache
                    Interaction.getInstance(ProfileSettingsActivity.this).showErrorDialog(
                            getString(R.string.ProfileSettingsFragmentPasswordError3)
                    ).setPositiveListener(
                            getString(R.string.Ok),
                            new ColorDialog.OnPositiveListener() {
                                @Override
                                public void onClick(ColorDialog colorDialog) {
                                    // TODO
                                    // must redo login
                                    // force user out of app
                                    System.exit(0);
                                }
                            }
                    );
                }
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.show();
        final Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        b.setEnabled(false);
        b.setAlpha(Constants.DISABLED_STATE_ALPHA);
        final String empty = getString(R.string.CoreActivityMessageFillEmptyField);
        final String notSame = getString(R.string.ProfileSettingsFragmentPasswordError1);

        final boolean[] isEmpty = new boolean[3];

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isEmpty[0] = input1.getText().toString().trim().length() == 0;
                isEmpty[1] = input2.getText().toString().trim().length() == 0;
                isEmpty[2] = input3.getText().toString().trim().length() == 0;

                if (isEmpty[0] || isEmpty[1] || isEmpty[2]) {
                    if (isEmpty[0]) {
                        inputLayout1.setError(empty);
                        if (!inputLayout1.isErrorEnabled())
                            inputLayout1.setErrorEnabled(true);
                    } else {
                        if (inputLayout1.isErrorEnabled())
                            inputLayout1.setErrorEnabled(false);
                    }

                    if (isEmpty[1]) {
                        inputLayout2.setError(empty);
                        if (!inputLayout2.isErrorEnabled())
                            inputLayout2.setErrorEnabled(true);
                    } else {
                        if (inputLayout2.isErrorEnabled())
                            inputLayout2.setErrorEnabled(false);
                    }

                    if (isEmpty[2]) {
                        inputLayout3.setError(empty);
                        if (!inputLayout3.isErrorEnabled())
                            inputLayout3.setErrorEnabled(true);
                    } else {
                        if (inputLayout3.isErrorEnabled())
                            inputLayout3.setErrorEnabled(false);
                    }

                    b.setEnabled(false);
                    b.setAlpha(Constants.DISABLED_STATE_ALPHA);
                } else {
                    if (input3.getText().toString().equals(input2.getText().toString())) {
                        b.setEnabled(true);
                        b.setAlpha(Constants.ENABLED_STATE_ALPHA);

                        inputLayout1.setErrorEnabled(false);
                        inputLayout2.setErrorEnabled(false);
                        inputLayout3.setErrorEnabled(false);
                    } else {
                        b.setEnabled(false);
                        b.setAlpha(Constants.DISABLED_STATE_ALPHA);

                        inputLayout3.setError(notSame);
                        if (!inputLayout3.isErrorEnabled())
                            inputLayout3.setErrorEnabled(true);
                    }
                }
            }
        };

        input1.addTextChangedListener(textWatcher);
        input2.addTextChangedListener(textWatcher);
        input3.addTextChangedListener(textWatcher);
    }

    private void changeGender() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_spinner,
                (ViewGroup) findViewById(android.R.id.content), false);
        final Spinner spinner = (Spinner) viewInflated.findViewById(R.id.spinner);

        builder.setTitle(R.string.ProfileSettingsFragmentGenderText);
        builder.setView(viewInflated);

        spinner.setAdapter(new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                stringArrayGender
        ));

        if (cachedUser.getGender() != null) {
            spinner.setSelection(cachedUser.getGender());
        }

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (cachedUser.getGender() != null &&
                        cachedUser.getGender() == spinner.getSelectedItemPosition())
                    return;

                request.setChange("gender");
                request.setGender((byte) spinner.getSelectedItemPosition());

                service.getUsersEndpoint().save(request);
                startLoading();
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void changeBirthDate() {
        Calendar calendar = null;

        if (cachedUser.getBirthDate() != null) {
            try {
                SimpleDateFormat format = new SimpleDateFormat(CoreModel.DATE_FORMAT);
                calendar = Calendar.getInstance();
                calendar.setTime(format.parse(cachedUser.getBirthDate()));
            } catch (Exception exc) {
            }
        }

        if (calendar == null) {
            calendar = Calendar.getInstance();
        }

        DatePickerDialog dialog = new DatePickerDialog(
                this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar cal = Calendar.getInstance();
                        cal.set(year, month, dayOfMonth);
                        Date date = cal.getTime();

                        try {
                            SimpleDateFormat format = new SimpleDateFormat(CoreModel.DATE_FORMAT);
                            String dateStr = format.format(date);

                            if (cachedUser.getBirthDate() != null &&
                                    cachedUser.getBirthDate().equals(dateStr))
                                return;

                            request.setChange("birthDate");
                            request.setBirthDate(dateStr);

                            service.getUsersEndpoint().save(request);
                            startLoading();
                        } catch (Exception exc) {
                        }
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );

        dialog.show();
    }

    private void changeCountry() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_spinner,
                (ViewGroup) findViewById(android.R.id.content), false);
        final Spinner spinner = (Spinner) viewInflated.findViewById(R.id.spinner);

        builder.setTitle(R.string.ProfileSettingsFragmentBirthDateText);
        builder.setView(viewInflated);

        spinner.setAdapter(new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                stringArrayCountry
        ));

        if (cachedUser.getCountry() != null) {
            spinner.setSelection(cachedUser.getCountry());
        }

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (cachedUser.getCountry() != null &&
                        cachedUser.getCountry() == spinner.getSelectedItemPosition())
                    return;

                request.setChange("country");
                request.setCountry(spinner.getSelectedItemPosition());

                service.getUsersEndpoint().save(request);
                startLoading();
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void changeInfo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_input,
                (ViewGroup) findViewById(android.R.id.content), false);
        final TextInputLayout inputLayout = (TextInputLayout) viewInflated.findViewById(R.id.inputLayout);
        final TextInputEditText input = (TextInputEditText) inputLayout.getEditText();
        inputLayout.setHint(getString(
                R.string.ProfileSettingsFragmentInfoText));
        builder.setTitle(R.string.ProfileSettingsFragmentInfoText);
        builder.setView(viewInflated);

        if (cachedUser.getInfo() != null) {
            input.setText(cachedUser.getInfo());
            input.setSelection(input.getText().length());
        }

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (input.getText().toString().trim().equals(textViewInfo.getText()))
                    return;

                request.setChange("info");
                request.setInfo(input.getText().toString().trim());

                service.getUsersEndpoint().save(request);
                startLoading();
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final Button button = builder.show().getButton(DialogInterface.BUTTON_POSITIVE);

        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() > 0) {
                    button.setEnabled(true);
                    button.setAlpha(Constants.ENABLED_STATE_ALPHA);

                    inputLayout.setErrorEnabled(false);
                } else {
                    button.setEnabled(false);
                    button.setAlpha(Constants.DISABLED_STATE_ALPHA);

                    inputLayout.setErrorEnabled(true);
                    inputLayout.setError(getString(R.string.CoreActivityMessageFillEmptyField));
                }
            }
        });
    }

    private void changeProfileDesign(byte option) {
        if (cachedUser.getProfileDesign() != null &&
                cachedUser.getProfileDesign() == option)
            return;

        request.setChange("profileDesign");
        request.setProfileDesign(option);

        service.getUsersEndpoint().save(request);
        startLoading();
    }

    @Subscribe
    public void onAzGramServiceResponse(UserReceivedEvent event) {
        stopLoading();
        UserModel user = event.getModel();

        if (user.getProfilePhoto() != null && user.getProfilePhoto().getSmallestPossible() != null) {
            PhotoModel photo = user.getProfilePhoto();
            cachedUser.setProfilePhoto(photo);
            cache.saveUser(cachedUser);
            notifyDataChanged();
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(SettingChangedEvent event) {
        stopLoading();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            switch (event.getModel().getChange()) {
                case "fullName":
                    cachedUser.setFullName(request.getFullName());
                    cache.saveUser(cachedUser);

                    request.setChange(null);
                    request.setFullName(null);
                    notifyDataChanged();
                    break;
                case "email":
                    cachedUser.setEmail(request.getEmail());
                    cache.saveUser(cachedUser);

                    request.setChange(null);
                    request.setEmail(null);
                    notifyDataChanged();
                    break;
                case "phoneNumber":
                    cachedUser.setPhoneNumber(request.getPhoneNumber());
                    cache.saveUser(cachedUser);

                    request.setChange(null);
                    request.setPhoneNumber(null);
                    notifyDataChanged();
                    break;
                case "username":
                    cachedUser.setUsername(request.getUsername());
                    cache.saveUser(cachedUser);

                    request.setChange(null);
                    request.setUsername(null);
                    notifyDataChanged();
                    break;
                case "passwordHash":
                    Toast.makeText(this, getString(R.string.ProfileSettingsFragmentPasswordSuccess1),
                            Toast.LENGTH_SHORT).show();
                    cachedUser.setPasswordHash(request.getPasswordHash());
                    cache.saveUser(cachedUser);

                    request.setChange(null);
                    request.setPasswordHash(null);
                    break;
                case "gender":
                    cachedUser.setGender(request.getGender());
                    cache.saveUser(cachedUser);

                    request.setChange(null);
                    request.setGender(null);
                    notifyDataChanged();
                    break;
                case "birthDate":
                    cachedUser.setBirthDate(request.getBirthDate());
                    cache.saveUser(cachedUser);

                    request.setChange(null);
                    request.setBirthDate(null);
                    notifyDataChanged();
                    break;
                case "country":
                    cachedUser.setCountry(request.getCountry());
                    cache.saveUser(cachedUser);

                    request.setChange(null);
                    request.setCountry(null);
                    notifyDataChanged();
                    break;
                case "info":
                    cachedUser.setInfo(request.getInfo());
                    cache.saveUser(cachedUser);

                    request.setChange(null);
                    request.setInfo(null);
                    notifyDataChanged();
                    break;
                case "profileDesign":
                    cachedUser.setProfileDesign(request.getProfileDesign());
                    cache.saveUser(cachedUser);

                    request.setChange(null);
                    request.setProfileDesign(null);
                    break;
            }

            return;
        } else if (event.getModel().getMessageId() == NetworkOperations.Users.EXISTING_USERNAME) {
            if (request.getChange().equals("username")) {
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageExistingUsername)
                );
                return;
            }
        } else if (event.getModel().getMessageId() == NetworkOperations.Users.EXISTING_EMAIL) {
            if (request.getChange().equals("email")) {
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageExistingEmail)
                );
                return;
            }
        }

        Interaction.getInstance(this).showErrorDialog(
                getString(R.string.UnknownError)
        );
    }

    @Subscribe
    public void onAzGramServiceResponse(ProfilePhotoChangedEvent event) {
        dialogLoading.hide();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            getUserFromServer();
            Toast.makeText(this, R.string.Success, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }
}