package az.azgram.android.app.events.notifications;

import az.azgram.android.app.models.response.NotificationListModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class FollowRequestListReceivedEvent {
    private NotificationListModel model;

    public FollowRequestListReceivedEvent(NotificationListModel model) {
        this.model = model;
    }

    public NotificationListModel getModel() {
        return model;
    }

    public void setModel(NotificationListModel model) {
        this.model = model;
    }
}
