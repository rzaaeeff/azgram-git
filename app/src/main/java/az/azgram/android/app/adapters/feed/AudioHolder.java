package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.otto.Subscribe;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterComment;
import az.azgram.android.app.custom.CustomLinkableTextView;
import az.azgram.android.app.events.ui.OptionsButtonClickedEvent;
import az.azgram.android.app.events.ui.UserStartedPlayingMediaEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.toro.SimpleVideoObject;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.LinkMechanism;
import az.azgram.android.app.operations.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.exoplayer2.ExoVideoView;
import im.ene.toro.extended.ExtVideoViewHolder;

/**
 * Created by Rzaaeeff on 1/11/2017.
 */

public class AudioHolder extends MediaHolder implements View.OnClickListener {
    // Constants
    public static final int LAYOUT_RES = R.layout.recycler_item_audio_post_list;

    // Widgets
    TextView textViewDuration;
    ToggleButton toggleButtonPlay;
    ProgressBar progressbarDuration;

    // Other elements
    private Handler handler;

    public AudioHolder(View itemView, Context context) {
        super(itemView, context);

        toggleButtonPlay = (ToggleButton) itemView.findViewById(R.id.toggleButtonPlayPostsListPostsListItem);
        progressbarDuration = (ProgressBar) itemView.findViewById(R.id.progressbarDurationPostsListItem);
        textViewDuration = (TextView) itemView.findViewById(R.id.textViewDurationPostsListPostsListItem);

        toggleButtonPlay.setOnClickListener(this);
    }

    @Override
    public void onPlaybackStarted() {
        super.onPlaybackStarted();

        toggleButtonPlay.setChecked(true);
        scheduleUpdatingProgress();
    }

    @Override
    public void onPlaybackPaused() {
        super.onPlaybackPaused();

        toggleButtonPlay.setChecked(false);
        unscheduleUpdatingProgress();
    }

    @Override
    public void onPlaybackCompleted() {
        super.onPlaybackCompleted();

        toggleButtonPlay.setChecked(false);
        unscheduleUpdatingProgress();
    }

    @Override
    public boolean onPlaybackError(Exception error) {
        toggleButtonPlay.setChecked(false);
        unscheduleUpdatingProgress();

        return super.onPlaybackError(error);
    }

    // Updates TextView and ProgressBar duration
    public void updateProgress() {
        long seconds = getCurrentPosition() / 100;

        // for ProgressBar
        // Strange bug here. progress tick doesn't reach the end.
        // So, incrementing by one will help. Finally, it will reach.
        progressbarDuration.setProgress((int) seconds + 1);

        // for TextView
        seconds /= 10;
        seconds %= 3600; // get hours out
        int minutes = (int) (seconds / 60);
        seconds %= 60; // get minutes out
        textViewDuration.setText(String.format(
                "%02d:%02d",
                minutes,
                seconds
        ));
    }

    private void scheduleUpdatingProgress() {
        progressbarDuration.setMax((int) getDuration() / 100);
        handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                updateProgress();

                handler.postDelayed(this, 100);
            }
        };
        handler.post(runnable);

        try {
            BusProvider.getInstance().register(this);
            BusProvider.getInstance().post(new UserStartedPlayingMediaEvent(videoItem.video));
        } catch (Exception exc) {}
    }

    private void unscheduleUpdatingProgress() {
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }

        try {
            BusProvider.getInstance().unregister(this);
        } catch (Exception exc) {}
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        stop();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        toggleButtonPlay.setChecked(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toggleButtonPlayPostsListPostsListItem:
                try {
                    if (isPlaying())
                        pause();
                    else
                        preparePlayer(true);
                } catch (Exception exc) {
                }
                break;
            default:
                super.onClick(v);
                break;
        }
    }

    @Subscribe
    public void onUserStartedPlayingMedia(UserStartedPlayingMediaEvent event) {
        // if user start playing another media
        if (!event.getUrl().equals(videoItem.video)) {
            pause();
        }
    }
}
