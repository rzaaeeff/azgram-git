package az.azgram.android.app.adapters.notification.holder.second;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import az.azgram.android.app.adapters.notification.holder.first.PhotoNotificationHolder;
import az.azgram.android.app.adapters.notification.holder.first.TextNotificationHolder;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.SpanFormatter;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class TwoUsernamePostHolder extends PhotoNotificationHolder {
    public TwoUsernamePostHolder(View itemView, Context context) {
        super(itemView, context);
    }

    public void bind(NotificationModel notification) {
        super.bind(notification, SpanFormatter.format(TEXT_FORMAT,
                UniversalListenerMethods.getHighlightedAndClickableUsername(
                        notification.getUsers().get(0),
                        context
                ),
                UniversalListenerMethods.getHighlightedAndClickableUsername(
                        notification.getUsers().get(1),
                        context
                ))
        );
    }
}
