package az.azgram.android.app.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import az.azgram.android.app.R;

import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.PostEvent;
import az.azgram.android.app.models.AdModel;
import az.azgram.android.app.models.PlaceModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.FileUtils;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DescribeAdActivity extends AppCompatActivity implements View.OnClickListener {

    // Constants
    private final static int REQUEST_PICK_PLACE = 0;

    // Widgets
    private ImageView imageView;
    private EditText editTextDescription, editTextUrl;
    private ViewGroup layoutLocation;
    private Spinner spinnerType, spinnerView;
    private TextView textViewLocation;
    private AppCompatButton buttonUpload;

    // Other Elements
    private Uri uri;
    private AdModel request;
    private ProgressDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_describe_ad);

        uri = getIntent().getParcelableExtra("data");

        init();
        initValues();
        setListeners();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onStop() {
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    public void finishDescribeAdActivity(View view) {
        finish();
    }

    private void init() {
        imageView = (ImageView) findViewById(R.id.imageView_DescribeAdActivity);
        editTextDescription = (EditText) findViewById(R.id.editText_DescribeAdActivity);
        editTextUrl = (EditText) findViewById(R.id.editTextUrl_DescribeAdActivity);
        layoutLocation = (ViewGroup) findViewById(R.id.layoutLocation_DescribeAdActivity);
        textViewLocation = (TextView) findViewById(R.id.textViewLocation_DescribeAdActivity);
        spinnerType = (Spinner) findViewById(R.id.spinnerType_DescribeAdActivity);
        spinnerView = (Spinner) findViewById(R.id.spinnerView_DescribeAdActivity);
        buttonUpload = (AppCompatButton) findViewById(R.id.buttonUpload_DescribeAdActivity);
    }

    private void initValues() {
        imageView.setImageURI(uri);

        request = new AdModel();
        request.setAuthor(new UserModel());
        request.getAuthor().setToken(new Cache(this).loadUser().getToken());
        request.getAuthor().setUsername(new Cache(this).loadUser().getUsername());
        request.setApiKey(Keys.API_KEY);
        request.setType(PostModel.Type.AD);

        dialogLoading = Interaction.getInstance(this).showLoadingDialog(
                getString(R.string.PleaseWait)
        );
        dialogLoading.hide();
    }

    private void setListeners() {
        layoutLocation.setOnClickListener(this);
        buttonUpload.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layoutLocation_DescribeAdActivity:
                Intent intent = new Intent(DescribeAdActivity.this, PlacePickerActivity.class);
                startActivityForResult(intent, REQUEST_PICK_PLACE);
                break;

            case R.id.buttonUpload_DescribeAdActivity:
                request.setText(editTextDescription.getText().toString().trim());
                request.setAdType((byte) (spinnerType.getSelectedItemPosition() + 1)); // because type_id starts from 1 not 0
                request.setViewCount((byte) (spinnerView.getSelectedItemPosition() + 1)); // because view_count starts from 1 not 0
                request.setAdsRedirectUrl(editTextUrl.getText().toString().trim());

                NetworkOperations service = new NetworkOperations(DescribeAdActivity.this, BusProvider.getInstance());
                service.getPostsEndpoint().uploadPhoto(request, uri);

                dialogLoading.show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PICK_PLACE) {
            if (resultCode == RESULT_OK) {
                request.setPlace((PlaceModel) data.getSerializableExtra("data"));
                textViewLocation.setText(request.getPlace().getName());
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(PostEvent event) {
        dialogLoading.hide();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            // TODO you may save photo
            Toast.makeText(this, getString(R.string.Success), Toast.LENGTH_SHORT).show();
            setResult(RESULT_OK);
            finish();
        } else {
            Toast.makeText(this, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }
}