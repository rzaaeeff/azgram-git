package az.azgram.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apradanas.simplelinkabletext.LinkableTextView;
import az.azgram.android.app.R;
import az.azgram.android.app.models.CommentModel;
import az.azgram.android.app.operations.LinkMechanism;
import az.azgram.android.app.operations.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rzaaeeff on 12/12/2016.
 */

public class RecyclerAdapterComment extends RecyclerView.Adapter<RecyclerAdapterComment.CommentViewHolder> {
    private List<CommentModel> comments;
    private Context context;

    public RecyclerAdapterComment(List<CommentModel> comments, Context context) {
        this.comments = comments;
        this.context = context;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_comment, parent, false);

        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        // TODO Add code here :)
        CommentModel comment = comments.get(position);
        holder.textViewAuthor.setText(comment.getAuthor().getUsername() + ": ");
        holder.textViewAuthor.setOnClickListener(
                new LinkMechanism.MentionViewOnClickListener(comment.getAuthor().getUsername())
        );
        holder.textViewComment.setText(comment.getText());
        holder.refreshLayout();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        TextView textViewAuthor;
        LinkableTextView textViewComment;

        public CommentViewHolder(View itemView) {
            super(itemView);

            textViewAuthor = (TextView) itemView.findViewById(R.id.textViewAuthorCommentItem);
            textViewComment = (LinkableTextView) itemView.findViewById(R.id.textViewCommentCommentItem);
        }

        public void refreshLayout() {
            List<View> views = new ArrayList<>();
            views.add(textViewComment);
            LinkMechanism.set(views, context);

            textViewAuthor.measure(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);

            SpannableString spannableComment = (SpannableString) textViewComment.getText();
            String stringComment = comments.get(getAdapterPosition()).getText();

            SpannableString newComment;

            // Setting margin only for the first line
            if (spannableComment.toString().trim().length() > 0) {
                newComment = Utility.createIndentedText(spannableComment, textViewAuthor.getMeasuredWidth(), 0);
            } else {
                newComment = Utility.createIndentedText(stringComment, textViewAuthor.getMeasuredWidth(), 0);
            }

            // set new comment to textView
            textViewComment.setText(newComment, TextView.BufferType.SPANNABLE);
        }
    }
}
