package az.azgram.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import az.azgram.android.app.R;

import az.azgram.android.app.models.PlaceModel;
import az.azgram.android.app.models.response.PlaceListModel;
import az.azgram.android.app.operations.PlaceSelectionController;

/**
 * Created by Rzaaeeff on 2/11/2017.
 */

public class RecyclerAdapterPlace extends RecyclerView.Adapter<RecyclerAdapterPlace.PlaceHolder> {
    private Context context;
    private PlaceListModel data;
    private PlaceSelectionController controller;

    public RecyclerAdapterPlace(Context context, PlaceListModel data, PlaceSelectionController controller) {
        this.context = context;
        this.data = data;
        this.controller = controller;
    }

    @Override
    public PlaceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.recycler_item_place,
                parent,
                false
        );

        return new PlaceHolder(view);
    }

    @Override
    public void onBindViewHolder(PlaceHolder holder, int position) {
        holder.bind(data.getPlaces().get(position));
    }

    @Override
    public int getItemCount() {
        return data.getPlaces().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class PlaceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textView;
        private LinearLayout rootLayout;

        public PlaceHolder(View itemView) {
            super(itemView);

            textView = (TextView) itemView.findViewById(R.id.textView_PlaceItem);
            rootLayout = (LinearLayout) itemView.findViewById(R.id.rootLayout_PlaceItem);

            rootLayout.setOnClickListener(this);
        }

        public void bind(PlaceModel place) {
            textView.setText(place.getName());
        }

        @Override
        public void onClick(View view) {
            controller.onClick(
                    data.getPlaces().get(getAdapterPosition())
            );
        }
    }
}