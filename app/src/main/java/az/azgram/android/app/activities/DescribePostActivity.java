package az.azgram.android.app.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.PostEvent;
import az.azgram.android.app.models.PlaceModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.FileUtils;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;
import com.squareup.otto.Subscribe;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DescribePostActivity extends AppCompatActivity implements View.OnClickListener {

    // Constants
    public static final String EXTRA_CHOICE = UploadControllerActivity.EXTRA_CHOICE;
    public static final int CHOICE_PHOTO = UploadControllerActivity.CHOICE_PHOTO;
    public static final int CHOICE_VIDEO = UploadControllerActivity.CHOICE_VIDEO;
    private final static int REQUEST_PICK_PLACE = 0;

    // Widgets
    private ImageView imageView;
    private EditText editText;
    private ViewGroup layoutLocation, layouPrivacy;
    private TextView textViewLocation;
    private Spinner spinnerPrivacy;
    private AppCompatButton buttonUpload;

    // Other elements
    private Uri uri;
    private PostModel request;
    private ProgressDialog dialogLoading;
    private int choice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_describe_post);

        choice = getIntent().getIntExtra(EXTRA_CHOICE, CHOICE_PHOTO);
        uri = getIntent().getParcelableExtra("data");
        init();
        initValues();
        setListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onStop() {
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    public void finishDescribePostActivity(View view) {
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void init() {
        imageView = (ImageView) findViewById(R.id.imageView_DescribePostActivity);
        editText = (EditText) findViewById(R.id.editText_DescribePostActivity);
        layoutLocation = (ViewGroup) findViewById(R.id.layoutLocation_DescribePostActivity);
        layouPrivacy = (ViewGroup) findViewById(R.id.layoutPrivacy_DescribePostActivity);
        textViewLocation = (TextView) findViewById(R.id.textViewLocation_DescribePostActivity);
        spinnerPrivacy = (Spinner) findViewById(R.id.spinnerPrivacy_DescribePostActivity);
        buttonUpload = (AppCompatButton) findViewById(R.id.buttonUpload_DescribePostActivity);
    }

    private void initValues() {
        if (choice == CHOICE_PHOTO) {
            imageView.setImageURI(uri);
        } else if (choice == CHOICE_VIDEO) {
            imageView.setImageBitmap(ThumbnailUtils.createVideoThumbnail(
                    FileUtils.getRealPathFromURI(this, uri),
                    MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
                    )
            );
        }

        request = new PostModel();
        request.setAuthor(new UserModel());
        request.getAuthor().setToken(new Cache(this).loadUser().getToken());
        request.getAuthor().setUsername(new Cache(this).loadUser().getUsername());
        request.setApiKey(Keys.API_KEY);

        dialogLoading = Interaction.getInstance(this).showLoadingDialog(
                getString(R.string.PleaseWait)
        );
        dialogLoading.hide();
    }

    private void setListeners() {
        layouPrivacy.setOnClickListener(this);
        layoutLocation.setOnClickListener(this);
        buttonUpload.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layoutPrivacy_DescribePostActivity:
                spinnerPrivacy.callOnClick();
                break;

            case R.id.layoutLocation_DescribePostActivity:
                Intent intent = new Intent(DescribePostActivity.this, PlacePickerActivity.class);
                startActivityForResult(intent, REQUEST_PICK_PLACE);
                break;

            case R.id.buttonUpload_DescribePostActivity:
                request.setPrivacy((byte) spinnerPrivacy.getSelectedItemPosition());
                request.setText(editText.getText().toString().trim());

                NetworkOperations service = new NetworkOperations(DescribePostActivity.this, BusProvider.getInstance());
                if (choice == CHOICE_PHOTO) {
                    service.getPostsEndpoint().uploadPhoto(request, uri);
                } else if (choice == CHOICE_VIDEO) {
                    service.getPostsEndpoint().uploadVideo(request, uri);
                }
                dialogLoading.show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PICK_PLACE) {
            if (resultCode == RESULT_OK) {
                request.setPlace((PlaceModel) data.getSerializableExtra("data"));
                textViewLocation.setText(request.getPlace().getName());
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(PostEvent event) {
        dialogLoading.hide();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            // TODO you may save photo
            Toast.makeText(this, getString(R.string.Success), Toast.LENGTH_SHORT).show();
            setResult(RESULT_OK);
            finish();
        } else {
            Toast.makeText(this, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }
}
