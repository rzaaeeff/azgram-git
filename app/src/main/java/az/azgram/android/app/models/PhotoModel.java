package az.azgram.android.app.models;

import java.io.Serializable;

/**
 * Created by Rzaaeeff on 11/30/2016.
 */

public class PhotoModel extends CoreModel implements Serializable {

    // Fields
    private String small;
    private String medium;
    private String large;

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getSmallestPossible() {
        if (small != null)
            return small;

        if (medium != null)
            return medium;

        if (large != null)
            return large;

        return null;
    }

    public String getLargestPossible() {
        if (large != null)
            return large;

        if (medium != null)
            return medium;

        if (small != null)
            return small;

        return null;
    }
}
