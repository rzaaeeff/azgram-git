package az.azgram.android.app.operations;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;

import com.apradanas.simplelinkabletext.Link;
import com.apradanas.simplelinkabletext.LinkableEditText;
import com.apradanas.simplelinkabletext.LinkableTextView;
import az.azgram.android.app.AzGram;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Rzaaeeff on 1/25/2017.
 */

public class LinkMechanism {

    private static final String HASHTAG_PATTERN = "(#\\w+)";
    private static final String MENTION_PATTERN = "(@\\w+)";
//    private static final String URL_PATTERN = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

    public static final HashtagOnClickListener hashtagOnClickListener = new HashtagOnClickListener();
    public static final MentionOnClickListener mentionOnClickListener = new MentionOnClickListener();
//    public static final UrlOnClickListener urlOnClickListener = new UrlOnClickListener();

    private static Context context;

    public LinkMechanism(Context context) {
        this.context = context;
    }

    public static final void set(final List<View> views, final Context paramContext) {

        if (context == null)
            context = paramContext;

        try {

            int color = Color.parseColor("#337ab7");

            // find hashtag
            Link linkHashtag = new Link(Pattern.compile(HASHTAG_PATTERN, Pattern.CASE_INSENSITIVE))
                    .setUnderlined(false)
                    .setTextColor(color)
                    .setClickListener(hashtagOnClickListener);

            // find username
            Link linkUsername = new Link(Pattern.compile(MENTION_PATTERN, Pattern.CASE_INSENSITIVE))
                    .setUnderlined(false)
                    .setTextColor(color)
                    .setClickListener(mentionOnClickListener);

            // find url
//            Link linkUrl = new Link(Pattern.compile(URL_PATTERN, Pattern.CASE_INSENSITIVE))
//                    .setUnderlined(true)
//                    .setTextColor(color)
//                    .setClickListener(urlOnClickListener);

            // other characters
            Link linkRegular = new Link(Pattern.compile(".", Pattern.CASE_INSENSITIVE))
                    .setUnderlined(false);

            List<Link> links = new ArrayList<>();
            links.add(linkHashtag);
            links.add(linkUsername);
//            links.add(linkUrl);

            for (View view : views) {
                if (view instanceof LinkableTextView) {
                    links.add(linkRegular);
                    ((LinkableTextView) view)
                            .addLinks(links)
                            .build();
                } else if (view instanceof LinkableEditText) {
                    ((LinkableEditText) view)
                            .addLinks(links);
                }
            }
        } catch (Exception exc) {
        }
    }

    private static class HashtagOnClickListener implements Link.OnClickListener {
        @Override
        public void onClick(String text) {
            Log.d(AzGram.TAG, "onClick: " + text);
        }
    }

    public static class MentionOnClickListener implements Link.OnClickListener {
        @Override
        public void onClick(String text) {
            UniversalListenerMethods.onClickUser(text, context);
        }
    }

//    private static class UrlOnClickListener implements Link.OnClickListener {
//        @Override
//        public void onClick(String text) {
//        }
//    }

    public static class MentionViewOnClickListener implements View.OnClickListener {
        private String username;

        public MentionViewOnClickListener(String username) {
            this.username = "@" + username;
        }

        @Override
        public void onClick(View view) {
            mentionOnClickListener.onClick(username);
        }
    }
}