package az.azgram.android.app.events.posts;

import az.azgram.android.app.models.response.CommentFeedModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class CommentsReceivedEvent {
    private CommentFeedModel model;

    public CommentsReceivedEvent(CommentFeedModel model) {
        this.model = model;
    }

    public CommentFeedModel getModel() {
        return model;
    }

    public void setModel(CommentFeedModel model) {
        this.model = model;
    }
}
