package az.azgram.android.app.custom;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaDataSource;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Rzaaeeff
 * based on @Link https://developer.android.com/reference/android/media/MediaPlayer.html#StateDiagram
 */

public class RzPlayer extends MediaPlayer implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {
    public enum State {
        IDLE,
        INITIALIZED,
        PREPARED,
        STARTED,
        PAUSED,
        STOPPED,
        PLAYBACK_COMPLETED,
        ERROR,
        ENDED
    }

    private State state = State.IDLE;
    private List<OnCompletionListener> onCompletionListeners = new ArrayList<>();
    private List<OnPreparedListener> onPreparedListeners = new ArrayList<>();
    private List<OnErrorListener> onErrorListeners = new ArrayList<>();

    public RzPlayer() {
        super();

        super.setOnCompletionListener(this);
        super.setOnPreparedListener(this);
        super.setOnErrorListener(this);
    }

    @Deprecated
    @Override
    public void setOnPreparedListener(OnPreparedListener listener) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    public void setOnCompletionListener(OnCompletionListener listener) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    public void setOnErrorListener(OnErrorListener listener) {
        throw new UnsupportedOperationException();
    }

    public State getState() {
        return state;
    }

    @Override
    public void reset() {
        super.reset();

        state = State.IDLE;
    }

    private void changeStateToInitialized() {
        state = State.INITIALIZED;
    }

    @Override
    public void setDataSource(@NonNull Context context, @NonNull Uri uri) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        super.setDataSource(context, uri);

        changeStateToInitialized();
    }

    @Override
    public void setDataSource(@NonNull Context context, @NonNull Uri uri, @Nullable Map<String, String> headers) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        super.setDataSource(context, uri, headers);

        changeStateToInitialized();
    }

    @Override
    public void setDataSource(String path) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        super.setDataSource(path);

        changeStateToInitialized();
    }

    @Override
    public void setDataSource(@NonNull AssetFileDescriptor afd) throws IOException, IllegalArgumentException, IllegalStateException {
        super.setDataSource(afd);

        changeStateToInitialized();
    }

    @Override
    public void setDataSource(FileDescriptor fd) throws IOException, IllegalArgumentException, IllegalStateException {
        super.setDataSource(fd);

        changeStateToInitialized();
    }

    @Override
    public void setDataSource(FileDescriptor fd, long offset, long length) throws IOException, IllegalArgumentException, IllegalStateException {
        super.setDataSource(fd, offset, length);

        changeStateToInitialized();
    }

    @Override
    public void setDataSource(MediaDataSource dataSource) throws IllegalArgumentException, IllegalStateException {
        super.setDataSource(dataSource);

        changeStateToInitialized();
    }

    @Override
    public void start() throws IllegalStateException {
        super.start();

        state = State.STARTED;
    }

    @Override
    public void pause() throws IllegalStateException {
        super.pause();

        state = State.PAUSED;
    }

    @Override
    public void stop() throws IllegalStateException {
        super.stop();

        state = State.STOPPED;
    }

    @Override
    public void release() {
        super.release();

        state = State.ENDED;
    }

    public void addOnCompletionListener(OnCompletionListener listener) {
        onCompletionListeners.add(listener);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        state = State.PLAYBACK_COMPLETED;

        for (OnCompletionListener listener : onCompletionListeners)
            listener.onCompletion(mp);
    }

    public void addOnPreparedListener(OnPreparedListener listener) {
        onPreparedListeners.add(listener);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        state = State.PREPARED;

        for (OnPreparedListener listener : onPreparedListeners)
            listener.onPrepared(mp);
    }

    public void addOnErrorListener(OnErrorListener listener) {
        onErrorListeners.add(listener);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        state = State.ERROR;
        boolean handled = false;

        for (OnErrorListener listener : onErrorListeners) {
            boolean handledByThis = listener.onError(mp, what, extra);

            // if event is handled by at least one listener
            if (!handled)
                handled = handledByThis;
        }

        return handled;
    }
}