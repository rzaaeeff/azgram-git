package az.azgram.android.app.operations;

import az.azgram.android.app.models.ConversationModel;

/**
 * Created by Rzaaeeff on 5/4/2017.
 */

public interface ChatController {
    void onMarkAsUnreadClicked(ConversationModel conversation);
    void onDeleteConversationClicked(ConversationModel conversation);
    void onAllMessagesClicked(ConversationModel conversation);
}
