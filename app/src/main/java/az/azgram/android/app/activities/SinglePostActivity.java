package az.azgram.android.app.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.feed.RecyclerAdapterPostsList;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.PostReceivedEvent;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.FeedModel;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import im.ene.toro.ToroAdapter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SinglePostActivity extends AppCompatActivity {

    // Widgets
    RecyclerView recyclerView;

    // Other elements
    Bus bus;
    FeedModel feed;
    UserModel request;
    ToroAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_post);

        Intent intent = getIntent();
        if (intent.getExtras().containsKey(PostModel.UID)) {
            PostModel post = (PostModel) intent.getSerializableExtra(PostModel.UID);

            Cache cache = new Cache(this);
            UserModel user = cache.loadUser();
            request = new UserModel();
            request.setApiKey(Keys.API_KEY);
            request.setUsername(user.getUsername());
            request.setToken(user.getToken());
            request.setId(post.getId());

            init();
            load();
        } else {
            // We didn't receive anything
            // Let's close activity
            finish();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();

        bus.register(this);
    }

    @Override
    protected void onPause() {
        bus.unregister(this);

        super.onPause();
    }

    private void init() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewSinglePostActivity);

        // RecyclerView
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        feed = new FeedModel();
        feed.setPosts(new ArrayList<PostModel>());
        adapter = new RecyclerAdapterPostsList(this, feed);
        recyclerView.setAdapter(adapter);

        // Bus
        bus = new Bus();
    }

    private void load() {
        NetworkOperations service = new NetworkOperations(this, bus);
        service.getPostsEndpoint().getPost(request);
    }

    /**
     * Referenced from XML
     * OnClick method
     * @param view
     */
    public void finishSinglePostActivity(View view) {
        finish();
    }

    @Subscribe
    public void onAzGramServiceResponse(PostReceivedEvent event) {
        if (event.getModel() != null) {
            feed.getPosts().add(event.getModel());
            adapter.notifyDataSetChanged();
        } else {
            // TODO: You may handle error
        }
    }
}
