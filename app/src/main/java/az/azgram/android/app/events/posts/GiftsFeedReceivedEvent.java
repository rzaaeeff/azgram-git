package az.azgram.android.app.events.posts;

import az.azgram.android.app.models.response.GiftFeedModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class GiftsFeedReceivedEvent {
    private GiftFeedModel model;

    public GiftsFeedReceivedEvent(GiftFeedModel model) {
        this.model = model;
    }

    public GiftFeedModel getModel() {
        return model;
    }

    public void setModel(GiftFeedModel model) {
        this.model = model;
    }
}
