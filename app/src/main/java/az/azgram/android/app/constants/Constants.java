package az.azgram.android.app.constants;
import android.os.Environment;

import java.io.File;

import az.azgram.android.app.BuildConfig;

/**
 * Created by Rzaaeeff on 12/5/2016.
 */

public class Constants {
    public static final String SHOULD_OPEN_INPUT_FIELD = BuildConfig.APPLICATION_ID +
            ".SHOULD_OPEN_INPUT_FIELD";
    public static final String BOTTOM_MENU_TAG = BuildConfig.APPLICATION_ID +
            ".BottomMenu";
    public static final String STATUS_DIALOG_TAG = BuildConfig.APPLICATION_ID +
            ".StatusDialog";

    public static final int SEARCH_DELAY = 500;
    public static final int AUDIO_ENCODING_BIT_RATE = 160 * 1024;

    public static class Directory {
        public static final File DIR_ROOT = new File(
                Environment.getExternalStorageDirectory(), "AzGram");
        public static final File DIR_POSTS = new File(DIR_ROOT, "Posts");
        public static final File DIR_MESSAGES = new File(DIR_ROOT, "Messages");
        public static final File DIR_MESSAGES_AUDIO = new File(DIR_MESSAGES, "Audio");
        public static final File[] ALL_DIRS = new File[] {
                DIR_ROOT, DIR_POSTS, DIR_MESSAGES, DIR_MESSAGES_AUDIO
        };
    }

    public static final int BADGE_REFRESH_INTERVAL = 500;
    public static final float DISABLED_STATE_ALPHA = 0.5f;
    public static final float ENABLED_STATE_ALPHA = 1f;
}
