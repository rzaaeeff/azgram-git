package az.azgram.android.app.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import az.azgram.android.app.R;
import az.azgram.android.app.custom.SquareImageView;

import java.util.List;

/**
 * Created by Rzaaeeff on 3/7/2017.
 */

public class GridAdapterPhotoChooseFragment extends BaseAdapter {
    private Activity context;
    private List<String> listPhoto;

    public GridAdapterPhotoChooseFragment(Activity context, List<String> listPhoto) {
        this.context = context;
        this.listPhoto = listPhoto;
    }

    @Override
    public int getCount() {
        return listPhoto.size();
    }

    @Override
    public Object getItem(int position) {
        return listPhoto.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SquareImageView imageView;

        if (convertView == null) {
            imageView = new SquareImageView(context);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(new GridView.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        } else {
            imageView = (SquareImageView) convertView;
        }

        Glide.with(context)
                .load(listPhoto.get(position))
                .error(R.drawable.post_photo_placeholder)
                .placeholder(R.drawable.post_photo_placeholder)
                .centerCrop()
                .into(imageView);

        return imageView;
    }
}