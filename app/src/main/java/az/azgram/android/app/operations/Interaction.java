package az.azgram.android.app.operations;

import android.app.ProgressDialog;
import android.content.Context;

import az.azgram.android.app.R;

import cn.refactor.lib.colordialog.ColorDialog;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class Interaction {
    private static Context context;
    private static Interaction instance;

    private Interaction(Context context) {
        this.context = context;
    }

    public static Interaction getInstance(Context context1) {
        if (instance == null || !context.equals(context1)) {
            instance = new Interaction(context1);
        }

        return instance;
    }

    public ColorDialog showErrorDialog(String contentText) {
        ColorDialog dialog = new ColorDialog(context);
        dialog.setAnimationEnable(true);
        dialog.setColor(context.getResources().getColor(R.color.DialogErrorColor));
        dialog.setTitle(context.getString(R.string.Error));
        dialog.setContentText(contentText);
        dialog.setPositiveListener(context.getString(R.string.Ok), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {
                dialog.dismiss();
            }
        });

        dialog.show();
        return dialog;
    }

    public void showSuccessDialog(String contentText) {
        ColorDialog dialog = new ColorDialog(context);
        dialog.setAnimationEnable(true);
        dialog.setColor(context.getResources().getColor(R.color.DialogSuccessColor));
        dialog.setTitle(context.getString(R.string.Success));
        dialog.setContentText(contentText);
        dialog.setPositiveListener(context.getString(R.string.Ok), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {
                dialog.dismiss();
            }
        }).show();
    }

    public ProgressDialog showLoadingDialog(String contentText) {
        ProgressDialog dialog = ProgressDialog.show(context,
                context.getString(R.string.Loading), contentText, true);

        // We don't dismiss it
        // We create and pass it to method caller
        // So, whenever process completes there
        // There we can dismiss it

        return dialog;
    }

    public ColorDialog showPromptDialog(String contentText) {
        ColorDialog dialog = new ColorDialog(context);
        dialog.setAnimationEnable(true);
        dialog.setColor(context.getResources().getColor(R.color.AzGramGrayColor));
        dialog.setTitle(context.getString(R.string.Prompt));
        dialog.setContentText(contentText);

        return dialog;
    }
}
