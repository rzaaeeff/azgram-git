package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import az.azgram.android.app.R;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 8/10/2017.
 */

public class AdHolder extends PostHolder {
    // Constant
    public static final int LAYOUT_RES = R.layout.recycler_item_ad_post_list;

    // Widgets
    ImageView imageViewPost;
    TextView textViewAdsButton;

    // Other elements

    public AdHolder(View itemView, Context context) {
        super(itemView, context);

        textViewAdsButton = (TextView) itemView.findViewById(R.id.textViewAdsPostsListItem);
        imageViewPost = (ImageView) itemView.findViewById(R.id.imageViewPostPostsListItem);
    }

    @Override
    public void bind(RecyclerView.Adapter adapter, @Nullable Object object) {
        super.bind(adapter, object);

        Utility.loadWithPicasso(
                context,
                post.getPhoto().getLargestPossible(),
                imageViewPost,
                R.drawable.post_photo_placeholder
        );

        textViewAdsButton.setText(post.getAdsRedirectText());
        final String url = post.getAdsRedirectUrl();
        textViewAdsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(browserIntent);
            }
        });
    }
}
