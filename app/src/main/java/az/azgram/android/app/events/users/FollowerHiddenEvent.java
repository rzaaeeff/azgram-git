package az.azgram.android.app.events.users;

import az.azgram.android.app.models.UserModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class FollowerHiddenEvent {
    private UserModel model;

    public FollowerHiddenEvent(UserModel model) {
        this.model = model;
    }

    public UserModel getModel() {
        return model;
    }

    public void setModel(UserModel model) {
        this.model = model;
    }
}
