package az.azgram.android.app.events.posts;

import az.azgram.android.app.models.CoreModel;

/**
 * Created by Rzaaeeff on 1/26/2017.
 */

public class QuestionAskedEvent {
    private CoreModel model;

    public QuestionAskedEvent(CoreModel model) {
        this.model = model;
    }

    public CoreModel getModel() {
        return model;
    }

    public void setModel(CoreModel model) {
        this.model = model;
    }
}
