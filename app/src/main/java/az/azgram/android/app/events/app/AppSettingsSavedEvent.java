package az.azgram.android.app.events.app;

import az.azgram.android.app.models.AppSettingsModel;

/**
 * Created by Rzaaeeff on 7/19/2017.
 */

public class AppSettingsSavedEvent {
    private AppSettingsModel model;

    public AppSettingsSavedEvent(AppSettingsModel model) {
        this.model = model;
    }

    public AppSettingsModel getModel() {
        return model;
    }

    public void setModel(AppSettingsModel model) {
        this.model = model;
    }
}
