package az.azgram.android.app.events.notifications;

import az.azgram.android.app.models.NotificationModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class NotificationReceivedEvent {
    private NotificationModel model;

    public NotificationReceivedEvent(NotificationModel model) {
        this.model = model;
    }

    public NotificationModel getModel() {
        return model;
    }

    public void setModel(NotificationModel model) {
        this.model = model;
    }
}
