package az.azgram.android.app.adapters;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import com.apradanas.simplelinkabletext.LinkableTextView;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import az.azgram.android.app.R;
import az.azgram.android.app.activities.CommentsActivity;
import az.azgram.android.app.models.CommentModel;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.response.CommentFeedModel;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.LinkMechanism;
import az.azgram.android.app.operations.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Rzaaeeff on 1/17/2017.
 */

public class RecyclerAdapterExtendedComment extends RecyclerView.Adapter<RecyclerAdapterExtendedComment.ExtendedCommentHolder> {

    private Context context;
    private CommentFeedModel feed;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private String mainUsername;

    public RecyclerAdapterExtendedComment(Context context, CommentFeedModel feed) {
        super();

        this.context = context;
        this.feed = feed;

        mainUsername = new Cache(context).loadUser().getUsername();
    }

    @Override
    public ExtendedCommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.recycler_item_extended_comment,
                parent,
                false
        );

        return new ExtendedCommentHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final ExtendedCommentHolder holder, final int position) {
        final CommentModel comment = feed.getComments().get(position);
        final String idForSwipeRevealLayout = comment.getDate() + "@" + position;
        viewBinderHelper.bind(holder.swipeRevealLayout, idForSwipeRevealLayout);

        if (comment.getAuthor().getProfilePhoto() != null) {
            Utility.loadWithPicasso(
                    context,
                    comment.getAuthor().getProfilePhoto().getSmallestPossible(),
                    holder.imageViewProfilePhoto,
                    R.drawable.profile_photo_placeholder
            );
        }

        if (comment.getPhoto() != null) {
            if (comment.getPhoto().getLargestPossible() != null) {
                holder.setImage(comment.getPhoto().getLargestPossible());
            }
        }

        if (comment.getAuthor().isOnline() != null) {
            holder.imageViewUserBadge.setImageResource(
                    comment.getAuthor().isOnline() ?
                            R.drawable.user_online_badge :
                            R.drawable.user_offline_badge
            );
        }

        holder.textViewAuthor.setText(comment.getAuthor().getUsername());


        LinkMechanism.MentionViewOnClickListener listener =
                new LinkMechanism.MentionViewOnClickListener(comment.getAuthor().getUsername());
        holder.imageViewProfilePhoto.setOnClickListener(listener);
        holder.textViewAuthor.setOnClickListener(listener);

        // Time ago and comment
        String agoStr = holder.getTimeAgo(comment.getDate());
        holder.setCommentAndTime(comment.getText(), agoStr);

        holder.textViewReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewBinderHelper.closeLayout(idForSwipeRevealLayout);
                ((CommentsActivity) context).reply(comment.getAuthor().getUsername());
            }
        });

        if (comment.getAuthor().getUsername().equals(mainUsername)) {
            holder.textViewDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewBinderHelper.closeLayout(idForSwipeRevealLayout);
                    ((CommentsActivity) context).delete(comment);
                }
            });
        } else {
            holder.textViewDelete.setVisibility(View.GONE);
            holder.space.setVisibility(View.GONE);
        }

        holder.swipeRevealLayout.requestLayout();
        holder.swipeLayout.setMinimumHeight(holder.commenLayout.getHeight());
    }

    @Override
    public int getItemCount() {
        return feed.getComments().size();
    }

    class ExtendedCommentHolder extends RecyclerView.ViewHolder {
        // Widgets
        SwipeRevealLayout swipeRevealLayout;
        LinearLayout swipeLayout, commenLayout;
        ImageView imageViewProfilePhoto, imageViewUserBadge, imageView;
        TextView textViewAuthor, textViewReply, textViewDelete;
        LinkableTextView textViewCommentAndTime;
        Space space;

        public ExtendedCommentHolder(View itemView) {
            super(itemView);

            swipeLayout = (LinearLayout) itemView.findViewById(R.id.swipeLayout);
            commenLayout = (LinearLayout) itemView.findViewById(R.id.commentLayout);
            space = (Space) itemView.findViewById(R.id.space);
            swipeRevealLayout = (SwipeRevealLayout) itemView.findViewById(R.id.swipeRevealLayoutExtendedCommentItem);
            imageViewProfilePhoto = (ImageView) itemView.findViewById(R.id.imageViewProfilePhotoExtendedCommentItem);
            imageViewUserBadge = (ImageView) itemView.findViewById(R.id.imageViewUserBadgeExtendedCommentItem);
            imageView = (ImageView) itemView.findViewById(R.id.imageViewExtendedCommentItem);
            textViewAuthor = (TextView) itemView.findViewById(R.id.textViewAuthorExtendedCommentItem);
            textViewReply = (TextView) itemView.findViewById(R.id.textViewReplyExtendedCommentItem);
            textViewDelete = (TextView) itemView.findViewById(R.id.textViewDeleteExtendedCommentItem);
            textViewCommentAndTime = (LinkableTextView) itemView.findViewById(R.id.textViewCommentExtendedCommentAndTimeItem);
        }

        public void setImage(String url) {
            imageView.setVisibility(View.VISIBLE);
            Utility.loadWithPicasso(context, url, imageView);
        }

        @SuppressWarnings("deprecation")
        public void setCommentAndTime(String comment, String timeAgo) {
            textViewCommentAndTime.setText(comment);

            List<View> views = new ArrayList<>();
            views.add(textViewCommentAndTime);
            LinkMechanism.set(views, context);

            String coloredText = "<font color=#808080>&nbsp;&nbsp;"
                    + timeAgo + "</font>";

            SpannableString result;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                result = new SpannableString(
                        Html.fromHtml(coloredText, Html.FROM_HTML_MODE_LEGACY)
                );
            } else {
                result = new SpannableString(
                        Html.fromHtml(coloredText)
                );
            }

            textViewCommentAndTime.append(result);
        }

        public String getTimeAgo(String dateStr) {
            try {
                SimpleDateFormat df = new SimpleDateFormat(CoreModel.DATE_TIME_FORMAT);
                Date date = df.parse(dateStr.trim());

                String agoStr = DateUtils.getTimeAgo(date, context);
                return agoStr;
            } catch (Exception exc) {
                // We couldn't parse it
                return dateStr;
            }
        }

    }
}
