package az.azgram.android.app.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;

import com.adobe.creativesdk.aviary.AdobeImageIntent;
import com.alexvasilkov.gestures.Settings;
import com.alexvasilkov.gestures.views.GestureImageView;
import com.bumptech.glide.Glide;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.GridAdapterPhotoChooseFragment;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.FileUtils;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * This Helper Activity lets user pick photo.
 * Returns URL of selected photo as a result.
 */
public class PhotoPickerActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_IMAGE_CAPTURE = 3756;
    private static final int REQUEST_IMAGE_EDIT = 3758;

    // Widgets
    private Button buttonNext, buttonEdit;
    private ImageButton imageButton;
    private GestureImageView imageView;
    private GridView gridView;

    // Other elements
    private List<String> listPhoto;
    private GridAdapterPhotoChooseFragment adapter;
    private Uri uriTemporaryImage;
    private int indexOfSelectedPhoto;
    private boolean editedPhotoLoaded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_upload);

        init();
        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();

        initValues();
        if (!editedPhotoLoaded)
            loadPhoto(indexOfSelectedPhoto);
    }

    @Override
    protected void onDestroy() {
        deleteTemporaryImage();

        super.onDestroy();
    }

    private void init() {
        buttonNext = (Button) findViewById(R.id.buttonNext_PhotoUploadActivity);
        buttonEdit = (Button) findViewById(R.id.buttonEdit_PhotoUploadActivity);
        imageButton = (ImageButton) findViewById(R.id.imageButton_PhotoUploadActivity);

        imageView = (GestureImageView) findViewById(R.id.imageView_PhotoUploadActivity);
        gridView = (GridView) findViewById(R.id.gridView_PhotoUploadActivity);

        imageButton.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
    }

    private void initValues() {
        listPhoto = FileUtils.getAllShownImagesPath(this);
        Collections.reverse(listPhoto);
        adapter = new GridAdapterPhotoChooseFragment(this, listPhoto);
        gridView.setAdapter(adapter);

        imageView.getController().getSettings()
                .setMaxZoom(4f)
                .setPanEnabled(true)
                .setZoomEnabled(true)
                .setDoubleTapEnabled(true)
                .setOverscrollDistance(0f, 0f)
                .setOverzoomFactor(2f)
                .setFillViewport(true)
                .setFitMethod(Settings.Fit.OUTSIDE)
                .setGravity(Gravity.CENTER);
    }

    private void setListeners() {
        imageButton.setOnClickListener(this);
        buttonNext.setOnClickListener(this);
        buttonEdit.setOnClickListener(this);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editedPhotoLoaded = false;
                loadPhoto(position);
            }
        });
    }

    private void loadPhoto(int position) {
        if (listPhoto == null || position >= listPhoto.size()
                || listPhoto.get(position) == null)
            return;

        Glide.with(this)
                .load(listPhoto.get(position))
                .into(imageView);

        gridView.smoothScrollToPosition(position);

        indexOfSelectedPhoto = position;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButton_PhotoUploadActivity: {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                }
                break;
            }
            case R.id.buttonNext_PhotoUploadActivity: {
                returnResult();
                break;
            }
            case R.id.buttonEdit_PhotoUploadActivity:
                deleteTemporaryImage();

                imageView.setDrawingCacheEnabled(true);
                Bitmap bitmap = imageView.getDrawingCache();

                uriTemporaryImage = Uri.parse(saveTemporaryImage(bitmap));

                Bundle options = new Bundle();
                options.putBoolean(AdobeImageIntent.EXTRA_RETURN_DATA, true);
                options.putBoolean(AdobeImageIntent.EXTRA_OUT_BITMAP_CHANGED, true);

                Intent intent = new AdobeImageIntent.Builder(this)
                        .setData(uriTemporaryImage)
                        .disableUserLogin()
                        .withOptions(options)
                        .saveWithNoChanges(false)
                        .withNoExitConfirmation(false)
                        .withAccentColor(Color.parseColor("#337ab7"))
                        .withOutputFormat(Bitmap.CompressFormat.JPEG)
                        .build();

                startActivityForResult(intent, REQUEST_IMAGE_EDIT);
                overridePendingTransition(R.anim.open_from_right_300, R.anim.close_to_left_300);
                imageView.setDrawingCacheEnabled(false);
                break;
        }
    }

    private void returnResult() {
        imageView.setDrawingCacheEnabled(true);
        Bitmap bitmap = imageView.getDrawingCache();
        Intent intent = new Intent();
        intent.putExtra("data", saveTemporaryImage(bitmap));
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                Bitmap bitmap = (Bitmap) extras.get("data");
                if (bitmap != null)
                    imageView.setImageBitmap(bitmap);
            }
        } else if (requestCode == REQUEST_IMAGE_EDIT) {
            deleteTemporaryImage();
            if (resultCode == RESULT_OK) {
                final Uri uriEditedImage = data.getParcelableExtra(AdobeImageIntent.EXTRA_OUTPUT_URI);

                Picasso.with(PhotoPickerActivity.this)
                        .load(uriEditedImage)
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                if (uriEditedImage != null) {
                                    getContentResolver().delete(uriEditedImage, null, null);
                                    initValues();
                                }

                                editedPhotoLoaded = true;
                            }

                            @Override
                            public void onError() {
                                if (uriEditedImage != null) {
                                    getContentResolver().delete(uriEditedImage, null, null);
                                    initValues();
                                    loadPhoto(indexOfSelectedPhoto);
                                }
                            }
                        });
            }
        }
    }

    /**
     * Store image and return URL for it.
     *
     * @param bitmap image to be stored.
     * @return The URL to the newly inserted image, or <code>null</code> if the
     * image failed to be stored for any reason.
     */
    private String saveTemporaryImage(Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String substring;
        try {
            substring = listPhoto.get(indexOfSelectedPhoto).
                    substring(listPhoto.get(indexOfSelectedPhoto).lastIndexOf('/') + 1);
        } catch (Exception exc) {
            substring = indexOfSelectedPhoto + DateUtils.getFormattedCurrentDate(CoreModel.DATE_TIME_FORMAT);
        }
        return MediaStore.Images.Media.insertImage(getContentResolver(), bitmap,
                "AzGram-" + substring, null);
    }

    /**
     * Delete temporarily stored image.
     */
    private void deleteTemporaryImage() {
        if (uriTemporaryImage != null) {
            getContentResolver().delete(uriTemporaryImage, null, null);
            uriTemporaryImage = null;
        }
    }
}