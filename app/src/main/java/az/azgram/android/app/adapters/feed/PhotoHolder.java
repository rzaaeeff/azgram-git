package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import az.azgram.android.app.R;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 8/10/2017.
 */

public class PhotoHolder extends ExtendedPostHolder {
    // Constants
    public static final int LAYOUT_RES = R.layout.recycler_item_post_post_list;

    // Widgets
    ImageView imageViewPost;

    // Other elements

    public PhotoHolder(View itemView, Context context) {
        super(itemView, context);

        imageViewPost = (ImageView) itemView.findViewById(R.id.imageViewPostPostsListItem);
    }

    @Override
    public void bind(RecyclerView.Adapter adapter, @Nullable Object object) {
        super.bind(adapter, object);

        Utility.loadWithPicasso(
                context,
                post.getPhoto().getLargestPossible(),
                imageViewPost,
                R.drawable.post_photo_placeholder
        );
    }
}
