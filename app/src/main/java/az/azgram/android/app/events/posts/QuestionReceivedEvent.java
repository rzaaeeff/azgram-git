package az.azgram.android.app.events.posts;

import az.azgram.android.app.models.response.QuestionFeedModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class QuestionReceivedEvent {
    private QuestionFeedModel model;

    public QuestionReceivedEvent(QuestionFeedModel model) {
        this.model = model;
    }

    public QuestionFeedModel getModel() {
        return model;
    }

    public void setModel(QuestionFeedModel model) {
        this.model = model;
    }
}
