package az.azgram.android.app.events.balance;

import az.azgram.android.app.models.UserModel;

/**
 * Created by Rzaaeeff on 7/19/2017.
 */

public class BalanceReceivedEvent {
    private UserModel model;

    public BalanceReceivedEvent(UserModel model) {
        this.model = model;
    }

    public UserModel getModel() {
        return model;
    }

    public void setModel(UserModel model) {
        this.model = model;
    }
}
