package az.azgram.android.app.adapters.notification.holder.third;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.holder.first.TextNotificationHolder;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class AnonymousQuestionHolder extends TextNotificationHolder {
    public AnonymousQuestionHolder(View itemView, final Context context) {
        super(itemView, context);

        TEXT_FORMAT = context.getString(R.string.notification_anonymous_question);

        setRootListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UniversalListenerMethods.onClickUser(
                        new Cache(context).loadUser().getUsername(),
                        context
                );
            }
        });
    }

    @Override
    public void bind(NotificationModel notification) {
        super.bind(notification, TEXT_FORMAT);
    }
}
