package az.azgram.android.app.adapters.notification.holder.third;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.holder.first.PhotoNotificationBundledHolder;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class MentionedYouBundledHolder extends PhotoNotificationBundledHolder {
    public MentionedYouBundledHolder(View itemView, Context context) {
        super(itemView, context);

        TEXT_FORMAT = context.getString(R.string.notification_mentioned_you_bundle);
    }

    public void bind(NotificationModel notification) {
        super.bind(notification, TextUtils.concat(
                UniversalListenerMethods.getHighlightedAndClickableUsername(
                        notification.getUsers().get(0), context
                ), " ", TEXT_FORMAT
        ));
    }
}
