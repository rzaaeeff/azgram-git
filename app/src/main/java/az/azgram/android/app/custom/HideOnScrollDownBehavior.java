package az.azgram.android.app.custom;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import az.azgram.android.app.R;

/**
 * Custom behavior made by Rzaaeeff.
 * It hides the view when "dependency" view collapses.
 *
 * @param <V>
 */
public class HideOnScrollDownBehavior<V extends View> extends CoordinatorLayout.Behavior<V> implements AppBarLayout.OnOffsetChangedListener {
    private final static String TAG = "RzLog";

    private boolean isListenerSet = false;
    private boolean isCollapsed = false;

    // Our main view which we are going to hide/show
    private View view;

    // Our Animations
    Animation fadeIn = null;
    Animation fadeOut = null;

    public HideOnScrollDownBehavior() {
    }

    public HideOnScrollDownBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);

        fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in_animation);
        fadeOut = AnimationUtils.loadAnimation(context, R.anim.fade_out_animation);
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, V child, View dependency) {
        if (!isListenerSet) {
            if (dependency instanceof AppBarLayout) {
                ((AppBarLayout) dependency).addOnOffsetChangedListener(this);
                isListenerSet = true;
                view = child;

                if (fadeIn == null)
                    fadeIn = AnimationUtils.loadAnimation(parent.getContext(), R.anim.fade_in_animation);

                if (fadeOut == null)
                    fadeOut = AnimationUtils.loadAnimation(parent.getContext(), R.anim.fade_out_animation);
            }
        }
        return super.onDependentViewChanged(parent, child, dependency);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset == 0) {
            // Toolbar shown
            if (isCollapsed) {
                isCollapsed = false;
                makeViewFadeInAndShow();
            }
        } else {
            // Toolbar collapsed
            if (!isCollapsed) {
                isCollapsed = true;
                makeViewFadeOutAndHide();
            }
        }
    }

    private void makeViewFadeInAndShow() {
        if (view != null) {
            if (view.getVisibility() != View.VISIBLE) {
                view.startAnimation(fadeIn);
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    private void makeViewFadeOutAndHide() {
        if (view != null) {
            if (view.getVisibility() == View.VISIBLE) {
                view.startAnimation(fadeOut);
                view.setVisibility(View.INVISIBLE);
            }
        }
    }
}
