package az.azgram.android.app.custom;

import android.content.Context;
import android.util.AttributeSet;

import com.apradanas.simplelinkabletext.LinkableTextView;

/**
 * Created by Rzaaeeff on 1/26/2017.
 */

public class CustomLinkableTextView extends LinkableTextView {
    public CustomLinkableTextView(Context context) {
        super(context);
    }

    public CustomLinkableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomLinkableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public LinkableTextView setText(String text) {
        return super.setText(text).build();
    }

    @Override
    public LinkableTextView build() {
        return super.build();
    }
}
