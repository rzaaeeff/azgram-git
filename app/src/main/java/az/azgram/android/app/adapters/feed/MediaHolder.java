package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.CallSuper;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.models.toro.SimpleVideoObject;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer2.ExoVideoView;
import im.ene.toro.extended.ExtPlayerViewHelper;
import im.ene.toro.extended.ExtToroPlayer;

/**
 * Created by Rzaaeeff on 8/10/2017.
 */

public class MediaHolder extends ExtendedPostHolder implements ExtToroPlayer, View.OnClickListener {
    // Constants

    // Widgets
    ExoVideoView videoView;

    // Other elements
    private ExtPlayerViewHelper helper;
    private boolean mPlayable = true; // normally true
    SimpleVideoObject videoItem;

    public MediaHolder(View itemView, Context context) {
        super(itemView, context);

        videoView = (ExoVideoView) itemView.findViewById(R.id.videoViewPostPostsListItem);
        helper = new ExtPlayerViewHelper(this, itemView);
        videoView.setPlayerCallback(helper);
    }

    @Override
    public void bind(RecyclerView.Adapter adapter, @Nullable Object object) {
        super.bind(adapter, object);

        videoItem = new SimpleVideoObject(post.getPhoto().getLargestPossible());
        videoView.setMedia(Uri.parse(videoItem.video));
    }

    protected ExoVideoView findVideoView(View itemView) {
        return videoView;
    }

    @Override
    public void setOnItemLongClickListener(final View.OnLongClickListener listener) {
        super.setOnItemLongClickListener(listener);
        videoView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return listener.onLongClick(v) && helper.onLongClick(v);
            }
        });
    }

    @Override
    public void preparePlayer(boolean playWhenReady) {
        videoView.preparePlayer(playWhenReady);
    }

    @Override
    public void releasePlayer() {
        videoView.releasePlayer();
    }

    // Client could override this method for better practice
    @Override
    public void start() {
        videoView.start();
    }

    @Override
    public void pause() {
        videoView.pause();
    }

    @Override
    public long getDuration() {
        return videoView.getDuration();
    }

    @Override
    public long getCurrentPosition() {
        return videoView.getCurrentPosition();
    }

    @Override
    public void seekTo(long pos) {
        videoView.seekTo(pos);
    }

    @Override
    public boolean isPlaying() {
        return videoView.isPlaying();
    }

    @Override
    public boolean wantsToPlay() {
        // Default implementation
        return visibleAreaOffset() >= 0.75 && mPlayable;
    }

    @CallSuper
    @Override
    public void onVideoPrepared() {
        mPlayable = true;
    }

    @CallSuper
    @Override
    public void onActivityActive() {

    }

    @CallSuper
    @Override
    public void onActivityInactive() {

    }

    @CallSuper
    @Override
    public void onAttachedToWindow() {
        helper.onAttachedToWindow();
    }

    @CallSuper
    @Override
    public void onDetachedFromWindow() {
        helper.onDetachedFromWindow();
    }

    @Override
    public int getPlayOrder() {
        return getAdapterPosition();
    }

    @Override
    public void onVideoPreparing() {

    }

    @Override
    public void onPlaybackStarted() {}

    @Override
    public void onPlaybackPaused() {
    }

    @Override
    public void onPlaybackCompleted() {
        mPlayable = false;
        this.videoView.stop();
    }

    @Override
    public boolean isPrepared() {
        return mPlayable;
    }

    @Override
    public float visibleAreaOffset() {
        return ToroUtil.visibleAreaOffset(this, itemView.getParent());
    }

    @Nullable
    @Override
    public String getMediaId() {
        return this.videoItem != null ? this.videoItem.video + "@" + getAdapterPosition() : null;
    }

    @Override
    public int getBufferPercentage() {
        return videoView.getBufferPercentage();
    }

    @Override
    public boolean onPlaybackError(Exception error) {
        mPlayable = false;
        return true;
    }

    @Override
    public void stop() {
        videoView.stop();
    }

    @NonNull
    @Override
    public View getPlayerView() {
        return videoView;
    }

    @Override
    public void setVolume(@FloatRange(from = 0.f, to = 1.f) float volume) {
        this.videoView.setVolume(volume);
    }

    // ExtToroPlayer
    @Override
    public Target getNextTarget() {
        return Target.NEXT_PLAYER;
    }
}
