package az.azgram.android.app.adapters.notification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.holder.first.RequestNotificationHolder;
import az.azgram.android.app.adapters.notification.holder.first.TextNotificationHolder;
import az.azgram.android.app.adapters.notification.holder.third.AcceptedRequestHolder;
import az.azgram.android.app.adapters.notification.holder.third.AnonymousQuestionHolder;
import az.azgram.android.app.adapters.notification.holder.third.AnsweredQuestionHolder;
import az.azgram.android.app.adapters.notification.holder.third.AskedQuestionHolder;
import az.azgram.android.app.adapters.notification.holder.third.CommentOnSubscribedPostHolder;
import az.azgram.android.app.adapters.notification.holder.third.CommentedOnPostBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.CommentedOnPostHolder;
import az.azgram.android.app.adapters.notification.holder.third.DislikedPostBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.DislikedPostHolder;
import az.azgram.android.app.adapters.notification.holder.third.LikedPostBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.LikedPostHolder;
import az.azgram.android.app.adapters.notification.holder.third.MentionedYouBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.MentionedYouHolder;
import az.azgram.android.app.adapters.notification.holder.third.NewPostBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.NewPostHolder;
import az.azgram.android.app.adapters.notification.holder.third.RepliedCommentBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.RepliedCommentHolder;
import az.azgram.android.app.adapters.notification.holder.third.SentGiftHolder;
import az.azgram.android.app.adapters.notification.holder.third.SentMoneyHolder;
import az.azgram.android.app.adapters.notification.holder.third.SharedPostBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.SharedPostHolder;
import az.azgram.android.app.adapters.notification.holder.third.StartedFollowingHolder;
import az.azgram.android.app.adapters.notification.holder.third.SubscribedYouHolder;
import az.azgram.android.app.adapters.notification.holder.third.SystemMessageHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserAskedQuestionHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserCommentedBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserCommentedHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserDislikedBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserDislikedHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserLikedBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserLikedHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserRepliedBundledHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserRepliedHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserSentGiftHolder;
import az.azgram.android.app.adapters.notification.holder.third.UserStartedFollowingHolder;
import az.azgram.android.app.adapters.notification.holder.third.WantsMoneyHolder;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.users.FollowEvent;
import az.azgram.android.app.events.users.FollowRequestEvent;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.NotificationListModel;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;

/**
 * Created by Rzaaeeff on 5/20/2017.
 */

public class RecyclerAdapterNotification extends RecyclerView.Adapter<TextNotificationHolder> {
    private Context context;
    private NotificationListModel data;
    public static NetworkOperations service;
    public static UserModel request;

    private static final int FLAG_BUNDLED = 64;

    public RecyclerAdapterNotification(Context context, NotificationListModel data) {
        this.context = context;
        this.data = data;

        if (service == null) {
            service = new NetworkOperations(context, BusProvider.getInstance());
            service.getUsersEndpoint();
        }

        if (request == null) {
            request = new UserModel();
            request.setApiKey(Keys.API_KEY);
            request.setToken(new Cache(context).loadUser().getToken());
        }

        BusProvider.getInstance().register(this);
    }

    @Override
    public TextNotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case NotificationModel.Type.COMMENTED_ON_POST:
                view = LayoutInflater.from(context).inflate(
                        CommentedOnPostHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new CommentedOnPostHolder(view, context);
            case NotificationModel.Type.COMMENTED_ON_POST | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        CommentedOnPostBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new CommentedOnPostBundledHolder(view, context);
            case NotificationModel.Type.LIKED_POST:
                view = LayoutInflater.from(context).inflate(
                        LikedPostHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new LikedPostHolder(view, context);
            case NotificationModel.Type.LIKED_POST | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        LikedPostBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new LikedPostBundledHolder(view, context);
            case NotificationModel.Type.DISLIKED_POST:
                view = LayoutInflater.from(context).inflate(
                        DislikedPostHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new DislikedPostHolder(view, context);
            case NotificationModel.Type.DISLIKED_POST | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        DislikedPostBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new DislikedPostBundledHolder(view, context);
            case NotificationModel.Type.SHARED_POST:
                view = LayoutInflater.from(context).inflate(
                        SharedPostHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new SharedPostHolder(view, context);
            case NotificationModel.Type.SHARED_POST | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        SharedPostBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new SharedPostBundledHolder(view, context);
            case NotificationModel.Type.REPLIED_COMMENT:
                view = LayoutInflater.from(context).inflate(
                        RepliedCommentHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new RepliedCommentHolder(view, context);
            case NotificationModel.Type.REPLIED_COMMENT | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        RepliedCommentBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new RepliedCommentBundledHolder(view, context);
            case NotificationModel.Type.MENTIONED_YOU:
                view = LayoutInflater.from(context).inflate(
                        MentionedYouHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new MentionedYouHolder(view, context);
            case NotificationModel.Type.MENTIONED_YOU | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        MentionedYouBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new MentionedYouBundledHolder(view, context);
            case NotificationModel.Type.NEW_POST:
                view = LayoutInflater.from(context).inflate(
                        NewPostHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new NewPostHolder(view, context);
            case NotificationModel.Type.NEW_POST | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        NewPostBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new NewPostBundledHolder(view, context);
            case NotificationModel.Type.COMMENT_ON_SUBSCRIBED_POST:
                view = LayoutInflater.from(context).inflate(
                        CommentOnSubscribedPostHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new CommentOnSubscribedPostHolder(view, context);
            case NotificationModel.Type.STARTED_FOLLOWING:
                view = LayoutInflater.from(context).inflate(
                        StartedFollowingHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new StartedFollowingHolder(view, context);
            case NotificationModel.Type.ACCEPTED_REQUEST:
                view = LayoutInflater.from(context).inflate(
                        AcceptedRequestHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new AcceptedRequestHolder(view, context);
            case NotificationModel.Type.SENT_REQUEST:
                view = LayoutInflater.from(context).inflate(
                        RequestNotificationHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new RequestNotificationHolder(view, context);
            case NotificationModel.Type.ASKED_QUESTION:
                view = LayoutInflater.from(context).inflate(
                        AskedQuestionHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new AskedQuestionHolder(view, context);
            case NotificationModel.Type.ANSWERED_QUESTION:
                view = LayoutInflater.from(context).inflate(
                        AnsweredQuestionHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new AnsweredQuestionHolder(view, context);
            case NotificationModel.Type.ANONYMOUS_QUESTION:
                view = LayoutInflater.from(context).inflate(
                        AnonymousQuestionHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new AnonymousQuestionHolder(view, context);
            case NotificationModel.Type.SUBSCRIBED_YOU:
                view = LayoutInflater.from(context).inflate(
                        SubscribedYouHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new SubscribedYouHolder(view, context);
            case NotificationModel.Type.SENT_MONEY:
                view = LayoutInflater.from(context).inflate(
                        SentMoneyHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new SentMoneyHolder(view, context);
            case NotificationModel.Type.WANTS_MONEY:
                view = LayoutInflater.from(context).inflate(
                        WantsMoneyHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new WantsMoneyHolder(view, context);
            case NotificationModel.Type.SENT_GIFT:
                view = LayoutInflater.from(context).inflate(
                        SentGiftHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new SentGiftHolder(view, context);
            case NotificationModel.Type.SYSTEM_MESSAGE:
            case NotificationModel.Type.WELCOME_MESSAGE:
                view = LayoutInflater.from(context).inflate(
                        SystemMessageHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new SystemMessageHolder(view, context);
            case NotificationModel.Type.USER_LIKED:
                view = LayoutInflater.from(context).inflate(
                        UserLikedHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserLikedHolder(view, context);
            case NotificationModel.Type.USER_LIKED | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        UserLikedBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserLikedBundledHolder(view, context);
            case NotificationModel.Type.USER_DISLIKED:
                view = LayoutInflater.from(context).inflate(
                        UserDislikedHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserDislikedHolder(view, context);
            case NotificationModel.Type.USER_DISLIKED | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        UserDislikedBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserDislikedBundledHolder(view, context);
            case NotificationModel.Type.USER_COMMENTED:
                view = LayoutInflater.from(context).inflate(
                        UserCommentedHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserCommentedHolder(view, context);
            case NotificationModel.Type.USER_COMMENTED | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        UserCommentedBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserCommentedBundledHolder(view, context);
            case NotificationModel.Type.USER_REPLIED:
                view = LayoutInflater.from(context).inflate(
                        UserRepliedHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserRepliedHolder(view, context);
            case NotificationModel.Type.USER_REPLIED | FLAG_BUNDLED:
                view = LayoutInflater.from(context).inflate(
                        UserRepliedBundledHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserRepliedBundledHolder(view, context);
            case NotificationModel.Type.USER_STARTED_FOLLOWING:
                view = LayoutInflater.from(context).inflate(
                        UserStartedFollowingHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserStartedFollowingHolder(view, context);
            case NotificationModel.Type.USER_ASKED_QUESTION:
                view = LayoutInflater.from(context).inflate(
                        UserAskedQuestionHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserAskedQuestionHolder(view, context);
            case NotificationModel.Type.USER_SENT_GIFT:
                view = LayoutInflater.from(context).inflate(
                        UserSentGiftHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new UserSentGiftHolder(view, context);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(TextNotificationHolder holder, int position) {
        holder.bind(data.getNotifications().get(position));
    }

    @Override
    public int getItemCount() {
        return data.getNotifications().size();
    }

    @Override
    public int getItemViewType(int position) {
        byte type = data.getNotifications().get(position).getType();

        if (data.getNotifications().get(position).getPosts() != null &&
                data.getNotifications().get(position).getPosts().size() > 1) {
            type |= FLAG_BUNDLED;
        }

        return type;
    }

    /**
     * Events...
     */
    @Subscribe
    public void onAzGramServiceResponse(FollowEvent event) {
        for (int position = 0; position < data.getNotifications().size(); position++)
            if (data.getNotifications().get(position).getUsers().get(0).getUsername().equals(
                    event.getModel().getUsername()
            )) {
                UserModel user = data.getNotifications().get(position).getUsers().get(0);

                if (event.getModel().getUsername().equals(user.getUsername())) {
                    if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {

                        if (user.getFollowStatus() == UserModel.FollowStatus.FOLLOWING ||
                                user.getFollowStatus() == UserModel.FollowStatus.PENDING) {
                            user.setFollowStatus(UserModel.FollowStatus.NOT_FOLLOWING);
                        } else {
                            user.setFollowStatus(UserModel.FollowStatus.FOLLOWING);
                        }

                        notifyItemChanged(position);
                    } else if (event.getModel().getMessageId() == NetworkOperations.Users.PENDING) {
                        user.setFollowStatus(UserModel.FollowStatus.PENDING);
                        notifyItemChanged(position);
                    } else {
                        Toast.makeText(context, context.getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                    }
                }

                break;
            }
    }

    @Subscribe
    public void onAzGramServiceResponse(FollowRequestEvent event) {
        for (int position = 0; position < data.getNotifications().size(); position++)
            if (data.getNotifications().get(position).getUsers().get(0).getUsername().equals(
                    event.getModel().getUsername()
            )) {
                if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
                    data.getNotifications().remove(position);
                    notifyItemRemoved(position);
                } else {
                    Toast.makeText(context, R.string.UnknownError, Toast.LENGTH_SHORT).show();
                }

                break;
            }
    }
}