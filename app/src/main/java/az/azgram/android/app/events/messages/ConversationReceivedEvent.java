package az.azgram.android.app.events.messages;

import az.azgram.android.app.models.ConversationModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class ConversationReceivedEvent {
    private ConversationModel model;

    public ConversationReceivedEvent(ConversationModel model) {
        this.model = model;
    }

    public ConversationModel getModel() {
        return model;
    }

    public void setModel(ConversationModel model) {
        this.model = model;
    }
}
