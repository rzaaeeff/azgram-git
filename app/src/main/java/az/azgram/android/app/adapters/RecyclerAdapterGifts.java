package az.azgram.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import az.azgram.android.app.R;

import az.azgram.android.app.models.response.GiftFeedModel;

/**
 * Created by Rzaaeeff on 12/12/2016.
 */

public class RecyclerAdapterGifts extends RecyclerView.Adapter<RecyclerAdapterGifts.GIFViewHolder> {
    // Constants
    public static final int COLUMN_COUNT = 3;
    public static final int SPACING = 1;

    // Other elements
    private Context context;
    private GiftFeedModel feed;

    public RecyclerAdapterGifts(Context context, GiftFeedModel feed) {
        this.context = context;
        this.feed = feed;
    }

    @Override
    public GIFViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_gif, parent, false);

        return new GIFViewHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final GIFViewHolder holder, int position) {
        ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFailure(String id, Throwable throwable) {
                holder.draweeView.setImageResource(R.drawable.post_photo_placeholder);
            }
        };

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(feed.getGifts().get(position).getUrl())
                .setControllerListener(controllerListener)
                .setAutoPlayAnimations(true)
                .build();

        holder.draweeView.setController(controller);
    }

    @Override
    public int getItemCount() {
        return feed.getGifts().size();
    }

    public class GIFViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView draweeView;

        public GIFViewHolder(View itemView) {
            super(itemView);

            draweeView = (SimpleDraweeView) itemView.findViewById(R.id.draweeViewRecyclerItemGIFPostsCollection);
        }
    }
}
