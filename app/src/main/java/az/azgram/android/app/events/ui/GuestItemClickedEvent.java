package az.azgram.android.app.events.ui;

import az.azgram.android.app.models.UserModel;

/**
 * Created by Rzaaeeff on 2/18/2017.
 */

public class GuestItemClickedEvent {
    private UserModel model;

    public GuestItemClickedEvent(UserModel model) {
        this.model = model;
    }

    public UserModel getModel() {
        return model;
    }

    public void setModel(UserModel model) {
        this.model = model;
    }
}
