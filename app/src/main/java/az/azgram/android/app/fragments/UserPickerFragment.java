package az.azgram.android.app.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterUserMini;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.search.UserSearchResultReceivedEvent;
import az.azgram.android.app.models.SearchModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserPickerFragment extends Fragment implements RecyclerAdapterUserMini.Controller {

    // Widgets
    private EditText editText;
    private SuperRecyclerView recyclerView;
    private TextView textView;

    // Other elements
    private Activity activity;
    private UserListModel result;
    private RecyclerAdapterUserMini adapter;
    private NetworkOperations service;
    private boolean isLoading;
    private SearchModel request;
    private String searchPhrase;
    private Handler handlerTextChange;
    private Runnable runnableTextChange;
    private boolean hasResult;
    private List<RecyclerAdapterUserMini.Controller> controllers = new ArrayList<>();

    public UserPickerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_picker, container, false);

        activity = getActivity();

        init(view);
        initValues();
        setListeners();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    private void init(View view) {
        editText = (EditText) view.findViewById(R.id.editText_UserPickerFragment);
        recyclerView = (SuperRecyclerView) view.findViewById(R.id.recyclerView_UserPickerFragment);
        textView = (TextView) view.findViewById(R.id.textView_UserPickerFragment);
    }

    private void initValues() {
        searchPhrase = "";
        handlerTextChange = new Handler();
        runnableTextChange = new Runnable() {
            @Override
            public void run() {
                search();
            }
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.getSwipeToRefresh().setEnabled(false);

        result = new UserListModel();
        result.setUsers(new ArrayList<UserModel>());

        adapter = new RecyclerAdapterUserMini(activity, result, this);
        adapter.setHasStableIds(true);

        recyclerView.setAdapter(adapter);

        request = new SearchModel();
        request.setApiKey(Keys.API_KEY);
        request.setToken(new Cache(activity).loadUser().getToken());

        service = new NetworkOperations(activity, BusProvider.getInstance());
        service.getSearchEndpoint();
    }

    private void setListeners() {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                handlerTextChange.removeCallbacks(runnableTextChange);
                handlerTextChange.postDelayed(runnableTextChange, Constants.SEARCH_DELAY);
            }
        });

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    handlerTextChange.removeCallbacks(runnableTextChange);
                    search();
                    Utility.hideSoftKeyboard(editText, activity);
                    return true;
                }

                return false;
            }
        });

        recyclerView.setOnMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                loadMore();
            }
        });
    }

    public void addController(RecyclerAdapterUserMini.Controller controller) {
        controllers.add(controller);
    }

    private void resetResult() {
        result.getUsers().clear();
        result.setPage(0);
        hasResult = true;
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    private void startLoading() {
        isLoading = true;
        recyclerView.getMoreProgressView().setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        recyclerView.getMoreProgressView().setVisibility(View.GONE);
    }

    private void search() {
        String phrase = getSearchPhrase();

        if (phrase != null) {
            if (searchPhrase.equals(phrase)) {
                showPage();
            } else {
                startLoading();

                resetResult();
                searchPhrase = phrase;
                request.setText(phrase);

                service.getSearchEndpoint().searchUsers(request);
            }
        }
    }

    @Nullable
    private String getSearchPhrase() {
        if (editText.getText().toString().trim().length() > 0) {
            return editText.getText().toString();
        }

        return null;
    }

    private void loadMore() {
        if (isLoading)
            return;

        startLoading();

        request.setText(searchPhrase);

        if (result.getPage() != null) {
            result.setPage(result.getPage());
            request.setPage(result.getPage());
        } else {
            stopLoading();
        }
    }

    private void showPage() {
        if (!hasResult) {
            recyclerView.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        adapter.notifyDataSetChanged();
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(UserSearchResultReceivedEvent event) {
        if (event.getModel().getText().equals(searchPhrase)) {
            stopLoading();

            if (event.getModel().getMessageId() == null) {
                result.getUsers().addAll(event.getModel().getUsers());
                result.setPage(event.getModel().getPage());

                hasResult = true;
                showPage();
            } else if (event.getModel().getMessageId() == NetworkOperations.Search.NO_USER_FOUND) {
                hasResult = false;
                showPage();
            } else {
                Toast.makeText(activity, getString(R.string.UnknownError), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onUserClicked(UserModel user) {
        for (RecyclerAdapterUserMini.Controller controller : controllers) {
            controller.onUserClicked(user);
        }
    }
}