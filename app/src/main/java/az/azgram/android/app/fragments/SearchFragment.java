package az.azgram.android.app.fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterPost;
import az.azgram.android.app.adapters.RecyclerAdapterPostsCollection;
import az.azgram.android.app.adapters.RecyclerAdapterUser;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.SuggestedPostsListReceivedEvent;
import az.azgram.android.app.events.search.PostSearchResultReceivedEvent;
import az.azgram.android.app.events.search.UserSearchResultReceivedEvent;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.SearchModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.FeedModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements TextWatcher {

    private static final byte PAGE_COUNT = 2;
    private static final byte USER_PAGE = 0;
    private static final byte POST_PAGE = 1;

    // Widgets
    private ImageButton imageButtonUser, imageButtonPost;
    private EditText editText;
    private TextView textView;
    private TextView textViewDiscover;
    private SuperRecyclerView recyclerView;
    private SuperRecyclerView recyclerViewDiscover;

    // Other elements
    private Context context;
    private UserListModel resultUser;
    private FeedModel resultPost;
    private FeedModel resultDiscover;
    private RecyclerAdapterUser adapterUser;
    private RecyclerAdapterPost adapterPost;
    private RecyclerAdapterPostsCollection adapterDiscover;
    private NetworkOperations service;
    private boolean isLoading;
    private SearchModel request;
    private byte currentPage;
    private String[] noResultText;
    private String[] searchPhrase = new String[PAGE_COUNT];
    private boolean[] hasResult = new boolean[PAGE_COUNT];
    private Handler handlerTextChange;
    private Runnable runnableTextChange;
    private boolean isInDiscoverMode = true;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        context = getContext();
        init(view);
        initValues();
        setListeners();
        loadMore(currentPage);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        BusProvider.getInstance().unregister(this);

        super.onPause();
    }

    /**
     * Assigns IDs and does some other initialization.
     *
     * @param view
     */
    private void init(View view) {
        imageButtonUser = (ImageButton) view.findViewById(R.id.imageButtonUserSearchFragment);
        imageButtonPost = (ImageButton) view.findViewById(R.id.imageButtonPostSearchFragment);
        editText = (EditText) view.findViewById(R.id.editTextSearchSearchFragment);
        textView = (TextView) view.findViewById(R.id.textViewNoResultSearchFragment);
        textViewDiscover = (TextView) view.findViewById(R.id.textViewDiscover);
        recyclerView = (SuperRecyclerView) view.findViewById(R.id.recyclerViewSearchFragment);
        recyclerViewDiscover = (SuperRecyclerView) view.findViewById(R.id.recyclerViewDiscover_SearchFragment);

        editText.addTextChangedListener(this);

        recyclerView.getSwipeToRefresh().setEnabled(false);
        recyclerViewDiscover.getSwipeToRefresh().setEnabled(false);

        setRetainInstance(true);
    }

    private void initValues() {
        currentPage = USER_PAGE;
        updateButtonStates(currentPage);

        noResultText = getResources().getStringArray(R.array.SearchFragmentNoResultArray);

        searchPhrase = new String[PAGE_COUNT];
        for (int i = 0; i < PAGE_COUNT; i++) {
            searchPhrase[i] = "";
        }

        handlerTextChange = new Handler();
        runnableTextChange = new Runnable() {
            @Override
            public void run() {
                search(currentPage);
            }
        };

        request = new SearchModel();
        request.setApiKey(Keys.API_KEY);
        request.setToken(new Cache(context).loadUser().getToken());

        resultUser = new UserListModel();
        resultUser.setUsers(new ArrayList<UserModel>());
        resultUser.setPage(0);

        resultPost = new FeedModel();
        resultPost.setPosts(new ArrayList<PostModel>());
        resultPost.setPage(0);

        resultDiscover = new FeedModel();
        resultDiscover.setPosts(new ArrayList<PostModel>());
        resultDiscover.setPage(0);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,
                RecyclerAdapterPostsCollection.COLUMN_COUNT);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerViewDiscover.setLayoutManager(layoutManager);

        adapterDiscover = new RecyclerAdapterPostsCollection(context, resultDiscover);
        adapterDiscover.setHasStableIds(true);
        recyclerViewDiscover.setAdapter(adapterDiscover);

        layoutManager = new LinearLayoutManager(context);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(layoutManager);

        service = new NetworkOperations(context, BusProvider.getInstance());
        service.getPostsEndpoint();
    }

    private void resetUserResult() {
        resultUser.getUsers().clear();
        resultUser.setPage(0);
        hasResult[USER_PAGE] = true;
        if (adapterUser != null) adapterUser.notifyDataSetChanged();
    }

    private void resetPostResult() {
        resultPost.getPosts().clear();
        resultPost.setPage(0);
        hasResult[POST_PAGE] = true;
        if (adapterPost != null) adapterPost.notifyDataSetChanged();
    }

    private void setListeners() {
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    handlerTextChange.removeCallbacks(runnableTextChange);
                    search(currentPage);
                    Utility.hideSoftKeyboard(editText, context);
                    return true;
                }

                return false;
            }
        });

        imageButtonUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navButtonClicked(USER_PAGE);
            }
        });

        imageButtonPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navButtonClicked(POST_PAGE);
            }
        });

        OnMoreListener onMoreListener = new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                loadMore(currentPage);
            }
        };

        recyclerView.setOnMoreListener(onMoreListener);
        recyclerViewDiscover.setOnMoreListener(onMoreListener);
    }

    private void navButtonClicked(byte position) {
        if (position == currentPage)
            return;

        currentPage = position;
        updateButtonStates(currentPage);

        search(position);
    }

    private void updateButtonStates(byte position) {
        switch (position) {
            case USER_PAGE:
                imageButtonUser.setSelected(true);
                imageButtonPost.setSelected(false);
                break;
            case POST_PAGE:
                imageButtonPost.setSelected(true);
                imageButtonUser.setSelected(false);
                break;
        }
    }

    private void startLoading() {
        isLoading = true;
        recyclerView.getMoreProgressView().setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        recyclerView.getMoreProgressView().setVisibility(View.GONE);
    }

    private void search(byte position) {
        String phrase = getSearchPhrase();

        isInDiscoverMode = phrase == null;

        if (isInDiscoverMode) {
            if (resultDiscover.getPosts().size() > 0) {
                showPage(position);
            } else {
                loadMore(position);
            }
        } else {
            if (searchPhrase[position].equals(phrase)) {
                showPage(position);
            } else {
                startLoading();

                switch (position) {
                    case USER_PAGE:
                        resetUserResult();
                        searchPhrase[position] = phrase;
                        request.setText(searchPhrase[position]);

                        service.getSearchEndpoint().searchUsers(request);
                        break;
                    case POST_PAGE:
                        resetPostResult();
                        searchPhrase[position] = phrase;
                        request.setText(searchPhrase[position]);

                        service.getSearchEndpoint().searchPosts(request);
                        break;
                }
            }
        }
    }

    @Nullable
    private String getSearchPhrase() {
        if (editText.getText().toString().trim().length() > 0) {
            return editText.getText().toString();
        }

        return null;
    }

    private void loadMore(byte position) {
        if (isLoading)
            return;

        startLoading();

        if (isInDiscoverMode) {
            if (resultDiscover.getPage() != null) {
                resultDiscover.setPage(resultDiscover.getPage() + 1);
                request.setPage(resultDiscover.getPage());
                service.getPostsEndpoint().getSuggestedPostsList(request);
            } else stopLoading();
        } else {
            request.setText(searchPhrase[position]);

            switch (position) {
                case USER_PAGE:
                    if (resultUser.getPage() != null) {
                        resultUser.setPage(resultUser.getPage() + 1);
                        request.setPage(resultUser.getPage());
                        service.getSearchEndpoint().searchUsers(request);
                    } else stopLoading();
                    break;
                case POST_PAGE:
                    if (resultPost.getPage() != null) {
                        resultPost.setPage(resultPost.getPage() + 1);
                        request.setPage(resultPost.getPage());
                        service.getSearchEndpoint().searchPosts(request);
                    } else stopLoading();
                    break;
            }
        }
    }

    private void showPage(byte position) {
        if (!isInDiscoverMode && !hasResult[position]) {
            recyclerView.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
            textView.setText(noResultText[position]);
        } else {
            textView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        if (isInDiscoverMode) {
            textViewDiscover.setVisibility(View.VISIBLE);
            recyclerViewDiscover.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

            adapterDiscover.notifyDataSetChanged();
        } else {
            textViewDiscover.setVisibility(View.GONE);
            recyclerViewDiscover.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            switch (position) {
                case USER_PAGE:
                    if (adapterUser == null) {
                        adapterUser = new RecyclerAdapterUser(context, resultUser);
                        adapterUser.setHasStableIds(true);
                    } else {
                        adapterUser.notifyDataSetChanged();
                    }

                    recyclerView.setAdapter(adapterUser);
                    break;
                case POST_PAGE:
                    if (adapterPost == null) {
                        adapterPost = new RecyclerAdapterPost(context, resultPost);
                        adapterPost.setHasStableIds(true);
                    } else {
                        adapterPost.notifyDataSetChanged();
                    }

                    recyclerView.setAdapter(adapterPost);
                    break;
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        handlerTextChange.removeCallbacks(runnableTextChange);
        handlerTextChange.postDelayed(runnableTextChange, Constants.SEARCH_DELAY);
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(UserSearchResultReceivedEvent event) {
        if (event.getModel().getText().equals(searchPhrase[USER_PAGE])) {
            stopLoading();
            if (event.getModel().getMessageId() == null) {
                resultUser.getUsers().addAll(event.getModel().getUsers());
                resultUser.setPage(event.getModel().getPage());

                hasResult[USER_PAGE] = true;

                if (currentPage == USER_PAGE && adapterUser != null)
                    adapterUser.notifyDataSetChanged();
                else showPage(USER_PAGE);
            } else if (event.getModel().getMessageId() == NetworkOperations.Search.NO_USER_FOUND) {
                hasResult[USER_PAGE] = false;
                if (currentPage == USER_PAGE) showPage(USER_PAGE);
            } else {
                Toast.makeText(context, getString(R.string.UnknownError), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(PostSearchResultReceivedEvent event) {
        if (event.getModel().getText().equals(searchPhrase[POST_PAGE])) {
            stopLoading();
            if (event.getModel().getMessageId() == null) {
                resultPost.getPosts().addAll(event.getModel().getPosts());
                resultPost.setPage(event.getModel().getPage());

                hasResult[POST_PAGE] = true;

                if (currentPage == POST_PAGE && adapterPost != null)
                    adapterPost.notifyDataSetChanged();
                else showPage(POST_PAGE);
            } else if (event.getModel().getMessageId() == NetworkOperations.Search.NO_POST_FOUND) {
                hasResult[POST_PAGE] = false;
                if (currentPage == POST_PAGE) showPage(POST_PAGE);
            } else {
                Toast.makeText(context, getString(R.string.UnknownError), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(SuggestedPostsListReceivedEvent event) {
        stopLoading();

        if (event.getModel().getMessageId() == null) {
            resultDiscover.getPosts().addAll(event.getModel().getPosts());
            resultDiscover.setPage(event.getModel().getPage());

            showPage(currentPage);
        } else {
            Toast.makeText(context, getString(R.string.UnknownError), Toast.LENGTH_SHORT).show();
        }
    }
}