package az.azgram.android.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterPlace;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.search.PlaceSearchResultReceivedEvent;
import az.azgram.android.app.models.PlaceModel;
import az.azgram.android.app.models.SearchModel;
import az.azgram.android.app.models.response.PlaceListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.PlaceSelectionController;
import az.azgram.android.app.operations.Utility;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PlacePickerActivity extends AppCompatActivity implements TextWatcher {

    // Widgets
    private EditText editText;
    private SuperRecyclerView recyclerView;
    private TextView textView;

    // Other elements
    private Context context = this;
    private PlaceListModel result;
    private RecyclerAdapterPlace adapter;
    private NetworkOperations service;
    private boolean isLoading;
    private SearchModel request;
    private String searchPhrase;
    private Handler handlerTextChange;
    private Runnable runnableTextChange;
    private PlaceSelectionController controller;
    private boolean hasResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_picker);

        init();
        initValues();
        setListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onStop() {
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void finishPlacePickerActivity(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void init() {
        editText = (EditText) findViewById(R.id.editText_PlacePickerActivity);
        recyclerView = (SuperRecyclerView) findViewById(R.id.recyclerView_PlacePickerActivity);
        textView = (TextView) findViewById(R.id.textView_PlacePickerActivity);
    }

    private void initValues() {
        controller = new PlaceSelectionController() {
            @Override
            public void onClick(PlaceModel place) {
                returnResult(place);
            }
        };

        searchPhrase = "";
        handlerTextChange = new Handler();
        runnableTextChange = new Runnable() {
            @Override
            public void run() {
                search();
            }
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.getSwipeToRefresh().setEnabled(false);

        result = new PlaceListModel();
        result.setPlaces(new ArrayList<PlaceModel>());

        adapter = new RecyclerAdapterPlace(context, result, controller);
        adapter.setHasStableIds(true);

        recyclerView.setAdapter(adapter);

        request = new SearchModel();
        request.setApiKey(Keys.API_KEY);
        request.setToken(new Cache(context).loadUser().getToken());

        service = new NetworkOperations(context, BusProvider.getInstance());
        service.getSearchEndpoint();
    }

    private void setListeners() {
        editText.addTextChangedListener(this);

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    handlerTextChange.removeCallbacks(runnableTextChange);
                    search();
                    Utility.hideSoftKeyboard(editText, context);
                    return true;
                }

                return false;
            }
        });

        recyclerView.setOnMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                loadMore();
            }
        });
    }

    private void resetResult() {
        result.getPlaces().clear();
        result.setPage(0);
        hasResult = true;
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    private void startLoading() {
        isLoading = true;
        recyclerView.getMoreProgressView().setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        recyclerView.getMoreProgressView().setVisibility(View.GONE);
    }

    private void search() {
        String phrase = getSearchPhrase();

        if (phrase != null) {
            if (searchPhrase.equals(phrase)) {
                showPage();
            } else {
                startLoading();

                resetResult();
                searchPhrase = phrase;
                request.setText(phrase);

                service.getSearchEndpoint().searchPlaces(request);
            }
        }
    }

    @Nullable
    private String getSearchPhrase() {
        if (editText.getText().toString().trim().length() > 0) {
            return editText.getText().toString();
        }

        return null;
    }

    private void loadMore() {
        if (isLoading)
            return;

        startLoading();

        request.setText(searchPhrase);

        if (result.getPage() != null) {
            result.setPage(result.getPage());
            request.setPage(result.getPage());
        } else {
            stopLoading();
        }
    }

    private void showPage() {
        if (!hasResult) {
            recyclerView.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        adapter.notifyDataSetChanged();
    }

    private void returnResult(PlaceModel place) {
        Intent intent = new Intent();
        intent.putExtra("data", place);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        handlerTextChange.removeCallbacks(runnableTextChange);
        handlerTextChange.postDelayed(runnableTextChange, Constants.SEARCH_DELAY);
    }

    @Subscribe
    public void onAzGramServiceResponse(PlaceSearchResultReceivedEvent event) {
        if (event.getModel().getText().equals(searchPhrase)) {
            stopLoading();

            if (event.getModel().getMessageId() == null) {
                result.getPlaces().addAll(event.getModel().getPlaces());
                result.setPage(event.getModel().getPage());

                hasResult = true;
                showPage();
            } else if (event.getModel().getMessageId() == NetworkOperations.Search.NO_PLACE_FOUND) {
                hasResult = false;
                showPage();
            } else {
                Toast.makeText(context, getString(R.string.UnknownError), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
