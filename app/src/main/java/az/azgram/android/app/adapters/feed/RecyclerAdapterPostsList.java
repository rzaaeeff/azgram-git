package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.FeedModel;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;
import com.squareup.otto.Bus;

import im.ene.toro.ToroAdapter;

/**
 * Created by Rzaaeeff on 12/12/2016.
 */

public class RecyclerAdapterPostsList extends ToroAdapter<ToroAdapter.ViewHolder> {
    public static String COMMENT_COUNT_FORMAT;
    public static String SHARE_COUNT_FORMAT;
    public static String VIEW_ALL_FORMAT;

    private Context context;
    private FeedModel feed;

    static UserModel request;
    static NetworkOperations.Posts serviceInstance;
    static Bus bus = new Bus();

    public RecyclerAdapterPostsList(Context context, FeedModel feed) {
        this.context = context;
        this.feed = feed;

        COMMENT_COUNT_FORMAT = context.getString(R.string.PostsListItemCommentCountText);
        SHARE_COUNT_FORMAT = context.getString(R.string.PostsListItemShareCountText);
        VIEW_ALL_FORMAT = context.getString(R.string.PostsListItemViewAllText);

        if (request == null) {
            request = new UserModel();

            Cache cache = new Cache(context);
            UserModel cachedUser = cache.loadUser();
            request.setToken(cachedUser.getToken());
            request.setApiKey(Keys.API_KEY);
        }

        if (serviceInstance == null) {
            NetworkOperations network = new NetworkOperations(context, bus);
            serviceInstance = network.getPostsEndpoint();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case PostModel.Type.PHOTO:
                view = LayoutInflater.from(context).inflate(
                        PhotoHolder.LAYOUT_RES,
                        parent,
                        false);

                return new PhotoHolder(view, context);
            case PostModel.Type.AD:
                view = LayoutInflater.from(context).inflate(
                        AdHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new AdHolder(view, context);
            case PostModel.Type.VIDEO:
                view = LayoutInflater.from(context).inflate(
                        VideoHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new VideoHolder(view, context);
            case PostModel.Type.STATUS:
                view = LayoutInflater.from(context).inflate(
                        StatusHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new StatusHolder(view, context);

            case PostModel.Type.AUDIO:
                view = LayoutInflater.from(context).inflate(
                        AudioHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new AudioHolder(view, context);
            default:
                view = LayoutInflater.from(context).inflate(
                        PhotoHolder.LAYOUT_RES,
                        parent,
                        false
                );

                return new PhotoHolder(view, context);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return feed.getPosts().get(position).getType();
    }

    @Nullable
    @Override
    protected Object getItem(int position) {
        return feed.getPosts().get(position);
    }

    @Override
    public int getItemCount() {
        return feed.getPosts().size();
    }
}