package az.azgram.android.app.models;

import android.view.View;

/**
 * Created by Rzaaeeff on 7/29/2017.
 */
public class AppSettingsModel extends CoreModel {
    public static final class LANGUAGE {
        public static final byte AZ = 0;
        public static final byte EN = 1;
        public static final byte RU = 2;
        public static final byte TR = 3;
    }

    public static final class STATUS {
        public static final byte ONLINE = 0;
        public static final byte STANDARD = 1;
        public static final byte OFFLINE = 2;
    }

    private String change;

    private Boolean privateProfile;
    private Boolean notifyAboutSubscription;
    private Boolean acceptQuestion;
    private Boolean acceptAnonymousQuestion;
    private Boolean notifyAboutOtherDevices;
    private Boolean pushNotification;
    private Boolean showInGuestList;

    private Byte language;
    private Byte status;

    // We don't want it to be serialized
    private transient View controlView;

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public Boolean getPrivateProfile() {
        return privateProfile;
    }

    public void setPrivateProfile(Boolean privateProfile) {
        this.privateProfile = privateProfile;
    }

    public Boolean getNotifyAboutSubscription() {
        return notifyAboutSubscription;
    }

    public void setNotifyAboutSubscription(Boolean notifyAboutSubscription) {
        this.notifyAboutSubscription = notifyAboutSubscription;
    }

    public Boolean getAcceptQuestion() {
        return acceptQuestion;
    }

    public void setAcceptQuestion(Boolean acceptQuestion) {
        this.acceptQuestion = acceptQuestion;
    }

    public Boolean getAcceptAnonymousQuestion() {
        return acceptAnonymousQuestion;
    }

    public void setAcceptAnonymousQuestion(Boolean acceptAnonymousQuestion) {
        this.acceptAnonymousQuestion = acceptAnonymousQuestion;
    }

    public Boolean getNotifyAboutOtherDevices() {
        return notifyAboutOtherDevices;
    }

    public void setNotifyAboutOtherDevices(Boolean notifyAboutOtherDevices) {
        this.notifyAboutOtherDevices = notifyAboutOtherDevices;
    }

    public Boolean getPushNotification() {
        return pushNotification;
    }

    public void setPushNotification(Boolean pushNotification) {
        this.pushNotification = pushNotification;
    }

    public Boolean getShowInGuestList() {
        return showInGuestList;
    }

    public void setShowInGuestList(Boolean showInGuestList) {
        this.showInGuestList = showInGuestList;
    }

    public Byte getLanguage() {
        return language;
    }

    public void setLanguage(Byte language) {
        this.language = language;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public View getControlView() {
        return controlView;
    }

    public void setControlView(View controlView) {
        this.controlView = controlView;
    }
}
