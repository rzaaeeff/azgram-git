package az.azgram.android.app.models;

import java.io.Serializable;

/**
 * Created by Rzaaeeff on 11/30/2016.
 */

public class LocationModel implements Serializable {

    private Double latitude = 0.0;
    private Double longitude = 0.0;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
