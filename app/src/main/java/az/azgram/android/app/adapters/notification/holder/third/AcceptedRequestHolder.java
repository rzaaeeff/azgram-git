package az.azgram.android.app.adapters.notification.holder.third;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.holder.second.UsernameTextHolder;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class AcceptedRequestHolder extends UsernameTextHolder {
    public AcceptedRequestHolder(View itemView, Context context) {
        super(itemView, context);

        TEXT_FORMAT = context.getString(R.string.notification_accepted_request);
    }
}
