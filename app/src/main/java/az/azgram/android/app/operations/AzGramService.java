package az.azgram.android.app.operations;

import az.azgram.android.app.models.AppSettingsModel;
import az.azgram.android.app.models.CommentModel;
import az.azgram.android.app.models.ComplaintModel;
import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.GiftModel;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.models.MoneyWrapperModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.QuestionModel;
import az.azgram.android.app.models.SearchModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.UserSettingsModel;
import az.azgram.android.app.models.UserWrapperModel;
import az.azgram.android.app.models.response.CommentFeedModel;
import az.azgram.android.app.models.response.ConversationListModel;
import az.azgram.android.app.models.response.ConversationWrapperModel;
import az.azgram.android.app.models.response.FeedModel;
import az.azgram.android.app.models.response.GiftFeedModel;
import az.azgram.android.app.models.response.GiftListModel;
import az.azgram.android.app.models.response.NotificationListModel;
import az.azgram.android.app.models.response.PlaceListModel;
import az.azgram.android.app.models.response.QuestionFeedModel;
import az.azgram.android.app.models.response.UserListModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Rzaaeeff on 12/5/2016.
 */

public interface AzGramService {
    String BASE_URL = "http://api.azgram.az/";

    interface Users {
        String ENDPOINT = "users/";

        @POST(ENDPOINT + "login")
        Call<UserModel> signIn(@Body UserModel user);

        @POST(ENDPOINT + "signup")
        Call<UserModel> signUp(@Body UserModel user);

        @POST(ENDPOINT + "forgot")
        Call<UserModel> forgot(@Body UserModel user);

        @POST(ENDPOINT + "getUser")
        Call<UserModel> getUser(@Body UserModel user);

        @POST(ENDPOINT + "follow")
        Call<UserModel> follow(@Body UserModel user);

        @POST(ENDPOINT + "getGuestList")
        Call<UserListModel> getGuestList(@Body CoreModel model);

        @POST(ENDPOINT + "clearGuestList")
        Call<CoreModel> clearGuestList(@Body CoreModel model);

        @POST(ENDPOINT + "acceptFollowRequest")
        Call<UserModel> acceptFollowRequest(@Body UserModel user);

        @POST(ENDPOINT + "rejectFollowRequest")
        Call<UserModel> rejectFollowRequest(@Body UserModel user);

        @POST(ENDPOINT + "getFollowerList")
        Call<UserListModel> getFollowerList(@Body UserModel user);

        @POST(ENDPOINT + "getFollowingList")
        Call<UserListModel> getFollowingList(@Body UserModel user);

        @POST(ENDPOINT + "hideFollower")
        Call<UserModel> hideFollower(@Body UserModel user);

        @POST(ENDPOINT + "hideFollowing")
        Call<UserModel> hideFollowing(@Body UserModel user);

        @POST(ENDPOINT + "save")
        Call<UserSettingsModel> save(@Body UserSettingsModel settings);

        @POST(ENDPOINT + "getSuggestedUsersList")
        Call<UserListModel> getSuggestedUsersList(@Body CoreModel request);

        @POST(ENDPOINT + "subscribe")
        Call<CoreModel> subscribe(@Body UserWrapperModel subscription);

        @POST(ENDPOINT + "report")
        Call<CoreModel> report(@Body UserWrapperModel complaint);

        @POST(ENDPOINT + "block")
        Call<UserModel> block(@Body UserWrapperModel block);

        @Multipart
        @POST(ENDPOINT + "savePP")
        Call<CoreModel> savePP(
                @Part MultipartBody.Part file,
                @Part("data") RequestBody request
        );

        @POST(ENDPOINT + "getBlockedList")
        Call<UserListModel> getBlockedList(@Body UserModel user);

        @POST(ENDPOINT + "getBlockerList")
        Call<UserListModel> getBlockerList(@Body UserModel user);

        @POST(ENDPOINT + "getNotFollowerList")
        Call<UserListModel> getNotFollowerList(@Body UserModel user);

        @POST(ENDPOINT + "logout")
        Call<CoreModel> logout(@Body UserModel user);

        @POST(ENDPOINT + "logoutAll")
        Call<CoreModel> logoutAll(@Body UserModel user);

        @POST(ENDPOINT + "delete")
        Call<CoreModel> delete(@Body UserModel user);
    }

    interface Posts {
        String ENDPOINT = "posts/";

        @POST(ENDPOINT + "getFeed")
        Call<FeedModel> getFeed(@Body UserModel user);

        @POST(ENDPOINT + "getUserPostsCollection")
        Call<FeedModel> getUserPostsCollection(@Body UserModel user);

        @POST(ENDPOINT + "getUserPostsList")
        Call<FeedModel> getUserPostsList(@Body UserModel user);

        @POST(ENDPOINT + "getTaggedPosts")
        Call<FeedModel> getTaggedPosts(@Body UserModel user);

        @POST(ENDPOINT + "getGifts")
        Call<GiftFeedModel> getGifts(@Body UserModel user);

        @POST(ENDPOINT + "getQuestions")
        Call<QuestionFeedModel> getQuestions(@Body UserModel user);

        @POST(ENDPOINT + "getPost")
        Call<PostModel> getPost(@Body UserModel user);

        @POST(ENDPOINT + "getComments")
        Call<CommentFeedModel> getComments(@Body UserModel userModel);

        @POST(ENDPOINT + "like")
        Call<CoreModel> like(@Body UserModel user);

        @POST(ENDPOINT + "dislike")
        Call<CoreModel> dislike(@Body UserModel user);

        @POST(ENDPOINT + "comment")
        Call<CoreModel> comment(@Body CommentModel comment);

        @POST(ENDPOINT + "deleteComment")
        Call<CoreModel> deleteComment(@Body PostModel post);

        @POST(ENDPOINT + "share")
        Call<CoreModel> share(@Body PostModel post);

        @POST(ENDPOINT + "askQuestion")
        Call<CoreModel> askQuestion(@Body QuestionModel question);

        @POST(ENDPOINT + "answerQuestion")
        Call<CoreModel> answerQuestion(@Body QuestionModel question);

        @POST(ENDPOINT + "deleteQuestion")
        Call<CoreModel> deleteQuestion(@Body QuestionModel question);

        @POST(ENDPOINT + "report")
        Call<CoreModel> report(@Body ComplaintModel complaint);

        @Multipart
        @POST(ENDPOINT + "upload/status")
        Call<PostModel> uploadStatus(
                @Part MultipartBody.Part file,
                @Part("description") RequestBody post
        );

        @Multipart
        @POST(ENDPOINT + "upload/status")
        Call<PostModel> uploadStatus(
                @Part("description") RequestBody post
        );

        @Multipart
        @POST(ENDPOINT + "upload/photo")
        Call<PostModel> uploadPhoto(
                @Part MultipartBody.Part file,
                @Part("description") RequestBody post
        );

        @Multipart
        @POST(ENDPOINT + "upload/video")
        Call<PostModel> uploadVideo(
                @Part MultipartBody.Part file,
                @Part("description") RequestBody post
        );

//        Canceled. Moved to upload/status
//        @Multipart
//        @POST(ENDPOINT + "upload/audio")
//        Call<PostModel> uploadAudio(
//                @Part MultipartBody.Part file,
//                @Part("description") RequestBody post
//        );

        @POST(ENDPOINT + "getLikeList")
        Call<UserListModel> getLikeList(@Body PostModel post);

        @POST(ENDPOINT + "getDislikeList")
        Call<UserListModel> getDislikeList(@Body PostModel post);

        @POST(ENDPOINT + "getSuggestedPostsList")
        Call<FeedModel> getSuggestedPostsList(@Body CoreModel request);

        @POST(ENDPOINT + "getPrivatePosts")
        Call<FeedModel> getPrivatePosts(@Body UserModel request);

        @POST(ENDPOINT + "getLikedPosts")
        Call<FeedModel> getLikedPosts(@Body UserModel request);

        @POST(ENDPOINT + "getDislikedPosts")
        Call<FeedModel> getDislikedPosts(@Body UserModel request);

        @POST(ENDPOINT + "getSubscribedPosts")
        Call<FeedModel> getSubscribedPosts(@Body UserModel request);

        @POST(ENDPOINT + "asPP")
        Call<CoreModel> asPP(@Body PostModel post);

        @POST(ENDPOINT + "delete")
        Call<CoreModel> delete(@Body PostModel post);
    }

    interface Search {
        String ENDPOINT = "search/";

        @POST(ENDPOINT + "user")
        Call<UserListModel> searchUsers(@Body SearchModel search);

        @POST(ENDPOINT + "post")
        Call<FeedModel> searchPosts(@Body SearchModel search);

        @POST(ENDPOINT + "place")
        Call<PlaceListModel> searchPlaces(@Body SearchModel search);
    }

    interface Messages {
        String ENDPOINT = "messages/";
        String CONVERSATION_ENDPOINT = "conversation/";

        /**
         * This function will return conversation with ID.
         * If required conversation exists, it will return,
         * ID of that conversation, otherwise it will create
         * and return ID of new conversation.
         *
         * @param request
         * @return
         */
        @POST(ENDPOINT + CONVERSATION_ENDPOINT + "create")
        Call<ConversationWrapperModel> createConversation(@Body ConversationModel request);

        @POST(ENDPOINT + CONVERSATION_ENDPOINT + "list")
        Call<ConversationListModel> getConversations(@Body CoreModel request);

        @POST(ENDPOINT + CONVERSATION_ENDPOINT + "delete")
        Call<CoreModel> deleteConversation(@Body CoreModel request);

        @POST(ENDPOINT + CONVERSATION_ENDPOINT + "accept")
        Call<CoreModel> acceptConversation(@Body CoreModel request);

        @POST(ENDPOINT + CONVERSATION_ENDPOINT + "reject")
        Call<CoreModel> rejectConversation(@Body CoreModel request);

        @POST(ENDPOINT + CONVERSATION_ENDPOINT + "get")
        Call<ConversationModel> getMessages(@Body CoreModel request);

        /**
         * Send message with file.
         * either <code>{@link az.azgram.android.app.models.MessageModel.Type.AUDIO}</code>
         * or <code>{@link az.azgram.android.app.models.MessageModel.Type.PHOTO}</code>
         *
         * @param file    can be either audio or photo.
         * @param message message object containing type.
         * @return result of operation.
         * @see az.azgram.android.app.models.MessageModel.Type
         */
        @Multipart
        @POST(ENDPOINT + "send")
        Call<MessageModel> send(
                @Part MultipartBody.Part file,
                @Part("message") MessageModel message
        );

        /**
         * Send message without file.
         * either <code>{@link az.azgram.android.app.models.MessageModel.Type.TEXT}</code>
         * either <code>{@link az.azgram.android.app.models.MessageModel.Type.POST}</code>
         * either <code>{@link az.azgram.android.app.models.MessageModel.Type.USER}</code>
         *
         * @param message message object containing type.
         * @return result of operation.
         * @see az.azgram.android.app.models.MessageModel.Type
         */
        @Multipart
        @POST(ENDPOINT + "send")
        Call<MessageModel> send(
                @Part("message") MessageModel message
        );

        @POST(ENDPOINT + CONVERSATION_ENDPOINT + "markAsUnread")
        Call<CoreModel> markAsUnread(@Body ConversationModel conversation);

        @POST(ENDPOINT + CONVERSATION_ENDPOINT + "clear")
        Call<CoreModel> clear(@Body ConversationModel conversation);
    }

    interface Notifications {
        String ENDPOINT = "notifications/";
        String USER_ACTIONS_ENDPOINT = "useractions/";
//        String REQUESTS_ENDPOINT = "requests/";

        @POST(ENDPOINT + "list")
        Call<NotificationListModel> getNotifications(@Body CoreModel request);

        @POST(ENDPOINT + "clear")
        Call<CoreModel> clearNotifications(@Body CoreModel request);

        @POST(ENDPOINT + USER_ACTIONS_ENDPOINT + "lister")
        Call<NotificationListModel> getUserActions(@Body CoreModel request);

//        @POST(ENDPOINT + USER_ACTIONS_ENDPOINT + "clear")
//        Call<CoreModel> clearUserActions(@Body CoreModel request);

        @POST(ENDPOINT + /*REQUESTS_ENDPOINT +*/ "requestsList")
        Call<NotificationListModel> getFollowRequests(@Body CoreModel request);

//        @POST(ENDPOINT + /*REQUESTS_ENDPOINT +*/ "requestsClear")
//        Call<CoreModel> clearFollowRequests(@Body CoreModel request);
    }

    interface Gifts {
        String ENDPOINT = "gifts/";

        @POST(ENDPOINT + "list")
        Call<GiftListModel> list(@Body CoreModel request);

        @POST(ENDPOINT + "send")
        Call<CoreModel> send(@Body GiftModel gift);
    }

    interface Balance {
        String ENDPOINT = "users/balance/";

        @POST(ENDPOINT + "get")
        Call<UserModel> get(@Body CoreModel request);

        @POST(ENDPOINT + "send")
        Call<CoreModel> send(@Body MoneyWrapperModel money);

        @POST(ENDPOINT + "request")
        Call<CoreModel> request(@Body MoneyWrapperModel money);
    }

    interface App {
        String ENDPOINT = "app/";

        @POST(ENDPOINT + "settings/save")
        Call<AppSettingsModel> saveSettings(@Body AppSettingsModel settings);

        @POST(ENDPOINT + "settings/get")
        Call<AppSettingsModel> getSettings(@Body UserModel request);

        @POST(ENDPOINT + "notifyOnline")
        Call<CoreModel> notifyOnline(@Body UserModel request);

        @POST(ENDPOINT + "notifyOffline")
        Call<CoreModel> notifyOffline(@Body UserModel request);
    }
}