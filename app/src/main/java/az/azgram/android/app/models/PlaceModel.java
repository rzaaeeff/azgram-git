package az.azgram.android.app.models;

import java.io.Serializable;

/**
 * Created by Rzaaeeff on 4/10/2017.
 */

public class PlaceModel implements Serializable {
    private String id_str;
    private String name;
    private LocationModel location;

    public String getId() {
        return id_str;
    }

    public void setId(String id_str) {
        this.id_str = id_str;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationModel getLocation() {
        return location;
    }

    public void setLocation(LocationModel location) {
        this.location = location;
    }
}
