package az.azgram.android.app.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import az.azgram.android.app.R;

/**
 * Created by Rzaaeeff on 5/1/2017.
 */

public class MaxDimenFrameLayout extends FrameLayout {
    private int maxWidth = Integer.MAX_VALUE;
    private int maxHeight = Integer.MAX_VALUE;

    public MaxDimenFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray attributes = getContext().obtainStyledAttributes(attrs, R.styleable.MaxDimenFrameLayout);
        maxWidth = attributes.getDimensionPixelSize(R.styleable.MaxDimenFrameLayout_maxWidth, Integer.MAX_VALUE);
        maxHeight = attributes.getDimensionPixelSize(R.styleable.MaxDimenFrameLayout_maxHeight, Integer.MAX_VALUE);

        attributes.recycle();
    }

    public MaxDimenFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray attributes = getContext().obtainStyledAttributes(attrs, R.styleable.MaxDimenFrameLayout);
        maxWidth = attributes.getDimensionPixelSize(R.styleable.MaxDimenFrameLayout_maxWidth, Integer.MAX_VALUE);
        maxHeight = attributes.getDimensionPixelSize(R.styleable.MaxDimenFrameLayout_maxHeight, Integer.MAX_VALUE);

        attributes.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (getMeasuredWidth() > maxWidth ||
                getMeasuredHeight() > maxHeight) {
            int width = getMeasuredWidth();
            int height = getMeasuredHeight();

            if (width > maxWidth)
                width = maxWidth;

            if (height > maxHeight)
                height = maxHeight;

            setMeasuredDimension(width, height);
        }
    }
}
