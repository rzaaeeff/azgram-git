package az.azgram.android.app.operations;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import az.azgram.android.app.AzGram;
import az.azgram.android.app.R;
import az.azgram.android.app.activities.CoreActivity;
import az.azgram.android.app.adapters.notification.RecyclerAdapterNotification;
import az.azgram.android.app.adapters.notification.holder.first.TextNotificationHolder;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.models.response.NotificationListModel;

/**
 * Created by Rzaaeeff on 6/12/2017.
 */

public class NotificationUtils {
    public static void showNotification(final Context context, final NotificationModel notification) {
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                Bitmap bitmap;

                try {
                    bitmap = Picasso.with(context).load(
                            notification.getUsers().get(0).getProfilePhoto().getSmallestPossible()
                    ).get();
                } catch (Exception exc) {
                    bitmap = null;
                }

                if (bitmap == null) {
                    bitmap = BitmapFactory.decodeResource(
                            context.getResources(),
                            R.drawable.profile_photo_placeholder
                    );
                }

                int width = Math.round(context.getResources().getDimension(android.R.dimen.notification_large_icon_width));
                int height = Math.round(context.getResources().getDimension(android.R.dimen.notification_large_icon_height));

                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);

                return ImageUtils.getCircleBitmap(bitmap);
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                NotificationListModel data = new NotificationListModel();
                data.setNotifications(new ArrayList<NotificationModel>());
                data.getNotifications().add(notification);
                RecyclerAdapterNotification adapter = new RecyclerAdapterNotification(context, data);
                int viewType = adapter.getItemViewType(0);
                TextNotificationHolder holder = adapter.onCreateViewHolder(new LinearLayout(context), viewType);
                holder.bind(notification);

                Intent intent = new Intent(context, CoreActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(CoreActivity.EXTRA_MODE, CoreActivity.MODE_OPEN_NOTIFICATIONS);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(context)
                                .setContentTitle(context.getString(R.string.app_name))
                                .setLargeIcon(bitmap)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentIntent(pendingIntent)
                                .setContentText(holder.getTextView().getText());

                Notification notification = notificationBuilder.build();
                notification.flags |= Notification.FLAG_AUTO_CANCEL;

                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(AzGram.getNextNotificationId(), notification);
            }
        }.execute();
    }
}