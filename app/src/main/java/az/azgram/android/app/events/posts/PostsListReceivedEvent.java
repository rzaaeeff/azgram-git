package az.azgram.android.app.events.posts;

import az.azgram.android.app.models.response.FeedModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class PostsListReceivedEvent {
    private FeedModel model;

    public PostsListReceivedEvent(FeedModel model) {
        this.model = model;
    }

    public FeedModel getModel() {
        return model;
    }

    public void setModel(FeedModel model) {
        this.model = model;
    }
}
