package az.azgram.android.app.operations;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;

/**
 * Created by Rzaaeeff on 2/2/2017.
 */

public class DownloadManagerProvider {
    private static DownloadManagerProvider instance;
    private static DownloadManager downloadManager;

    private DownloadManagerProvider() {

    }

    public static void init(Context context) {
        downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        instance = new DownloadManagerProvider();
    }

    public static DownloadManagerProvider getInstance() {
        return instance;
    }

    public void downloadFromUrl(String url) {
        DownloadManager.Request request = new DownloadManager.Request(
                Uri.parse(url)
        );

        downloadManager.enqueue(request);
    }
}
