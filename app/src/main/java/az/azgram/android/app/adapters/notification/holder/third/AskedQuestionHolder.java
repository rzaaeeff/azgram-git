package az.azgram.android.app.adapters.notification.holder.third;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.holder.second.UsernameTextHolder;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class AskedQuestionHolder extends UsernameTextHolder {
    public AskedQuestionHolder(View itemView, final Context context) {
        super(itemView, context);

        TEXT_FORMAT = context.getString(R.string.notification_asked_question);

        setRootListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UniversalListenerMethods.onClickUser(
                        new Cache(context).loadUser().getUsername(),
                        context
                );
            }
        });
    }
}
