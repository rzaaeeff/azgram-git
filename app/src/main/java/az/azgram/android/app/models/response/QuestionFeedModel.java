package az.azgram.android.app.models.response;

import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.QuestionModel;

import java.util.List;

/**
 * Created by Rzaaeeff on 12/28/2016.
 */

public class QuestionFeedModel extends CoreModel {
    private List<QuestionModel> questions;

    public List<QuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionModel> questions) {
        this.questions = questions;
    }
}
