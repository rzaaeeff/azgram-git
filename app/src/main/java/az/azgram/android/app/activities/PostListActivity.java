package az.azgram.android.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterUser;
import az.azgram.android.app.adapters.RecyclerAdapterUserSecond;
import az.azgram.android.app.adapters.feed.RecyclerAdapterPostsList;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.DislikeListReceivedEvent;
import az.azgram.android.app.events.posts.DislikedPostListReceivedEvent;
import az.azgram.android.app.events.posts.LikeListReceivedEvent;
import az.azgram.android.app.events.posts.LikedPostListReceivedEvent;
import az.azgram.android.app.events.posts.PrivatePostListReceivedEvent;
import az.azgram.android.app.events.posts.SubscribedPostListReceivedEvent;
import az.azgram.android.app.events.users.BlockedListReceivedEvent;
import az.azgram.android.app.events.users.BlockerListReceivedEvent;
import az.azgram.android.app.events.users.FollowerListReceivedEvent;
import az.azgram.android.app.events.users.FollowingListReceivedEvent;
import az.azgram.android.app.events.users.NotFollowerListReceivedEvent;
import az.azgram.android.app.events.users.SuggestedUsersListReceivedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.FeedModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PostListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    public static final String EXTRA_TYPE = "type";

    public static final class Type {
        public static final byte Private = 0;
        public static final byte Liked = 1;
        public static final byte Disliked = 2;
        public static final byte Subscribed = 3;
    }

    // Widgets
    private SuperRecyclerView recyclerView;
    private TextView textViewEmpty;

    // Other Elements
    private UserModel request;
    private boolean isLoading = false;
    private FeedModel data;
    private RecyclerView.Adapter adapter;
    private byte type;
    private Context context = this;
    private NetworkOperations service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);

        type = getIntent().getByteExtra(EXTRA_TYPE, Type.Liked);

        init();
        initValues();
        setListeners();
        loadMore();
    }

    @Override
    protected void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onStop() {
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void init() {
        recyclerView = (SuperRecyclerView) findViewById(R.id.recyclerViewUserListActivity);
        textViewEmpty = (TextView) findViewById(R.id.textView_empty);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
    }

    private void initValues() {
        data = new FeedModel();
        data.setPosts(new ArrayList<PostModel>());

        UserModel cachedUser = new Cache(this).loadUser();

        request = new UserModel();
        request.setApiKey(Keys.API_KEY);
        request.setPage(0);
        request.setUsername(cachedUser.getUsername());
        request.setToken(cachedUser.getToken());

        adapter = new RecyclerAdapterPostsList(this, data);

        recyclerView.setAdapter(adapter);

        service = new NetworkOperations(context, BusProvider.getInstance());
        service.getUsersEndpoint();

        textViewEmpty.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void setListeners() {
        recyclerView.setOnMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                loadMore();
            }
        });

        recyclerView.setRefreshListener(this);
    }

    private void startLoading() {
        isLoading = true;
        recyclerView.getMoreProgressView().setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        recyclerView.getMoreProgressView().setVisibility(View.GONE);
    }

    private void loadMore() {
        if (isLoading)
            return;

        if (request.getPage() != null) {
            startLoading();

            request.setPage(request.getPage() + 1);

            switch (type) {
                case Type.Private:
                    service.getPostsEndpoint().getPrivatePosts(request);
                    break;
                case Type.Liked:
                    service.getPostsEndpoint().getLikedPosts(request);
                    break;
                case Type.Disliked:
                    service.getPostsEndpoint().getDislikedPosts(request);
                    break;
                case Type.Subscribed:
                    service.getPostsEndpoint().getSubscribedPosts(request);
                    break;
            }
        } else {
            stopLoading();
        }
    }

    @Override
    public void onRefresh() {
        init();
        initValues();
        setListeners();
        loadMore();
    }

    private void setResultEmpty(boolean isResultEmpty) {
        recyclerView.setVisibility(isResultEmpty ? View.GONE : View.VISIBLE);
        textViewEmpty.setVisibility(isResultEmpty ? View.VISIBLE : View.GONE);
    }


    @Subscribe
    public void onAzGramServiceResponse(PrivatePostListReceivedEvent event) {
        if (type == Type.Private) {
            stopLoading();

            if (event.getModel().getPosts() != null) {
                onEvent(event.getModel());
            } else if (event.getModel().getMessageId() != null &&
                    event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                setResultEmpty(true);
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(LikedPostListReceivedEvent event) {
        if (type == Type.Liked) {
            stopLoading();

            if (event.getModel().getPosts() != null) {
                onEvent(event.getModel());
            } else if (event.getModel().getMessageId() != null &&
                    event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                setResultEmpty(true);
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(DislikedPostListReceivedEvent event) {
        if (type == Type.Disliked) {
            stopLoading();

            if (event.getModel().getPosts() != null) {
                onEvent(event.getModel());
            } else if (event.getModel().getMessageId() != null &&
                    event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                setResultEmpty(true);
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(SubscribedPostListReceivedEvent event) {
        if (type == Type.Subscribed) {
            stopLoading();

            if (event.getModel().getPosts() != null) {
                onEvent(event.getModel());
            } else if (event.getModel().getMessageId() != null &&
                    event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                setResultEmpty(true);
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    private void onEvent(FeedModel response) {
        data.getPosts().addAll(response.getPosts());
        adapter.notifyDataSetChanged();

        request.setPage(response.getPage());

        if (response.getPage() == null)
            recyclerView.setOnMoreListener(null);
    }
}