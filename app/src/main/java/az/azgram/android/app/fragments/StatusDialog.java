package az.azgram.android.app.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.PostEvent;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;

import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.text.DecimalFormat;

import cn.refactor.lib.colordialog.ColorDialog;

/**
 * Created by Rzaaeeff on 4/17/2017.
 */

public class StatusDialog extends DialogFragment implements MediaRecorder.OnErrorListener, MediaRecorder.OnInfoListener, View.OnClickListener, View.OnTouchListener, DialogInterface.OnKeyListener, TextWatcher, MediaPlayer.OnCompletionListener {
    // Constants
    private static final String FILENAME = "AzGram-AudioStatus.3gpp";
    private static final byte MODE_READY_TO_RECORD = 0;
    private static final byte MODE_RECORDING = 1;
    private static final byte MODE_READY_TO_PLAY = 2;
    private static final byte MODE_PLAYING = 3;
    private static final byte MODE_PAUSED = 4;
    private static final byte HANDLER_DELAY = 10;

    // Widgets
    private TextView textViewUsername, textViewDuration;
    private AppCompatSpinner spinnerPrivacy;
    private Button buttonPost;
    private EditText editTextStatus;
    private Button buttonRecord;
    private ProgressBar progressBarDuration, progressBarLoading;
    private ViewGroup rootLayout;
    private ImageButton imageButtonDelete;

    // Other elements
    private Activity activity;
    private PostModel request;
    private MediaRecorder recorder;
    private MediaPlayer player;
    private File directory = new File(
            Environment.getExternalStorageDirectory().getPath() + "/AzGram/Posts"
    );
    private File fileOutput = new File(
            directory.getPath(),
            FILENAME);
    private int mode = MODE_READY_TO_RECORD;
    private boolean hasText;
    private boolean hasAudio;
    private SoundPool soundPool;
    private Handler handlerDuration = new Handler();
    private int duration = 0;
    private int soundDelete, soundStart, soundStop, soundError;
    private Runnable runnableDuration = new Runnable() {
        @Override
        public void run() {
            duration++;
            setCurrentPosition(duration);

            handlerDuration.postDelayed(this, HANDLER_DELAY);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_status, container, false);

        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setOnKeyListener(this);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.status_dialog_rounded_background);
        activity = getActivity();

        init(view);
        initValues();
        setListeners();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    @Override
    public void dismiss() {
        if (hasAudio || hasText) {
            StatusDialog.this.getDialog().setOnKeyListener(null);
            Interaction.getInstance(activity).showPromptDialog(
                    activity.getString(R.string.StatusDialogFragmentDismissStatusPrompt)
            ).setPositiveListener(R.string.Yes, new ColorDialog.OnPositiveListener() {
                @Override
                public void onClick(ColorDialog colorDialog) {
                    StatusDialog.super.dismiss();
                    colorDialog.dismiss();
                }
            }).setNegativeListener(R.string.No, new ColorDialog.OnNegativeListener() {
                @Override
                public void onClick(ColorDialog colorDialog) {
                    StatusDialog.this.getDialog().setOnKeyListener(StatusDialog.this);
                    colorDialog.dismiss();
                }
            }).show();
        } else {
            super.dismiss();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        deleteFile();

        super.onDismiss(dialog);
    }

    private void init(View view) {
        textViewUsername = (TextView) view.findViewById(R.id.textViewUsername_StatusDialogFragment);
        textViewDuration = (TextView) view.findViewById(R.id.textViewDuration_StatusDialogFragment);
        spinnerPrivacy = (AppCompatSpinner) view.findViewById(R.id.spinnerPrivacy_StatusDialogFragment);
        buttonPost = (Button) view.findViewById(R.id.buttonPost_StatusDialogFragment);
        editTextStatus = (EditText) view.findViewById(R.id.editText_StatusDialogFragment);
        buttonRecord = (Button) view.findViewById(R.id.buttonRecord_StatusDialogFragment);
        progressBarDuration = (ProgressBar) view.findViewById(R.id.progressbarDuration_StatusDialogFragment);
        progressBarLoading = (ProgressBar) view.findViewById(R.id.progressBarLoading_StatusDialogFragment);
        imageButtonDelete = (ImageButton) view.findViewById(R.id.imageButtonDelete_StatusDialogFragment);
        rootLayout = (ViewGroup) view.findViewById(R.id.rootLayout_StatusDialogFragment);

    }

    private void initValues() {
        UserModel user = new Cache(activity).loadUser();

        request = new PostModel();
        request.setAuthor(new UserModel());
        request.getAuthor().setUsername(user.getUsername());
        request.getAuthor().setToken(user.getToken());
        request.setApiKey(Keys.API_KEY);

        textViewUsername.setText(user.getUsername());

        // it also disables
        disableDeleteButton();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            soundPool = new SoundPool.Builder()
                    .setMaxStreams(1)
                    .build();
        } else {
            soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 1);
        }

        soundStart = soundPool.load(activity, R.raw.sound_start_recording, 1);
        soundStop = soundPool.load(activity, R.raw.sound_stop_recording, 1);
        soundError = soundPool.load(activity, R.raw.sound_error, 1);
        soundDelete = soundPool.load(activity, R.raw.sound_delete, 1);

        try {
            if (!directory.exists() || !directory.isDirectory()) {
                directory.mkdirs();
            }
        } catch (Exception exc) {
        }
    }

    private void setListeners() {
        imageButtonDelete.setOnClickListener(this);
        buttonRecord.setOnTouchListener(this);
        buttonPost.setOnClickListener(this);
        editTextStatus.addTextChangedListener(this);
    }

    private void startLoading() {
        rootLayout.setEnabled(false);
        rootLayout.setAlpha(Constants.DISABLED_STATE_ALPHA);
        progressBarLoading.setVisibility(View.VISIBLE);
        progressBarLoading.setAlpha(Constants.ENABLED_STATE_ALPHA);
    }

    private void stopLoading() {
        rootLayout.setEnabled(true);
        rootLayout.setAlpha(Constants.ENABLED_STATE_ALPHA);
        progressBarLoading.setVisibility(View.GONE);
    }

    private void disableDeleteButton() {
        imageButtonDelete.setEnabled(false);
        imageButtonDelete.setAlpha(Constants.DISABLED_STATE_ALPHA);

        hasAudio = false;

        editTextStatus.addTextChangedListener(this);

        if (!hasText)
            disablePostButton();
    }

    private void enableDeteleButton() {
        imageButtonDelete.setEnabled(true);
        imageButtonDelete.setAlpha(Constants.ENABLED_STATE_ALPHA);

        editTextStatus.removeTextChangedListener(this);

        hasAudio = true;
        enablePostButton();
    }

    private void disablePostButton() {
        buttonPost.setEnabled(false);
        buttonPost.setAlpha(Constants.DISABLED_STATE_ALPHA);
    }

    private void enablePostButton() {
        buttonPost.setEnabled(true);
        buttonPost.setAlpha(Constants.ENABLED_STATE_ALPHA);
    }

    private void setCurrentPosition(int currentPosition) {
        if (mode == MODE_PLAYING) {
            currentPosition = player.getCurrentPosition() / 10;
            progressBarDuration.setProgress(currentPosition);
        } else {
            progressBarDuration.setProgress(0);
        }

        int seconds = currentPosition / 100;
        int minutes = seconds / 60;
        seconds %= 60;

        DecimalFormat formatter = new DecimalFormat("00");
        textViewDuration.setText(
                formatter.format(minutes) + ":" +
                        formatter.format(seconds)
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButtonDelete_StatusDialogFragment:
                deleteFile();
                playUiSound(soundDelete, 0);
                break;
            case R.id.buttonPost_StatusDialogFragment:
                if (hasText) {
                    request.setText(editTextStatus.getText().toString().trim());
                }

                request.setPrivacy((byte) spinnerPrivacy.getSelectedItemPosition());

                NetworkOperations service = new NetworkOperations(activity);
                if (hasAudio) {
                    service.getPostsEndpoint().uploadStatus(request, fileOutput);
                } else {
                    service.getPostsEndpoint().uploadStatus(request, null);
                }

                startLoading();
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (mode) {
                    case MODE_READY_TO_RECORD:
                        playUiSound(soundStart, 0);
                        buttonRecord.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                startRecording();
                                buttonRecord.setOnLongClickListener(null);

                                return true;
                            }
                        });
                        break;
                    case MODE_READY_TO_PLAY:
                        startPlaying();
                        break;
                    case MODE_PLAYING:
                        pausePlaying();
                        break;
                    case MODE_PAUSED:
                        resumePlaying();
                        break;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mode == MODE_RECORDING) {
                    handlerDuration.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            stopRecording();
                        }
                    }, 500);
                }
                break;
        }

        return false;
    }

    private void startRecording() {
        deleteFile();

        try {
            recorder = new MediaRecorder();
            recorder.setOnErrorListener(this);
            recorder.setOnInfoListener(this);
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            recorder.setOutputFile(fileOutput.getAbsolutePath());
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            recorder.setAudioEncodingBitRate(Constants.AUDIO_ENCODING_BIT_RATE);
            recorder.setAudioChannels(2);

            try {
                recorder.prepare();
            } catch (Exception exc) {
            }

            recorder.start();
            changeMode(MODE_RECORDING);
            changeMode(MODE_RECORDING);

            duration = 0;
            handlerDuration.removeCallbacks(runnableDuration);
            handlerDuration.postDelayed(runnableDuration, HANDLER_DELAY);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    private void stopRecording() {
        handlerDuration.removeCallbacks(runnableDuration);

        try {
            recorder.stop();
            recorder.release();
            recorder = null;

            buttonRecord.setEnabled(false);
            buttonRecord.setAlpha(Constants.DISABLED_STATE_ALPHA);

            if (duration > 100) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonRecord.setEnabled(true);
                        buttonRecord.setAlpha(Constants.ENABLED_STATE_ALPHA);
                        changeMode(MODE_READY_TO_PLAY);
                    }
                }, 400);
            } else {
                Toast.makeText(activity, R.string.TooShort, Toast.LENGTH_SHORT).show();
                playUiSound(soundError, 1);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        deleteFile();
                        buttonRecord.setEnabled(true);
                        buttonRecord.setAlpha(Constants.ENABLED_STATE_ALPHA);
                        changeMode(MODE_READY_TO_RECORD);
                    }
                }, 400);
            }
        } catch (Exception exc) {
        } finally {
            playUiSound(soundStop, 0);
        }

    }

    private void startPlaying() {
        try {
            player = new MediaPlayer();
            player.setDataSource(activity, Uri.parse(fileOutput.getAbsolutePath()));
            player.setOnCompletionListener(this);
            //player.setAudioStreamType(AudioManager.USE_DEFAULT_STREAM_TYPE);
            player.prepare();
            progressBarDuration.setMax((player.getDuration() / 10) - 10);
            player.start();
            changeMode(MODE_PLAYING);

            duration = 0;
            handlerDuration.removeCallbacks(runnableDuration);
            handlerDuration.postDelayed(runnableDuration, HANDLER_DELAY);
        } catch (Exception e) {
        }
    }

    private void pausePlaying() {
        try {
            player.pause();
            changeMode(MODE_PAUSED);

            handlerDuration.removeCallbacks(runnableDuration);
        } catch (Exception exc) {
        }
    }

    private void resumePlaying() {
        try {
            player.start();
            changeMode(MODE_PLAYING);

            handlerDuration.removeCallbacks(runnableDuration);
            handlerDuration.post(runnableDuration);
        } catch (Exception exc) {
        }
    }

    private void stopPlaying() {
        try {
            handlerDuration.removeCallbacks(runnableDuration);
            setCurrentPosition(0);
            player.stop();
            player.release();
            player = null;
            duration = 0;
        } catch (Exception exc) {
        }
    }

    private void deleteFile() {
        if (fileOutput.exists()) {
            changeMode(MODE_READY_TO_RECORD);
            disableDeleteButton();
            setCurrentPosition(0);
            fileOutput.delete();
            stopPlaying();
        }
    }

    private void playUiSound(int soundId, int priority) {
        soundPool.play(soundId, 1, 1, priority, 0, 1);
    }

    private void changeMode(byte desiredMode) {
        mode = desiredMode;

        switch (desiredMode) {
            case MODE_READY_TO_RECORD:
                buttonRecord.setBackgroundResource(R.drawable.ic_microphone_gray);
                break;
            case MODE_RECORDING:
                buttonRecord.setBackgroundResource(R.drawable.ic_microphone_red);
                break;
            case MODE_PAUSED:
            case MODE_READY_TO_PLAY:
                buttonRecord.setBackgroundResource(R.drawable.ic_play);
                enableDeteleButton();
                break;
            case MODE_PLAYING:
                buttonRecord.setBackgroundResource(R.drawable.ic_pause);
                break;
        }
    }

    @Override
    public void onError(MediaRecorder mr, int what, int extra) {
        Toast.makeText(activity, R.string.UnknownError, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        switch (what) {
            case MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:
                Toast.makeText(
                        activity,
                        R.string.StatusDialogFragmentMaxDuration,
                        Toast.LENGTH_SHORT
                ).show();
                break;
            case MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:
                Toast.makeText(
                        activity,
                        R.string.StatusDialogFragmentMaxSize,
                        Toast.LENGTH_SHORT
                ).show();
                break;
            default:
                onError(mr, what, extra);
        }
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                dismiss();
                break;
        }

        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        hasText = s.toString().trim().length() > 0;

        if (hasText)
            enablePostButton();
        else
            disablePostButton();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        handlerDuration.removeCallbacks(runnableDuration);
        player.release();
        player = null;
        changeMode(MODE_READY_TO_PLAY);
        setCurrentPosition(0);
    }

    @Produce
    @Subscribe
    public void onAzGramServiceResponse(PostEvent event) {
        stopLoading();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(activity, R.string.StatusPosted, Toast.LENGTH_SHORT).show();
            editTextStatus.setText("");
            deleteFile();
            dismiss();
        } else {
            Toast.makeText(activity, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }
}