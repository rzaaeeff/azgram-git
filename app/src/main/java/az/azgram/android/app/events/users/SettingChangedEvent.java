package az.azgram.android.app.events.users;

import az.azgram.android.app.models.UserSettingsModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class SettingChangedEvent {
    private UserSettingsModel model;

    public SettingChangedEvent(UserSettingsModel model) {
        this.model = model;
    }

    public UserSettingsModel getModel() {
        return model;
    }

    public void setModel(UserSettingsModel model) {
        this.model = model;
    }
}
