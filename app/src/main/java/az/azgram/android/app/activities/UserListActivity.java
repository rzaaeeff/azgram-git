package az.azgram.android.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterUser;
import az.azgram.android.app.adapters.RecyclerAdapterUserSecond;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.DislikeListReceivedEvent;
import az.azgram.android.app.events.posts.LikeListReceivedEvent;
import az.azgram.android.app.events.users.BlockedListReceivedEvent;
import az.azgram.android.app.events.users.BlockerListReceivedEvent;
import az.azgram.android.app.events.users.FollowerListReceivedEvent;
import az.azgram.android.app.events.users.FollowingListReceivedEvent;
import az.azgram.android.app.events.users.NotFollowerListReceivedEvent;
import az.azgram.android.app.events.users.SuggestedUsersListReceivedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class UserListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    public static final String EXTRA_TYPE = "type";

    public static final class Type {
        public static final byte Follower = 0;
        public static final byte Following = 1;
        public static final byte Suggested = 2;
        public static final byte Like = 3;
        public static final byte Dislike = 4;
        public static final byte Blocked = 5;
        public static final byte Blocker = 6;
        public static final byte NotFollower = 7;
    }

    // Widgets
    private SuperRecyclerView recyclerView;
    private TextView textViewEmpty;
    private ImageView backButton;

    // Other Elements
    private CoreModel request;
    private boolean isLoading = false;
    private UserListModel data;
    private RecyclerView.Adapter adapter;
    private byte type;
    private Context context = this;
    private NetworkOperations service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        type = getIntent().getByteExtra(EXTRA_TYPE, Type.Follower);

        init();
        initValues();
        setListeners();
        loadMore();
    }

    @Override
    protected void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onStop() {
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void init() {
        recyclerView = (SuperRecyclerView) findViewById(R.id.recyclerViewUserListActivity);
        textViewEmpty = (TextView) findViewById(R.id.textView_empty);
        backButton = (ImageView) findViewById(R.id.ic_back);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
    }

    private void initValues() {
        data = new UserListModel();
        data.setUsers(new ArrayList<UserModel>());

        switch (type) {
            case Type.Follower:
            case Type.Following:
                request = (UserModel) getIntent().getSerializableExtra(UserModel.UID);
                adapter = new RecyclerAdapterUserSecond(context, data, type);
                request.setToken(new Cache(context).loadUser().getToken());
                break;
            case Type.Suggested:
                request = new UserModel();
                adapter = new RecyclerAdapterUser(context, data);
                request.setToken(new Cache(context).loadUser().getToken());
                break;
            case Type.Like:
            case Type.Dislike:
                request = new PostModel();
                PostModel post = (PostModel) getIntent().getSerializableExtra(PostModel.UID);
                request.setId(post.getId());
                adapter = new RecyclerAdapterUser(context, data);
                ((PostModel) request).setAuthor(new UserModel());
                ((PostModel) request).getAuthor().setToken(new Cache(context).loadUser().getToken());
                break;
            case Type.Blocked:
            case Type.Blocker:
            case Type.NotFollower:
                request = (UserModel) getIntent().getSerializableExtra(UserModel.UID);
                adapter = new RecyclerAdapterUser(context, data);
                request.setToken(new Cache(context).loadUser().getToken());
                break;
        }

        request.setApiKey(Keys.API_KEY);
        request.setPage(0);

        recyclerView.setAdapter(adapter);

        service = new NetworkOperations(context, BusProvider.getInstance());
        service.getUsersEndpoint();

        textViewEmpty.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void setListeners() {
        recyclerView.setOnMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                loadMore();
            }
        });

        recyclerView.setRefreshListener(this);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void startLoading() {
        isLoading = true;
        recyclerView.getMoreProgressView().setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        recyclerView.getMoreProgressView().setVisibility(View.GONE);
    }

    private void loadMore() {
        if (isLoading)
            return;

        if (request.getPage() != null) {
            startLoading();

            request.setPage(request.getPage() + 1);

            switch (type) {
                case Type.Follower:
                    service.getUsersEndpoint().getFollowerList((UserModel) request);
                    break;
                case Type.Following:
                    service.getUsersEndpoint().getFollowingList((UserModel) request);
                    break;
                case Type.Suggested:
                    service.getUsersEndpoint().getSuggestedUsersList(request);
                    break;
                case Type.Like:
                    service.getPostsEndpoint().getLikeList((PostModel) request);
                    break;
                case Type.Dislike:
                    service.getPostsEndpoint().getDislikeList((PostModel) request);
                    break;
                case Type.Blocked:
                    service.getUsersEndpoint().getBlockedList((UserModel) request);
                    break;
                case Type.Blocker:
                    service.getUsersEndpoint().getBlockerList((UserModel) request);
                    break;
                case Type.NotFollower:
                    service.getUsersEndpoint().getNotFollowerList((UserModel) request);
                    break;
            }
        } else {
            stopLoading();
        }
    }

    @Override
    public void onRefresh() {
        init();
        initValues();
        setListeners();
        loadMore();
    }

    private void setResultEmpty(boolean isResultEmpty) {
        recyclerView.setVisibility(isResultEmpty ? View.GONE : View.VISIBLE);
        textViewEmpty.setVisibility(isResultEmpty ? View.VISIBLE : View.GONE);
    }

    @Subscribe
    public void onAzGramServiceResponse(FollowerListReceivedEvent event) {
        if (type == Type.Follower) {
            stopLoading();

            if (event.getModel().getUsers() != null) {
                onEvent(event.getModel());
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(FollowingListReceivedEvent event) {
        if (type == Type.Following) {
            stopLoading();

            if (event.getModel().getUsers() != null) {
                onEvent(event.getModel());
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(SuggestedUsersListReceivedEvent event) {
        if (type == Type.Suggested) {
            stopLoading();

            if (event.getModel().getUsers() != null) {
                onEvent(event.getModel());
            } else if (event.getModel().getMessageId() != null &&
                    event.getModel().getMessageId() == NetworkOperations.Users.EMPTY_SUGGESTED_LIST) {
                setResultEmpty(true);
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(LikeListReceivedEvent event) {
        if (type == Type.Like) {
            stopLoading();

            if (event.getModel().getUsers() != null) {
                onEvent(event.getModel());
            } else if (event.getModel().getMessageId() != null &&
                    event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY_REACTION_LIST) {
                setResultEmpty(true);
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(DislikeListReceivedEvent event) {
        if (type == Type.Dislike) {
            stopLoading();

            if (event.getModel().getUsers() != null) {
                onEvent(event.getModel());
            } else if (event.getModel().getMessageId() != null &&
                    event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY_REACTION_LIST) {
                setResultEmpty(true);
            } else {
                setResultEmpty(true);
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(BlockedListReceivedEvent event) {
        if (type == Type.Blocked) {
            stopLoading();

            if (event.getModel().getUsers() != null) {
                onEvent(event.getModel());
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(BlockerListReceivedEvent event) {
        if (type == Type.Blocker) {
            stopLoading();

            if (event.getModel().getUsers() != null) {
                onEvent(event.getModel());
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(NotFollowerListReceivedEvent event) {
        if (type == Type.NotFollower) {
            stopLoading();

            if (event.getModel().getUsers() != null) {
                onEvent(event.getModel());
            } else {
                Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                setResultEmpty(true);
            }
        }
    }

    private void onEvent(UserListModel response) {
        data.getUsers().addAll(response.getUsers());
        adapter.notifyDataSetChanged();

        request.setPage(response.getPage());

        if (response.getPage() == null)
            recyclerView.setOnMoreListener(null);
    }
}