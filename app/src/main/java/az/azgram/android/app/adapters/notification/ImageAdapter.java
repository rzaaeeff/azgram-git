package az.azgram.android.app.adapters.notification;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import az.azgram.android.app.R;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.operations.Utility;

/**
 * from internet on 6/3/2017.
 */

public class ImageAdapter extends BaseAdapter {
    private Context context;
    private List<PostModel> posts = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public ImageAdapter(Context context, List<PostModel> posts) {
        this.context = context;
        this.posts = posts;

        layoutInflater = LayoutInflater.from(context);
    }

    public void updateData(List<PostModel> posts) {
        this.posts = posts;
        notifyDataSetChanged();
    }

    public int getCount() {
        return posts.size();
    }

    public Object getItem(int position) {
        return posts.get(position);
    }

    public long getItemId(int position) {
        return posts.get(position).getId();
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            convertView = layoutInflater.inflate(R.layout.grid_item_photo, null);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageViewPhoto_PhotoItem);

        Utility.loadWithPicasso(
                context,
                posts.get(position).getPhoto().getSmallestPossible(),
                imageView,
                R.drawable.post_photo_placeholder
        );

        return convertView;
    }
}