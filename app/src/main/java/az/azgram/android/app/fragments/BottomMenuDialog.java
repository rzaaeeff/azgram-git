package az.azgram.android.app.fragments;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterMinimalUser;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.search.UserSearchResultReceivedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.SearchModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.BottomMenuController;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Utility;

import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class BottomMenuDialog extends BottomSheetDialogFragment implements View.OnClickListener, TextWatcher, RecyclerAdapterMinimalUser.Controller {

    // Constants
    public static final String EXTRA_SHOULD_SHOW_AS_PP = "extra_should_show_as_pp";
    public static final String EXTRA_SHOULD_SHOW_DELETE = "extra_should_show_delete";

    // Widgets
    private TextView textViewShare, textViewDownload,
            textViewSend, textViewAsPP, textViewReport,
            textViewDelete, textViewNoResult;
    private ProgressBar progressBar;
    private EditText editTextSearch;
    private RecyclerView recyclerViewSearch;
    private LinearLayout linearLayoutSearch;
    private Button buttonCancel;

    // Other Elements
    private RecyclerView.OnScrollListener onScrollListener;
    private Context context;
    private BottomMenuController controller;
    private UserListModel result;
    private RecyclerAdapterMinimalUser adapter;
    private LinearLayoutManager layoutManager;
    private String searchPhrase;
    private SearchModel request;
    private NetworkOperations service;
    private Handler handlerTextChange;
    private Runnable runnableTextChange;
    private boolean hasResult;
    private boolean isLoading;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean shouldShowAsPP, shouldShowDelete;

    public BottomMenuDialog() {
        // Required empty public constructor
    }

    public void setController(BottomMenuController controller) {
        this.controller = controller;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.dialog_fragment_bottom_menu, null);
        dialog.setContentView(contentView);

        ((View) contentView.getParent()).setBackgroundColor(Color.TRANSPARENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_bottom_menu, container, false);
        context = getContext();

        init(view);
        initValues();
        setListeners();

        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getActivity(), getTheme());
    }

    @Override
    public void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        BusProvider.getInstance().unregister(this);

        super.onPause();
    }

    private void init(View view) {
        if (getArguments() != null) {
            shouldShowAsPP = getArguments().getBoolean(EXTRA_SHOULD_SHOW_AS_PP);
            shouldShowDelete = getArguments().getBoolean(EXTRA_SHOULD_SHOW_DELETE);
        }

        textViewShare = (TextView) view.findViewById(R.id.textViewShareBottomMenu);
        textViewDownload = (TextView) view.findViewById(R.id.textViewDownloadBottomMenu);
        textViewSend = (TextView) view.findViewById(R.id.textViewSendBottomMenu);
        textViewNoResult = (TextView) view.findViewById(R.id.textViewNoResultBottomMenu);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBarBottomMenu);
        editTextSearch = (EditText) view.findViewById(R.id.editTextSearchBottomMenu);
        recyclerViewSearch = (RecyclerView) view.findViewById(R.id.recyclerViewSearchBottomMenu);
        linearLayoutSearch = (LinearLayout) view.findViewById(R.id.linearLayoutSearchBottomMenu);
        buttonCancel = (Button) view.findViewById(R.id.buttonCancelBottomMenu);

        textViewShare.setOnClickListener(this);
        textViewDownload.setOnClickListener(this);
        textViewSend.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        editTextSearch.addTextChangedListener(this);

        if (shouldShowAsPP) {
            textViewAsPP = (TextView) view.findViewById(R.id.textViewAsPPBottomMenu);
            textViewAsPP.setVisibility(View.VISIBLE);
            textViewAsPP.setOnClickListener(this);
        }

        if (shouldShowDelete) {
            textViewDelete = (TextView) view.findViewById(R.id.textViewDeleteBottomMenu);
            textViewDelete.setVisibility(View.VISIBLE);
            textViewDelete.setOnClickListener(this);
        } else {
            textViewReport = (TextView) view.findViewById(R.id.textViewReportBottomMenu);
            textViewReport.setVisibility(View.VISIBLE);
            textViewReport.setOnClickListener(this);
        }
    }

    private void initValues() {
        handlerTextChange = new Handler();
        runnableTextChange = new Runnable() {
            @Override
            public void run() {
                search();
            }
        };

        searchPhrase = "";

        String[] noResultText = getResources().getStringArray(R.array.SearchFragmentNoResultArray);
        textViewNoResult.setText(noResultText[0]);

        request = new SearchModel();
        request.setToken(new Cache(context).loadUser().getToken());
        request.setApiKey(Keys.API_KEY);
        request.setPage(0);

        result = new UserListModel();
        result.setUsers(new ArrayList<UserModel>());

        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewSearch.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapterMinimalUser(context, result, this);
        recyclerViewSearch.setAdapter(adapter);

        service = new NetworkOperations(context, BusProvider.getInstance());
        service.getSearchEndpoint();
    }

    private void setListeners() {
        editTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    handlerTextChange.removeCallbacks(runnableTextChange);
                    handlerTextChange.post(runnableTextChange);
                    Utility.hideSoftKeyboard(editTextSearch, context);
                    return true;
                }

                return false;
            }
        });

        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (isLoading)
                    return;

                if (dx > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    int lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                    if (pastVisiblesItems + visibleItemCount >= totalItemCount) {
                        loadMore();
                    }
                }
            }
        };

        recyclerViewSearch.addOnScrollListener(onScrollListener);
    }

    private void resetResult() {
        result.getUsers().clear();
        result.setPage(0);
        hasResult = true;
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    private void startLoading() {
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textViewShareBottomMenu:
                hideSearchLayout();
                controller.shareButtonClicked();
                dismiss();
                break;
            case R.id.textViewDownloadBottomMenu:
                hideSearchLayout();
                controller.downloadButtonClicked();
                dismiss();
                break;
            case R.id.textViewSendBottomMenu:
                showSearchLayout();
                break;
            case R.id.textViewAsPPBottomMenu:
                hideSearchLayout();
                controller.asPPButtonClicked();
                dismiss();
                break;
            case R.id.textViewReportBottomMenu:
                hideSearchLayout();
                controller.reportButtonClicked();
                break;
            case R.id.textViewDeleteBottomMenu:
                controller.deleteButtonClicked();
                dismiss();
                break;
            case R.id.buttonCancelBottomMenu:
                dismiss();
                break;
        }
    }

    private void hideSearchLayout() {
        linearLayoutSearch.setVisibility(View.GONE);
        recyclerViewSearch.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        textViewNoResult.setVisibility(View.GONE);
    }

    private void showSearchLayout() {
        linearLayoutSearch.setVisibility(View.VISIBLE);
        recyclerViewSearch.setVisibility(View.VISIBLE);
    }

    private void search() {
        String phrase = getSearchPhrase();

        if (phrase != null) {
            if (!searchPhrase.equals(phrase)) {
                startLoading();

                resetResult();
                searchPhrase = phrase;
                request.setText(searchPhrase);

                service.getSearchEndpoint().searchUsers(request);
            }
        }
    }

    private String getSearchPhrase() {
        if (editTextSearch.getText().toString().trim().length() > 0) {
            return editTextSearch.getText().toString();
        }

        return null;
    }

    private void loadMore() {
        if (isLoading)
            return;

        startLoading();

        if (searchPhrase != null && request.getPage() != null) {
            request.setPage(request.getPage() + 1);
            service.getSearchEndpoint().searchUsers(request);
        } else stopLoading();
    }

    private void showPage() {
        if (hasResult) {
            textViewNoResult.setVisibility(View.GONE);
            recyclerViewSearch.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        } else {
            recyclerViewSearch.setVisibility(View.GONE);
            textViewNoResult.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSearchResultClicked(CoreModel clickedItem) {
        if (controller != null) {
            controller.searchResultClicked(clickedItem);
            dismiss();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        handlerTextChange.removeCallbacks(runnableTextChange);
        handlerTextChange.postDelayed(runnableTextChange, Constants.SEARCH_DELAY);
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(UserSearchResultReceivedEvent event) {
        if (event.getModel().getText().equals(searchPhrase)) {
            stopLoading();
            if (event.getModel().getMessageId() == null) {
                result.getUsers().addAll(event.getModel().getUsers());
                request.setPage(event.getModel().getPage());

                if (event.getModel().getPage() == null)
                    recyclerViewSearch.removeOnScrollListener(onScrollListener);

                hasResult = true;

                showPage();
            } else if (event.getModel().getMessageId() == NetworkOperations.Search.NO_USER_FOUND) {
                hasResult = false;
                showPage();
            } else {
                Toast.makeText(context, getString(R.string.UnknownError), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
