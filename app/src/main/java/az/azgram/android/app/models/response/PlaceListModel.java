package az.azgram.android.app.models.response;

import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.PlaceModel;

import java.util.List;

/**
 * Created by Rzaaeeff on 4/10/2017.
 */

public class PlaceListModel extends CoreModel {
    private List<PlaceModel> places;
    private String text;

    public List<PlaceModel> getPlaces() {
        return places;
    }

    public void setPlaces(List<PlaceModel> places) {
        this.places = places;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
