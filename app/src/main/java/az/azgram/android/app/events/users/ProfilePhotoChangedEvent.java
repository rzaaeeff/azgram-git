package az.azgram.android.app.events.users;

import az.azgram.android.app.models.CoreModel;

/**
 * Created by Rzaaeeff on 7/28/2017.
 */

public class ProfilePhotoChangedEvent {
    private CoreModel model;

    public ProfilePhotoChangedEvent(CoreModel model) {
        this.model = model;
    }

    public CoreModel getModel() {
        return model;
    }

    public void setModel(CoreModel model) {
        this.model = model;
    }
}
