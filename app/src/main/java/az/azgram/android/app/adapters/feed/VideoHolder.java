package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterComment;
import az.azgram.android.app.custom.CustomLinkableTextView;
import az.azgram.android.app.events.ui.OptionsButtonClickedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.toro.SimpleVideoObject;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.LinkMechanism;
import az.azgram.android.app.operations.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.exoplayer2.ExoVideoView;
import im.ene.toro.extended.ExtVideoViewHolder;

/**
 * Created by Rzaaeeff on 1/11/2017.
 */

public class VideoHolder extends MediaHolder implements View.OnClickListener {
    // Constants
    public static final int LAYOUT_RES = R.layout.recycler_item_video_post_list;

    // Widgets
    private ImageView imageViewControl;
    private ImageView imageViewControlBackground;

    // Other elements
    private boolean isControlVisible = true;
    private boolean userWantsToPlay = false;
    private Handler handlerControl = new Handler();
    private Runnable runnableControl = new Runnable() {
        @Override
        public void run() {
            hideControl();
        }
    };

    public VideoHolder(View itemView, Context context) {
        super(itemView, context);

        imageViewControl = (ImageView) itemView.findViewById(R.id.imageViewControlPostsListItem);
        imageViewControlBackground = (ImageView) itemView.findViewById(R.id.imageViewControlBackgroundPostsListItem);

        imageViewControlBackground.setOnClickListener(this);
        videoView.setOnClickListener(this);
        imageViewControl.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.videoViewPostPostsListItem:
            case R.id.imageViewControlBackgroundPostsListItem:
                if (isControlVisible) {
                    hideControl();
                } else {
                    showControl();
                }
                break;
            case R.id.imageViewControlPostsListItem:
                try {
                    if (isPlaying()) {
                        pause();
                        userWantsToPlay = false;
                    }
                    else {
                        preparePlayer(true);
                        userWantsToPlay = true;
                    }
                } catch (Exception exc) {
                }
                break;
            default:
                super.onClick(v);
                break;
        }
    }

    private void showControl() {
        isControlVisible = true;

        imageViewControl.setVisibility(View.VISIBLE);
        imageViewControlBackground.setVisibility(View.VISIBLE);

        handlerControl.removeCallbacks(runnableControl);
        handlerControl.postDelayed(runnableControl, 1000);
    }

    private void hideControl() {
        isControlVisible = false;

        imageViewControl.setVisibility(View.GONE);
        imageViewControlBackground.setVisibility(View.GONE);

        handlerControl.removeCallbacks(runnableControl);
    }

    @Override
    public void start() {
        if (userWantsToPlay) {
            super.start();
        }
    }

    @Override
    public void onPlaybackStarted() {
        super.onPlaybackStarted();

        imageViewControl.setImageResource(R.drawable.ic_pause_white);
        showControl();
    }

    @Override
    public void onPlaybackPaused() {
        super.onPlaybackPaused();

        imageViewControl.setImageResource(R.drawable.ic_play_white);
        userWantsToPlay = false;
    }

    @Override
    public void onPlaybackCompleted() {
        super.onPlaybackCompleted();

        imageViewControl.setImageResource(R.drawable.ic_play_white);
        userWantsToPlay = false;
    }

    @Override
    public boolean onPlaybackError(Exception error) {
        imageViewControl.setImageResource(R.drawable.ic_play_white);

        return super.onPlaybackError(error);
    }
}
