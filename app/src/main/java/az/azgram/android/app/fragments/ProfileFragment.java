package az.azgram.android.app.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import az.azgram.android.app.AzGram;
import az.azgram.android.app.R;
import az.azgram.android.app.activities.AccountActivity;
import az.azgram.android.app.activities.CoreActivity;
import az.azgram.android.app.activities.SettingsActivity;
import az.azgram.android.app.adapters.RecyclerAdapterGifts;
import az.azgram.android.app.adapters.RecyclerAdapterPostsCollection;
import az.azgram.android.app.adapters.RecyclerAdapterQuestions;
import az.azgram.android.app.adapters.feed.RecyclerAdapterPostsList;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.custom.SpacesItemDecoration;
import az.azgram.android.app.events.posts.GiftsFeedReceivedEvent;
import az.azgram.android.app.events.posts.PostDeletedEvent;
import az.azgram.android.app.events.posts.PostsCollectionReceivedEvent;
import az.azgram.android.app.events.posts.PostsListReceivedEvent;
import az.azgram.android.app.events.posts.QuestionReceivedEvent;
import az.azgram.android.app.events.posts.TaggedPostReceivedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.GiftModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.QuestionModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.FeedModel;
import az.azgram.android.app.models.response.GiftFeedModel;
import az.azgram.android.app.models.response.QuestionFeedModel;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;
import com.squareup.otto.Bus;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.Toro;
import im.ene.toro.ToroAdapter;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    // Constants
    private static final byte CONTENT_PAGE_COUNT = 5;
    private static final byte POSTS_COLLECTION_PAGE = 0;
    private static final byte POSTS_LIST_PAGE = 1;
    private static final byte TAGS_PAGE = 2;
    private static final byte GIFTS_PAGE = 3;
    private static final byte QUESTIONS_PAGE = 4;
    private static final int[] THEMES = {
            R.style.ProfileFragmentColorOption1,
            R.style.ProfileFragmentColorOption2,
            R.style.ProfileFragmentColorOption3,
    };
    private static final int DEFAULT_THEME_INDEX = UserModel.ProfileDesign.OPTION3;

    // Widgets
    private RecyclerView recyclerViewContent;
    private ImageButton[] imageButtons;
    private TextView textViewFullName, textViewBio, textViewPostCount, textViewEmpty,
            textViewFollowerCount, textViewFollowingCount, textViewQuestionCount;
    private ImageView imageViewUserBadge, imageViewCover;
    private CircleImageView imageViewProfilePhoto;
    private ImageButton imageButtonSettings;
    private ProgressBar progressBarLoadMore;
    private NestedScrollView scrollView;

    // Other elements
    private RecyclerAdapterPostsCollection adapterPostsCollection,
            adapterTags;
    private RecyclerAdapterGifts adapterGifts;
    private ToroAdapter adapterPostsList;
    private RecyclerAdapterQuestions adapterQuestions;
    private RecyclerView.ItemDecoration itemDecoration =
            new SpacesItemDecoration(RecyclerAdapterPostsCollection.SPACING);
    private FeedModel feedPostsCollection = null, feedPostsList = null,
            feedTag = null;
    private GiftFeedModel feedGift = null;
    private QuestionFeedModel feedQuestion = null;
    private byte currentPage = -1;
    private boolean[] isEmpty = new boolean[CONTENT_PAGE_COUNT];
    private boolean[] isInitialized = new boolean[CONTENT_PAGE_COUNT];
    private boolean isLoading = false;
    private String[] emptyText;

    private Bus bus = new Bus();
    private Context context;
    private UserModel request;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        context = getContext();

        // Setting theme
        final int prefferedTheme;
        if (CoreActivity.user.getProfileDesign() != null) {
            prefferedTheme = CoreActivity.user.getProfileDesign();
        } else {
            prefferedTheme = DEFAULT_THEME_INDEX;
        }

        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), THEMES[prefferedTheme]);
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);

        // Inflate the layout for this fragment
        View view = localInflater.inflate(R.layout.fragment_profile, container, false);

        init(view);
        initUser(CoreActivity.user);
        initValues(CoreActivity.user);
        setListeners();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        bus.register(this);
        Toro.register(recyclerViewContent);
    }

    @Override
    public void onPause() {
        super.onPause();

        bus.unregister(this);
        Toro.unregister(recyclerViewContent);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // first load
        if (currentPage == -1) {
            currentPage = 0;
        }

        if (isVisibleToUser) {
            Toro.register(recyclerViewContent);

            if (!isInitialized[currentPage]) {
                loadMore(currentPage);

            }
        } else {
            if (recyclerViewContent != null) Toro.unregister(recyclerViewContent);
        }
    }

    /**
     * Assigns IDs and does initialization...
     */
    private void init(View view) {
        // Standard initialization
        // TextViews
        textViewFullName = (TextView) view.findViewById(R.id.textViewFullNameProfileFragment);
        textViewBio = (TextView) view.findViewById(R.id.textViewBioProfileFragment);
        textViewPostCount = (TextView) view.findViewById(R.id.textViewPostCountProfileFragment);
        textViewEmpty = (TextView) view.findViewById(R.id.textViewEmptyProfileFragment);
        textViewFollowerCount = (TextView) view.findViewById(R.id.textViewFollowerCountProfileFragment);
        textViewFollowingCount = (TextView) view.findViewById(R.id.textViewFollowingCountProfileFragment);
        textViewQuestionCount = (TextView) view.findViewById(R.id.textViewQuestionCountProfileFragment);

        imageViewUserBadge = (ImageView) view.findViewById(R.id.imageViewUserBadgeProfileFragment);
        imageViewProfilePhoto = (CircleImageView) view.findViewById(R.id.imageViewProfilePhotoProfileFragment);
        imageViewCover = (ImageView) view.findViewById(R.id.main_backdrop_ProfileFragment);

        imageButtonSettings = (ImageButton) view.findViewById(R.id.buttonSettingProfileFragment);

        recyclerViewContent = (RecyclerView) view.findViewById(R.id.recyclerViewContentProfileFragment);
        recyclerViewContent.setNestedScrollingEnabled(false);
        recyclerViewContent.setHasFixedSize(true);
        recyclerViewContent.setItemViewCacheSize(30);
        recyclerViewContent.setDrawingCacheEnabled(true);
        recyclerViewContent.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // Navigation Buttons
        imageButtons = new ImageButton[CONTENT_PAGE_COUNT];
        imageButtons[POSTS_COLLECTION_PAGE] = (ImageButton) view.findViewById(R.id.imageButtonPostsCollectionProfileFragment);
        imageButtons[POSTS_LIST_PAGE] = (ImageButton) view.findViewById(R.id.imageButtonPostsListProfileFragment);
        imageButtons[TAGS_PAGE] = (ImageButton) view.findViewById(R.id.imageButtonTagsProfileFragment);
        imageButtons[GIFTS_PAGE] = (ImageButton) view.findViewById(R.id.imageButtonGiftsProfileFragment);
        imageButtons[QUESTIONS_PAGE] = (ImageButton) view.findViewById(R.id.imageButtonQuestionsProfileFragment);

        progressBarLoadMore = (ProgressBar) view.findViewById(R.id.progressBarProfileFragment);
        scrollView = (NestedScrollView) view.findViewById(R.id.scrollViewProfileFragment);

        // Retaining fragment instance
        setRetainInstance(true);

        // Showing default page
        //changePage(POSTS_COLLECTION_PAGE);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == textViewFollowerCount.getId()) {
            UniversalListenerMethods.onClickFollower(request.getUsername(), context);
        } else if (v.getId() == textViewFollowingCount.getId()) {
            UniversalListenerMethods.onClickFollowing(request.getUsername(), context);
        } else if (v.getId() == imageButtonSettings.getId()) {
            Intent intent = new Intent(context, SettingsActivity.class);
            context.startActivity(intent);
        } else {
            for (byte position = 0; position < imageButtons.length; position++)
                if (v.getId() == imageButtons[position].getId())
                    if (!isLoading) {
                        if (isEmpty[position]) {
                            showPage(position);
                            return;
                        }

                        if (!isInitialized[position]) {
                            loadMore(position);

                            recyclerViewContent.setVisibility(View.GONE);
                            textViewEmpty.setVisibility(View.GONE);
                        } else {
                            currentPage = position;
                            showPage(position);
                        }
                    }
        }
    }

    private void initUser(UserModel user) {
        // Setting data
        textViewFullName.setText(user.getFullName());
        textViewBio.setText(user.getInfo());
        textViewPostCount.setText(String.format(getString(R.string.ProfileFragmentPostCountText), user.getPostCount()));
        textViewFollowerCount.setText(String.format(getString(R.string.ProfileFragmentFollowerCountText), user.getFollowerCount()));
        textViewFollowingCount.setText(String.format(getString(R.string.ProfileFragmentFollowingCountText), user.getFollowingCount()));
        textViewQuestionCount.setText(String.format(getString(R.string.ProfileFragmentQuestionCountText), user.getQuestionCount()));

        if (user.getProfilePhoto() != null) {
            String photoUrl = user.getProfilePhoto().getLargestPossible();

            // Profile Photo
            Utility.loadWithPicasso(
                    context,
                    photoUrl,
                    imageViewProfilePhoto,
                    R.drawable.profile_photo_placeholder
            );

            // Cover Photo
            Utility.loadWithPicasso(
                    context,
                    photoUrl,
                    imageViewCover,
                    R.drawable.profile_photo_placeholder
            );

        }

//        imageViewUserBadge.setImageResource(
//                request.isOnline() ?
//                        R.drawable.user_online_badge :
//                        R.drawable.user_offline_badge
//        );
    }

    private void initValues(UserModel user) {
        // We assume no page will be empty
        // We assume no page is initialized
        for (int i = 0; i < isEmpty.length; i++) {
            isEmpty[i] = false;
            isInitialized[i] = false;
        }

        // Getting error messages for empty page
        emptyText = context.getResources().getStringArray(R.array.ProfileFragmentEmptyTextArray);

        // Creating request
        request = new UserModel();
        request.setUsername(user.getUsername());
        request.setApiKey(Keys.API_KEY);
        request.setToken(user.getToken());

        // Feed
        int defaultPage = 0;

        feedPostsCollection = new FeedModel();
        List<PostModel> posts = new ArrayList<>();
        feedPostsCollection.setPosts(posts);
        feedPostsCollection.setPage(defaultPage);

        feedPostsList = new FeedModel();
        posts = new ArrayList<>();
        feedPostsList.setPosts(posts);
        feedPostsList.setPage(defaultPage);

        feedTag = new FeedModel();
        posts = new ArrayList<>();
        feedTag.setPosts(posts);
        feedTag.setPage(defaultPage);

        feedQuestion = new QuestionFeedModel();
        List<QuestionModel> questions = new ArrayList<>();
        feedQuestion.setQuestions(questions);
        feedQuestion.setPage(defaultPage);

        feedGift = new GiftFeedModel();
        List<GiftModel> gifts = new ArrayList<>();
        feedGift.setGifts(gifts);
        feedGift.setPage(defaultPage);
    }

    private void setListeners() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if (isLoading)
                        return;

                    loadMore(currentPage);
                    Log.d(AzGram.TAG, "onScrollChange: " + currentPage);
                }
            }
        });

        // Navigation listeners
        for (ImageButton imageButton : imageButtons) {
            imageButton.setOnClickListener(this);
        }

        textViewFollowerCount.setOnClickListener(this);
        textViewFollowingCount.setOnClickListener(this);

        imageButtonSettings.setOnClickListener(this);
    }

    private void startLoading() {
        isLoading = true;
        progressBarLoadMore.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        progressBarLoadMore.setVisibility(View.GONE);
    }

    private void loadMore(byte position) {
        if (isEmpty[position]) {
            showPage(position);
            return;
        }

        currentPage = position;
        NetworkOperations service = new NetworkOperations(context, bus);

        switch (position) {
            case POSTS_COLLECTION_PAGE:
                if (feedPostsCollection.getPage() == null)
                    return;

                request.setPage(feedPostsCollection.getPage() + 1);
                service.getPostsEndpoint().getUserPostsCollection(request);
                startLoading();
                break;
            case POSTS_LIST_PAGE:
                if (feedPostsList.getPage() == null)
                    return;

                request.setPage(feedPostsList.getPage() + 1);
                service.getPostsEndpoint().getUserPostsList(request);
                startLoading();
                break;
            case TAGS_PAGE:
                if (feedTag.getPage() == null)
                    return;

                request.setPage(feedTag.getPage() + 1);
                service.getPostsEndpoint().getTaggedPosts(request);
                startLoading();
                break;
            case GIFTS_PAGE:
                if (feedGift.getPage() == null)
                    return;

                request.setPage(feedGift.getPage() + 1);
                service.getPostsEndpoint().getGifts(request);
                startLoading();
                break;
            case QUESTIONS_PAGE:
                if (feedQuestion.getPage() == null)
                    return;

                request.setPage(feedQuestion.getPage() + 1);
                service.getPostsEndpoint().getQuestions(request);
                startLoading();
                break;
        }
    }

    private void showPage(byte position) {
        // if page is empty just skip process, show message
        if (isEmpty[position]) {
            recyclerViewContent.setAdapter(null);
            textViewEmpty.setVisibility(View.VISIBLE);
            textViewEmpty.setText(emptyText[position]);
            return;
        } else {
            recyclerViewContent.setVisibility(View.VISIBLE);
            textViewEmpty.setVisibility(View.GONE);
        }

        RecyclerView.LayoutManager layoutManager;
        recyclerViewContent.removeItemDecoration(itemDecoration);

        switch (position) {
            case POSTS_COLLECTION_PAGE:
                if (adapterPostsCollection == null) {
                    adapterPostsCollection = new RecyclerAdapterPostsCollection(context, feedPostsCollection);
                    adapterPostsCollection.setHasStableIds(true);
                }

                recyclerViewContent.setAdapter(adapterPostsCollection);

                layoutManager = new GridLayoutManager(context,
                        RecyclerAdapterPostsCollection.COLUMN_COUNT);
                layoutManager.setAutoMeasureEnabled(true);
                recyclerViewContent.setLayoutManager(layoutManager);

                recyclerViewContent.addItemDecoration(itemDecoration);

                break;

            case POSTS_LIST_PAGE:

                if (adapterPostsList == null) {
                    adapterPostsList = new RecyclerAdapterPostsList(context, feedPostsList);
                    adapterPostsList.setHasStableIds(true);
                }

                recyclerViewContent.setAdapter(adapterPostsList);

                layoutManager = new LinearLayoutManager(context);
                layoutManager.setAutoMeasureEnabled(true);
                recyclerViewContent.setLayoutManager(layoutManager);

                break;

            case TAGS_PAGE:

                if (adapterTags == null) {
                    adapterTags = new RecyclerAdapterPostsCollection(context, feedTag);
                    adapterTags.setHasStableIds(true);
                }

                recyclerViewContent.setAdapter(adapterTags);

                layoutManager = new GridLayoutManager(context,
                        RecyclerAdapterPostsCollection.COLUMN_COUNT);
                layoutManager.setAutoMeasureEnabled(true);
                recyclerViewContent.setLayoutManager(layoutManager);

                recyclerViewContent.addItemDecoration(itemDecoration);

                break;

            case GIFTS_PAGE:
                if (adapterGifts == null) {
                    adapterGifts = new RecyclerAdapterGifts(context, feedGift);
                    adapterGifts.setHasStableIds(true);
                }

                recyclerViewContent.setAdapter(adapterGifts);

                layoutManager = new GridLayoutManager(getActivity(),
                        RecyclerAdapterPostsCollection.COLUMN_COUNT);
                layoutManager.setAutoMeasureEnabled(true);
                recyclerViewContent.setLayoutManager(layoutManager);

                recyclerViewContent.addItemDecoration(itemDecoration);

                break;

            case QUESTIONS_PAGE:
                if (adapterQuestions == null) {
                    adapterQuestions = new RecyclerAdapterQuestions(context, feedQuestion, request);
                    adapterQuestions.setHasStableIds(true);
                }

                recyclerViewContent.setAdapter(adapterQuestions);

                layoutManager = new LinearLayoutManager(context);
                layoutManager.setAutoMeasureEnabled(true);
                recyclerViewContent.setLayoutManager(layoutManager);

                break;
        }
    }

    @Subscribe
    @Produce
    public void onResponse(PostsCollectionReceivedEvent event) {
        stopLoading();
        isInitialized[POSTS_COLLECTION_PAGE] = true;

        if (event.getModel() != null) {
            if (event.getModel().getPosts() != null) {
                feedPostsCollection.getPosts().addAll(event.getModel().getPosts());
                feedPostsCollection.setPage(event.getModel().getPage());

                if (currentPage == POSTS_COLLECTION_PAGE)
                    showPage(POSTS_COLLECTION_PAGE);
            } else if (event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                isEmpty[POSTS_COLLECTION_PAGE] = true;
                showPage(POSTS_COLLECTION_PAGE);
            }
        }
    }

    @Subscribe
    @Produce
    public void onResponse(PostsListReceivedEvent event) {
        stopLoading();
        isInitialized[POSTS_LIST_PAGE] = true;

        if (event.getModel() != null) {
            if (event.getModel().getPosts() != null) {
                feedPostsList.getPosts().addAll(event.getModel().getPosts());
                feedPostsList.setPage(event.getModel().getPage());

                if (currentPage == POSTS_LIST_PAGE)
                    showPage(POSTS_LIST_PAGE);
            } else if (event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                isEmpty[POSTS_LIST_PAGE] = true;
                showPage(POSTS_LIST_PAGE);
            }
        }
    }

    @Subscribe
    @Produce
    public void onResponse(TaggedPostReceivedEvent event) {
        stopLoading();
        isInitialized[TAGS_PAGE] = true;

        if (event.getModel() != null) {
            if (event.getModel().getPosts() != null) {
                feedTag.getPosts().addAll(event.getModel().getPosts());
                feedTag.setPage(event.getModel().getPage());

                if (currentPage == TAGS_PAGE)
                    showPage(TAGS_PAGE);
            } else if (event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                isEmpty[TAGS_PAGE] = true;
                showPage(TAGS_PAGE);
            }
        }
    }

    @Subscribe
    @Produce
    public void onResponse(GiftsFeedReceivedEvent event) {
        stopLoading();
        isInitialized[GIFTS_PAGE] = true;

        if (event.getModel() != null) {
            if (event.getModel().getGifts() != null) {
                feedGift.getGifts().addAll(event.getModel().getGifts());
                feedGift.setPage(event.getModel().getPage());

                if (currentPage == GIFTS_PAGE)
                    showPage(GIFTS_PAGE);
            } else if (event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                isEmpty[GIFTS_PAGE] = true;
                showPage(GIFTS_PAGE);
            }
        }
    }

    @Subscribe
    @Produce
    public void onResponse(QuestionReceivedEvent event) {
        stopLoading();
        isInitialized[QUESTIONS_PAGE] = true;

        if (event.getModel() != null) {
            if (event.getModel().getQuestions() != null) {
                feedQuestion.getQuestions().addAll(event.getModel().getQuestions());
                feedQuestion.setPage(event.getModel().getPage());

                if (currentPage == QUESTIONS_PAGE)
                    showPage(QUESTIONS_PAGE);
            } else if (event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                isEmpty[QUESTIONS_PAGE] = true;
                showPage(QUESTIONS_PAGE);
            }
        }
    }

    @Produce
    @Subscribe
    public void onAzGramServiceResponse(PostDeletedEvent event) {
        CoreModel post = event.getModel();

        if (post.getMessageId() == NetworkOperations.API_SUCCESS) {
            for (int i = 0; i < feedPostsList.getPosts().size(); i++) {
                if (post.getId().equals(feedPostsList.getPosts().get(i).getId())) {
                    feedPostsList.getPosts().remove(i);
                    adapterPostsList.notifyItemRemoved(i);
                }
            }
        }
    }
}