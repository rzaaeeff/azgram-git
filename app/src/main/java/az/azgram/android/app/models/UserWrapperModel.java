package az.azgram.android.app.models;

/**
 * Created by Rzaaeeff on 7/26/2017.
 */
public class UserWrapperModel extends CoreModel {
    private UserModel target;
    private UserModel author;
    private String text;

    public UserModel getTarget() {
        return target;
    }

    public void setTarget(UserModel target) {
        this.target = target;
    }

    public UserModel getAuthor() {
        return author;
    }

    public void setAuthor(UserModel author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
