package az.azgram.android.app.adapters.notification.holder.first;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import az.azgram.android.app.R;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 5/19/2017.
 */

public class PhotoNotificationHolder extends TextNotificationHolder implements View.OnClickListener {
    public static final int LAYOUT_RES = R.layout.recycler_item_notification_photo;
    // Widgets
    private ImageView imageViewPhoto;

    public PhotoNotificationHolder(View itemView, final Context context) {
        super(itemView, context);

        imageViewPhoto = (ImageView) itemView.findViewById(R.id.imageViewPhoto_NotificationItem);

        setRootListener(this);
        imageViewPhoto.setOnClickListener(this);
    }

    public void bind(NotificationModel notification, CharSequence text) {
        super.bind(notification, text);

        Utility.loadWithPicasso(
                context,
                notification.getPosts().get(0).getPhoto().getSmallestPossible(),
                imageViewPhoto,
                R.drawable.post_photo_placeholder
        );
    }

    public void bind(final NotificationModel notification, String textFormat) {
        SpannableString username = UniversalListenerMethods.getHighlightedAndClickableUsername(
                notification.getUsers().get(0),
                context
        );

        bind(notification, TextUtils.concat(username, " ", textFormat));
    }

    @Override
    public void onClick(View v) {
        UniversalListenerMethods.onClickPost(
                notification.getPosts().get(0).getId(),
                context
        );
    }
}