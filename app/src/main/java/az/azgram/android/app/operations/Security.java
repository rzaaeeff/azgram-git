package az.azgram.android.app.operations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Rzaaeeff on 12/5/2016.
 */

/**
 * @author Rzaaeeff
 */

public class Security {

    @Retention(RetentionPolicy.SOURCE)
    public @interface Algorithm {}

    public static final String MD5 = "MD5";
    public static final String SHA256 = "SHA-256";

    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static String getHash(String stringToHash, @Algorithm String hashAlgorithm) {
        MessageDigest digest = null;
        String hash = null;

        try {
            digest = MessageDigest.getInstance(hashAlgorithm);
            digest.update(stringToHash.getBytes());

            hash = bytesToHexString(digest.digest());
        } catch (NoSuchAlgorithmException e1) {
        }

        return hash;
    }
}
