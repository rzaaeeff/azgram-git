package az.azgram.android.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.users.ForgotEvent;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Utility;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordActivity extends AppCompatActivity {

    // Widgets
    private EditText editTextEmail;

    Bus bus = new Bus();
    boolean requested = false;
    Dialog loadingDialog = null;
    ForgotTask forgotTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        editTextEmail = (EditText) findViewById(R.id.editTextFPAEmail);
    }

    @Override
    protected void onResume() {
        super.onResume();

        requested = false;
        bus.register(this);
    }

    @Override
    protected void onPause() {
        requested = true;
        bus.unregister(this);
        if (forgotTask != null) forgotTask.cancel(true);

        super.onPause();
    }

    public void sendButtonClicked(View view) {
        if (editTextEmail.getText().toString().trim().length() > 0) {
            String email = editTextEmail.getText().toString().trim();

            if (Utility.isValidEmailAddress(email)) {
                UserModel user = new UserModel();
                user.setEmail(email);
                user.setApiKey(Keys.API_KEY);
                // Unique ANDROID_ID
                user.setDeviceId(Utility.getDeviceId(this));
                user.setIpAddress(Utility.getIPAddress(true));

                send(user);
            } else {
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageInvalidEmail)
                );
            }
        }
    }

    private void send(UserModel user) {
        if (!requested) {
            requested = true;

            loadingDialog = Interaction.getInstance(this).showLoadingDialog(
                    getString(R.string.PleaseWait)
            );

            if (forgotTask == null) {
                forgotTask = new ForgotTask(this, user);
                forgotTask.execute();
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(ForgotEvent event) {
        switch (event.getModel().getMessageId()) {
            case NetworkOperations.API_SUCCESS:
                setResult(Activity.RESULT_OK);
                finish();
                break;
            case NetworkOperations.NOT_CONNECTED:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.NotConnected)
                );
                break;
            case NetworkOperations.CONNECTED_NOT_ONLINE:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.NotOnline)
                );
                break;
            case NetworkOperations.Users.NOT_EXISTING_ACCOUNT:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.ForgotPasswordActivityMessageNotExistingAccount)
                );
                Utility.clearFields((ViewGroup) findViewById(R.id.activity_forgot_password));
                break;
            default:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.UnknownError) + "\n"
                                + getString(R.string.TryAgain)
                );
                break;
        }

        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog = null;
            }
        }

        requested = false;
    }

    public void closeActivity(View view) {
        finish();
    }

    // To support fonts
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private class ForgotTask extends AsyncTask<Void, Void, Integer> {
        private Context context;
        private UserModel user;

        public ForgotTask(Context context, UserModel user) {
            this.context = context;
            this.user = user;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            int code = NetworkOperations.UNKNOWN_ERROR;

            if (!isCancelled()) {
                NetworkOperations network = new NetworkOperations(context);
                code = network.isConnectedAndOnline();
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer code) {
            if (!isCancelled()) {
                switch (code) {
                    case NetworkOperations.CONNECTED_AND_ONLINE:
                        NetworkOperations service = new NetworkOperations(context, bus);
                        service.getUsersEndpoint().forgot(user);
                        break;
                    default:
                        UserModel error = new UserModel();
                        error.setMessageId(code);

                        bus.post(new ForgotEvent(error));
                        break;
                }
            }

            forgotTask = null;
        }
    }
}
