package az.azgram.android.app.operations;

import az.azgram.android.app.models.UserModel;

/**
 * Created by Rzaaeeff on 6/30/2017.
 */

public interface ProfileOptionsListener {
    void onShareProfileAsMessageClicked(UserModel user);
    void onSubscribeClicked(UserModel user);
    void onSendGiftClicked(UserModel user);
    void onReportClicked(UserModel user);
    void onBlockClicked(UserModel user);
}
