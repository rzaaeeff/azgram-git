package az.azgram.android.app.events.posts;

import az.azgram.android.app.models.CoreModel;

/**
 * Created by Rzaaeeff on 2/2/2017.
 */

public class ShareEvent {
    private CoreModel model;

    public ShareEvent(CoreModel model) {
        this.model = model;
    }

    public CoreModel getModel() {
        return model;
    }

    public void setModel(CoreModel model) {
        this.model = model;
    }
}
