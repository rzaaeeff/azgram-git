package az.azgram.android.app.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rzaaeeff on 11/30/2016.
 */

public class MessageModel extends CoreModel implements Serializable {

    public class Type {
        public static final byte TEXT = 0;
        public static final byte PHOTO = 1;
        public static final byte AUDIO = 2;
        public static final byte POST = 3;
        public static final byte USER = 4;
    }

    public class Status {

        public static final byte SENDING = 0;
        public static final byte SENT = 1;
        public static final byte NOT_SENT = 2;
    }

    // Fields
    private UserModel author;
    private String content;
    private List<CoreModel> recipients;
    private ConversationModel conversation;
    private MessageModel messageContent;
    private PhotoModel photo;
    private PostModel post;
    private UserModel user;
    private String audio;
    private Byte type;
    private Byte status = Status.SENT;
    private Boolean seen;

    public List<CoreModel> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<CoreModel> recipients) {
        this.recipients = recipients;
    }

    public UserModel getAuthor() {
        return author;
    }

    public void setAuthor(UserModel author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public PostModel getPost() {
        return post;
    }

    public void setPost(PostModel post) {
        this.post = post;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public PhotoModel getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoModel photo) {
        this.photo = photo;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Boolean isSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getStatus() {
        return status;
    }

    public ConversationModel getConversation() {
        return conversation;
    }

    public void setConversation(ConversationModel conversation) {
        this.conversation = conversation;
    }

    public MessageModel getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(MessageModel messageContent) {
        this.messageContent = messageContent;
    }
}