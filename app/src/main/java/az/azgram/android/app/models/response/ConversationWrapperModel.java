package az.azgram.android.app.models.response;

import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.CoreModel;

/**
 * Created by Rzaaeeff on 6/21/2017.
 */

public class ConversationWrapperModel extends CoreModel {
    private ConversationModel conversation;

    public ConversationModel getConversation() {
        return conversation;
    }

    public void setConversation(ConversationModel conversation) {
        this.conversation = conversation;
    }
}
