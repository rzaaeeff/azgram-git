package az.azgram.android.app.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import az.azgram.android.app.fragments.GuestsFragment;
import az.azgram.android.app.fragments.HomeFragment;
import az.azgram.android.app.fragments.NotificationFragment;
import az.azgram.android.app.fragments.ProfileFragment;
import az.azgram.android.app.fragments.SearchFragment;

/**
 * Created by Rzaaeeff on 12/12/2016.
 */

public class PagerAdapterCoreActivity extends FragmentPagerAdapter {

    // Constants
    public static final int PAGE_COUNT = 6;

    public static final int HOME_PAGE = 0;
    public static final int SEARCH_PAGE = 1;
    public static final int NOTIFICATIONS_PAGE = 3;
    public static final int GUESTS_PAGE = 4;
    public static final int PROFILE_PAGE = 5;


    public PagerAdapterCoreActivity(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment page;

        switch (position) {
            case HOME_PAGE:
                page = new HomeFragment();
                break;
            case SEARCH_PAGE:
                page = new SearchFragment();
                break;
            case NOTIFICATIONS_PAGE:
                page = new NotificationFragment();
                break;
            case GUESTS_PAGE:
                page = new GuestsFragment();
                break;
            case PROFILE_PAGE:
                page = new ProfileFragment();
                break;
            default:
                page = new Fragment();
        }

        return page;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
