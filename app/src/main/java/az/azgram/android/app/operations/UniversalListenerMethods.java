package az.azgram.android.app.operations;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import az.azgram.android.app.activities.ProfileActivity;
import az.azgram.android.app.activities.SinglePostActivity;
import az.azgram.android.app.activities.UserListActivity;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;

/**
 * Created by Rzaaeeff on 2/21/2017.
 */

public class UniversalListenerMethods {
    public static void onClickUser(String text, Context context) {
        String username = text.replace("@", "");
        if (!ProfileActivity.isSameUser(username)) {
            String mainUsername = new Cache(context).loadUser().getUsername();
            if (!username.equals(mainUsername)) {
                Intent intent = new Intent(context, ProfileActivity.class);
                UserModel user = new UserModel();
                user.setUsername(username);
                intent.putExtra(UserModel.UID, user);
                context.startActivity(intent);
            }
        }
    }

    public static void onClickFollower(String username, Context context) {
        UserModel user = new UserModel();
        user.setUsername(username);

        Intent intent = new Intent(context, UserListActivity.class);
        intent.putExtra(UserModel.UID, user);
        intent.putExtra(UserListActivity.EXTRA_TYPE, UserListActivity.Type.Follower);
        context.startActivity(intent);
    }

    public static void onClickFollowing(String username, Context context) {
        UserModel user = new UserModel();
        user.setUsername(username);

        Intent intent = new Intent(context, UserListActivity.class);
        intent.putExtra(UserModel.UID, user);
        intent.putExtra(UserListActivity.EXTRA_TYPE, UserListActivity.Type.Following);
        context.startActivity(intent);
    }

    public static void onClickPost(long postId, Context context) {
        PostModel post = new PostModel();
        post.setId(postId);

        Intent intent = new Intent(context, SinglePostActivity.class);
        intent.putExtra(PostModel.UID, post);
        context.startActivity(intent);
    }

    public static SpannableString getHighlightedAndClickableUsername(final UserModel user, final Context context) {
        SpannableString username = new SpannableString(user.getUsername());
        username.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                onClickUser(user.getUsername(), context);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(Color.parseColor("#337ab7"));
//                ds.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                ds.setUnderlineText(false);
            }
        }, 0, username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        username.setSpan(new ForegroundColorSpan(Color.parseColor("#337ab7")), 0, username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return username;
    }
}