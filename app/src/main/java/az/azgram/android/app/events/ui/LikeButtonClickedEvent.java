package az.azgram.android.app.events.ui;

/**
 * Created by Rzaaeeff on 1/26/2017.
 */

public class LikeButtonClickedEvent {
    private int position;

    public LikeButtonClickedEvent(int model) {
        this.position = model;
    }

    public int getPosition() {

        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
