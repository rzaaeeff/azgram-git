package az.azgram.android.app.adapters.chat;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import az.azgram.android.app.R;
import az.azgram.android.app.models.MessageModel;

/**
 * Created by Rzaaeeff on 5/3/2017.
 */

class TextHolder extends MessageHolder {
    static final int LAYOUT_RES_IN = R.layout.recycler_item_message_text_incoming;
    static final int LAYOUT_RES_OUT = R.layout.recycler_item_message_text_outgoing;

    private TextView textViewMessage;

    public TextHolder(View itemView, Context context) {
        super(itemView, context);

        textViewMessage = (TextView) itemView.findViewById(R.id.textViewMessage_MessageItem);
    }

    public void bind(MessageModel message, boolean shouldShowDetails) {
        super.bind(message, shouldShowDetails);

        if (message.getContent() != null) {
            textViewMessage.setText(message.getContent());
        } else {
            textViewMessage.setVisibility(View.GONE);
        }
    }
}
