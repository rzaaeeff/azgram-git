package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterComment;
import az.azgram.android.app.custom.CustomLinkableTextView;
import az.azgram.android.app.events.ui.OptionsButtonClickedEvent;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.LinkMechanism;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;
import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.ToroAdapter;
import im.ene.toro.exoplayer2.ExoVideoView;
import im.ene.toro.extended.ExtVideoViewHolder;

/**
 * Created by Rzaaeeff on 8/8/2017.
 */

public class PostHolder extends ToroAdapter.ViewHolder implements View.OnClickListener {
    // Constants

    // Widgets
    CircleImageView imageViewProfilePhoto;
    ImageView imageViewUserBadge;
    TextView textViewAuthor, textViewLike, textViewDislike,
            textViewComment, textViewShare, textViewPlace;
    CustomLinkableTextView textViewPost;
    ToggleButton toggleButtonLike, toggleButtonDislike;
    ImageButton imageButtonComment, imageButtonOptions;
    TextView textViewViewAll;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    // Other elements
    Context context;
    PostModel post;
    private ReactionClickListener reactionClickListener;

    public PostHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;

        imageViewProfilePhoto = (CircleImageView) itemView.findViewById(R.id.imageViewProfilePhotoPostsListItem);
        imageViewUserBadge = (ImageView) itemView.findViewById(R.id.imageViewUserBadgePostsListItem);
        textViewAuthor = (TextView) itemView.findViewById(R.id.textViewAuthorPostsListItem);
        textViewPost = (CustomLinkableTextView) itemView.findViewById(R.id.textViewPostPostsListItem);
        textViewLike = (TextView) itemView.findViewById(R.id.textViewLikePostsListItem);
        textViewDislike = (TextView) itemView.findViewById(R.id.textViewDislikePostsListItem);
        textViewComment = (TextView) itemView.findViewById(R.id.textViewCommentPostsListItem);
        textViewShare = (TextView) itemView.findViewById(R.id.textViewShareCountPostsListItem);
        textViewViewAll = (TextView) itemView.findViewById(R.id.textViewAllPostsListItem);
        textViewPlace = (TextView) itemView.findViewById(R.id.textViewPlacePostsListItem);
        recyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerViewCommentsPostsListItem);
        toggleButtonLike = (ToggleButton) itemView.findViewById(R.id.toggleButtonLikePostsListItem);
        toggleButtonDislike = (ToggleButton) itemView.findViewById(R.id.toggleButtonDislikePostsListItem);
        imageButtonComment = (ImageButton) itemView.findViewById(R.id.imageButtonCommentPostsListItem);
        imageButtonOptions = (ImageButton) itemView.findViewById(R.id.imageButtonOptionsPostsListItem);

        reactionClickListener = new ReactionClickListener(context);
        textViewLike.setOnClickListener(this);
        textViewDislike.setOnClickListener(this);
        toggleButtonLike.setOnClickListener(this);
        toggleButtonDislike.setOnClickListener(this);
        textViewAuthor.setOnClickListener(this);
        imageViewProfilePhoto.setOnClickListener(this);
    }

    @Override
    public void bind(RecyclerView.Adapter adapter, @Nullable Object object) {
        post = (PostModel) object;

        Utility.loadWithPicasso(
                context,
                post.getAuthor().getProfilePhoto().getSmallestPossible(),
                imageViewProfilePhoto,
                R.drawable.profile_photo_placeholder
        );

        if (post.getAuthor().isOnline() != null) {
            imageViewUserBadge.setImageResource(
                    post.getAuthor().isOnline() ?
                            R.drawable.user_online_badge :
                            R.drawable.user_offline_badge
            );
        }

        textViewAuthor.setText(post.getAuthor().getUsername());

        if (post.getText() != null && !post.getText().isEmpty()) {
            List<View> views = new ArrayList<>();
            views.add(textViewPost);
            LinkMechanism.set(views, context);
            textViewPost.setText(post.getText());
        } else {
            textViewPost.setVisibility(View.GONE);
        }

        textViewLike.setText(String.valueOf(post.getLikeCount()));
        textViewDislike.setText(String.valueOf(post.getDislikeCount()));
        textViewComment.setText(
                String.format(
                        RecyclerAdapterPostsList.COMMENT_COUNT_FORMAT,
                        post.getCommentCount()
                )
        );
        textViewShare.setText(
                String.format(
                        RecyclerAdapterPostsList.SHARE_COUNT_FORMAT,
                        post.getShareCount()
                )
        );

        setToggleButtonStates(post);

        // RecyclerView for comments
        if (post.getCommentCount() > 0) {
            RecyclerView.LayoutManager lm = new LinearLayoutManager(context);
            lm.setAutoMeasureEnabled(true);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.setLayoutManager(lm);
            adapter = new RecyclerAdapterComment(post.getComments(), context);
            recyclerView.setAdapter(adapter);

            if (post.getCommentCount() - 2 > 0) {
                textViewViewAll.setText(
                        String.format(
                                RecyclerAdapterPostsList.VIEW_ALL_FORMAT,
                                post.getCommentCount()
                        )
                );

                // We only need ID of uploadPhoto
                final PostModel minPost = new PostModel();
                minPost.setId(post.getId());
                minPost.setCommentCount(post.getCommentCount());

                textViewViewAll.setClickable(true);
                textViewViewAll.setOnClickListener(
                        new CommentButtonOnClickListener(context, minPost, false)
                );
            } else {
                textViewViewAll.setVisibility(View.GONE);
            }
        } else {
            recyclerView.setVisibility(View.GONE);
            textViewViewAll.setVisibility(View.GONE);
        }

        final PostModel minPost = new PostModel();
        minPost.setId(post.getId());
        minPost.setAuthor(new UserModel());
        minPost.getAuthor().setUsername(post.getAuthor().getUsername());
        minPost.setCommentCount(post.getCommentCount());

        imageButtonComment.setOnClickListener(
                new CommentButtonOnClickListener(context, minPost, true)
        );

        imageButtonOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BusProvider.getInstance().post(new OptionsButtonClickedEvent(post));
            }
        });

        if (post.getPlace() != null) {
            textViewPlace.setText(post.getPlace().getName());
        } else {
            textViewPlace.setVisibility(View.GONE);
        }
    }

    private void setToggleButtonStates(PostModel post) {
        if (post.isLiked() != null) {
            toggleButtonLike.setChecked(post.isLiked());
        }

        if (post.isDisliked() != null) {
            toggleButtonDislike.setChecked(post.isDisliked());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textViewLikePostsListItem:
                reactionClickListener.onLikeCountClicked(post);
                break;
            case R.id.textViewDislikePostsListItem:
                reactionClickListener.onDislikeCountClicked(post);
                break;
            case R.id.textViewAuthorPostsListItem:
            case R.id.imageViewProfilePhotoPostsListItem:
                UniversalListenerMethods.onClickUser(post.getAuthor().getUsername(), context);
                break;
            case R.id.toggleButtonLikePostsListItem:
                likeButtonClicked();
                break;
            case R.id.toggleButtonDislikePostsListItem:
                dislikeButtonClicked();
                break;
        }
    }

    private void likeButtonClicked() {
        RecyclerAdapterPostsList.request.setId(post.getId());
        RecyclerAdapterPostsList.request.setUsername(post.getAuthor().getUsername());
        RecyclerAdapterPostsList.serviceInstance.like(RecyclerAdapterPostsList.request);

        if (post.isDisliked()) {
            undoDislikePost(post);
        }

        if (post.isLiked()) {
            undoLikePost(post);
        } else {
            likePost(post);
        }

        setToggleButtonStates(post);
    }

    private void dislikeButtonClicked() {
        RecyclerAdapterPostsList.request.setId(post.getId());
        RecyclerAdapterPostsList.request.setUsername(post.getAuthor().getUsername());
        RecyclerAdapterPostsList.serviceInstance.dislike(RecyclerAdapterPostsList.request);

        if (post.isLiked()) {
            undoLikePost(post);
        }

        if (post.isDisliked()) {
            undoDislikePost(post);
        } else {
            dislikePost(post);
        }

        setToggleButtonStates(post);
    }

    private void likePost(PostModel post) {
        post.setLikeCount(post.getLikeCount() + 1);
        post.setLiked(true);
        textViewLike.setText(String.valueOf(post.getLikeCount()));
    }

    private void undoLikePost(PostModel post) {
        post.setLikeCount(post.getLikeCount() - 1);
        post.setLiked(false);
        textViewLike.setText(String.valueOf(post.getLikeCount()));
    }

    private void dislikePost(PostModel post) {
        post.setDislikeCount(post.getDislikeCount() + 1);
        post.setDisliked(true);
        textViewDislike.setText(String.valueOf(post.getDislikeCount()));
    }

    private void undoDislikePost(PostModel post) {
        post.setDislikeCount(post.getDislikeCount() - 1);
        post.setDisliked(false);
        textViewDislike.setText(String.valueOf(post.getDislikeCount()));
    }

    @Override
    public void onAttachedToWindow() {}

    @Override
    public void onDetachedFromWindow() {}
}
