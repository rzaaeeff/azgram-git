package az.azgram.android.app.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StyleRes;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import az.azgram.android.app.R;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rzaaeeff on 2/21/2017.
 */

public class GuestDetailDialog extends Dialog implements View.OnClickListener {
    // Widgets
    private ImageView imageViewProfile, imageViewBadge;
    private TextView textViewName, textViewTime, textViewInfo,
            textViewPost, textViewFollower, textViewFollowing;
    private Button button;

    // Other elements
    private UserModel guest;
    private Context context;

    protected GuestDetailDialog(Context context) {
        super(context);
        this.context = context;
    }

    protected GuestDetailDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
    }

    protected GuestDetailDialog(Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        this.context = context;
    }

    public void setGuest(UserModel guest) {
        this.guest = guest;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // request window feature
        setContentView(R.layout.dialog_guest_detail);

        init();
    }

    private void init() {
        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfilePhotoGuestDialog);
        imageViewBadge = (ImageView) findViewById(R.id.imageViewUserBadgeGuestDialog);
        textViewName = (TextView) findViewById(R.id.textViewNameGuestDialog);
        textViewTime = (TextView) findViewById(R.id.textViewTimeGuestDialog);
        textViewInfo = (TextView) findViewById(R.id.textViewInfoGuestDetailDialog);
        textViewPost = (TextView) findViewById(R.id.textViewPostGuestDetailDialog);
        textViewFollower = (TextView) findViewById(R.id.textViewFollowerGuestDetailDialog);
        textViewFollowing = (TextView) findViewById(R.id.textViewFollowingGuestDetailDialog);
        button = (Button) findViewById(R.id.buttonGuestDetailDialog);
    }

    private void setListeners() {
        button.setOnClickListener(this);
        imageViewProfile.setOnClickListener(this);
        textViewName.setOnClickListener(this);
    }

    private void initViews() {
        if (guest.getProfilePhoto() != null &&
                guest.getProfilePhoto().getLargestPossible() != null) {
            Utility.loadWithPicasso(
                    context,
                    guest.getProfilePhoto().getLargestPossible(),
                    imageViewProfile,
                    R.drawable.profile_photo_placeholder
            );
        }

        imageViewBadge.setImageResource(
                guest.isOnline() ? R.drawable.user_online_badge :
                        R.drawable.user_offline_badge
        );

        if (guest.getFullName() != null) {
            if (guest.getFullName().length() > 12)
                textViewName.setText(guest.getUsername());
            else
                textViewName.setText(guest.getFullName());
        } else {
            textViewName.setText(guest.getUsername());
        }

        textViewInfo.setText(guest.getInfo());

        textViewPost.setText(String.format(
                context.getString(R.string.ProfileFragmentPostCountText),
                guest.getPostCount()
        ));

        textViewFollower.setText(String.format(
                context.getString(R.string.ProfileFragmentFollowerCountText),
                guest.getFollowerCount()
        ));

        textViewFollowing.setText(String.format(
                context.getString(R.string.ProfileFragmentFollowingCountText),
                guest.getFollowingCount()
        ));

        try {
            SimpleDateFormat format = new SimpleDateFormat(CoreModel.DATE_TIME_FORMAT);
            Date date = format.parse(guest.getDate());
            textViewTime.setText(DateUtils.getTimeAgo(date, context));
        } catch (Exception exc) {
            textViewTime.setText(guest.getDate());
        }
    }

    @Override
    public void show() {
        super.show();

        setListeners();
        initViews();
    }

    @Override
    public void onClick(View view) {
        if (guest.getUsername() != null) {
            UniversalListenerMethods.onClickUser("@" + guest.getUsername(), context);
        }
    }
}
