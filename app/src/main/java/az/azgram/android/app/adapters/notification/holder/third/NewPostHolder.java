package az.azgram.android.app.adapters.notification.holder.third;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.holder.first.PhotoNotificationHolder;
import az.azgram.android.app.models.NotificationModel;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class NewPostHolder extends PhotoNotificationHolder {
    public NewPostHolder(View itemView, Context context) {
        super(itemView, context);

        TEXT_FORMAT = context.getString(R.string.notification_new_post);
    }

    public void bind(NotificationModel notification) {
        super.bind(notification, TEXT_FORMAT);
    }
}