package az.azgram.android.app;

import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.adobe.creativesdk.foundation.AdobeCSDKFoundation;
import com.adobe.creativesdk.foundation.auth.IAdobeAuthClientCredentials;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.operations.DownloadManagerProvider;
import az.azgram.android.app.operations.FirebaseTokenHandler;
import az.azgram.android.app.services.AppUpdateChecker;

/**
 * Created by Rzaaeeff on 11/9/2016.
 */

public class AzGram extends Application implements IAdobeAuthClientCredentials {
    public static final String TAG = "RzLog";

    private static final String CREATIVE_SDK_CLIENT_ID = Keys.CSDK_CLIENT_ID;
    private static final String CREATIVE_SDK_CLIENT_SECRET = Keys.CSDK_CLIENT_SECRET;
    private static final String CREATIVE_SDK_REDIRECT_URI = Keys.CSDK_REDIRECT_URI;
    private static final String[] CREATIVE_SDK_SCOPES = Keys.CSDK_SCOPES;

    private static int nextNotificationId = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        // For editing photos
        AdobeCSDKFoundation.initializeCSDKFoundation(getApplicationContext());

        if (isMainProcess()) {
            // For caching photos
            Picasso.Builder builder = new Picasso.Builder(this);
            builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
            Picasso built = builder.build();
            built.setIndicatorsEnabled(true);   // TODO: disable in release
            built.setLoggingEnabled(false);
            Picasso.setSingletonInstance(built);

            // For GIF, using fresco by facebook
            Fresco.initialize(this);

            // For downloading
            DownloadManagerProvider.init(this);

            // Application Initializing
            notifyServer();
            createDirectories();

            // Check for update
            startService(new Intent(this, AppUpdateChecker.class));

            // TODO: remove after testing
            if (!FirebaseApp.getApps(this).isEmpty()) {
                Log.d(TAG, "onCreate: FirebaseToken: " + FirebaseInstanceId.getInstance().getToken());
            }
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public String getClientID() {
        return CREATIVE_SDK_CLIENT_ID;
    }

    @Override
    public String getClientSecret() {
        return CREATIVE_SDK_CLIENT_SECRET;
    }

    @Override
    public String[] getAdditionalScopesList() {
        return CREATIVE_SDK_SCOPES;
    }

    @Override
    public String getRedirectURI() {
        return CREATIVE_SDK_REDIRECT_URI;
    }

    private void initSettings() {
    }

    // your package name is the same with your main process name
    private boolean isMainProcess() {
        return getPackageName().equals(getProcessName());
    }

    // you can use this method to get current process name, you will get
    // name like "com.package.name"(main process name) or "com.package.name:remote"
    private String getProcessName() {
        int mypid = android.os.Process.myPid();
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> infos = manager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo info : infos) {
            if (info.pid == mypid) {
                return info.processName;
            }
        }
        // may never return null
        return null;
    }



    private void notifyServer() {
        // TODO: check conditions and tell server that the user is online
        // TODO: server needs to send unseen notifications&messages

        FirebaseTokenHandler tokenHandler = new FirebaseTokenHandler(this);
        tokenHandler.sendToServer();
    }

    private void createDirectories() {
        for (File directory : Constants.Directory.ALL_DIRS) {
            try {
                if (!directory.exists() || !directory.isDirectory()) {
                    directory.mkdirs();
                }
            } catch (Exception exc) {
            }
        }
    }

    public static int getNextNotificationId() {
        return nextNotificationId++;
    }
}