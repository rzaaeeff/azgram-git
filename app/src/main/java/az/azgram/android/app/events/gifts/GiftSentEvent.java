package az.azgram.android.app.events.gifts;

import az.azgram.android.app.models.CoreModel;

/**
 * Created by Rzaaeeff on 1/26/2017.
 */

public class GiftSentEvent {
    private CoreModel model;

    public GiftSentEvent(CoreModel model) {
        this.model = model;
    }

    public CoreModel getModel() {
        return model;
    }

    public void setModel(CoreModel model) {
        this.model = model;
    }
}
