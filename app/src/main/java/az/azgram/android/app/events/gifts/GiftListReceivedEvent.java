package az.azgram.android.app.events.gifts;

import az.azgram.android.app.models.response.GiftListModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class GiftListReceivedEvent {
    private GiftListModel model;

    public GiftListReceivedEvent(GiftListModel model) {
        this.model = model;
    }

    public GiftListModel getModel() {
        return model;
    }

    public void setModel(GiftListModel model) {
        this.model = model;
    }
}
