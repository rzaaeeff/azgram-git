package az.azgram.android.app.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dewarder.holdinglibrary.HoldingButtonLayout;
import com.dewarder.holdinglibrary.HoldingButtonLayoutListener;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import az.azgram.android.app.R;
import az.azgram.android.app.activities.PhotoPickerActivity;
import az.azgram.android.app.adapters.chat.RecyclerAdapterChat;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.messages.ConversationClearedEvent;
import az.azgram.android.app.events.messages.ConversationMarkedAsUnreadEvent;
import az.azgram.android.app.events.messages.ConversationReceivedEvent;
import az.azgram.android.app.events.messages.MessageReceivedEvent;
import az.azgram.android.app.events.messages.MessageSentEvent;
import az.azgram.android.app.events.ui.MessageRetryClickedEvent;
import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.models.PhotoModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.ChatController;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.FileUtils;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.MessagingUtils;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Utility;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment implements View.OnClickListener,
        MediaRecorder.OnErrorListener, MediaRecorder.OnInfoListener, ChatBottomMenuDialog.Controller {

    // Constants
    public static final String ARG_CONVERSATION_ID = "arg_conversation_id";
    private static final int REQUEST_PHOTO_PICK = 333;
    private static final String FILENAME_FORMAT = "AzGram-AudioMessage-%d.3gpp";
    private static final int HANDLER_DELAY = 10;

    // Widget
    private SuperRecyclerView recyclerView;
    private ImageButton imageButtonPick, imageButtonEmoji, imageButtonSend, imageButtonRecord;
    private EmojiconEditText editText;
    private TextView textViewError, textViewSlide;
    private LinearLayout layoutMessage;
    private HoldingButtonLayout holdingButtonLayout;

    // Other elements
    private Activity activity;
    private EmojIconActions emojiProvider;
    private ConversationModel data;
    private RecyclerAdapterChat adapter;
    private CoreModel request;
    private UserModel user;
    private boolean isLoading;
    private List<ChatController> controllers = new ArrayList<>();
    private ChatBottomMenuDialog dialogBottom;
    private NetworkOperations service;
    private long conversationId;
    private long localId;
    private ProgressDialog dialogLoading;
    private String imageLocalUrl;
    private SoundPool soundPool;
    private int soundStart, soundStop, soundDelete, soundError;
    private MediaRecorder recorder;
    private Toast toast;
    private long timeStart;
    private File fileOutput;
    private Handler handlerProgress = new Handler();
    private Runnable runnableProgress = new Runnable() {
        @Override
        public void run() {
            showProgress();

            handlerProgress.postDelayed(this, HANDLER_DELAY);
        }
    };

    public ChatFragment() {
        // Required empty public constructor
    }

    public void addController(ChatController controller) {
        controllers.add(controller);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        activity = getActivity();

        init(view);
        initValues();
        setListeners();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        BusProvider.getInstance().unregister(this);

        super.onStop();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_chat_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.more_ChatFragmentMenu:
                if (dialogBottom != null) {
                    dialogBottom.dismiss();
                    dialogBottom = null;
                }

                dialogBottom = new ChatBottomMenuDialog();
                dialogBottom.addController(this);
                dialogBottom.show(getFragmentManager(), Constants.BOTTOM_MENU_TAG);
                break;
        }

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PHOTO_PICK) {
            if (resultCode == Activity.RESULT_OK) {
                imageLocalUrl = data.getStringExtra("data");
                // indicating that photo is chosen
                imageButtonPick.setColorFilter(Color.parseColor("#337ab7"), PorterDuff.Mode.SRC_ATOP);
                setSendButtonVisible(true);
            }
        }
    }

    private void init(View view) {
        recyclerView = (SuperRecyclerView) view.findViewById(R.id.recyclerView_ChatFragment);
        imageButtonPick = (ImageButton) view.findViewById(R.id.imageButtonPick_ChatFragment);
        imageButtonEmoji = (ImageButton) view.findViewById(R.id.imageButtonEmoji_ChatFragment);
        imageButtonSend = (ImageButton) view.findViewById(R.id.imageButtonSend_ChatFragment);
        imageButtonRecord = (ImageButton) view.findViewById(R.id.imageButtonRecord_ChatFragment);
        editText = (EmojiconEditText) view.findViewById(R.id.editText_ChatFragment);
        textViewError = (TextView) view.findViewById(R.id.textViewError_ChatFragment);
        textViewSlide = (TextView) view.findViewById(R.id.textViewSlide_ChatFragment);
        layoutMessage = (LinearLayout) view.findViewById(R.id.layoutMessage_ChatFragment);
        holdingButtonLayout = (HoldingButtonLayout) view.findViewById(R.id.holdingButtonLayout_ChatFragment);

        emojiProvider = new EmojIconActions(activity, view, editText, imageButtonEmoji);
        emojiProvider.ShowEmojIcon();
    }

    private void initValues() {
        conversationId = getArguments().getLong(ARG_CONVERSATION_ID);

        setHasOptionsMenu(true);

        UserModel userTemp = new Cache(activity).loadUser();
        user = new UserModel();
        user.setUsername(userTemp.getUsername());
        user.setProfilePhoto(userTemp.getProfilePhoto());

        request = new CoreModel();
        request.setId(conversationId);
        request.setToken(userTemp.getToken());
        request.setApiKey(Keys.API_KEY);

        recyclerView.getSwipeToRefresh().setEnabled(false);
        recyclerView.getRecyclerView().setHasFixedSize(true);

        data = new ConversationModel();
        data.setMessages(new ArrayList<MessageModel>());
        data.setPage(0);

        adapter = new RecyclerAdapterChat(activity, data);
        adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setAutoMeasureEnabled(true);
        layoutManager.setReverseLayout(true);
        //layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);

        service = new NetworkOperations(activity, BusProvider.getInstance());
        service.getMessagesEndpoint();

//        editText.setUseSystemDefault(true);
//        emojiProvider.setUseSystemEmoji(true);

        // init
        loadMore();

        // Sound
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            soundPool = new SoundPool.Builder()
                    .setMaxStreams(1)
                    .build();
        } else {
            soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 1);
        }

        soundStart = soundPool.load(activity, R.raw.sound_start_recording, 1);
        soundStop = soundPool.load(activity, R.raw.sound_stop_recording, 1);
        soundError = soundPool.load(activity, R.raw.sound_error, 1);
        soundDelete = soundPool.load(activity, R.raw.sound_delete, 1);

        toast = Toast.makeText(activity, "", Toast.LENGTH_SHORT);
    }

    private void setListeners() {
        imageButtonSend.setOnClickListener(this);
        imageButtonPick.setOnClickListener(this);

        holdingButtonLayout.addListener(new HoldingButtonLayoutListener() {
            @Override
            public void onBeforeExpand() {
                layoutMessage.setVisibility(View.GONE);
                textViewSlide.setVisibility(View.VISIBLE);
                textViewSlide.setAlpha(Constants.ENABLED_STATE_ALPHA);

                playUISound(soundStart, 1);
            }

            @Override
            public void onExpand() {
                startRecording();
            }

            @Override
            public void onBeforeCollapse() {
            }

            @Override
            public void onCollapse(boolean isCancel) {
                layoutMessage.setVisibility(View.VISIBLE);
                textViewSlide.setVisibility(View.GONE);

                stopRecording(isCancel);
            }

            @Override
            public void onOffsetChanged(float offset, boolean isCancel) {
                float alpha = 1f - 1.5f * offset;
                if (alpha < 0)
                    alpha = 0;
                textViewSlide.setAlpha(alpha);
            }
        });

        recyclerView.setOnMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                loadMore();
            }
        });


        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (imageLocalUrl == null && s.toString().isEmpty()) {
                    setSendButtonVisible(false);
                } else {
                    setSendButtonVisible(true);
                }
            }
        });
    }

    private void showProgress() {
        long timeCurrent = System.nanoTime();

        int seconds = (int) TimeUnit.NANOSECONDS.toSeconds(timeCurrent - timeStart);
        int minutes = seconds / 1000;
        seconds %= 1000;

        DecimalFormat formatter = new DecimalFormat("00");
        toast.setText(formatter.format(minutes) + ":" + formatter.format(seconds));
        toast.show();
    }

    private void setSendButtonVisible(boolean visible) {
        if (visible) {
            imageButtonSend.setVisibility(View.VISIBLE);
            imageButtonRecord.setVisibility(View.GONE);
            holdingButtonLayout.setEnabled(false);
        } else {
            imageButtonSend.setVisibility(View.GONE);
            imageButtonRecord.setVisibility(View.VISIBLE);
            holdingButtonLayout.setEnabled(true);
        }
    }

    private void startRecording() {
        try {
            timeStart = System.nanoTime();
            fileOutput = new File(
                    Constants.Directory.DIR_MESSAGES_AUDIO,
                    String.format(
                            FILENAME_FORMAT,
                            timeStart
                    )
            );

            recorder = new MediaRecorder();
            recorder.setOnErrorListener(this);
            recorder.setOnInfoListener(this);
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            recorder.setOutputFile(fileOutput.getAbsolutePath());
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            recorder.setAudioEncodingBitRate(Constants.AUDIO_ENCODING_BIT_RATE);
            recorder.setAudioChannels(2);

            recorder.prepare();
            recorder.start();

            handlerProgress.removeCallbacks(runnableProgress);
            handlerProgress.postDelayed(runnableProgress, HANDLER_DELAY);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    private void stopRecording(boolean canceled) {
        handlerProgress.removeCallbacks(runnableProgress);

        try {
            recorder.stop();
            recorder.release();
            recorder = null;

            if (canceled) {
                deleteFile();
                toast.cancel();
                playUISound(soundDelete, 3);
                return;
            }

            long duration = System.nanoTime() - timeStart;
            if (duration < TimeUnit.SECONDS.toNanos(1)) {
                toast.setText(R.string.TooShort);
                toast.show();
                playUISound(soundError, 3);
                deleteFile();
            } else {
                toast.cancel();
                send();
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            playUISound(soundStop, 2);
        }
    }

    private void deleteFile() {
        if (fileOutput != null) {
            try {
                fileOutput.delete();
            } finally {
                fileOutput = null;
            }
        }
    }

    private void send() {
        MessageModel message = new MessageModel();
        message.setLocalId(localId++);
        ConversationModel conversation = new ConversationModel();
        conversation.setId(conversationId);
        message.setRecipients(new ArrayList<CoreModel>());
        message.getRecipients().add(conversation);
        message.setApiKey(request.getApiKey());
        message.setAuthor(new UserModel());
        message.getAuthor().setToken(request.getToken());

        MessageModel messageLocal = new MessageModel();
        conversation = new ConversationModel();
        conversation.setId(conversationId);
        messageLocal.setRecipients(new ArrayList<CoreModel>());
        messageLocal.getRecipients().add(conversation);
        messageLocal.setApiKey(request.getApiKey());
        messageLocal.setAuthor(new UserModel());
        messageLocal.getAuthor().setUsername(user.getUsername());
        messageLocal.getAuthor().setProfilePhoto(user.getProfilePhoto());
        messageLocal.setLocalId(message.getLocalId());
        messageLocal.setStatus(MessageModel.Status.SENDING);
        messageLocal.setDate(DateUtils.getFormattedCurrentDate(CoreModel.DATE_TIME_FORMAT));

        // sending photo
        if (imageLocalUrl != null) {
            File filePhoto = new File(FileUtils.getRealPathFromURI(
                    activity, Uri.parse(imageLocalUrl)
            ));

            message.setType(MessageModel.Type.PHOTO);
            message.setContent(editText.getText().toString());
            messageLocal.setType(message.getType());
            messageLocal.setContent(message.getContent());
            messageLocal.setPhoto(new PhotoModel());
            messageLocal.getPhoto().setSmall(imageLocalUrl);

            service.getMessagesEndpoint().send(message, filePhoto);

            // changing button state back to normal
            imageButtonPick.setColorFilter(null);
            imageLocalUrl = null;
            editText.setText("");
        } else if (editText.getText().toString().trim().length() > 0) {
            message.setType(MessageModel.Type.TEXT);
            message.setContent(editText.getText().toString());
            messageLocal.setType(message.getType());
            messageLocal.setContent(message.getContent());

            service.getMessagesEndpoint().send(message);

            editText.setText("");
        } else if (fileOutput != null) {
            message.setType(MessageModel.Type.AUDIO);
            messageLocal.setType(MessageModel.Type.AUDIO);
            messageLocal.setAudio(fileOutput.getPath());

            service.getMessagesEndpoint().send(message, fileOutput);
            fileOutput = null;
        }

        data.getMessages().add(0, messageLocal); // add to front
        adapter.notifyDataSetChanged();

        if (textViewError.getVisibility() == View.VISIBLE)
            textViewError.setVisibility(View.GONE);
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
        recyclerView.setLoadingMore(loading);
        recyclerView.getSwipeToRefresh().setRefreshing(loading);
        recyclerView.getMoreProgressView().setVisibility(View.GONE);
    }

    private void loadMore() {
        if (isLoading)
            return;

        if (data.getPage() != null) {
            request.setPage(data.getPage() + 1);
        }

        service.getMessagesEndpoint().getMessages(request);
        setLoading(true);
    }

    private void playUISound(int soundId, int priority) {
        soundPool.play(soundId, 1, 1, priority, 0, 1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButtonSend_ChatFragment:
                send();
                break;
            case R.id.imageButtonPick_ChatFragment:
                startActivityForResult(
                        new Intent(activity, PhotoPickerActivity.class),
                        REQUEST_PHOTO_PICK
                );
                break;
        }
    }

    @Override
    public void markAsUnreadClicked() {
        dialogLoading = Interaction.getInstance(activity).showLoadingDialog(getString(R.string.PleaseWait));
        ConversationModel conversation = new ConversationModel();
        conversation.setAuthor(new UserModel());
        conversation.getAuthor().setToken(request.getToken());
        conversation.setApiKey(request.getApiKey());
        conversation.setId(conversationId);

        service.getMessagesEndpoint().markAsUnread(conversation);
    }

    @Override
    public void clearClicked() {
        if (data.getMessages().size() > 0) {
            dialogLoading = Interaction.getInstance(activity).showLoadingDialog(getString(R.string.PleaseWait));
            ConversationModel conversation = new ConversationModel();
            conversation.setAuthor(new UserModel());
            conversation.getAuthor().setToken(request.getToken());
            conversation.setApiKey(request.getApiKey());
            conversation.setId(conversationId);

            service.getMessagesEndpoint().clear(conversation);
        } else {
            Toast.makeText(activity, R.string.CHatFragmentNoMessageToClear, Toast.LENGTH_SHORT).show();
        }
    }

    @Produce
    @Subscribe
    public void onAzGramServiceResponse(ConversationReceivedEvent event) {
        setLoading(false);

        if (event.getModel().getMessageId() == null ||
                event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            data.getMessages().addAll(event.getModel().getMessages());
            data.setPage(event.getModel().getPage());

            adapter.notifyDataSetChanged();

            textViewError.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            return;
        }

        if (event.getModel().getMessageId() == NetworkOperations.Messages.MESSAGES_NOT_FOUND) {
            textViewError.setVisibility(View.VISIBLE);
            textViewError.setText(R.string.ChatFragmentNoMessageError);
        }
    }

    private MessageModel getMessageWithId(long id) {
        for (MessageModel message : data.getMessages())
            if (message.getId() != null &&
                    message.getId() == id)
                return message;

        return null;
    }

    private int getMessagePositionWithLocalId(long localId) {
        for (int i = 0; i < data.getMessages().size(); i++) {
            MessageModel message = data.getMessages().get(i);

            if (message.getLocalId() != null &&
                    message.getLocalId() == localId) {
                return i;
            }
        }

        return RecyclerView.NO_POSITION;
    }

    @Override
    public void onError(MediaRecorder mr, int what, int extra) {
        long duration = System.nanoTime() - timeStart;
        if (duration < TimeUnit.SECONDS.toNanos(1)) {
            toast.setText(R.string.TooShort);
            toast.show();
            playUISound(soundError, 2);
            deleteFile();
        } else {
            toast.cancel();
        }
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        switch (what) {
            case MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:
                Toast.makeText(
                        activity,
                        R.string.StatusDialogFragmentMaxDuration,
                        Toast.LENGTH_SHORT
                ).show();
                break;
            case MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:
                Toast.makeText(
                        activity,
                        R.string.StatusDialogFragmentMaxSize,
                        Toast.LENGTH_SHORT
                ).show();
                break;
            default:
                onError(mr, what, extra);
        }
    }

    @Produce
    @Subscribe
    public void onAzGramServiceResponse(MessageSentEvent event) {
        int position = getMessagePositionWithLocalId(event.getModel().getLocalId());
        if (position != RecyclerView.NO_POSITION) {
            MessageModel message = data.getMessages().get(position);
            if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
                message.setStatus(MessageModel.Status.SENT);
                message.setId(event.getModel().getMessageContent().getId());
                message.setAuthor(event.getModel().getMessageContent().getAuthor());
            } else {
                message.setStatus(MessageModel.Status.NOT_SENT);
            }

            adapter.notifyItemChanged(position);
        }
    }

    @Produce
    @Subscribe
    public void onUIEvent(MessageRetryClickedEvent event) {
        int position = event.getPositionOfMessage();

        if (position != RecyclerView.NO_POSITION) {
            MessageModel source = data.getMessages().get(position);
            source.setStatus(MessageModel.Status.SENDING);
            adapter.notifyItemChanged(position);

            MessageModel messageToSend = (MessageModel) Utility.getDeepCopy(source, MessageModel.class);
            messageToSend.getAuthor().setUsername(null);
            messageToSend.getAuthor().setToken(request.getToken());
            messageToSend.setDate(null);
            messageToSend.setAudio(null);
            messageToSend.setPhoto(null);
            messageToSend.setStatus(null);

            try {
                switch (messageToSend.getType()) {
                    case MessageModel.Type.TEXT:
                        service.getMessagesEndpoint().send(messageToSend);
                        break;
                    case MessageModel.Type.PHOTO:
                        File filePhoto = new File(FileUtils.getRealPathFromURI(
                                activity, Uri.parse(messageToSend.getPhoto().getSmallestPossible())
                        ));

                        service.getMessagesEndpoint().send(messageToSend, filePhoto);
                        break;
                    case MessageModel.Type.AUDIO:
                        File fileAudio = new File(source.getAudio());

                        service.getMessagesEndpoint().send(messageToSend, fileAudio);
                        break;
                }
            } catch (Exception exc) {
                source.setStatus(MessageModel.Status.NOT_SENT);
                adapter.notifyItemChanged(position);
            }
        }
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(ConversationMarkedAsUnreadEvent event) {
        if (dialogLoading != null) dialogLoading.hide();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            activity.onBackPressed();
        } else {
            Toast.makeText(activity, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(ConversationClearedEvent event) {
        if (dialogLoading != null) dialogLoading.hide();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            int sizeOfData = data.getMessages().size();
            data.getMessages().clear();
            adapter.notifyItemRangeRemoved(0, sizeOfData);
            textViewError.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(activity, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    @Produce
    public void onAzGramPushNotification(MessageReceivedEvent event) {
        MessageModel message = event.getModel();

        if (message.getConversation().getId().equals(conversationId)) {
            data.getMessages().add(0, message);
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
//            adapter.notifyItemInserted(0);
        } else {
            MessagingUtils.showNotification(activity, message);
        }
    }
}