package az.azgram.android.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.users.SignInEvent;
import az.azgram.android.app.models.AppSettingsModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.FirebaseTokenHandler;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Security;
import az.azgram.android.app.operations.Utility;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    // Widgets
    EditText editTextLogin, editTextPassword;
    Button buttonAz, buttonEn, buttonRu, buttonTr;

    // Data
    String login, passwordHash;

    boolean requested = false;
    Bus bus = new Bus();
    Dialog loadingDialog = null;
    SignInTask signInTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        assignIds();

        buttonAz.setOnClickListener(this);
        buttonEn.setOnClickListener(this);
        buttonRu.setOnClickListener(this);
        buttonTr.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        bus.register(this);
        requested = false;
    }

    @Override
    protected void onPause() {
        bus.unregister(this);
        requested = true;
        if (signInTask != null) signInTask.cancel(true);

        super.onPause();
    }

    // Helper method to assign ids
    private void assignIds() {
        editTextLogin = (EditText) findViewById(R.id.editTextLoginActivityLogin);
        editTextPassword = (EditText) findViewById(R.id.editTextLoginActivityPassword);
        buttonAz = (Button) findViewById(R.id.buttonAz);
        buttonEn = (Button) findViewById(R.id.buttonEn);
        buttonRu = (Button) findViewById(R.id.buttonRu);
        buttonTr = (Button) findViewById(R.id.buttonTr);
    }

    /**
     * Buton click event
     *
     * @param view
     */
    public void signInClicked(View view) {
        if (getCurrentFocus() instanceof EditText) {
            Utility.hideSoftKeyboard(getCurrentFocus(), this);
        }

        if (editTextLogin.getText().toString().trim().length() > 0 &&
                editTextPassword.getText().toString().trim().length() > 0) {

            login = editTextLogin.getText().toString().trim();
            passwordHash = Security.getHash(
                    editTextPassword.getText().toString().trim(),
                    Security.MD5
            );

            // Creating user model
            UserModel user = new UserModel();
            user.setLogin(login);
            user.setPasswordHash(passwordHash);
            user.setApiKey(Keys.API_KEY);
            // Unique Android ID
            user.setDeviceId(Utility.getDeviceId(this));
            user.setIpAddress(Utility.getIPAddress(true));

//            To show the example input to the backend developer...
//            String json = new Gson().toJson(user, UserModel.class);

            signIn(user);
        } else {
            // Empty field
            Interaction.getInstance(this).showErrorDialog(
                    getString(R.string.LoginActivityMessageFillAllFields));

        }

    }

    private void signIn(UserModel user) {
        if (!requested) {
            requested = true;

            // show loading dialog
            loadingDialog = Interaction.getInstance(this).showLoadingDialog(
                    getString(R.string.PleaseWait));

            if (signInTask == null) {
                signInTask = new SignInTask(this, user);
                signInTask.execute();
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(SignInEvent event) {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) loadingDialog.dismiss();
            loadingDialog = null;
        }

        switch (event.getModel().getMessageId()) {
            case NetworkOperations.API_SUCCESS:
                // TODO: Save user details to database or cache them somehow
                Cache cache = new Cache(this);
                event.getModel().setPasswordHash(passwordHash);
                cache.saveUser(event.getModel());
                startActivity(new Intent(this, CoreActivity.class));
                FirebaseTokenHandler tokenHandler = new FirebaseTokenHandler(this);
                tokenHandler.setTokenSent(false);
                tokenHandler.sendToServer();
                finish();
                break;
            case NetworkOperations.NOT_CONNECTED:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.NotConnected)
                );
                break;
            case NetworkOperations.CONNECTED_NOT_ONLINE:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.NotOnline)
                );
                break;
            case NetworkOperations.Users.WRONG_CREDENTIALS:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.LoginActivityMessageWrongCredentials)
                );
                Utility.clearFields((ViewGroup) findViewById(R.id.activity_login));
                break;
            default:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.UnknownError) + "\n"
                                + getString(R.string.TryAgain)
                );
                break;
        }

        requested = false;
    }

    // Opens Registration Window
    public void openRegistrationActivity(View view) {
        startActivity(new Intent(this, RegistrationActivity.class));
    }

    // Open ForgotPassword Window
    public void openForgotPasswordActivity(View view) {
        startActivityForResult(new Intent(this, ForgotPasswordActivity.class), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK)
            Interaction.getInstance(this).showSuccessDialog(
                    getString(R.string.ForgotPasswordActivityMessageSuccess)
            );
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonAz:
                changeLanguage(AppSettingsModel.LANGUAGE.AZ);
                break;
            case R.id.buttonEn:
                changeLanguage(AppSettingsModel.LANGUAGE.EN);
                break;
            case R.id.buttonRu:
                changeLanguage(AppSettingsModel.LANGUAGE.RU);
                break;
            case R.id.buttonTr:
                changeLanguage(AppSettingsModel.LANGUAGE.TR);
                break;
        }
    }

    private void changeLanguage(byte language) {
        Cache cache = new Cache(this);
        AppSettingsModel settings = cache.loadSettings();

        if (settings == null) {
            settings = new AppSettingsModel();
        }

        settings.setLanguage(language);

        cache.saveSettings(settings);
        startActivity(new Intent(this, StarterActivity.class));
        finish();
    }

    private class SignInTask extends AsyncTask<Void, Void, Integer> {
        private Context context;
        private UserModel user;

        public SignInTask(Context context, UserModel user) {
            this.context = context;
            this.user = user;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            int code = NetworkOperations.UNKNOWN_ERROR;

            if (!isCancelled()) {
                NetworkOperations network = new NetworkOperations(context);
                code = network.isConnectedAndOnline();
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer code) {
            if (!isCancelled()) {
                switch (code) {
                    case NetworkOperations.CONNECTED_AND_ONLINE:
                        NetworkOperations service = new NetworkOperations(context, bus);
                        service.getUsersEndpoint().signIn(user);
                        break;
                    default:
                        UserModel error = new UserModel();
                        error.setMessageId(code);

                        bus.post(new SignInEvent(error));
                        break;
                }
            }

            signInTask = null;
        }
    }
}
