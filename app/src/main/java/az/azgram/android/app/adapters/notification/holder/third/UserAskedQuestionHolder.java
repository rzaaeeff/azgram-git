package az.azgram.android.app.adapters.notification.holder.third;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.holder.second.TwoUsernamePostHolder;
import az.azgram.android.app.adapters.notification.holder.second.TwoUsernameTextHolder;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class UserAskedQuestionHolder extends TwoUsernameTextHolder {
    public UserAskedQuestionHolder(View itemView, final Context context) {
        super(itemView, context);

        TEXT_FORMAT = context.getString(R.string.notification_user_asked_question);

        setRootListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UniversalListenerMethods.onClickUser(
                        notification.getUsers().get(1).getUsername(),
                        context
                );
            }
        });
    }
}
