package az.azgram.android.app.events.users;

import az.azgram.android.app.models.response.UserListModel;

/**
 * Created by Rzaaeeff on 2/11/2017.
 */

public class GuestsReceivedEvent {
    private UserListModel model;

    public GuestsReceivedEvent(UserListModel model) {
        this.model = model;
    }

    public UserListModel getModel() {
        return model;
    }

    public void setModel(UserListModel model) {
        this.model = model;
    }
}
