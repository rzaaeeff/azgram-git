package az.azgram.android.app.operations;

import az.azgram.android.app.models.PlaceModel;

/**
 * Created by Rzaaeeff on 4/12/2017.
 */

public interface PlaceSelectionController {
    void onClick(PlaceModel place);
}
