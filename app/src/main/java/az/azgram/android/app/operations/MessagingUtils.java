package az.azgram.android.app.operations;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;

import com.squareup.picasso.Picasso;

import az.azgram.android.app.AzGram;
import az.azgram.android.app.R;
import az.azgram.android.app.activities.MessagingActivity;
import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.MessageModel;

/**
 * Created by Rzaaeeff on 6/12/2017.
 */

public class MessagingUtils {
    public static void showNotification(final Context context, final MessageModel message) {
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                Bitmap bitmap;

                try {
                    bitmap = Picasso.with(context).load(
                            message.getAuthor().getProfilePhoto().getSmallestPossible()
                    ).get();
                } catch (Exception exc) {
                    bitmap = null;
                }

                if (bitmap == null) {
                    bitmap = BitmapFactory.decodeResource(
                            context.getResources(),
                            R.drawable.profile_photo_placeholder
                    );
                }

                int width = Math.round(context.getResources().getDimension(android.R.dimen.notification_large_icon_width));
                int height = Math.round(context.getResources().getDimension(android.R.dimen.notification_large_icon_height));

                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);

                return ImageUtils.getCircleBitmap(bitmap);
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                StringBuilder preview = new StringBuilder(message.getAuthor().getUsername());

                switch (message.getType()) {
                    case MessageModel.Type.TEXT:
                        preview.append(": ").append(message.getContent());
                        break;
                    case MessageModel.Type.PHOTO:
                        preview.append(" ").append(context.getString(R.string.noficiation_user_sent_message_photo));
                        break;
                    case MessageModel.Type.AUDIO:
                        preview.append(" ").append(context.getString(R.string.noficiation_user_sent_message_audio));
                        break;
                    case MessageModel.Type.POST:
                        preview.append(" ").append(context.getString(R.string.noficiation_user_sent_message_post));
                        break;
                    case MessageModel.Type.USER:
                        preview.append(" ").append(context.getString(R.string.noficiation_user_sent_message_user));
                        break;
                }

                Intent intent = new Intent(context, MessagingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(MessagingActivity.EXTRA_MODE, MessagingActivity.MODE_OPEN_CONVERSATION);
                intent.putExtra(ConversationModel.UID, message.getConversation());
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(context)
                                .setContentTitle(context.getString(R.string.app_name))
                                .setLargeIcon(bitmap)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentIntent(pendingIntent)
                                .setContentText(preview.toString());

                Notification notification = notificationBuilder.build();
                notification.flags |= Notification.FLAG_AUTO_CANCEL;

                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(AzGram.getNextNotificationId(), notification);
            }
        }.execute();
    }
}