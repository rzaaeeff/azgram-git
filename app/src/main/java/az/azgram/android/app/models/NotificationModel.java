package az.azgram.android.app.models;

import java.util.List;

/**
 * Created by Rzaaeeff on 5/15/2017.
 */
public class NotificationModel extends CoreModel {
    public static final class Type {
        // Post notifications
        public static final byte COMMENTED_ON_POST = 1;
        public static final byte LIKED_POST = 2;
        public static final byte DISLIKED_POST = 3;
        public static final byte SHARED_POST = 7;
        public static final byte REPLIED_COMMENT = 8;
        public static final byte MENTIONED_YOU = 9;
        public static final byte NEW_POST = 16;
        public static final byte COMMENT_ON_SUBSCRIBED_POST = 22;

        // User notifications
        public static final byte STARTED_FOLLOWING = 4;
        public static final byte ACCEPTED_REQUEST = 5;
        public static final byte SENT_REQUEST = 6;
        public static final byte ASKED_QUESTION = 12;
        public static final byte ANSWERED_QUESTION = 13;
        public static final byte ANONYMOUS_QUESTION = 14;
        public static final byte SUBSCRIBED_YOU = 15;
        public static final byte SENT_MONEY = 19;
        public static final byte WANTS_MONEY = 20;
        public static final byte SENT_GIFT = 21;

        // System notifications
        public static final byte WELCOME_MESSAGE = 17;
        public static final byte SYSTEM_MESSAGE = 18;

        // User Actions
        public static final byte USER_LIKED = 23;
        public static final byte USER_DISLIKED = 24;
        public static final byte USER_COMMENTED = 25;
        public static final byte USER_REPLIED = 26;
        public static final byte USER_STARTED_FOLLOWING = 27;
        public static final byte USER_ASKED_QUESTION = 28;
        public static final byte USER_SENT_GIFT = 29;
    }

    private List<UserModel> users;
    private List<PostModel> posts;
    private String money;
    private String systemMessage;
    private Byte type;

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }

    public List<PostModel> getPosts() {
        return posts;
    }

    public void setPosts(List<PostModel> posts) {
        this.posts = posts;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getSystemMessage() {
        return systemMessage;
    }

    public void setSystemMessage(String systemMessage) {
        this.systemMessage = systemMessage;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}