package az.azgram.android.app.models.response;

import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.UserModel;

import java.util.List;

/**
 * Created by Rzaaeeff on 1/14/2017.
 */

public class UserListModel extends CoreModel {
    private List<UserModel> users;
    private String text;

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
