package az.azgram.android.app.models;

/**
 * Created by Rzaaeeff on 7/14/2017.
 */
public class MoneyWrapperModel extends CoreModel {
    private UserModel author;
    private UserModel user;
    private Long amount;

    public UserModel getAuthor() {
        return author;
    }

    public void setAuthor(UserModel author) {
        this.author = author;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
