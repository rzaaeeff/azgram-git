package az.azgram.android.app.events.ui;

/**
 * Created by Rzaaeeff on 6/28/2017.
 */

public class MessageRetryClickedEvent {
    private int positionOfMessage;

    public MessageRetryClickedEvent(int positionOfMessage) {
        this.positionOfMessage = positionOfMessage;
    }

    public int getPositionOfMessage() {
        return positionOfMessage;
    }

    public void setPositionOfMessage(int positionOfMessage) {
        this.positionOfMessage = positionOfMessage;
    }
}
