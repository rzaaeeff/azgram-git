package az.azgram.android.app.models.response;

import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.PostModel;

import java.util.List;

/**
 * Created by Rzaaeeff on 12/20/2016.
 */

public class FeedModel extends CoreModel {

    private List<PostModel> posts;
    private String text;

    public List<PostModel> getPosts() {
        return posts;
    }

    public void setPosts(List<PostModel> posts) {
        this.posts = posts;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
