package az.azgram.android.app.adapters.notification.holder.first;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.ImageAdapter;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 5/19/2017.
 */

public class PhotoNotificationBundledHolder extends TextNotificationHolder implements AdapterView.OnItemClickListener {
    public static final int LAYOUT_RES = R.layout.recycler_item_notification_photo_bundled;
    // Widgets
    private GridView gridView;
    private ImageAdapter imageAdapter;
    private List<PostModel> posts;

    public PhotoNotificationBundledHolder(View itemView, Context context) {
        super(itemView, context);

        gridView = (GridView) itemView.findViewById(R.id.gridView_NotificationItem);

        imageAdapter = new ImageAdapter(context, new ArrayList<PostModel>());
        gridView.setAdapter(imageAdapter);
        gridView.setOnItemClickListener(this);
    }

    public void bind(NotificationModel notification, CharSequence text) {
        super.bind(notification, text);

        posts = notification.getPosts();

        imageAdapter.updateData(posts);
        //imageAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UniversalListenerMethods.onClickPost(
                posts.get(position).getId(),
                context
        );
    }
}