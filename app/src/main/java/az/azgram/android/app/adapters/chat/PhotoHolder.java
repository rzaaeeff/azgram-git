package az.azgram.android.app.adapters.chat;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.fragments.PhotoDialog;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 5/3/2017.
 */

class PhotoHolder extends TextHolder implements View.OnClickListener {
    static final int LAYOUT_RES_IN = R.layout.recycler_item_message_photo_incoming;
    static final int LAYOUT_RES_OUT = R.layout.recycler_item_message_photo_outgoing;

    private ImageView imageViewMessage;

    public PhotoHolder(View itemView, Context context) {
        super(itemView, context);

        imageViewMessage = (ImageView) itemView.findViewById(R.id.imageViewMessage_MessageItem);
        super.setRootListener(this);
    }

    public void bind(MessageModel message, boolean shouldShowDetails) {
        super.bind(message, shouldShowDetails);

        Utility.loadWithPicasso(
                context,
                message.getPhoto().getSmallestPossible(),
                imageViewMessage,
                R.drawable.post_photo_placeholder
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rootLayoutMessage_MessageItem:
                PhotoDialog photoDialog = new PhotoDialog();
                Bundle args = new Bundle();
                args.putString(PhotoDialog.ARG_URL, message.getPhoto().getLargestPossible());
                photoDialog.setArguments(args);
                photoDialog.show(((AppCompatActivity) context).getSupportFragmentManager(), null);
                break;
        }
    }
}