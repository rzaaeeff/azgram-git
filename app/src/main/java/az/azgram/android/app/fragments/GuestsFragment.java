package az.azgram.android.app.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.mmin18.widget.RealtimeBlurView;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import az.azgram.android.app.R;
import az.azgram.android.app.activities.CoreActivity;
import az.azgram.android.app.adapters.RecyclerAdapterGuest;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.ui.GuestItemClickedEvent;
import az.azgram.android.app.events.users.GuestsClearedEvent;
import az.azgram.android.app.events.users.GuestsReceivedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.NetworkOperations;

/**
 * A simple {@link Fragment} subclass.
 */
public class GuestsFragment extends Fragment implements DialogInterface.OnCancelListener, DialogInterface.OnDismissListener {

    // Constant Fields
    private static final long MAX_LOADING_TIME = TimeUnit.SECONDS.toMillis(10);

    // Widgets
    private Button button;
    private SuperRecyclerView recyclerView;
    private ImageView imageViewBlur;
    private RealtimeBlurView realtimeBlurView;
    private LinearLayout linearLayoutBlur;

    // Other Fields
    private Context context;
    private RecyclerAdapterGuest adapter;
    private UserListModel result;
    private boolean isLoading = false;
    private CoreModel request;
    private NetworkOperations service;
    private boolean isInitialized = false;
    private GuestDetailDialog dialog;

    // Sometimes there is no response
    // we have to handle loading and
    // not make the user wait
    private Handler handlerLoading;
    private Runnable runnableLoading;

    public GuestsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_guests, container, false);

        context = getContext();

        init(view);
        initValues();
        setListeners();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        BusProvider.getInstance().unregister(this);

        super.onPause();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            if (!isInitialized) {
                isInitialized = true;
                loadMore();
            }
        }
    }

    private void init(View view) {
        button = (Button) view.findViewById(R.id.buttonClearGuestsFragment);
        recyclerView = (SuperRecyclerView) view.findViewById(R.id.recyclerViewGuestsFragment);
        imageViewBlur = (ImageView) view.findViewById(R.id.imageViewBlurGuestsFragment);
        realtimeBlurView = (RealtimeBlurView) view.findViewById(R.id.realtimeBlurGuestsFragment);
        linearLayoutBlur = (LinearLayout) view.findViewById(R.id.linearLayoutBlurGuestsFragment);

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), RecyclerAdapterGuest.COLUMN_COUNT));
        recyclerView.getSwipeToRefresh().setEnabled(false);
    }

    private void initValues() {
        request = new CoreModel();
        request.setApiKey(Keys.API_KEY);
        request.setToken(CoreActivity.user.getToken());
        request.setPage(0);

        result = new UserListModel();
        result.setUsers(new ArrayList<UserModel>());

        adapter = new RecyclerAdapterGuest(context, result);
        recyclerView.setAdapter(adapter);

        service = new NetworkOperations(context, BusProvider.getInstance());
        service.getUsersEndpoint();

        handlerLoading = new Handler();
        runnableLoading = new Runnable() {
            @Override
            public void run() {
                if (isLoading) {
                    stopLoading();
                }
            }
        };

        dialog = new GuestDetailDialog(context, R.style.GuestDetailDialogStyle);
    }

    private void setListeners() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearGuestList();
            }
        });

        recyclerView.setOnMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                loadMore();
            }
        });
    }

    private void startLoading() {
        isLoading = true;
        recyclerView.getMoreProgressView().setVisibility(View.VISIBLE);

        handlerLoading.postDelayed(runnableLoading, MAX_LOADING_TIME);
    }

    private void stopLoading() {
        isLoading = false;
        recyclerView.getMoreProgressView().setVisibility(View.GONE);

        handlerLoading.removeCallbacks(runnableLoading);
    }

    private void clearGuestList() {
        if (isLoading)
            return;

        startLoading();
        service.getUsersEndpoint().clearGuestList(request);
    }

    private void loadMore() {
        if (isLoading)
            return;

        if (request.getPage() != null) {
            startLoading();

            request.setPage(request.getPage() + 1);
            service.getUsersEndpoint().getGuestList(request);
        } else {
            stopLoading();
        }
    }

    private void showDetails(UserModel user) {
        realtimeBlurView.setVisibility(View.VISIBLE);

        dialog.setGuest(user);
        dialog.show();

        // making it full width
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.copyFrom(dialog.getWindow().getAttributes());
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(params);

        dialog.setOnCancelListener(this);
        dialog.setOnDismissListener(this);
    }

    private void dismissDialog() {
        realtimeBlurView.setVisibility(View.GONE);
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        dismissDialog();
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        dismissDialog();
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(GuestsClearedEvent event) {
        stopLoading();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            result.getUsers().clear();
            adapter.notifyDataSetChanged();
        } else {
            Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    @Produce
    public void onAzGramServiceResponse(GuestsReceivedEvent event) {
        stopLoading();

        if (event.getModel().getUsers() != null) {
            result.getUsers().addAll(event.getModel().getUsers());
            request.setPage(event.getModel().getPage());

            if (event.getModel().getPage() == null)
                recyclerView.setOnMoreListener(null);

            adapter.notifyDataSetChanged();
        } else if (event.getModel().getMessageId() == NetworkOperations.Users.EMPTY_GUEST_LIST) {
            // do nothing
            // getNotifications is empty
        } else {
            Toast.makeText(context, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    @Produce
    public void onGuestItemClicked(GuestItemClickedEvent event) {
        showDetails(event.getModel());
    }
}