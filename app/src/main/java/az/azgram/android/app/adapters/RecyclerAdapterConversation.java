package az.azgram.android.app.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import az.azgram.android.app.R;
import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.models.response.ConversationListModel;
import az.azgram.android.app.operations.ConversationController;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.Utility;
import uk.co.chrisjenx.calligraphy.CalligraphyUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rzaaeeff on 1/17/2017.
 */

public class RecyclerAdapterConversation extends RecyclerView.Adapter<RecyclerAdapterConversation.ConversationHolder> {

    private Context context;
    private ConversationListModel data;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private ConversationController controller;

    public RecyclerAdapterConversation(Context context, ConversationListModel data,
                                       ConversationController controller) {
        this.context = context;
        this.data = data;
        this.controller = controller;
    }

    @Override
    public ConversationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.recycler_item_conversation,
                parent,
                false
        );

        return new ConversationHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final ConversationHolder holder, final int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.getConversations().size();
    }

    class ConversationHolder extends RecyclerView.ViewHolder implements View.OnClickListener, ViewTreeObserver.OnGlobalLayoutListener {
        // Widgets
        SwipeRevealLayout swipeRevealLayout;
        ImageView imageViewProfilePhoto, imageViewUserBadge;
        TextView textViewAuthor, textViewPreview, textViewTime;
        ImageButton imageButtonDelete;
        ViewGroup primaryLayout;

        public ConversationHolder(View itemView) {
            super(itemView);

            swipeRevealLayout = (SwipeRevealLayout) itemView.findViewById(R.id.swipeRevealLayout_ConversationItem);
            imageViewProfilePhoto = (ImageView) itemView.findViewById(R.id.imageViewProfilePhoto_ConversationItem);
            imageViewUserBadge = (ImageView) itemView.findViewById(R.id.imageViewUserBadge_ConversationItem);
            textViewAuthor = (TextView) itemView.findViewById(R.id.textViewUsername_ConversationItem);
            textViewPreview = (TextView) itemView.findViewById(R.id.textViewPreview_ConversationItem);
            textViewTime = (TextView) itemView.findViewById(R.id.textViewTime_ConversationItem);
            imageButtonDelete = (ImageButton) itemView.findViewById(R.id.imageButtonDelete_ConversationItem);
            primaryLayout = (ViewGroup) itemView.findViewById(R.id.primaryLayout_ConversationItem);

            itemView.findViewById(R.id.textViewAccept_ConversationItem)
                    .setOnClickListener(this);
            itemView.findViewById(R.id.textViewReject_ConversationItem)
                    .setOnClickListener(this);
            primaryLayout.setOnClickListener(this);
            imageButtonDelete.setOnClickListener(this);
        }

        public void bind(int position) {
            ConversationModel conversation = data.getConversations().get(position);
            viewBinderHelper.bind(swipeRevealLayout, conversation.getId().toString());

            if (conversation.isPending()) {
                primaryLayout.getViewTreeObserver().addOnGlobalLayoutListener(this);
            } else {
                swipeRevealLayout.setLockDrag(true);
                imageButtonDelete.setVisibility(View.VISIBLE);
            }

            if (conversation.getAuthor().getProfilePhoto() != null &&
                    conversation.getAuthor().getProfilePhoto().getSmallestPossible() != null) {
                Utility.loadWithPicasso(
                        context,
                        conversation.getAuthor().getProfilePhoto().getSmallestPossible(),
                        imageViewProfilePhoto,
                        R.drawable.profile_photo_placeholder
                );
            }

            imageViewUserBadge.setImageResource(
                    conversation.getAuthor().isOnline() == null ||
                            !conversation.getAuthor().isOnline() ?
                            R.drawable.user_offline_badge :
                            R.drawable.user_online_badge
            );

            switch (conversation.getPreviewType()) {
                case MessageModel.Type.TEXT:
                    textViewPreview.setText(conversation.getPreview());
                    break;
                case MessageModel.Type.PHOTO:
                    textViewPreview.setText(R.string.ConversationItemPreviewPhoto);
                    break;
                case MessageModel.Type.AUDIO:
                    textViewPreview.setText(R.string.ConversationItemPreviewAudio);
                    break;
                case MessageModel.Type.POST:
                    textViewPreview.setText(R.string.ConversationItemPreviewPost);
                    break;
                case MessageModel.Type.USER:
                    textViewPreview.setText(R.string.ConversationItemPreviewUser);
                    break;
            }

            if (!conversation.isSeen()) {
//                primaryLayout.setBackgroundColor(Color.parseColor("#e5f1f9"));
                CalligraphyUtils.applyFontToTextView(
                        context,
                        textViewPreview,
                        "fonts/DINBold.otf"
                );
            }

            textViewAuthor.setText(conversation.getAuthor().getUsername());
            setTimeAgo(conversation.getDate());
        }

        public void setTimeAgo(String dateStr) {
            try {
                SimpleDateFormat df = new SimpleDateFormat(CoreModel.DATE_TIME_FORMAT);
                Date date = df.parse(dateStr.trim());

                String agoStr = DateUtils.getTimeAgo(date, context);
                textViewTime.setText(agoStr);
            } catch (Exception exc) {
                // We couldn't parse it
                textViewTime.setText(dateStr);
            }
        }

        @Override
        public void onClick(View v) {
            Log.d("RzLog", "onClick: position=" + getAdapterPosition());
            if (getAdapterPosition() == RecyclerView.NO_POSITION)
                return;

            ConversationModel conversation = data.getConversations()
                    .get(getAdapterPosition());

            switch (v.getId()) {
                case R.id.primaryLayout_ConversationItem:
                    controller.onConversationClicked(conversation);
                    break;
                case R.id.imageButtonDelete_ConversationItem:
                    controller.onConversationDeleteClicked(conversation);
                    break;
                case R.id.textViewAccept_ConversationItem:
                    controller.onConversationAcceptClicked(conversation);
                    break;
                case R.id.textViewReject_ConversationItem:
                    controller.onConversationRejectClicked(conversation);
                    break;
            }
        }

        @Override
        public void onGlobalLayout() {
            primaryLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            int height = primaryLayout.getMeasuredHeight();
            swipeRevealLayout.setMinimumHeight(height);
        }
    }
}
