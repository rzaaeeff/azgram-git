package az.azgram.android.app.models;

import java.util.List;

import az.azgram.android.app.BuildConfig;

/**
 * Created by Rzaaeeff on 4/24/2017.
 */

public class ConversationModel extends CoreModel {
    public static final String UID = BuildConfig.APPLICATION_ID +
            ".ConversationModel";

    private List<CoreModel> recipients;
    private List<MessageModel> messages;
    private UserModel author;
    private String preview;
    private Boolean pending;
    private Boolean seen;
    private Byte previewType;

    public List<CoreModel> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<CoreModel> recipients) {
        this.recipients = recipients;
    }

    public UserModel getAuthor() {
        return author;
    }

    public void setAuthor(UserModel author) {
        this.author = author;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public Boolean isPending() {
        return pending;
    }

    public void setPending(Boolean pending) {
        this.pending = pending;
    }

    public Boolean isSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public Byte getPreviewType() {
        return previewType;
    }

    public void setPreviewType(Byte previewType) {
        this.previewType = previewType;
    }

    public List<MessageModel> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageModel> messages) {
        this.messages = messages;
    }
}
