package az.azgram.android.app.models;

import java.io.Serializable;
import java.util.List;

import az.azgram.android.app.BuildConfig;

/**
 * Created by Rzaaeeff on 11/30/2016.
 */

public class PostModel extends CoreModel implements Serializable {

    // Constants
    public static final String UID = BuildConfig.APPLICATION_ID +
            ".PostModel";

    // Type
    public class Type {
        public static final byte AD = 0; // advertisement = reklam
        public static final byte PHOTO = 1;
        public static final byte VIDEO = 2;
        public static final byte AUDIO = 3;
        public static final byte STATUS = 4;
    }

    // Privacy
    public class Privacy {
        public static final byte PUBLIC = 0;
        public static final byte FOLLOWERS = 1;
        public static final byte PRIVATE = 2;
    }

    // Fields
    private UserModel author;
    private String text;
    private Byte type;
    private Integer likeCount;
    private Integer dislikeCount;
    private Integer commentCount;
    private Integer shareCount;
    private Byte privacy;
    private PlaceModel place;
    private PhotoModel photo;
    private String adsRedirectUrl;
    private String adsRedirectText;
    private Boolean isLiked;
    private Boolean isDisliked;
    private List<LikeModel> likes;
    private List<DislikeModel> dislikes;
    private List<CommentModel> comments;
    private List<ShareModel> shares;
    private List<UserModel> taggedUsers;

    public UserModel getAuthor() {
        return author;
    }

    public void setAuthor(UserModel author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(Integer dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public Integer getShareCount() {
        return shareCount;
    }

    public void setShareCount(Integer shareCount) {
        this.shareCount = shareCount;
    }

    public Byte getPrivacy() {
        return privacy;
    }

    public void setPrivacy(Byte privacy) {
        this.privacy = privacy;
    }

    public PlaceModel getPlace() {
        return place;
    }

    public void setPlace(PlaceModel place) {
        this.place = place;
    }

    public PhotoModel getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoModel photo) {
        this.photo = photo;
    }

    public String getAdsRedirectUrl() {
        return adsRedirectUrl;
    }

    public void setAdsRedirectUrl(String adsRedirectUrl) {
        this.adsRedirectUrl = adsRedirectUrl;
    }

    public String getAdsRedirectText() {
        return adsRedirectText;
    }

    public void setAdsRedirectText(String adsRedirectText) {
        this.adsRedirectText = adsRedirectText;
    }

    public Boolean isLiked() {
        return isLiked;
    }

    public void setLiked(Boolean liked) {
        isLiked = liked;
    }

    public Boolean isDisliked() {
        return isDisliked;
    }

    public void setDisliked(Boolean disliked) {
        isDisliked = disliked;
    }

    public List<LikeModel> getLikes() {
        return likes;
    }

    public void setLikes(List<LikeModel> likes) {
        this.likes = likes;
    }

    public List<DislikeModel> getDislikes() {
        return dislikes;
    }

    public void setDislikes(List<DislikeModel> dislikes) {
        this.dislikes = dislikes;
    }

    public List<CommentModel> getComments() {
        return comments;
    }

    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }

    public List<ShareModel> getShares() {
        return shares;
    }

    public void setShares(List<ShareModel> shares) {
        this.shares = shares;
    }

    public List<UserModel> getTaggedUsers() {
        return taggedUsers;
    }

    public void setTaggedUsers(List<UserModel> taggedUsers) {
        this.taggedUsers = taggedUsers;
    }
}
