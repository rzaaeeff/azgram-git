package az.azgram.android.app.operations;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import com.google.gson.Gson;
import com.squareup.otto.Bus;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.app.AppSettingsReceivedEvent;
import az.azgram.android.app.events.app.AppSettingsSavedEvent;
import az.azgram.android.app.events.balance.BalanceReceivedEvent;
import az.azgram.android.app.events.balance.MoneyRequestedEvent;
import az.azgram.android.app.events.balance.MoneySentEvent;
import az.azgram.android.app.events.gifts.GiftListReceivedEvent;
import az.azgram.android.app.events.gifts.GiftSentEvent;
import az.azgram.android.app.events.messages.ConversationAcceptedEvent;
import az.azgram.android.app.events.messages.ConversationCreatedEvent;
import az.azgram.android.app.events.messages.ConversationDeletedEvent;
import az.azgram.android.app.events.messages.ConversationListReceivedEvent;
import az.azgram.android.app.events.messages.ConversationReceivedEvent;
import az.azgram.android.app.events.messages.ConversationRejectedEvent;
import az.azgram.android.app.events.messages.ConversationMarkedAsUnreadEvent;
import az.azgram.android.app.events.messages.ConversationClearedEvent;
import az.azgram.android.app.events.messages.MessageSentEvent;
import az.azgram.android.app.events.notifications.FollowRequestListReceivedEvent;
import az.azgram.android.app.events.notifications.NotificationListReceivedEvent;
import az.azgram.android.app.events.notifications.NotificationsClearedEvent;
import az.azgram.android.app.events.notifications.UserActionListReceivedEvent;
import az.azgram.android.app.events.posts.CommentDeletedEvent;
import az.azgram.android.app.events.posts.CommentPostedEvent;
import az.azgram.android.app.events.posts.CommentsReceivedEvent;
import az.azgram.android.app.events.posts.DislikeListReceivedEvent;
import az.azgram.android.app.events.posts.DislikeSentEvent;
import az.azgram.android.app.events.posts.DislikedPostListReceivedEvent;
import az.azgram.android.app.events.posts.FeedReceivedEvent;
import az.azgram.android.app.events.posts.GiftsFeedReceivedEvent;
import az.azgram.android.app.events.posts.LikeListReceivedEvent;
import az.azgram.android.app.events.posts.LikeSentEvent;
import az.azgram.android.app.events.posts.LikedPostListReceivedEvent;
import az.azgram.android.app.events.posts.PostDeletedEvent;
import az.azgram.android.app.events.posts.PostEvent;
import az.azgram.android.app.events.posts.PostReceivedEvent;
import az.azgram.android.app.events.posts.PostSetAsPPEvent;
import az.azgram.android.app.events.posts.PostsCollectionReceivedEvent;
import az.azgram.android.app.events.posts.PostsListReceivedEvent;
import az.azgram.android.app.events.posts.PrivatePostListReceivedEvent;
import az.azgram.android.app.events.posts.QuestionAnsweredEvent;
import az.azgram.android.app.events.posts.QuestionAskedEvent;
import az.azgram.android.app.events.posts.QuestionDeletedEvent;
import az.azgram.android.app.events.posts.QuestionReceivedEvent;
import az.azgram.android.app.events.posts.ReportEvent;
import az.azgram.android.app.events.posts.ShareEvent;
import az.azgram.android.app.events.posts.SubscribedPostListReceivedEvent;
import az.azgram.android.app.events.posts.SuggestedPostsListReceivedEvent;
import az.azgram.android.app.events.posts.TaggedPostReceivedEvent;
import az.azgram.android.app.events.search.PlaceSearchResultReceivedEvent;
import az.azgram.android.app.events.search.PostSearchResultReceivedEvent;
import az.azgram.android.app.events.search.UserSearchResultReceivedEvent;
import az.azgram.android.app.events.users.BlockedListReceivedEvent;
import az.azgram.android.app.events.users.BlockedUserEvent;
import az.azgram.android.app.events.users.BlockerListReceivedEvent;
import az.azgram.android.app.events.users.FollowEvent;
import az.azgram.android.app.events.users.FollowRequestEvent;
import az.azgram.android.app.events.users.FollowerHiddenEvent;
import az.azgram.android.app.events.users.FollowerListReceivedEvent;
import az.azgram.android.app.events.users.FollowingHiddenEvent;
import az.azgram.android.app.events.users.FollowingListReceivedEvent;
import az.azgram.android.app.events.users.ForgotEvent;
import az.azgram.android.app.events.users.GuestsClearedEvent;
import az.azgram.android.app.events.users.GuestsReceivedEvent;
import az.azgram.android.app.events.users.LoggedOutEvent;
import az.azgram.android.app.events.users.NotFollowerListReceivedEvent;
import az.azgram.android.app.events.users.ProfileDeletedEvent;
import az.azgram.android.app.events.users.ProfilePhotoChangedEvent;
import az.azgram.android.app.events.users.ReportedUserEvent;
import az.azgram.android.app.events.users.SettingChangedEvent;
import az.azgram.android.app.events.users.SignInEvent;
import az.azgram.android.app.events.users.SignUpEvent;
import az.azgram.android.app.events.users.SubscribedToUserEvent;
import az.azgram.android.app.events.users.SuggestedUsersListReceivedEvent;
import az.azgram.android.app.events.users.UserReceivedEvent;
import az.azgram.android.app.fragments.NotificationFragment;
import az.azgram.android.app.models.AppSettingsModel;
import az.azgram.android.app.models.CommentModel;
import az.azgram.android.app.models.ComplaintModel;
import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.GiftModel;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.models.MoneyWrapperModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.QuestionModel;
import az.azgram.android.app.models.SearchModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.UserSettingsModel;
import az.azgram.android.app.models.UserWrapperModel;
import az.azgram.android.app.models.response.CommentFeedModel;
import az.azgram.android.app.models.response.ConversationListModel;
import az.azgram.android.app.models.response.ConversationWrapperModel;
import az.azgram.android.app.models.response.FeedModel;
import az.azgram.android.app.models.response.GiftFeedModel;
import az.azgram.android.app.models.response.GiftListModel;
import az.azgram.android.app.models.response.NotificationListModel;
import az.azgram.android.app.models.response.PlaceListModel;
import az.azgram.android.app.models.response.QuestionFeedModel;
import az.azgram.android.app.models.response.UserListModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rzaaeeff on 12/1/2016.
 */

public class NetworkOperations {
    // Constant fields
    public static final String CHECK_URL = "http://google.com";

    private static final int CONNECTION_TIMEOUT = 60; //seconds
    private static final int READ_TIMEOUT = 60; // seconds
    private static final int WRITE_TIMEOUT = 60; // seconds
    private static final int HTTP_GET_SUCCESS = 200;
    public static final int API_SUCCESS = 1000;
    public static final int EMPTY = 1010;
    public static final int RETROFIT_ERROR = -1;
    public static final int UNKNOWN_ERROR = -2;
    public static final int NOT_CONNECTED = -3;
    public static final int CONNECTED_NOT_ONLINE = -4;
    public static final int CONNECTED_AND_ONLINE = -5;

    private Context context;
    // Our lovely API client :)
    private Retrofit retrofit;

    // Event bus (otto)
    private Bus bus;

    // Endpoints
    private Users usersEndpoint;
    private Posts postsEndpoint;
    private Search searchEndpoint;
    private Messages messagesEndpoint;
    private Notifications notificationsEndpoint;
    private Gifts giftsEndpoint;
    private Balance balanceEndpoint;
    private App appEndpoint;

    public NetworkOperations() {
        init();
    }

    public NetworkOperations(Context context) {
        this.context = context;
        init();
    }

    public NetworkOperations(Context context, Bus bus) {
        this.context = context;
        this.bus = bus;
        init();
    }

    // Basic initialization
    private void init() {
//        setup cache
//        File httpCacheDirectory = new File(context.getCacheDir(), "responses");
//        int cacheSize = 10 * 1024 * 1024; // 10 MiB
//        okhttp3.Cache cache = new okhttp3.Cache(httpCacheDirectory, cacheSize);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
//                .cache(cache)
//                .addInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR)
//                .addInterceptor(new LoggingInterceptor(true))
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(AzGramService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public Users getUsersEndpoint() {
        if (usersEndpoint == null) {
            usersEndpoint = new Users();
        }

        return usersEndpoint;
    }

    public Posts getPostsEndpoint() {
        if (postsEndpoint == null) {
            postsEndpoint = new Posts();
        }

        return postsEndpoint;
    }

    public Search getSearchEndpoint() {
        if (searchEndpoint == null) {
            searchEndpoint = new Search();
        }

        return searchEndpoint;
    }

    public Messages getMessagesEndpoint() {
        if (messagesEndpoint == null) {
            messagesEndpoint = new Messages();
        }

        return messagesEndpoint;
    }

    public Notifications getNotificationsEndpoint() {
        if (notificationsEndpoint == null) {
            notificationsEndpoint = new Notifications();
        }

        return notificationsEndpoint;
    }

    public Gifts getGiftsEndpoint() {
        if (giftsEndpoint == null) {
            giftsEndpoint = new Gifts();
        }

        return giftsEndpoint;
    }

    public Balance getBalanceEndpoint() {
        if (balanceEndpoint == null) {
            balanceEndpoint = new Balance();
        }

        return balanceEndpoint;
    }

    public App getAppEndpoint() {
        if (appEndpoint == null) {
            appEndpoint = new App();
        }

        return appEndpoint;
    }

    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public boolean isOnline() {
        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(CHECK_URL).openConnection());
            connection.setRequestProperty("User-Agent", "Test");
            connection.setRequestProperty("Connection", "close");
            connection.setConnectTimeout((int) TimeUnit.SECONDS.toMillis(CONNECTION_TIMEOUT));
            connection.setReadTimeout((int) TimeUnit.SECONDS.toMillis(READ_TIMEOUT));
            connection.setChunkedStreamingMode(0);
            connection.connect();

            return (connection.getResponseCode() == HTTP_GET_SUCCESS);

        } catch (IOException e) {
            return false;
        }
    }

    public int isConnectedAndOnline() {
        if (isConnected()) {
            if (isOnline()) {
                return CONNECTED_AND_ONLINE;
            } else {
                return CONNECTED_NOT_ONLINE;
            }
        } else {
            return NOT_CONNECTED;
        }
    }

    // Users endpoint
    public class Users {
        // Error codes for this endpoint
        public static final int FIELDS_EMPTY = 1001;
        public static final int WRONG_CREDENTIALS = 1002;
        public static final int FUNCTION_NOT_FOUND = 1003;
        public static final int INVALID_USERNAME = 1004;
        public static final int EXISTING_USERNAME = 1005;
        public static final int EXISTING_EMAIL = 1006;
        public static final int INVALID_EMAIL = 1007;
        public static final int NOT_EXISTING_ACCOUNT = 1008;
        public static final int BLOCKED = 1012;
        public static final int PRIVATE_PROFILE = 1020;
        public static final int PENDING = 1021;
        public static final int EMPTY_GUEST_LIST = 1028;
        public static final int EMPTY_SUGGESTED_LIST = 1041;

        AzGramService.Users service;

        public void signIn(UserModel user) {

            try {
                service = retrofit.create(AzGramService.Users.class);
                service.signIn(user).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        bus.post(new SignInEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new SignInEvent(error));
                    }
                });

            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new SignInEvent(error));
            }
        }

        public void signUp(UserModel user) {

            try {

                service = retrofit.create(AzGramService.Users.class);
                service.signUp(user).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        // Send event containing response object back to Activity
                        bus.post(new SignUpEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new SignUpEvent(error));
                    }
                });

            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new SignUpEvent(error));
            }
        }

        public void forgot(UserModel user) {

            try {
                service = retrofit.create(AzGramService.Users.class);
                service.forgot(user).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        bus.post(new ForgotEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new ForgotEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new ForgotEvent(error));
            }
        }

        public void getUser(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.getUser(user).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        bus.post(new UserReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setMessage(t.getMessage());
                        error.setMessageId(RETROFIT_ERROR);

                        bus.post(new UserReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new UserReceivedEvent(error));
            }
        }

        public void follow(final UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.follow(user).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        response.body().setUsername(user.getUsername());
                        bus.post(new FollowEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setUsername(user.getUsername());
                        error.setMessage(t.getMessage());
                        error.setMessageId(RETROFIT_ERROR);

                        bus.post(new FollowEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setUsername(user.getUsername());
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new FollowEvent(error));
            }
        }

        public void getGuestList(CoreModel model) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.getGuestList(model).enqueue(new Callback<UserListModel>() {
                    @Override
                    public void onResponse(Call<UserListModel> call, Response<UserListModel> response) {
                        bus.post(new GuestsReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserListModel> call, Throwable t) {
                        UserListModel error = new UserListModel();
                        error.setMessage(t.getMessage());
                        error.setMessageId(RETROFIT_ERROR);

                        bus.post(new GuestsReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserListModel error = new UserListModel();
                error.setMessage(exc.getMessage());
                error.setMessageId(UNKNOWN_ERROR);

                bus.post(new GuestsReceivedEvent(error));
            }
        }

        public void clearGuestList(CoreModel model) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.clearGuestList(model).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new GuestsClearedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessage(t.getMessage());
                        error.setMessageId(RETROFIT_ERROR);

                        bus.post(new GuestsClearedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessage(exc.getMessage());
                error.setMessageId(UNKNOWN_ERROR);

                bus.post(new GuestsClearedEvent(error));
            }
        }

        public void acceptFollowRequest(final UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.acceptFollowRequest(user).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        response.body().setUsername(user.getUsername());
                        bus.post(new FollowRequestEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setUsername(user.getUsername());
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new FollowRequestEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setUsername(user.getUsername());
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new FollowRequestEvent(error));
            }
        }

        public void rejectFollowRequest(final UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.rejectFollowRequest(user).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        response.body().setUsername(user.getUsername());
                        bus.post(new FollowRequestEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setUsername(user.getUsername());
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new FollowRequestEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setUsername(user.getUsername());
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new FollowRequestEvent(error));
            }
        }

        public void getFollowerList(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.getFollowerList(user).enqueue(new Callback<UserListModel>() {
                    @Override
                    public void onResponse(Call<UserListModel> call, Response<UserListModel> response) {
                        bus.post(new FollowerListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserListModel> call, Throwable t) {
                        UserListModel error = new UserListModel();
                        error.setMessage(t.getMessage());
                        error.setMessageId(RETROFIT_ERROR);

                        bus.post(new FollowerListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserListModel error = new UserListModel();
                error.setMessage(exc.getMessage());
                error.setMessageId(UNKNOWN_ERROR);

                bus.post(new FollowerListReceivedEvent(error));
            }
        }

        public void getFollowingList(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.getFollowingList(user).enqueue(new Callback<UserListModel>() {
                    @Override
                    public void onResponse(Call<UserListModel> call, Response<UserListModel> response) {
                        bus.post(new FollowingListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserListModel> call, Throwable t) {
                        UserListModel error = new UserListModel();
                        error.setMessage(t.getMessage());
                        error.setMessageId(RETROFIT_ERROR);

                        bus.post(new FollowingListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserListModel error = new UserListModel();
                error.setMessage(exc.getMessage());
                error.setMessageId(UNKNOWN_ERROR);

                bus.post(new FollowingListReceivedEvent(error));
            }
        }

        public void hideFollower(final UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.hideFollower(user).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        response.body().setUsername(user.getUsername());
                        bus.post(new FollowerHiddenEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setUsername(user.getUsername());
                        error.setMessage(t.getMessage());
                        error.setMessageId(RETROFIT_ERROR);

                        bus.post(new FollowerHiddenEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setUsername(user.getUsername());
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new FollowerHiddenEvent(error));
            }
        }

        public void hideFollowing(final UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.hideFollowing(user).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        response.body().setUsername(user.getUsername());
                        bus.post(new FollowingHiddenEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setUsername(user.getUsername());
                        error.setMessage(t.getMessage());
                        error.setMessageId(RETROFIT_ERROR);

                        bus.post(new FollowingHiddenEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setUsername(user.getUsername());
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new FollowingHiddenEvent(error));
            }
        }

        public void save(final UserSettingsModel settings) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.save(settings).enqueue(new Callback<UserSettingsModel>() {
                    @Override
                    public void onResponse(Call<UserSettingsModel> call, Response<UserSettingsModel> response) {
                        response.body().setChange(settings.getChange());
                        bus.post(new SettingChangedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserSettingsModel> call, Throwable t) {
                        UserSettingsModel error = new UserSettingsModel();
                        error.setChange(settings.getChange());
                        error.setMessage(t.getMessage());
                        error.setMessageId(RETROFIT_ERROR);

                        bus.post(new SettingChangedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserSettingsModel error = new UserSettingsModel();
                error.setChange(settings.getChange());
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new SettingChangedEvent(error));
            }
        }

        public void getSuggestedUsersList(CoreModel request) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.getSuggestedUsersList(request).enqueue(new Callback<UserListModel>() {
                    @Override
                    public void onResponse(Call<UserListModel> call, Response<UserListModel> response) {
                        bus.post(new SuggestedUsersListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserListModel> call, Throwable t) {
                        UserListModel error = new UserListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new SuggestedUsersListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserListModel error = new UserListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new SuggestedUsersListReceivedEvent(error));
            }
        }

        public void subscribe(UserWrapperModel subscription) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.subscribe(subscription).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new SubscribedToUserEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new SubscribedToUserEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new SubscribedToUserEvent(error));
            }
        }

        public void report(UserWrapperModel complaint) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.report(complaint).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new ReportedUserEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new ReportedUserEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new ReportedUserEvent(error));
            }
        }

        public void block(final UserWrapperModel block) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.block(block).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        response.body().setUsername(block.getTarget().getUsername());
                        bus.post(new BlockedUserEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setUsername(block.getTarget().getUsername());

                        bus.post(new BlockedUserEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setUsername(block.getTarget().getUsername());

                bus.post(new BlockedUserEvent(error));
            }
        }

        /**
         * Using this method requires NetworkOperations(Context) constructor
         *
         * @param request
         * @param uri  uri of file
         */
        public void savePP(CoreModel request, Uri uri) {
            try {
                File file = new File(FileUtils.getRealPathFromURI(context, uri));
                RequestBody requestBody = RequestBody.create(
                        MediaType.parse(context.getContentResolver().getType(uri)), file);
                MultipartBody.Part photoPart = MultipartBody.Part
                        .createFormData("file", file.getName(), requestBody);
                String postJson = new Gson().toJson(request);
                service = retrofit.create(AzGramService.Users.class);
                service.savePP(photoPart, RequestBody.create(MediaType.parse("application/json"), postJson))
                        .enqueue(new Callback<CoreModel>() {
                            @Override
                            public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                                bus.post(new ProfilePhotoChangedEvent(response.body()));
                            }

                            @Override
                            public void onFailure(Call<CoreModel> call, Throwable t) {
                                CoreModel error = new CoreModel();
                                error.setMessage(t.getMessage());
                                error.setMessageId(RETROFIT_ERROR);

                                bus.post(new ProfilePhotoChangedEvent(error));
                            }
                        });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessage(exc.getMessage());
                error.setMessageId(RETROFIT_ERROR);

                bus.post(new ProfilePhotoChangedEvent(error));
            }
        }

        public void getBlockedList(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.getBlockedList(user).enqueue(new Callback<UserListModel>() {
                    @Override
                    public void onResponse(Call<UserListModel> call, Response<UserListModel> response) {
                        bus.post(new BlockedListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserListModel> call, Throwable t) {
                        UserListModel error = new UserListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new BlockedListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserListModel error = new UserListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new BlockedListReceivedEvent(error));
            }
        }

        public void getBlockerList(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.getBlockerList(user).enqueue(new Callback<UserListModel>() {
                    @Override
                    public void onResponse(Call<UserListModel> call, Response<UserListModel> response) {
                        bus.post(new BlockerListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserListModel> call, Throwable t) {
                        UserListModel error = new UserListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new BlockerListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserListModel error = new UserListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new BlockerListReceivedEvent(error));
            }
        }

        public void getNotFollowerList(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.getNotFollowerList(user).enqueue(new Callback<UserListModel>() {
                    @Override
                    public void onResponse(Call<UserListModel> call, Response<UserListModel> response) {
                        bus.post(new NotFollowerListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserListModel> call, Throwable t) {
                        UserListModel error = new UserListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new NotFollowerListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserListModel error = new UserListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new NotFollowerListReceivedEvent(error));
            }
        }

        public void logout(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.logout(user).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new LoggedOutEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new LoggedOutEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new LoggedOutEvent(error));
            }
        }

        public void logoutAll(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.logoutAll(user).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new LoggedOutEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new LoggedOutEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new LoggedOutEvent(error));
            }
        }

        public void delete(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Users.class);
                service.delete(user).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new ProfileDeletedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new ProfileDeletedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new ProfileDeletedEvent(error));
            }
        }
    }

    // Posts endpoint
    public class Posts {
        public static final int NOT_EXISTING_USERNAME = 1008;
        public static final int EMPTY = 1010;
        public static final int BLOCKED = 1012;
        public static final int MUST_FOLLOW = 1013;
        public static final int COMMENTS_NOT_FOUND = 1017;
        public static final int SAME_COMMENT = 1018;
        public static final int EMPTY_REACTION_LIST = 1040;

        AzGramService.Posts service;

        public void getFeed(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getFeed(user).enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        bus.post(new FeedReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        FeedModel error = new FeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new FeedReceivedEvent(error));
                    }
                });

            } catch (Exception exc) {
                FeedModel error = new FeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new FeedReceivedEvent(error));
            }
        }

        public void getUserPostsList(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getUserPostsList(user).enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        bus.post(new PostsListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        FeedModel error = new FeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new PostsListReceivedEvent(error));
                    }
                });

            } catch (Exception exc) {
                FeedModel error = new FeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new PostsListReceivedEvent(error));
            }
        }

        public void getUserPostsCollection(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getUserPostsCollection(user).enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        bus.post(new PostsCollectionReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        FeedModel error = new FeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new PostsCollectionReceivedEvent(error));
                    }
                });

            } catch (Exception exc) {
                FeedModel error = new FeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new PostsCollectionReceivedEvent(error));
            }
        }

        public void getTaggedPosts(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getTaggedPosts(user).enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        bus.post(new TaggedPostReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        FeedModel error = new FeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new TaggedPostReceivedEvent(error));
                    }
                });

            } catch (Exception exc) {
                FeedModel error = new FeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new TaggedPostReceivedEvent(error));
            }
        }

        public void getGifts(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getGifts(user).enqueue(new Callback<GiftFeedModel>() {
                    @Override
                    public void onResponse(Call<GiftFeedModel> call, Response<GiftFeedModel> response) {
                        bus.post(new GiftsFeedReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<GiftFeedModel> call, Throwable t) {
                        GiftFeedModel error = new GiftFeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new GiftsFeedReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                GiftFeedModel error = new GiftFeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new GiftsFeedReceivedEvent(error));
            }
        }

        public void getQuestions(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getQuestions(user).enqueue(new Callback<QuestionFeedModel>() {
                    @Override
                    public void onResponse(Call<QuestionFeedModel> call, Response<QuestionFeedModel> response) {
                        bus.post(new QuestionReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<QuestionFeedModel> call, Throwable t) {
                        QuestionFeedModel error = new QuestionFeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new QuestionReceivedEvent(error));
                    }
                });

            } catch (Exception exc) {
                QuestionFeedModel error = new QuestionFeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new QuestionReceivedEvent(error));
            }
        }

        public void getPost(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getPost(user).enqueue(new Callback<PostModel>() {
                    @Override
                    public void onResponse(Call<PostModel> call, Response<PostModel> response) {
                        bus.post(new PostReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<PostModel> call, Throwable t) {
                        PostModel error = new PostModel();
                        error.setMessage(t.getMessage());
                        error.setMessageId(RETROFIT_ERROR);

                        bus.post(new PostReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                PostModel error = new PostModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new PostReceivedEvent(error));
            }
        }

        public void getComments(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getComments(user).enqueue(new Callback<CommentFeedModel>() {
                    @Override
                    public void onResponse(Call<CommentFeedModel> call, Response<CommentFeedModel> response) {
                        bus.post(new CommentsReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CommentFeedModel> call, Throwable t) {
                        CommentFeedModel error = new CommentFeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new CommentsReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CommentFeedModel error = new CommentFeedModel();
                error.setMessage(exc.getMessage());
                error.setMessageId(UNKNOWN_ERROR);

                bus.post(new CommentsReceivedEvent(error));
            }
        }

        public void like(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.like(user).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new LikeSentEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new LikeSentEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new LikeSentEvent(error));
            }
        }

        public void dislike(UserModel user) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.dislike(user).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new DislikeSentEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new DislikeSentEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new DislikeSentEvent(error));
            }
        }

        public void comment(CommentModel comment) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.comment(comment).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new CommentPostedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new CommentPostedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new CommentPostedEvent(error));
            }
        }

        public void deleteComment(final PostModel post) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.deleteComment(post).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        CoreModel model = response.body();
                        model.setId(post.getComments().get(0).getId());
                        bus.post(new CommentDeletedEvent(model));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new CommentDeletedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new CommentDeletedEvent(error));
            }
        }

        public void share(PostModel post) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.share(post).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new ShareEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new ShareEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new ShareEvent(error));
            }
        }

        public void askQuestion(QuestionModel question) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.askQuestion(question).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new QuestionAskedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new QuestionAskedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new QuestionAskedEvent(error));
            }
        }

        public void answerQuestion(QuestionModel question) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.answerQuestion(question).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new QuestionAnsweredEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new QuestionAnsweredEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new QuestionAnsweredEvent(error));
            }
        }

        public void deleteQuestion(final QuestionModel question) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.deleteQuestion(question).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        CoreModel model = response.body();
                        model.setId(question.getId());
                        bus.post(new QuestionDeletedEvent(model));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setId(question.getId());
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new QuestionDeletedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setId(question.getId());
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new QuestionDeletedEvent(error));
            }
        }

        public void report(ComplaintModel complaint) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.report(complaint).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new ReportEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new ReportEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new ReportEvent(error));
            }
        }

        /**
         * Using this method requires NetworkOperations(Context) constructor
         *
         * @param post
         * @param file file
         */
        public void uploadStatus(PostModel post, File file) {
            if (file != null) {
                _uploadStatusWithAudio(post, file);
            } else {
                _uploadStatus(post);
            }
        }

        /**
         * Using this method requires NetworkOperations(Context) constructor
         *
         * @param post
         * @param file file
         */
        private void _uploadStatusWithAudio(PostModel post, File file) {
            try {
                RequestBody body = RequestBody.create(
                        MediaType.parse("audio/3gpp"),
                        file
                );
                MultipartBody.Part partFile = MultipartBody.Part
                        .createFormData("audio", file.getName(), body);
                RequestBody partJson = RequestBody.create(
                        MediaType.parse("application/json"),
                        new Gson().toJson(post)
                );
                service = retrofit.create(AzGramService.Posts.class);
                service.uploadStatus(partFile, partJson)
                        .enqueue(new Callback<PostModel>() {
                            @Override
                            public void onResponse(Call<PostModel> call, Response<PostModel> response) {
                                BusProvider.getInstance().post(new PostEvent(response.body()));
                            }

                            @Override
                            public void onFailure(Call<PostModel> call, Throwable t) {
                                PostModel error = new PostModel();
                                error.setMessage(t.getMessage());
                                error.setMessageId(RETROFIT_ERROR);

                                BusProvider.getInstance().post(new PostEvent(error));
                            }
                        });
            } catch (Exception exc) {
                PostModel error = new PostModel();
                error.setMessage(exc.getMessage());
                error.setMessageId(RETROFIT_ERROR);

                BusProvider.getInstance().post(new PostEvent(error));
            }
        }

        private void _uploadStatus(PostModel post) {
            try {
                RequestBody partJson = RequestBody.create(
                        MediaType.parse("application/json"),
                        new Gson().toJson(post)
                );
                service = retrofit.create(AzGramService.Posts.class);
                service.uploadStatus(partJson)
                        .enqueue(new Callback<PostModel>() {
                            @Override
                            public void onResponse(Call<PostModel> call, Response<PostModel> response) {
                                BusProvider.getInstance().post(new PostEvent(response.body()));
                            }

                            @Override
                            public void onFailure(Call<PostModel> call, Throwable t) {
                                PostModel error = new PostModel();
                                error.setMessage(t.getMessage());
                                error.setMessageId(RETROFIT_ERROR);

                                BusProvider.getInstance().post(new PostEvent(error));
                            }
                        });
            } catch (Exception exc) {
                PostModel error = new PostModel();
                error.setMessage(exc.getMessage());
                error.setMessageId(RETROFIT_ERROR);

                BusProvider.getInstance().post(new PostEvent(error));
            }
        }

        /**
         * Using this method requires NetworkOperations(Context) constructor
         *
         * @param post
         * @param uri  uri of file
         */
        public void uploadPhoto(PostModel post, Uri uri) {
            try {
                File file = new File(FileUtils.getRealPathFromURI(context, uri));
                RequestBody requestBody = RequestBody.create(
                        MediaType.parse(context.getContentResolver().getType(uri)), file);
                MultipartBody.Part photoPart = MultipartBody.Part
                        .createFormData("photo", file.getName(), requestBody);
                String postJson = new Gson().toJson(post);
                service = retrofit.create(AzGramService.Posts.class);
                service.uploadPhoto(photoPart, RequestBody.create(MediaType.parse("application/json"), postJson))
                        .enqueue(new Callback<PostModel>() {
                            @Override
                            public void onResponse(Call<PostModel> call, Response<PostModel> response) {
                                bus.post(new PostEvent(response.body()));
                            }

                            @Override
                            public void onFailure(Call<PostModel> call, Throwable t) {
                                PostModel error = new PostModel();
                                error.setMessage(t.getMessage());
                                error.setMessageId(RETROFIT_ERROR);

                                bus.post(new PostEvent(error));
                            }
                        });
            } catch (Exception exc) {
                PostModel error = new PostModel();
                error.setMessage(exc.getMessage());
                error.setMessageId(RETROFIT_ERROR);

                bus.post(new PostEvent(error));
            }
        }

        /**
         * Using this method requires NetworkOperations(Context) constructor
         *
         * @param post
         * @param uri  uri of file
         */
        public void uploadVideo(PostModel post, Uri uri) {
            try {
                File file = new File(FileUtils.getRealPathFromURI(context, uri));
                RequestBody requestBody = RequestBody.create(
                        MediaType.parse(context.getContentResolver().getType(uri)),
                        file
                );
                MultipartBody.Part partFile = MultipartBody.Part.
                        createFormData("video", file.getName(), requestBody);
                String postJson = new Gson().toJson(post);
                service = retrofit.create(AzGramService.Posts.class);
                service.uploadVideo(partFile, RequestBody.create(MediaType.parse("application/json"), postJson))
                        .enqueue(new Callback<PostModel>() {
                            @Override
                            public void onResponse(Call<PostModel> call, Response<PostModel> response) {
                                bus.post(new PostEvent(response.body()));
                            }

                            @Override
                            public void onFailure(Call<PostModel> call, Throwable t) {
                                PostModel error = new PostModel();
                                error.setMessage(t.getMessage());
                                error.setMessageId(RETROFIT_ERROR);

                                bus.post(new PostEvent(error));
                            }
                        });
            } catch (Exception exc) {
                PostModel error = new PostModel();
                error.setMessage(exc.getMessage());
                error.setMessageId(UNKNOWN_ERROR);

                bus.post(new PostEvent(error));
            }
        }

        public void getLikeList(PostModel post) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getLikeList(post).enqueue(new Callback<UserListModel>() {
                    @Override
                    public void onResponse(Call<UserListModel> call, Response<UserListModel> response) {
                        bus.post(new LikeListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserListModel> call, Throwable t) {
                        UserListModel error = new UserListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new LikeListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserListModel error = new UserListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new LikeListReceivedEvent(error));
            }
        }

        public void getDislikeList(PostModel post) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getDislikeList(post).enqueue(new Callback<UserListModel>() {
                    @Override
                    public void onResponse(Call<UserListModel> call, Response<UserListModel> response) {
                        bus.post(new DislikeListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserListModel> call, Throwable t) {
                        UserListModel error = new UserListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new DislikeListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserListModel error = new UserListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new DislikeListReceivedEvent(error));
            }
        }

        public void getSuggestedPostsList(CoreModel request) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getSuggestedPostsList(request).enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        bus.post(new SuggestedPostsListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        FeedModel error = new FeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new SuggestedPostsListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                FeedModel error = new FeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new SuggestedPostsListReceivedEvent(error));
            }
        }

        public void getPrivatePosts(UserModel request) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getPrivatePosts(request).enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        bus.post(new PrivatePostListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        FeedModel error = new FeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new PrivatePostListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                FeedModel error = new FeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new PrivatePostListReceivedEvent(error));
            }
        }

        public void getLikedPosts(UserModel request) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getLikedPosts(request).enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        bus.post(new LikedPostListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        FeedModel error = new FeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new LikedPostListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                FeedModel error = new FeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new LikedPostListReceivedEvent(error));
            }
        }

        public void getDislikedPosts(UserModel request) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getDislikedPosts(request).enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        bus.post(new DislikedPostListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        FeedModel error = new FeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new DislikedPostListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                FeedModel error = new FeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new DislikedPostListReceivedEvent(error));
            }
        }

        public void getSubscribedPosts(UserModel request) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.getSubscribedPosts(request).enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        bus.post(new SubscribedPostListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        FeedModel error = new FeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new SubscribedPostListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                FeedModel error = new FeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new SubscribedPostListReceivedEvent(error));
            }
        }

        public void asPP(PostModel post) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.asPP(post).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new PostSetAsPPEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new PostSetAsPPEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new PostSetAsPPEvent(error));
            }
        }

        public void delete(final PostModel post) {
            try {
                service = retrofit.create(AzGramService.Posts.class);
                service.delete(post).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        response.body().setId(post.getId());
                        bus.post(new PostDeletedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setId(post.getId());

                        bus.post(new PostDeletedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setId(post.getId());

                bus.post(new PostDeletedEvent(error));
            }
        }
    }

    public class Search {
        public static final int NO_USER_FOUND = 1025;
        public static final int NO_POST_FOUND = 1026;
        public static final int NO_PLACE_FOUND = 1035;

        private AzGramService.Search service;

        public void searchUsers(final SearchModel search) {
            try {
                service = retrofit.create(AzGramService.Search.class);
                service.searchUsers(search).enqueue(new Callback<UserListModel>() {
                    @Override
                    public void onResponse(Call<UserListModel> call, Response<UserListModel> response) {
                        UserListModel model = response.body();
                        model.setText(search.getText());
                        bus.post(new UserSearchResultReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserListModel> call, Throwable t) {
                        UserListModel error = new UserListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setText(search.getText());

                        bus.post(new UserSearchResultReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserListModel error = new UserListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setText(search.getText());

                bus.post(new UserSearchResultReceivedEvent(error));
            }
        }

        public void searchPosts(final SearchModel search) {
            try {
                service = retrofit.create(AzGramService.Search.class);
                service.searchPosts(search).enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        FeedModel model = response.body();
                        model.setText(search.getText());
                        bus.post(new PostSearchResultReceivedEvent(model));
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        FeedModel error = new FeedModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setText(search.getText());

                        bus.post(new PostSearchResultReceivedEvent(error));

                    }
                });
            } catch (Exception exc) {
                FeedModel error = new FeedModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setText(search.getText());

                bus.post(new PostSearchResultReceivedEvent(error));
            }
        }

        public void searchPlaces(final SearchModel search) {
            try {
                service = retrofit.create(AzGramService.Search.class);
                service.searchPlaces(search).enqueue(new Callback<PlaceListModel>() {
                    @Override
                    public void onResponse(Call<PlaceListModel> call, Response<PlaceListModel> response) {
                        PlaceListModel model = response.body();
                        model.setText(search.getText());
                        bus.post(new PlaceSearchResultReceivedEvent(model));
                    }

                    @Override
                    public void onFailure(Call<PlaceListModel> call, Throwable t) {
                        PlaceListModel error = new PlaceListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setText(search.getText());

                        bus.post(new PlaceSearchResultReceivedEvent(error));

                    }
                });
            } catch (Exception exc) {
                PlaceListModel error = new PlaceListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setText(search.getText());

                bus.post(new PlaceSearchResultReceivedEvent(error));
            }
        }
    }

    public class Messages {
        public static final int CONVERSATION_NOT_FOUND = 1036;
        public static final int MESSAGES_NOT_FOUND = 1037;

        private AzGramService.Messages service = retrofit.create(AzGramService.Messages.class);

        public void createConversation(ConversationModel request) {
            try {
                service.createConversation(request).enqueue(new Callback<ConversationWrapperModel>() {
                    @Override
                    public void onResponse(Call<ConversationWrapperModel> call, Response<ConversationWrapperModel> response) {
                        bus.post(new ConversationCreatedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<ConversationWrapperModel> call, Throwable t) {
                        ConversationWrapperModel error = new ConversationWrapperModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new ConversationCreatedEvent(error));
                    }
                });
            } catch (Exception exc) {
                ConversationWrapperModel error = new ConversationWrapperModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new ConversationCreatedEvent(error));
            }
        }

        public void getConversations(final CoreModel request) {
            try {
                service.getConversations(request).enqueue(new Callback<ConversationListModel>() {
                    @Override
                    public void onResponse(Call<ConversationListModel> call, Response<ConversationListModel> response) {
                        bus.post(new ConversationListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<ConversationListModel> call, Throwable t) {
                        ConversationListModel error = new ConversationListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new ConversationListReceivedEvent(error));

                    }
                });
            } catch (Exception exc) {
                ConversationListModel error = new ConversationListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new ConversationListReceivedEvent(error));
            }
        }

        public void deleteConversation(final CoreModel request) {
            try {
                service.deleteConversation(request).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        response.body().setId(request.getId());
                        bus.post(new ConversationDeletedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setId(request.getId());

                        bus.post(new ConversationDeletedEvent(error));

                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setId(request.getId());

                bus.post(new ConversationDeletedEvent(error));
            }
        }

        public void acceptConversation(final CoreModel request) {
            try {
                service.acceptConversation(request).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        response.body().setId(request.getId());
                        bus.post(new ConversationAcceptedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setId(request.getId());

                        bus.post(new ConversationAcceptedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setId(request.getId());

                bus.post(new ConversationAcceptedEvent(error));
            }
        }

        public void rejectConversation(final CoreModel request) {
            try {
                service.rejectConversation(request).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        response.body().setId(request.getId());
                        bus.post(new ConversationRejectedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setId(request.getId());

                        bus.post(new ConversationRejectedEvent(error));

                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setId(request.getId());

                bus.post(new ConversationRejectedEvent(error));
            }
        }

        public void getMessages(final CoreModel request) {
            try {
                service.getMessages(request).enqueue(new Callback<ConversationModel>() {
                    @Override
                    public void onResponse(Call<ConversationModel> call, Response<ConversationModel> response) {
                        response.body().setId(request.getId());
                        bus.post(new ConversationReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<ConversationModel> call, Throwable t) {
                        ConversationModel error = new ConversationModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setId(request.getId());

                        bus.post(new ConversationReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                ConversationModel error = new ConversationModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setId(request.getId());

                bus.post(new ConversationReceivedEvent(error));
            }
        }

        public void send(final MessageModel message, File file) {
            try {
                RequestBody requestFile = RequestBody.create(
                        MediaType.parse(URLConnection.guessContentTypeFromName(file.getName())),
                        file
                );
                MultipartBody.Part partFile = MultipartBody.Part.createFormData(
                        "file", file.getName(), requestFile
                );

                service.send(partFile, message).enqueue(new Callback<MessageModel>() {
                    @Override
                    public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                        response.body().setLocalId(message.getLocalId());
                        bus.post(new MessageSentEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<MessageModel> call, Throwable t) {
                        MessageModel error = new MessageModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setLocalId(message.getLocalId());

                        bus.post(new MessageSentEvent(error));
                    }
                });
            } catch (Exception exc) {
                MessageModel error = new MessageModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setLocalId(message.getLocalId());

                bus.post(new MessageSentEvent(error));
            }
        }

        public void send(final MessageModel message) {
            try {
                service.send(message).enqueue(new Callback<MessageModel>() {
                    @Override
                    public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                        response.body().setLocalId(message.getLocalId());
                        bus.post(new MessageSentEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<MessageModel> call, Throwable t) {
                        MessageModel error = new MessageModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setLocalId(message.getLocalId());

                        bus.post(new MessageSentEvent(error));
                    }
                });
            } catch (Exception exc) {
                MessageModel error = new MessageModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setLocalId(message.getLocalId());

                bus.post(new MessageSentEvent(error));
            }
        }

        public void markAsUnread(ConversationModel conversation) {
            try {
                service.markAsUnread(conversation).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {

                        bus.post(new ConversationMarkedAsUnreadEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new ConversationMarkedAsUnreadEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new ConversationMarkedAsUnreadEvent(error));
            }
        }

        public void clear(ConversationModel conversation) {
            try {
                service.clear(conversation).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {

                        bus.post(new ConversationClearedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new ConversationClearedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new ConversationClearedEvent(error));
            }
        }
    }

    public class Notifications {
        private AzGramService.Notifications service = retrofit.create(AzGramService.Notifications.class);

        public void autoGet(CoreModel request, byte category) {
            switch (category) {
                case NotificationFragment.CATEGORY_NOTIFICATIONS:
                    getNotifications(request);
                    break;
                case NotificationFragment.CATEGORY_USER_ACTIONS:
                    getUserActions(request);
                    break;
                case NotificationFragment.CATEGORY_FOLLOW_REQUESTS:
                    getFollowRequests(request);
                    break;
            }
        }

        public void getNotifications(CoreModel request) {
            try {
                service.getNotifications(request).enqueue(new Callback<NotificationListModel>() {
                    @Override
                    public void onResponse(Call<NotificationListModel> call, Response<NotificationListModel> response) {
                        bus.post(new NotificationListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<NotificationListModel> call, Throwable t) {
                        NotificationListModel error = new NotificationListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new NotificationListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                NotificationListModel error = new NotificationListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new NotificationListReceivedEvent(error));
            }
        }

        public void clearNotifications(CoreModel request) {
            try {
                service.clearNotifications(request).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new NotificationsClearedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new NotificationsClearedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new NotificationsClearedEvent(error));
            }
        }

        public void getUserActions(CoreModel request) {
            try {
                service.getUserActions(request).enqueue(new Callback<NotificationListModel>() {
                    @Override
                    public void onResponse(Call<NotificationListModel> call, Response<NotificationListModel> response) {
                        bus.post(new UserActionListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<NotificationListModel> call, Throwable t) {
                        NotificationListModel error = new NotificationListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new UserActionListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                NotificationListModel error = new NotificationListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new UserActionListReceivedEvent(error));
            }
        }

        public void getFollowRequests(CoreModel request) {
            try {
                service.getFollowRequests(request).enqueue(new Callback<NotificationListModel>() {
                    @Override
                    public void onResponse(Call<NotificationListModel> call, Response<NotificationListModel> response) {
                        bus.post(new FollowRequestListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<NotificationListModel> call, Throwable t) {
                        NotificationListModel error = new NotificationListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new FollowRequestListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                NotificationListModel error = new NotificationListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new FollowRequestListReceivedEvent(error));
            }
        }
    }

    public class Gifts {
        private AzGramService.Gifts service = retrofit.create(AzGramService.Gifts.class);

        public void list(CoreModel request) {
            try {
                service.list(request).enqueue(new Callback<GiftListModel>() {
                    @Override
                    public void onResponse(Call<GiftListModel> call, Response<GiftListModel> response) {
                        bus.post(new GiftListReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<GiftListModel> call, Throwable t) {
                        GiftListModel error = new GiftListModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new GiftListReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                GiftListModel error = new GiftListModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new GiftListReceivedEvent(error));
            }
        }

        public void send(GiftModel gift) {
            try {
                service.send(gift).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new GiftSentEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new GiftSentEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new GiftSentEvent(error));
            }
        }
    }

    public class Balance {
        private AzGramService.Balance service = retrofit.create(AzGramService.Balance.class);
        public static final int WRONG_AMOUNT = 1044;
        public static final int NOT_ENOUGH_BALANCE = 1045;

        public void get(CoreModel request) {
            try {
                service.get(request).enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        bus.post(new BalanceReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        UserModel error = new UserModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new BalanceReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                UserModel error = new UserModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new BalanceReceivedEvent(error));
            }
        }

        public void send(MoneyWrapperModel money) {
            try {
                service.send(money).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new MoneySentEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new MoneySentEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new MoneySentEvent(error));
            }
        }

        public void request(MoneyWrapperModel money) {
            try {
                service.request(money).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        bus.post(new MoneyRequestedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {
                        CoreModel error = new CoreModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new MoneyRequestedEvent(error));
                    }
                });
            } catch (Exception exc) {
                CoreModel error = new CoreModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new MoneyRequestedEvent(error));
            }
        }
    }

    public class App {
        public final static int APP_NEEDS_UPDATE = 1900;

        private AzGramService.App service = retrofit.create(AzGramService.App.class);

        public void saveSettings(final AppSettingsModel settings) {
            try {
                service.saveSettings(settings).enqueue(new Callback<AppSettingsModel>() {
                    @Override
                    public void onResponse(Call<AppSettingsModel> call, Response<AppSettingsModel> response) {
                        response.body().setChange(settings.getChange());
                        response.body().setControlView(settings.getControlView());
                        bus.post(new AppSettingsSavedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<AppSettingsModel> call, Throwable t) {
                        AppSettingsModel error = new AppSettingsModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());
                        error.setChange(settings.getChange());
                        error.setControlView(settings.getControlView());

                        bus.post(new AppSettingsSavedEvent(error));
                    }
                });
            } catch (Exception exc) {
                AppSettingsModel error = new AppSettingsModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());
                error.setChange(settings.getChange());
                error.setControlView(settings.getControlView());

                bus.post(new AppSettingsSavedEvent(error));
            }
        }

        public void getSettings(UserModel request) {
            try {
                service.getSettings(request).enqueue(new Callback<AppSettingsModel>() {
                    @Override
                    public void onResponse(Call<AppSettingsModel> call, Response<AppSettingsModel> response) {
                        bus.post(new AppSettingsReceivedEvent(response.body()));
                    }

                    @Override
                    public void onFailure(Call<AppSettingsModel> call, Throwable t) {
                        AppSettingsModel error = new AppSettingsModel();
                        error.setMessageId(RETROFIT_ERROR);
                        error.setMessage(t.getMessage());

                        bus.post(new AppSettingsReceivedEvent(error));
                    }
                });
            } catch (Exception exc) {
                AppSettingsModel error = new AppSettingsModel();
                error.setMessageId(UNKNOWN_ERROR);
                error.setMessage(exc.getMessage());

                bus.post(new AppSettingsReceivedEvent(error));
            }
        }

        public void notifyOnline() {
            try {
                UserModel cachedUser = new Cache(context).loadUser();

                UserModel request = new UserModel();
                request.setApiKey(Keys.API_KEY);
                request.setToken(cachedUser.getToken());
                request.setUsername(cachedUser.getUsername());
                service.notifyOnline(request).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        //
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {

                    }
                });
            } catch (Exception exc) {}
        }

        public void notifyOffline() {
            try {
                UserModel cachedUser = new Cache(context).loadUser();

                UserModel request = new UserModel();
                request.setApiKey(Keys.API_KEY);
                request.setToken(cachedUser.getToken());
                request.setUsername(cachedUser.getUsername());
                service.notifyOffline(request).enqueue(new Callback<CoreModel>() {
                    @Override
                    public void onResponse(Call<CoreModel> call, Response<CoreModel> response) {
                        //
                    }

                    @Override
                    public void onFailure(Call<CoreModel> call, Throwable t) {

                    }
                });
            } catch (Exception exc) {}
        }
    }

// Caching
//    private final Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
//        @Override
//        public okhttp3.Response intercept(Chain chain) throws IOException {
//            okhttp3.Response originalResponse = chain.proceed(chain.request());
//            if (isConnected()) {
//                int maxAge = 60; // read from cache for 1 minute
//                return originalResponse.newBuilder()
//                        .header("Cache-Control", "public, max-age=" + maxAge)
//                        .build();
//            } else {
//                int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
//                return originalResponse.newBuilder()
//                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
//                        .build();
//            }
//        }
//    };
}