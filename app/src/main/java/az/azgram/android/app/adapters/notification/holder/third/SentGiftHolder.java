package az.azgram.android.app.adapters.notification.holder.third;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.holder.second.UsernameTextHolder;

/**
 * Created by Rzaaeeff on 6/5/2017.
 */

public class SentGiftHolder extends UsernameTextHolder {
    public SentGiftHolder(View itemView, Context context) {
        super(itemView, context);

        TEXT_FORMAT = context.getString(R.string.notification_sent_gift);
    }
}
