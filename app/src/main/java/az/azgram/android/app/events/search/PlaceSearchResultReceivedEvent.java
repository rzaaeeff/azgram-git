package az.azgram.android.app.events.search;

import az.azgram.android.app.models.response.PlaceListModel;

/**
 * Created by Rzaaeeff on 2/11/2017.
 */

public class PlaceSearchResultReceivedEvent {
    private PlaceListModel model;

    public PlaceSearchResultReceivedEvent(PlaceListModel model) {
        this.model = model;
    }

    public PlaceListModel getModel() {
        return model;
    }

    public void setModel(PlaceListModel model) {
        this.model = model;
    }
}
