package az.azgram.android.app.models;

/**
 * Created by Rzaaeeff on 7/24/2017.
 */

public class AdModel extends PostModel {
    public static final class TYPE {
        public static final byte SITE = 1;
        public static final byte APPLICATION_GAME = 2;
        public static final byte PAGE_PROFILE = 3;
        public static final byte PROMOTION = 4;
    }

    public static final class VIEW {
        public static final byte V100 = 1;
        public static final byte V150 = 2;
        public static final byte V200 = 3;
        public static final byte V500 = 4;
        public static final byte V1000 = 5;
        public static final byte V1500 = 6;
        public static final byte V2000 = 7;
    }

    private Byte adType;
    private Byte viewCount;

    public Byte getAdType() {
        return adType;
    }

    public void setAdType(Byte adType) {
        this.adType = adType;
    }

    public Byte getViewCount() {
        return viewCount;
    }

    public void setViewCount(Byte viewCount) {
        this.viewCount = viewCount;
    }
}
