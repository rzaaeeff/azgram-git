package az.azgram.android.app.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.users.SignUpEvent;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.FirebaseTokenHandler;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.Security;
import az.azgram.android.app.operations.Utility;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegistrationActivity extends AppCompatActivity {

    // Widgets
    private EditText editTextUsername, editTextPassword, editTextPasswordConfirm,
            editTextEmail, editTextFullName;
    private Spinner spinnerCountry, spinnerGender;
    private CheckBox checkBoxRules;

    // Data
    private String username;
    private String password;
    private String passwordConfirm;
    private String email;
    private String fullname;
    private int country;
    private byte gender;
    private UserModel user;

    Bus bus = new Bus();
    boolean requested = false;
    Dialog loadingDialog = null;
    RegistrationTask regTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        assignIds();
    }

    @Override
    protected void onResume() {
        super.onResume();
        requested = false;

        bus.register(this);
    }

    @Override
    protected void onPause() {
        bus.unregister(this);
        requested = true;
        if (regTask != null) regTask.cancel(true);

        super.onPause();
    }

    private void assignIds() {
        editTextUsername = (EditText) findViewById(R.id.editTextRAUsername);
        editTextPassword = (EditText) findViewById(R.id.editTextRAPassword);
        editTextPasswordConfirm = (EditText) findViewById(R.id.editTextRAPasswordConfirm);
        editTextEmail = (EditText) findViewById(R.id.editTextRAEmail);
        editTextFullName = (EditText) findViewById(R.id.editTextRAFullName);

        spinnerCountry = (Spinner) findViewById(R.id.spinnerRACountry);
        spinnerGender = (Spinner) findViewById(R.id.spinnerRAGender);

        checkBoxRules = (CheckBox) findViewById(R.id.checkboxRARules);
    }

    /**
     * Buton click event
     *
     * @param view
     */
    public void signUpClicked(View view) {
        if (getCurrentFocus() instanceof EditText) {
            Utility.hideSoftKeyboard(getCurrentFocus(), this);
        }

        if (editTextUsername.getText().toString().toString().trim().length() > 0 &&
                editTextPassword.getText().toString().trim().length() > 0 &&
                editTextPasswordConfirm.getText().toString().trim().length() > 0 &&
                editTextEmail.getText().toString().trim().length() > 0 &&
                editTextFullName.getText().toString().trim().length() > 0) {

            username = editTextUsername.getText().toString().trim();
            password = editTextPassword.getText().toString().trim();
            passwordConfirm = editTextPasswordConfirm.getText().toString().trim();
            email = editTextEmail.getText().toString().trim();
            fullname = editTextFullName.getText().toString().trim();
            country = spinnerCountry.getSelectedItemPosition();
            gender = (byte) spinnerGender.getSelectedItemPosition();
            if (password.equals(passwordConfirm) && checkBoxRules.isChecked() &&
                    Utility.isValidEmailAddress(email) && Utility.isValidUsername(username)) {
                // Confirmed!
                String hash = Security.getHash(password, Security.MD5);

                user = new UserModel();
                user.setUsername(username);
                user.setPasswordHash(hash);
                user.setEmail(email);
                user.setFullName(fullname);
                user.setCountry(country);
                user.setGender(gender);
                user.setApiKey(Keys.API_KEY);
                // Unique Android ID
                user.setDeviceId(Utility.getDeviceId(this));
                user.setIpAddress(Utility.getIPAddress(true));

//                To show the example input to the backend developer...
//                String json = new Gson().toJson(user, UserModel.class);

                signUp(user);


            } else if (!Utility.isValidUsername(username)) {
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageInvalidUsername)
                );
            } else if (!password.equals(passwordConfirm)) {
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageDifferentPasswords)
                );
            } else if (!Utility.isValidEmailAddress(email)) {
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageInvalidEmail)
                );
            } else if (!checkBoxRules.isChecked()) {
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageAcceptEULA)
                );
            }
        } else {
            Interaction.getInstance(this).showErrorDialog(
                    getString(R.string.LoginActivityMessageFillAllFields)
            );
        }
    }

    private void signUp(UserModel user) {
        if (!requested) {
            requested = true;

            loadingDialog = Interaction.getInstance(this).showLoadingDialog(
                    getString(R.string.PleaseWait)
            );

            if (regTask == null) {
                regTask = new RegistrationTask(this, user);
                regTask.execute();
            }

        } else {
            Toast.makeText(this, "Wait", Toast.LENGTH_LONG).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(SignUpEvent event) {
        switch (event.getModel().getMessageId()) {
            case NetworkOperations.API_SUCCESS:
                UserModel user = event.getModel();
                // Dummy
                user.setPasswordHash(this.user.getPasswordHash());

                if (user.getPostCount() == null)
                    user.setPostCount(0);

                if (user.getFollowerCount() == null)
                    user.setFollowerCount(0);

                if (user.getFollowingCount() == null)
                    user.setFollowingCount(0);

                if (user.getQuestionCount() == null)
                    user.setQuestionCount(0);

                if (user.getInfo() == null)
                    user.setInfo("Add a bio");

                Cache cache = new Cache(this);
                cache.saveUser(user);

                startActivity(new Intent(this, CoreActivity.class));
                Toast.makeText(this, "Success", Toast.LENGTH_LONG).show();

                FirebaseTokenHandler tokenHandler = new FirebaseTokenHandler(this);
                tokenHandler.setTokenSent(false);
                tokenHandler.sendToServer();
                finish();
                break;
            case NetworkOperations.NOT_CONNECTED:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.NotConnected)
                );
                break;
            case NetworkOperations.CONNECTED_NOT_ONLINE:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.NotOnline)
                );
                break;
            case NetworkOperations.Users.EXISTING_USERNAME:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageExistingUsername)
                );
                break;
            case NetworkOperations.Users.EXISTING_EMAIL:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageExistingEmail)
                );
                break;
            case NetworkOperations.Users.INVALID_USERNAME:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageInvalidUsername)
                );
                break;
            case NetworkOperations.Users.INVALID_EMAIL:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.RegistrationActivityMessageInvalidEmail)
                );
                break;
            default:
                Interaction.getInstance(this).showErrorDialog(
                        getString(R.string.UnknownError) + ":\n"
                                + getString(R.string.TryAgain)
                );
                break;
        }

        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog = null;
            }
        }

        requested = false;
    }

    public void closeActivity(View view) {
        finish();
    }

    // To support fonts
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private class RegistrationTask extends AsyncTask<Void, Void, Integer> {
        private Context context;
        private UserModel user;

        public RegistrationTask(Context context, UserModel user) {
            this.context = context;
            this.user = user;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            int code = NetworkOperations.UNKNOWN_ERROR;

            if (!isCancelled()) {
                NetworkOperations network = new NetworkOperations(context);
                code = network.isConnectedAndOnline();
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer code) {
            if (!isCancelled()) {
                switch (code) {
                    case NetworkOperations.CONNECTED_AND_ONLINE:
                        NetworkOperations service = new NetworkOperations(context, bus);
                        service.getUsersEndpoint().signUp(user);
                        break;
                    default:
                        UserModel error = new UserModel();
                        error.setMessageId(code);

                        bus.post(new SignUpEvent(error));
                        break;
                }
            }

            regTask = null;
        }
    }
}
