package az.azgram.android.app.adapters.notification.holder.first;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 5/19/2017.
 */

public class TextNotificationHolder extends NotificationHolder {
    public static final int LAYOUT_RES = R.layout.recycler_item_notification_text;
    public TextNotificationHolder(View itemView, final Context context) {
        super(itemView, context);
    }

    @Override
    public void bind(NotificationModel notification, CharSequence text) {
        super.bind(notification, text);
    }

    public void bind(NotificationModel notification) {
        super.bind(notification, "");
    }
}
