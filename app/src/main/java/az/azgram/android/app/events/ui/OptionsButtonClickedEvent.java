package az.azgram.android.app.events.ui;

import az.azgram.android.app.models.PostModel;

/**
 * Created by Rzaaeeff on 2/2/2017.
 */

public class OptionsButtonClickedEvent {
    private PostModel model;

    public OptionsButtonClickedEvent(PostModel model) {
        this.model = model;
    }

    public PostModel getModel() {
        return model;
    }

    public void setModel(PostModel model) {
        this.model = model;
    }
}
