package az.azgram.android.app.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import az.azgram.android.app.R;

public class BrowserActivity extends AppCompatActivity {

    // Constants
    public static final String EXTRA_URL = "extra_url";

    // Widgets
    private WebView webView;

    // Other elements

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        if (getIntent().hasExtra(EXTRA_URL)) {
            String url = getIntent().getStringExtra(EXTRA_URL);

            webView = (WebView) findViewById(R.id.webView);
            webView.loadUrl(url);
        } else {
            finish();
        }
    }

    public void finishBrowserActivity(View view) {
        finish();
    }
}
