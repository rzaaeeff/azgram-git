package az.azgram.android.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import az.azgram.android.app.R;
import az.azgram.android.app.activities.SinglePostActivity;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.response.FeedModel;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 2/11/2017.
 */

public class RecyclerAdapterPost extends RecyclerView.Adapter<RecyclerAdapterPost.PostHolder> {
    private Context context;
    private FeedModel data;

    public RecyclerAdapterPost(Context context, FeedModel data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public PostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.recycler_item_post,
                parent,
                false
        );

        return new PostHolder(view);
    }

    @Override
    public void onBindViewHolder(PostHolder holder, int position) {
        holder.bind(data.getPosts().get(position));
    }

    @Override
    public int getItemCount() {
        return data.getPosts().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class PostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageView;
        private TextView textView;
        private LinearLayout rootLayout;

        public PostHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imageViewPostItem);
            textView = (TextView) itemView.findViewById(R.id.textViewPostItem);
            rootLayout = (LinearLayout) itemView.findViewById(R.id.rootLayoutPostItem);

            rootLayout.setOnClickListener(this);

//            List<View> views = new ArrayList<>();
//            views.add(textView);
//            LinkMechanism.set(views, context);
        }

        public void bind(PostModel post) {
            Utility.loadWithPicasso(
                    context,
                    post.getPhoto().getSmallestPossible(),
                    imageView,
                    R.drawable.post_photo_placeholder
            );

            if (post.getText() != null)
                textView.setText(post.getText());
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                switch (view.getId()) {
                    case R.id.rootLayoutPostItem:
                        Intent intent = new Intent(context, SinglePostActivity.class);
                        intent.putExtra(PostModel.UID, data.getPosts().get(position));
                        context.startActivity(intent);
                        break;
                }
            }
        }
    }
}