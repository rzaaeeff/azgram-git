package az.azgram.android.app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ToggleButton;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.QuestionAskedEvent;
import az.azgram.android.app.models.PhotoModel;
import az.azgram.android.app.models.QuestionModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.ImageUtils;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.LinkMechanism;
import az.azgram.android.app.operations.NetworkOperations;
import com.squareup.otto.Subscribe;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cn.refactor.lib.colordialog.ColorDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AskQuestionActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 123;
    // Widgets
    private ToggleButton toggleButtonAnonymous;
    private EditText editText;
    private ImageButton imageButtonPick, imageButtonSend;
    private ProgressBar progressBar;

    // Other elements
    private UserModel target;
    private QuestionModel request;
    private boolean isLoading = false;
    private Bitmap image = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_question);

        if (getIntent().hasExtra(UserModel.UID))
            target = (UserModel) getIntent().getSerializableExtra(UserModel.UID);
        else
            finish();

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        BusProvider.getInstance().unregister(this);

        super.onPause();
    }

    /**
     * Assigns IDs and does some other initialization
     */
    private void init() {
        toggleButtonAnonymous = (ToggleButton) findViewById(R.id.toggleButtonAnonymousAskQuestionActivity);
        editText = (EditText) findViewById(R.id.editTextAskQuestionActivity);
        imageButtonPick = (ImageButton) findViewById(R.id.imageButtonPickAskQuestionActivity);
        imageButtonSend = (ImageButton) findViewById(R.id.imageButtonSendAskQuestionActivity);
        progressBar = (ProgressBar) findViewById(R.id.progressBarAskQuestionActivity);

        editText.setTextIsSelectable(true);
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        // Hashtag and Mention
        List<View> views = new ArrayList<>();
        views.add(editText);
        LinkMechanism.set(views, this);

        // Initializing request
        request = new QuestionModel();

        request.setTarget(target);

        request.setToken(new Cache(this).loadUser().getToken());
        request.setApiKey(Keys.API_KEY);

        stopLoading();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * OnClick method for back button
     * Referenced from XML
     */
    public void finishAskQuestionActivity(View view) {
        final ColorDialog dialog;
        if (isLoading) {
            dialog = Interaction.getInstance(this).showPromptDialog(
                    getString(R.string.ProcessOngoing) + "\n" + getString(R.string.DoYouWantCancel));

            dialog.setPositiveListener(getString(R.string.Yes), new ColorDialog.OnPositiveListener() {
                @Override
                public void onClick(ColorDialog colorDialog) {
                    finish();
                }
            })
                    .setNegativeListener(getString(R.string.No), new ColorDialog.OnNegativeListener() {
                        @Override
                        public void onClick(ColorDialog colorDialog) {
                            dialog.dismiss();
                        }
                    });

            dialog.show();
        } else {
            finish();
        }
    }

    private void startLoading() {
        isLoading = true;

        final Context context = this;
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,
                        getString(R.string.ProcessOngoing) + "\n" + getString(R.string.PleaseWait)
                        , Toast.LENGTH_SHORT).show();
            }
        };

        imageButtonSend.setOnClickListener(onClickListener);
        imageButtonPick.setOnClickListener(onClickListener);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        progressBar.setVisibility(View.GONE);

        imageButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ask();
            }
        });

        imageButtonPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.PickAnImage)), PICK_IMAGE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Toast.makeText(this, getString(R.string.UnknownError), Toast.LENGTH_SHORT).show();
                image = null;
                return;
            }

            try {
                InputStream inputStreamImage = getContentResolver().openInputStream(data.getData());
                image = ImageUtils.inputStreamToBitmap(inputStreamImage);
            } catch (Exception exc) {
                image = null;
            }
        }
    }

    private void ask() {
        if (!isLoading) {
            if (editText.getText() != null && editText.getText().toString().trim().length() > 0) {
                request.setText(editText.getText().toString());

                request.setAnonymous(toggleButtonAnonymous.isChecked());

                if (image != null) {
                    try {
                        String background = ImageUtils.encodeBitmapToBase64(image);
                        request.setBackgroundPhoto(new PhotoModel());
                        request.getBackgroundPhoto().setLarge(background);
                    } catch (Exception exc) {
                        request.setBackgroundPhoto(null);
                    }
                }

                NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
                service.getPostsEndpoint().askQuestion(request);
                startLoading();
            }
        } else
            Toast.makeText(this,
                    getString(R.string.ProcessOngoing) + "\n" + getString(R.string.PleaseWait),
                    Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onAzGramServiceResponse(QuestionAskedEvent event) {
        stopLoading();

        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, getString(R.string.AskQuestionActivitySuccessMessage), Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }
}