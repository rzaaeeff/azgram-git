package az.azgram.android.app.events.messages;

import az.azgram.android.app.models.CoreModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class ConversationMarkedAsUnreadEvent {
    private CoreModel model;

    public ConversationMarkedAsUnreadEvent(CoreModel model) {
        this.model = model;
    }

    public CoreModel getModel() {
        return model;
    }

    public void setModel(CoreModel model) {
        this.model = model;
    }
}
