package az.azgram.android.app.adapters.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.DateUtils;
import az.azgram.android.app.operations.NetworkOperations;

/**
 * Created by Rzaaeeff on 5/3/2017.
 */

public class RecyclerAdapterChat extends RecyclerView.Adapter<MessageHolder> {
    public class _Type {
        public static final byte TEXT_IN = 1;
        public static final byte TEXT_OUT = 2;
        public static final byte PHOTO_IN = 3;
        public static final byte PHOTO_OUT = 4;
        public static final byte AUDIO_IN = 5;
        public static final byte AUDIO_OUT = 6;
        public static final byte POST_IN = 7;
        public static final byte POST_OUT = 8;
        public static final byte USER_IN = 9;
        public static final byte USER_OUT = 10;
    }

    private ConversationModel data;
    private Context context;

    private MessageModel request;
    private NetworkOperations serviceInstance;
    private String mainUsername;

    public RecyclerAdapterChat(Context context, ConversationModel data) {
        super();

        this.context = context;
        this.data = data;

        mainUsername = new Cache(context).loadUser().getUsername();

        serviceInstance = new NetworkOperations(context, BusProvider.getInstance());
        serviceInstance.getMessagesEndpoint();
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case _Type.TEXT_OUT:
                view = LayoutInflater.from(context).inflate(
                        TextHolder.LAYOUT_RES_OUT,
                        parent,
                        false
                );

                return new TextHolder(view, context);
            case _Type.TEXT_IN:
                view = LayoutInflater.from(context).inflate(
                        TextHolder.LAYOUT_RES_IN,
                        parent,
                        false
                );

                return new TextHolder(view, context);
            case _Type.PHOTO_OUT:
                view = LayoutInflater.from(context).inflate(
                        PhotoHolder.LAYOUT_RES_OUT,
                        parent,
                        false
                );

                return new PhotoHolder(view, context);
            case _Type.PHOTO_IN:
                view = LayoutInflater.from(context).inflate(
                        PhotoHolder.LAYOUT_RES_IN,
                        parent,
                        false
                );

                return new PhotoHolder(view, context);
            case _Type.AUDIO_OUT:
                view = LayoutInflater.from(context).inflate(
                        AudioHolder.LAYOUT_RES_OUT,
                        parent,
                        false
                );

                return new AudioHolder(view, context);
            case _Type.AUDIO_IN:
                view = LayoutInflater.from(context).inflate(
                        AudioHolder.LAYOUT_RES_IN,
                        parent,
                        false
                );

                return new AudioHolder(view, context);
            case _Type.POST_OUT:
                view = LayoutInflater.from(context).inflate(
                        PostHolder.LAYOUT_RES_OUT,
                        parent,
                        false
                );

                return new PostHolder(view, context);
            case _Type.POST_IN:
                view = LayoutInflater.from(context).inflate(
                        PostHolder.LAYOUT_RES_IN,
                        parent,
                        false
                );

                return new PostHolder(view, context);
            case _Type.USER_OUT:
                view = LayoutInflater.from(context).inflate(
                        UserHolder.LAYOUT_RES_OUT,
                        parent,
                        false
                );

                return new UserHolder(view, context);
            case _Type.USER_IN:
                view = LayoutInflater.from(context).inflate(
                        UserHolder.LAYOUT_RES_IN,
                        parent,
                        false
                );

                return new UserHolder(view, context);
            default:
                view = LayoutInflater.from(context).inflate(
                        TextHolder.LAYOUT_RES_IN,
                        parent,
                        false
                );

                return new TextHolder(view, context);
        }
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
//        boolean shouldShowDetails = false;
//
//        if (position == 0 ||
//                !DateUtils.isSameHour(
//                        DateUtils.getDateFromString(data.getMessages().get(position).getDate(), CoreModel.DATE_TIME_FORMAT),
//                        DateUtils.getDateFromString(data.getMessages().get(position - 1).getDate(), CoreModel.DATE_TIME_FORMAT)) ||
//                !data.getMessages().get(position).getAuthor().getUsername().equals(
//                        data.getMessages().get(position - 1).getAuthor().getUsername()
//                )) shouldShowDetails = true;

        holder.bind(data.getMessages().get(position), true);
    }

    @Override
    public int getItemCount() {
        return data.getMessages().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        MessageModel message = data.getMessages().get(position);
        int type = _Type.TEXT_IN;

        switch (message.getType()) {
            case MessageModel.Type.TEXT:
                if (message.getAuthor().getUsername() != null &&
                        message.getAuthor().getUsername().equals(mainUsername))
                    type = _Type.TEXT_OUT;
                else
                    type = _Type.TEXT_IN;
                break;
            case MessageModel.Type.PHOTO:
                if (message.getAuthor().getUsername() != null &&
                        message.getAuthor().getUsername().equals(mainUsername))
                    type = _Type.PHOTO_OUT;
                else
                    type = _Type.PHOTO_IN;
                break;
            case MessageModel.Type.AUDIO:
                if (message.getAuthor().getUsername() != null &&
                        message.getAuthor().getUsername().equals(mainUsername))
                    type = _Type.AUDIO_OUT;
                else
                    type = _Type.AUDIO_IN;
                break;
            case MessageModel.Type.POST:
                if (message.getAuthor().getUsername() != null &&
                        message.getAuthor().getUsername().equals(mainUsername))
                    type = _Type.POST_OUT;
                else
                    type = _Type.POST_IN;
                break;
            case MessageModel.Type.USER:
                if (message.getAuthor().getUsername() != null &&
                        message.getAuthor().getUsername().equals(mainUsername))
                    type = _Type.USER_OUT;
                else
                    type = _Type.USER_IN;
                break;
        }

        return type;
    }
}