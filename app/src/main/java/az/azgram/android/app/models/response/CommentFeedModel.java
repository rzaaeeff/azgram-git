package az.azgram.android.app.models.response;

import az.azgram.android.app.models.CommentModel;
import az.azgram.android.app.models.CoreModel;

import java.util.List;

/**
 * Created by Rzaaeeff on 1/14/2017.
 */

public class CommentFeedModel extends CoreModel {
    private List<CommentModel> comments;

    public List<CommentModel> getComments() {
        return comments;
    }

    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }
}
