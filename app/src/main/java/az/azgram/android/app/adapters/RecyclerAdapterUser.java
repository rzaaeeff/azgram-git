package az.azgram.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.users.BlockedUserEvent;
import az.azgram.android.app.events.users.FollowEvent;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.UserWrapperModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;

import com.squareup.otto.Subscribe;

/**
 * Created by Rzaaeeff on 2/11/2017.
 */

public class RecyclerAdapterUser extends RecyclerView.Adapter<RecyclerAdapterUser.UserHolder> {
    private Context context;
    private UserListModel data;
    private static NetworkOperations service;
    private static UserModel request;

    public RecyclerAdapterUser(Context context, UserListModel data) {
        this.context = context;
        this.data = data;

        BusProvider.getInstance().register(this);

        if (service == null) {
            service = new NetworkOperations(context, BusProvider.getInstance());
            service.getUsersEndpoint();
        }

        if (request == null) {
            request = new UserModel();
            request.setApiKey(Keys.API_KEY);
            request.setToken(new Cache(context).loadUser().getToken());
        }
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.recycler_item_user,
                parent,
                false
        );

        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        holder.bind(data.getUsers().get(position));
    }

    @Override
    public int getItemCount() {
        return data.getUsers().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Subscribe
    public void onAzGramServiceResponse(FollowEvent event) {
        for (int position = 0; position < data.getUsers().size(); position++)
            if (data.getUsers().get(position).getUsername().equals(
                    event.getModel().getUsername()
            )) {
                UserModel user = data.getUsers().get(position);

                if (event.getModel().getUsername().equals(user.getUsername())) {
                    if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {

                        if (user.getFollowStatus() == UserModel.FollowStatus.FOLLOWING ||
                                user.getFollowStatus() == UserModel.FollowStatus.PENDING) {
                            user.setFollowStatus(UserModel.FollowStatus.NOT_FOLLOWING);
                        } else {
                            user.setFollowStatus(UserModel.FollowStatus.FOLLOWING);
                        }

                        notifyItemChanged(position);
                    } else if (event.getModel().getMessageId() == NetworkOperations.Users.PENDING) {
                        user.setFollowStatus(UserModel.FollowStatus.PENDING);
                        notifyItemChanged(position);
                    } else {
                        Toast.makeText(context, context.getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                    }
                }

                break;
            }
    }

    @Subscribe
    public void onAzGramServiceResponse(BlockedUserEvent event) {
        for (int position = 0; position < data.getUsers().size(); position++)
            if (data.getUsers().get(position).getUsername().equals(
                    event.getModel().getUsername()
            )) {
                UserModel user = data.getUsers().get(position);

                if (event.getModel().getUsername().equals(user.getUsername())) {
                    if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
                        user.setBlocked(!user.isBlocked());
                        notifyItemChanged(position);
                    } else {
                        Toast.makeText(context, context.getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
                    }
                }

                break;
            }
    }

    public class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageViewProfilePhoto, imageViewUserBadge;
        private TextView textView;
        private Button buttonNotFollowing, buttonFollowing, buttonPending,
                buttonBlock, buttonUnblock;
        private LinearLayout rootLayout;

        public UserHolder(View itemView) {
            super(itemView);

            imageViewProfilePhoto = (ImageView) itemView.findViewById(R.id.imageViewProfilePhotoUserItem);
            imageViewUserBadge = (ImageView) itemView.findViewById(R.id.imageViewUserBadgeUserItem);
            textView = (TextView) itemView.findViewById(R.id.textViewUserItem);
            buttonNotFollowing = (Button) itemView.findViewById(R.id.buttonNotFollowingUserItem);
            buttonFollowing = (Button) itemView.findViewById(R.id.buttonFollowingUserItem);
            buttonPending = (Button) itemView.findViewById(R.id.buttonPendingUserItem);
            buttonBlock = (Button) itemView.findViewById(R.id.buttonBlockUserItem);
            buttonUnblock = (Button) itemView.findViewById(R.id.buttonUnblockUserItem);
            rootLayout = (LinearLayout) itemView.findViewById(R.id.rootLayoutUserItem);

            rootLayout.setOnClickListener(this);
            buttonFollowing.setOnClickListener(this);
            buttonNotFollowing.setOnClickListener(this);
            buttonPending.setOnClickListener(this);
            buttonBlock.setOnClickListener(this);
            buttonUnblock.setOnClickListener(this);
        }

        public void bind(UserModel user) {
            Utility.loadWithPicasso(
                    context,
                    user.getProfilePhoto().getSmallestPossible(),
                    imageViewProfilePhoto,
                    R.drawable.profile_photo_placeholder
            );

            imageViewUserBadge.setImageResource(
                    user.isOnline() ? R.drawable.user_online_badge :
                            R.drawable.user_offline_badge
            );

            if (user.getFullName() != null) {
                if (user.getFullName().length() > 20)
                    textView.setText(user.getUsername());
                else
                    textView.setText(user.getFullName());
            } else {
                textView.setText(user.getUsername());
            }

            updateFollowStatus(user);
        }

        private void updateFollowStatus(UserModel user) {
            buttonNotFollowing.setVisibility(View.GONE);
            buttonFollowing.setVisibility(View.GONE);
            buttonPending.setVisibility(View.GONE);
            buttonBlock.setVisibility(View.GONE);
            buttonUnblock.setVisibility(View.GONE);

            if (user.getFollowStatus() != null) {
                switch (user.getFollowStatus()) {
                    case UserModel.FollowStatus.NOT_FOLLOWING:
                        buttonNotFollowing.setVisibility(View.VISIBLE);
                        break;
                    case UserModel.FollowStatus.FOLLOWING:
                        buttonFollowing.setVisibility(View.VISIBLE);
                        break;
                    case UserModel.FollowStatus.PENDING:
                        buttonPending.setVisibility(View.VISIBLE);
                        break;
                }
            } else {
                if (user.isBlocked()) {
                    buttonUnblock.setVisibility(View.VISIBLE);
                } else {
                    buttonBlock.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                switch (view.getId()) {
                    case R.id.rootLayoutUserItem:
                        UniversalListenerMethods.onClickUser(
                                "@" + data.getUsers().get(position).getUsername(), context);
                        break;
                    case R.id.buttonFollowingUserItem:
                    case R.id.buttonNotFollowingUserItem:
                    case R.id.buttonPendingUserItem:
                        request.setUsername(
                                data.getUsers().get(position).getUsername()
                        );
                        service.getUsersEndpoint().follow(
                                request
                        );
                        break;
                    case R.id.buttonBlockUserItem:
                    case R.id.buttonUnblockUserItem:
                        UserWrapperModel blockRequest = new UserWrapperModel();
                        blockRequest.setTarget(new UserModel());
                        blockRequest.getTarget().setUsername(
                                data.getUsers().get(position).getUsername()
                        );
                        blockRequest.setApiKey(request.getApiKey());
                        blockRequest.setToken(request.getToken());
                        service.getUsersEndpoint().block(blockRequest);
                        break;
                }
            }
        }
    }
}