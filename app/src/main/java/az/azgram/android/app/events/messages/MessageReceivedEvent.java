package az.azgram.android.app.events.messages;

import az.azgram.android.app.models.MessageModel;

/**
 * Created by Rzaaeeff on 7/5/2017.
 */

public class MessageReceivedEvent {
    private MessageModel model;

    public MessageReceivedEvent(MessageModel model) {
        this.model = model;
    }

    public MessageModel getModel() {
        return model;
    }

    public void setModel(MessageModel model) {
        this.model = model;
    }
}
