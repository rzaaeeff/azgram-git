package az.azgram.android.app.events.ui;

import az.azgram.android.app.models.CommentModel;

/**
 * Created by Rzaaeeff on 1/26/2017.
 */

public class CommentButtonClickedEvent {
    private CommentModel model;

    public CommentButtonClickedEvent(CommentModel model) {
        this.model = model;
    }

    public CommentModel getModel() {
        return model;
    }

    public void setModel(CommentModel model) {
        this.model = model;
    }
}
