package az.azgram.android.app.models;

import java.io.Serializable;

/**
 * Created by Rzaaeeff on 11/30/2016.
 */

public class UserSettingsModel extends CoreModel implements Serializable {

    // Fields
    private String fullName;
    private String email;
    private String username;
    private String login;
    private String phoneNumber;
    private String passwordHash;
    private String info;
    private String birthDate;
    private String lastSeenDate;
    private String change;
    private String firebaseToken;
    private PhotoModel profilePhoto;
    private Byte gender;
    private Byte profileDesign;
    private Byte followStatus;
    private Integer country;
    private Integer postCount;
    private Integer followerCount;
    private Integer followingCount;
    private Integer questionCount;
    private Boolean isOnline;
    private Boolean isHidden;
    private Boolean wantsToFollowYou;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getLastSeenDate() {
        return lastSeenDate;
    }

    public void setLastSeenDate(String lastSeenDate) {
        this.lastSeenDate = lastSeenDate;
    }

    public PhotoModel getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(PhotoModel profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public Byte getProfileDesign() {
        return profileDesign;
    }

    public void setProfileDesign(Byte profileDesign) {
        this.profileDesign = profileDesign;
    }

    public Byte getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(Byte followStatus) {
        this.followStatus = followStatus;
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public Integer getPostCount() {
        return postCount;
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }

    public Integer getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(Integer followerCount) {
        this.followerCount = followerCount;
    }

    public Integer getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(Integer followingCount) {
        this.followingCount = followingCount;
    }

    public Integer getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(Integer questionCount) {
        this.questionCount = questionCount;
    }

    public Boolean isOnline() {
        return isOnline;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public Boolean isHidden() {
        return isHidden;
    }

    public void setHidden(Boolean hidden) {
        isHidden = hidden;
    }

    public Boolean wantsToFollowYou() {
        return wantsToFollowYou;
    }

    public void setWantsToFollowYou(Boolean wantsToFollowYou) {
        this.wantsToFollowYou = wantsToFollowYou;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }
}
