package az.azgram.android.app.adapters.chat;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import az.azgram.android.app.R;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 5/3/2017.
 */

class PostHolder extends MessageHolder implements View.OnClickListener {
    static final int LAYOUT_RES_IN = R.layout.recycler_item_message_post_incoming;
    static final int LAYOUT_RES_OUT = R.layout.recycler_item_message_post_outgoing;

    private ImageView imageViewPost;
    private TextView textViewUsernameAndBadge, textViewPost;
    private RelativeLayout relativeLayout;

    // Other elements
    private long postId;
    private String errorPrivate;

    public PostHolder(View itemView, Context context) {
        super(itemView, context);

        imageViewPost = (ImageView) itemView.findViewById(R.id.imageViewPost_MessageItem);
        textViewUsernameAndBadge = (TextView) itemView.findViewById(R.id.textViewUsernameAndBadge_MessageItem);
        textViewPost = (TextView) itemView.findViewById(R.id.textViewPost_MessageItem);
        relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayoutMessage_MessageItem);

        errorPrivate = context.getString(R.string.MessageItemPrivatePostText);

        super.setRootListener(this);
    }

    public void bind(MessageModel message, boolean shouldShowDetails) {
        super.bind(message, shouldShowDetails);
        // TODO: postId = message.getPost().getId();

        PostModel post = message.getPost();
        if (post != null) {
            postId = post.getId();
            switch (post.getType()) {
                case PostModel.Type.STATUS:
                    imageViewPost.setImageResource(R.drawable.ic_post_thumbnail_status);
                    break;
                case PostModel.Type.PHOTO:
                    Utility.loadWithPicasso(
                            context,
                            post.getPhoto().getSmallestPossible(),
                            imageViewPost,
                            R.drawable.ic_post_thumbnail_photo
                    );
                    break;
                case PostModel.Type.AUDIO:
                    imageViewPost.setImageResource(R.drawable.ic_post_thumbnail_audio);
                    break;
                case PostModel.Type.VIDEO:
                    imageViewPost.setImageResource(R.drawable.ic_post_thumbnail_video);
                    break;
                default:
                    imageViewPost.setImageResource(R.drawable.ic_post_thumbnail_photo);
            }

            if (post.getText() != null) {
                textViewPost.setText(post.getText());
            } else {
                textViewPost.setVisibility(View.GONE);
            }

            textViewUsernameAndBadge.setText(
                    String.format(
                            context.getString(R.string.by_username),
                            post.getAuthor().getUsername()
                    )
            );

            SpannableString badge = new SpannableString(" \u2B24"/* use \u2022 for normal bullet*/);

            if (post.getAuthor().isOnline()) {
                badge.setSpan(new ForegroundColorSpan(Color.parseColor("#02ab56")), 0,
                        badge.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else {
                badge.setSpan(new ForegroundColorSpan(Color.RED), 0,
                        badge.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            textViewUsernameAndBadge.append(badge);
        } else {
            // Post is private
            super.setRootListener(null);

            for (int i = 0; i < relativeLayout.getChildCount(); i++) {
                View view = relativeLayout.getChildAt(i);
                view.setVisibility(View.GONE);
            }

            TextView textView = new TextView(context);
            textView.setText(errorPrivate);
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lock, 0, 0, 0);
            textView.setGravity(Gravity.CENTER);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            textView.setLayoutParams(params);

            relativeLayout.addView(textView);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rootLayoutMessage_MessageItem:
                UniversalListenerMethods.onClickPost(postId, context);
                break;
        }
    }
}
