package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import az.azgram.android.app.activities.CommentsActivity;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.models.PostModel;

/**
 * Created by Rzaaeeff on 1/24/2017.
 */

class CommentButtonOnClickListener implements View.OnClickListener {

    private final Context context;
    private final PostModel minPost;
    private final boolean shouldOpenInputField;

    public CommentButtonOnClickListener(Context context, PostModel minPost, boolean shouldOpenInputField) {
        this.context = context;
        this.minPost = minPost;
        this.shouldOpenInputField = shouldOpenInputField;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(context, CommentsActivity.class);
        intent.putExtra(PostModel.UID, minPost);
        intent.putExtra(Constants.SHOULD_OPEN_INPUT_FIELD, shouldOpenInputField);
        context.startActivity(intent);
    }
}
