package az.azgram.android.app.adapters.notification.holder.first;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.View;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.notification.RecyclerAdapterNotification;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.operations.UniversalListenerMethods;

/**
 * Created by Rzaaeeff on 5/19/2017.
 */

public class RequestNotificationHolder extends TextNotificationHolder implements View.OnClickListener {
    public static final int LAYOUT_RES = R.layout.recycler_item_notification_request;
    // Widgets
    private AppCompatButton buttonAccept, buttonReject;

    public RequestNotificationHolder(View itemView, final Context context) {
        super(itemView, context);

        buttonAccept = (AppCompatButton) itemView.findViewById(R.id.buttonAccept_NotificationItem);
        buttonReject = (AppCompatButton) itemView.findViewById(R.id.buttonReject_NotificationItem);

        buttonAccept.setOnClickListener(this);
        buttonReject.setOnClickListener(this);

        setRootListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UniversalListenerMethods.onClickUser(
                        notification.getUsers().get(0).getUsername(),
                        context
                );
            }
        });

        TEXT_FORMAT = context.getString(R.string.notification_sent_request);
    }

    public void bind(NotificationModel notification) {
        super.bind(notification, TextUtils.concat(
                UniversalListenerMethods.getHighlightedAndClickableUsername(
                        notification.getUsers().get(0),
                        context
                ), " ", TEXT_FORMAT
        ));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonAccept_NotificationItem:
                RecyclerAdapterNotification.request.setUsername(notification.getUsers().get(0).getUsername());
                RecyclerAdapterNotification.service.getUsersEndpoint().acceptFollowRequest(
                        RecyclerAdapterNotification.request
                );
                break;
            case R.id.buttonReject_NotificationItem:
                RecyclerAdapterNotification.request.setUsername(notification.getUsers().get(0).getUsername());
                RecyclerAdapterNotification.service.getUsersEndpoint().rejectFollowRequest(
                        RecyclerAdapterNotification.request
                );
                break;
        }
    }
}