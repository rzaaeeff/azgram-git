package az.azgram.android.app.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rzaaeeff on 1/12/2017.
 */

public class GiftModel extends CoreModel {
    private UserModel author;
    private UserModel receiver;
    @SerializedName("photo")
    private String url; // TODO: change it to URL
    private String text;
    private Long price;
    private boolean selected;

    public UserModel getAuthor() {
        return author;
    }

    public void setAuthor(UserModel author) {
        this.author = author;
    }

    public UserModel getReceiver() {
        return receiver;
    }

    public void setReceiver(UserModel receiver) {
        this.receiver = receiver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}