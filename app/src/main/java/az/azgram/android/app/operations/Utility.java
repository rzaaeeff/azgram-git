package az.azgram.android.app.operations;

import android.content.Context;
import android.content.res.Resources;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.LeadingMarginSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Rzaaeeff on 12/5/2016.
 */

public class Utility {

    private static final String USERNAME = "[a-zA-Z0-9\\._]+"; // removed hyphen (-)
    private static final Pattern USERNAME_PATTERN =
            Pattern.compile(USERNAME);

    public static String getDeviceId(Context context) {
        String id = null;

        id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return id;
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions

        return "";
    }

    public static void hideSoftKeyboard(View view, Context context) {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    // Clears all input fields within group
    public static void clearFields(ViewGroup group) {
        for (int i = 0; i < group.getChildCount(); i++) {
            View view = group.getChildAt(i);
            if (view instanceof EditText) {
                ((EditText) view).setText("");
            }

            if (view instanceof ViewGroup && (((ViewGroup) view).getChildCount() > 0))
                clearFields((ViewGroup) view);
        }
    }

    public static boolean isValidEmailAddress(CharSequence emailAdress) {
        if (TextUtils.isEmpty(emailAdress)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(emailAdress).matches();
        }
    }

    public static boolean isValidUsername(CharSequence username) {
        if (TextUtils.isEmpty(username)) {
            return false;
        } else {
            return USERNAME_PATTERN.matcher(username).matches();
        }
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    // Used for comments
    public static SpannableString createIndentedText(String text, int marginFirstLine, int marginNextLines) {
        SpannableString result = new SpannableString(text);
        result.setSpan(new LeadingMarginSpan.Standard(marginFirstLine, marginNextLines), 0, text.length(), 0);
        return result;
    }

    // Used for comments
    public static SpannableString createIndentedText(SpannableString text, int marginFirstLine, int marginNextLines) {
        text.setSpan(new LeadingMarginSpan.Standard(marginFirstLine, marginNextLines), 0, text.length(), 0);
        return text;
    }

    // TODO: You may want to replace it with Glide,
    // TODO: otherwise remove Glide from dependencies
    public static void loadWithPicasso(final Context context, final String urlToLoad, final ImageView target, @DrawableRes final int placeHolderResId) {
//        Glide.with(context)
//                .load(urlToLoad)
//                .placeholder(placeHolderResId)
//                .error(placeHolderResId)
//                .into(target);
        if (urlToLoad.isEmpty()) {
            target.setImageResource(placeHolderResId);
            return;
        }


        Picasso.with(context)
                .load(urlToLoad)
                .placeholder(placeHolderResId)
                .error(placeHolderResId)
                //.fit()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(target, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(context)
                                .load(urlToLoad)
                                //.fit()
                                .placeholder(placeHolderResId)
                                .error(placeHolderResId)
                                .into(target);
                    }
                });
    }

    public static void loadWithPicasso(final Context context, final String urlToLoad, final ImageView target) {
//        Glide.with(context)
//                .load(urlToLoad)
//                .into(target);
        if (urlToLoad.isEmpty())
            return;

        Picasso.with(context)
                .load(urlToLoad)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(target, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(context)
                                .load(urlToLoad)
                                .into(target);
                    }
                });
    }

    public static Object getDeepCopy(Object source, Type typeOfSource) {
        Gson gson = new Gson();

        return gson.fromJson(gson.toJson(source), typeOfSource);
    }
}