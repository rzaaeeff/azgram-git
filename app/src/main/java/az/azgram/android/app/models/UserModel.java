package az.azgram.android.app.models;

import java.io.Serializable;

import az.azgram.android.app.BuildConfig;

/**
 * Created by Rzaaeeff on 11/30/2016.
 */

public class UserModel extends CoreModel implements Serializable {

    // Constants
    public static final String UID = BuildConfig.APPLICATION_ID +
            ".UserModel";

    // Follow Status
    public static class FollowStatus {
        public static final byte NOT_FOLLOWING = 0;
        public static final byte FOLLOWING = 1;
        public static final byte PENDING = 2;
    }

    // Gender
    public static class Gender {
        public static final byte OTHER = 0;
        public static final byte MALE = 1;
        public static final byte FEMALE = 2;
    }

    // Profile Design
    public class ProfileDesign {
        public static final byte OPTION1 = 0;
        public static final byte OPTION2 = 1;
        public static final byte OPTION3 = 2;
//    public static final byte OPTION4 = 3;
//    public static final byte OPTION5 = 4;
//    public static final byte OPTION6 = 5;
//    public static final byte OPTION7 = 6;
//    public static final byte OPTION8 = 7;
    }

    // Fields
    private String fullName;
    private String email;
    private String username;
    private String login;
    private String phoneNumber;
    private String passwordHash;
    private String info;
    private String birthDate;
    private String lastSeenDate;
    private PhotoModel profilePhoto;
    private Byte gender;
    private Byte profileDesign;
    private Byte followStatus;
    private Integer country;
    private Integer postCount;
    private Integer followerCount;
    private Integer followingCount;
    private Integer questionCount;
    private Long balance;
    private Boolean isOnline;
    private Boolean isHidden;
    private Boolean isSubscribed;
    private Boolean isBlocked;
    private Boolean wantsToFollowYou;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getLastSeenDate() {
        return lastSeenDate;
    }

    public void setLastSeenDate(String lastSeenDate) {
        this.lastSeenDate = lastSeenDate;
    }

    public PhotoModel getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(PhotoModel profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public Byte getProfileDesign() {
        return profileDesign;
    }

    public void setProfileDesign(Byte profileDesign) {
        this.profileDesign = profileDesign;
    }

    public Byte getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(Byte followStatus) {
        this.followStatus = followStatus;
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public Integer getPostCount() {
        return postCount;
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }

    public Integer getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(Integer followerCount) {
        this.followerCount = followerCount;
    }

    public Integer getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(Integer followingCount) {
        this.followingCount = followingCount;
    }

    public Integer getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(Integer questionCount) {
        this.questionCount = questionCount;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Boolean isOnline() {
        return isOnline;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public Boolean isHidden() {
        return isHidden;
    }

    public void setHidden(Boolean hidden) {
        isHidden = hidden;
    }

    public Boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(Boolean subscribed) {
        isSubscribed = subscribed;
    }

    public Boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(Boolean blocked) {
        isBlocked = blocked;
    }

    public Boolean wantsToFollowYou() {
        return wantsToFollowYou;
    }

    public void setWantsToFollowYou(Boolean wantsToFollowYou) {
        this.wantsToFollowYou = wantsToFollowYou;
    }
}
