package az.azgram.android.app.models;

/**
 * Created by Rzaaeeff on 2/5/2017.
 */

public class BlockModel extends CoreModel {
    private UserModel target;
    private String text;

    public UserModel getTarget() {
        return target;
    }

    public void setTarget(UserModel target) {
        this.target = target;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
