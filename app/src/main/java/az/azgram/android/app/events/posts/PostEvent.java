package az.azgram.android.app.events.posts;

import az.azgram.android.app.models.PostModel;

/**
 * Created by Rzaaeeff on 1/26/2017.
 */

public class PostEvent {
    private PostModel model;

    public PostEvent(PostModel model) {
        this.model = model;
    }

    public PostModel getModel() {
        return model;
    }

    public void setModel(PostModel model) {
        this.model = model;
    }
}
