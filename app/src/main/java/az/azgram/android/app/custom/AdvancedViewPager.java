package az.azgram.android.app.custom;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Rzaaeeff on 12/12/2016.
 */

/**
 * This ViewPager can wrap its content {normal one can't do sometimes}
 * And also have the ability of disabling paging (swiping)
 */

public class AdvancedViewPager extends ViewPager {

//    private int currentPagePosition = 0;
    private boolean enabled;

    public AdvancedViewPager(Context context) {
        super(context);
        enabled = true;
    }

    public AdvancedViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        enabled = true;
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        try {
//            View child = getChildAt(currentPagePosition);
//            if (child != null) {
//                child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
//                int h = child.getMeasuredHeight();
//                heightMeasureSpec = MeasureSpec.makeMeasureSpec(h, MeasureSpec.EXACTLY);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//    }

//    public void reMeasureCurrentPage(int position) {
//        currentPagePosition = position;
//        requestLayout();
//    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (enabled)
            return super.onTouchEvent(ev);

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (enabled)
            return super.onInterceptTouchEvent(ev);

        return false;
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
