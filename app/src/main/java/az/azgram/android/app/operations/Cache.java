package az.azgram.android.app.operations;

import android.content.Context;
import android.content.SharedPreferences;

import az.azgram.android.app.BuildConfig;
import az.azgram.android.app.models.AppSettingsModel;
import az.azgram.android.app.models.PhotoModel;
import az.azgram.android.app.models.UserModel;

/**
 * Created by Rzaaeeff on 12/22/2016.
 */

public class Cache {
    private static final String SP_ID = BuildConfig.APPLICATION_ID;
    private static final String SP_USER_ID = SP_ID + ".User";
    private static final String SP_DATA_ID = SP_ID + ".Data";
    private static final String SP_SETTINGS_ID = SP_USER_ID;
    private static final String USER_CACHED = "user_cached";

    private class Key {
        public class User {
            public static final String ID = "user_id";
            public static final String TOKEN = "user_token";
            public static final String FULL_NAME = "user_fullname";
            public static final String EMAIL = "user_email";
            public static final String USERNAME = "user_username";
            public static final String PASSWORD_HASH = "user_psshsh";
            public static final String PHONE_NUMBER = "user_phonenumber";
            public static final String INFO = "user_info";
            public static final String BIRTH_DATE = "user_birthdate";
            public static final String GENDER = "user_gender";
            public static final String PROFILE_DESIGN = "user_profiledesign";
            public static final String COUNTRY = "user_country";
            public static final String POST_COUNT = "user_postcount";
            public static final String FOLLOWER_COUNT = "user_followercount";
            public static final String FOLLOWING_COUNT = "user_followingcount";
            public static final String QUESTION_COUNT = "user_questioncount";
        }

        public class Photo {
            public static final String SMALL = "photo_small";
            public static final String MEDIUM = "photo_small";
            public static final String LARGE = "photo_small";
        }

        public class Data {
            public static final String FIREBASE_TOKEN_SENT = "firebase_token_sent";
            public static final String UNSEEN_NOTIFICATION_COUNT = "unseen_notification_count";
            public static final String UNSEEN_MESSAGE_COUNT = "unseen_message_count";
            public static final String IS_UPDATE_NEEDED = "is_update_needed";
        }

        public class Settings {
            public static final String PRIVATE_PROFILE = "settings_private_profile";
            public static final String NOTIFY_ABOUT_SUBSCRIPTION = "settings_notify_about_subscription";
            public static final String ACCEPT_QUESTION = "settings_accept_question";
            public static final String ACCEPT_ANONYMOUS_QUESTION = "settings_accept_anonymous_question";
            public static final String NOTIFY_ABOUT_OTHER_DEVICES = "settings_notify_about_other_devices";
            public static final String PUSH_NOTIFICAION = "settings_push_notification";
            public static final String SHOW_IN_GUEST_LIST = "settings_show_in_guest_list";
            public static final String LANGUAGE = "settings_language";
            public static final String STATUS = "settings_status";
        }
    }

    private Context context;

    public Cache(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        SharedPreferences sp = context.getSharedPreferences(SP_USER_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        if (!sp.contains(USER_CACHED)) {
            editor.putBoolean(USER_CACHED, false);
        }

        editor.commit();
    }

    public void saveUser(UserModel user) {
        SharedPreferences sp = context.getSharedPreferences(SP_USER_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();

        try {
            // Key
            if (user.getId() != null) {
                editor.putLong(Key.User.ID, user.getId());
            }

            // TOKEN
            if (user.getToken() != null) {
                editor.putString(Key.User.TOKEN, user.getToken());
            }

            // FULL_NAME
            if (user.getFullName() != null) {
                editor.putString(Key.User.FULL_NAME, user.getFullName());
            }

            // EMAIL
            if (user.getEmail() != null) {
                editor.putString(Key.User.EMAIL, user.getEmail());
            }

            // USERNAME
            if (user.getUsername() != null) {
                editor.putString(Key.User.USERNAME, user.getUsername());
            }

            // Password
            if (user.getPasswordHash() != null) {
                editor.putString(Key.User.PASSWORD_HASH, user.getPasswordHash());
            }

            // PHONE_NUMBER
            if (user.getPhoneNumber() != null) {
                editor.putString(Key.User.PHONE_NUMBER, user.getPhoneNumber());
            }

            // INFO
            if (user.getInfo() != null) {
                editor.putString(Key.User.INFO, user.getInfo());
            }

            // BIRTH_DATE
            if (user.getBirthDate() != null) {
                editor.putString(Key.User.BIRTH_DATE, user.getBirthDate());
            }

            // GENDER
            if (user.getGender() != null) {
                editor.putInt(Key.User.GENDER, user.getGender());
            }

            // PROFILE_DESIGN
            if (user.getProfileDesign() != null) {
                editor.putInt(Key.User.PROFILE_DESIGN, user.getProfileDesign());
            }

            // COUNTRY
            if (user.getCountry() != null) {
                editor.putInt(Key.User.COUNTRY, user.getCountry());
            }

            // POST_COUNT
            if (user.getPostCount() != null) {
                editor.putInt(Key.User.POST_COUNT, user.getPostCount());
            }

            // FOLLOWER_COUNT
            if (user.getFollowerCount() != null) {
                editor.putInt(Key.User.FOLLOWER_COUNT, user.getFollowerCount());
            }

            // FOLLOWING_COUNT
            if (user.getFollowingCount() != null) {
                editor.putInt(Key.User.FOLLOWING_COUNT, user.getFollowingCount());
            }

            // QUESTION_COUNT
            if (user.getQuestionCount() != null) {
                editor.putInt(Key.User.QUESTION_COUNT, user.getQuestionCount());
            }

            // PROFILE_PHOTO
            if (user.getProfilePhoto() != null) {

                // SMALL
                if (user.getProfilePhoto().getSmall() != null) {
                    editor.putString(Key.Photo.SMALL, user.getProfilePhoto().getSmall());
                }

                // MEDIUM
                if (user.getProfilePhoto().getMedium() != null) {
                    editor.putString(Key.Photo.MEDIUM, user.getProfilePhoto().getMedium());
                }

                // LARGE
                if (user.getProfilePhoto().getLarge() != null) {
                    editor.putString(Key.Photo.LARGE, user.getProfilePhoto().getLarge());
                }
            }

            // We cached everything
            editor.putBoolean(USER_CACHED, true);
        } catch (Exception exc) {
            // If error occurs, we don't need to save
            // instead we sound_delete information
            editor.clear();

            // We say that we have not cached anything.
            editor.putBoolean(USER_CACHED, false);
        }

        // Closing Transaction
        editor.apply();
    }

    public UserModel loadUser() {
        UserModel user = new UserModel();

        SharedPreferences sp = context.getSharedPreferences(SP_USER_ID, Context.MODE_PRIVATE);

        try {
            // ID
            if (sp.contains(Key.User.ID)) {
                user.setId(sp.getLong(Key.User.ID, -1));
            }

            // TOKEN
            if (sp.contains(Key.User.TOKEN)) {
                user.setToken(sp.getString(Key.User.TOKEN, null));
            }

            // FULL_NAME
            if (sp.contains(Key.User.FULL_NAME)) {
                user.setFullName(sp.getString(Key.User.FULL_NAME, null));
            }

            // EMAIL
            if (sp.contains(Key.User.EMAIL)) {
                user.setEmail(sp.getString(Key.User.EMAIL, null));
            }

            // USERNAME
            if (sp.contains(Key.User.USERNAME)) {
                user.setUsername(sp.getString(Key.User.USERNAME, null));
            }

            if (sp.contains(Key.User.PASSWORD_HASH)) {
                user.setPasswordHash(sp.getString(Key.User.PASSWORD_HASH, null));
            }

            // PHONE_NUMBER
            if (sp.contains(Key.User.PHONE_NUMBER)) {
                user.setPhoneNumber(sp.getString(Key.User.PHONE_NUMBER, null));
            }

            // INFO
            if (sp.contains(Key.User.INFO)) {
                user.setInfo(sp.getString(Key.User.INFO, null));
            }

            // BIRTH_DATE
            if (sp.contains(Key.User.BIRTH_DATE)) {
                user.setBirthDate(sp.getString(Key.User.BIRTH_DATE, null));
            }

            // GENDER
            if (sp.contains(Key.User.GENDER)) {
                user.setGender((byte) sp.getInt(Key.User.GENDER, -1));
            }

            // PROFILE_DESIGN
            if (sp.contains(Key.User.PROFILE_DESIGN)) {
                user.setProfileDesign((byte) sp.getInt(Key.User.PROFILE_DESIGN, -1));
            }

            // COUNTRY
            if (sp.contains(Key.User.COUNTRY)) {
                user.setCountry(sp.getInt(Key.User.COUNTRY, -1));
            }

            // POST_COUNT
            if (sp.contains(Key.User.POST_COUNT)) {
                user.setPostCount(sp.getInt(Key.User.POST_COUNT, -1));
            }

            // FOLLOWER_COUNT
            if (sp.contains(Key.User.FOLLOWER_COUNT)) {
                user.setFollowerCount(sp.getInt(Key.User.FOLLOWER_COUNT, -1));
            }

            // FOLLOWING_COUNT
            if (sp.contains(Key.User.FOLLOWING_COUNT)) {
                user.setFollowingCount(sp.getInt(Key.User.FOLLOWING_COUNT, -1));
            }

            // QUESTION_COUNT
            if (sp.contains(Key.User.QUESTION_COUNT)) {
                user.setQuestionCount(sp.getInt(Key.User.QUESTION_COUNT, -1));
            }

            // PROFILE_PHOTO
            PhotoModel photo = new PhotoModel();

            // Small
            if (sp.contains(Key.Photo.SMALL)) {
                photo.setSmall(sp.getString(Key.Photo.SMALL, null));
            }

            // Medium
            if (sp.contains(Key.Photo.MEDIUM)) {
                photo.setMedium(sp.getString(Key.Photo.MEDIUM, null));
            }

            // Large
            if (sp.contains(Key.Photo.LARGE)) {
                photo.setLarge(sp.getString(Key.Photo.LARGE, null));
            }

            user.setProfilePhoto(photo);

        } catch (Exception exc) {
            // If error occurs we cancel loading.
            user = null;
        }

        return user;
    }

    public boolean hasUser() {
        SharedPreferences sp = context.getSharedPreferences(SP_USER_ID, Context.MODE_PRIVATE);

        if (sp.contains(USER_CACHED)) {
            return sp.getBoolean(USER_CACHED, false);
        } else {
            return false;
        }
    }

    public void clearUser() {
        SharedPreferences sp = context.getSharedPreferences(SP_USER_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
    }

    public void clearData() {
        SharedPreferences sp = context.getSharedPreferences(SP_DATA_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
    }

    public void setFirebaseTokenSent(boolean firebaseTokenSent) {
        SharedPreferences sp = context.getSharedPreferences(SP_DATA_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        try {
            editor.putBoolean(Key.Data.FIREBASE_TOKEN_SENT, firebaseTokenSent);
        } finally {
            editor.apply();
        }
    }

    public boolean isFirebaseTokenSent() {
        SharedPreferences sp = context.getSharedPreferences(SP_DATA_ID, Context.MODE_PRIVATE);

        try {
            return sp.getBoolean(Key.Data.FIREBASE_TOKEN_SENT, false);
        } catch (Exception exc) {
        }

        return true;
    }

    public void setUnseenNotificationCount(int count) {
        SharedPreferences sp = context.getSharedPreferences(SP_DATA_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        try {
            editor.putInt(Key.Data.UNSEEN_NOTIFICATION_COUNT, count);
        } catch (Exception exc) {}

        editor.apply();
    }

    public int getUnseenNotificationCount() {
        SharedPreferences sp = context.getSharedPreferences(SP_DATA_ID, Context.MODE_PRIVATE);

        try {
            return sp.getInt(Key.Data.UNSEEN_NOTIFICATION_COUNT, 0);
        } catch (Exception exc) {}

        return 0;
    }

    public void increaseUnseenNotificationCountBy(int count) {
        int oldCount = getUnseenNotificationCount();
        setUnseenNotificationCount(oldCount + count);
    }

    public void setUnseenMessageCount(int count) {
        SharedPreferences sp = context.getSharedPreferences(SP_DATA_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        try {
            editor.putInt(Key.Data.UNSEEN_MESSAGE_COUNT, count);
        } catch (Exception exc) {}

        editor.apply();
    }

    public int getUnseenMessageCount() {
        SharedPreferences sp = context.getSharedPreferences(SP_DATA_ID, Context.MODE_PRIVATE);

        try {
            return sp.getInt(Key.Data.UNSEEN_MESSAGE_COUNT, 0);
        } catch (Exception exc) {}

        return 0;
    }

    public void increaseUnseenMessageCountBy(int count) {
        int oldCount = getUnseenMessageCount();
        setUnseenMessageCount(oldCount + count);
    }

    public AppSettingsModel loadSettings() {
        AppSettingsModel settings = new AppSettingsModel();

        SharedPreferences sp = context.getSharedPreferences(SP_USER_ID, Context.MODE_PRIVATE);

        try {
            if (sp.contains(Key.Settings.PRIVATE_PROFILE)) {
                settings.setPrivateProfile(sp.getBoolean(Key.Settings.PRIVATE_PROFILE, false));
            }

            if (sp.contains(Key.Settings.NOTIFY_ABOUT_SUBSCRIPTION)) {
                settings.setNotifyAboutSubscription(sp.getBoolean(Key.Settings.NOTIFY_ABOUT_SUBSCRIPTION, false));
            }

            if (sp.contains(Key.Settings.ACCEPT_QUESTION)) {
                settings.setAcceptQuestion(sp.getBoolean(Key.Settings.ACCEPT_QUESTION, false));
            }

            if (sp.contains(Key.Settings.ACCEPT_ANONYMOUS_QUESTION)) {
                settings.setAcceptAnonymousQuestion(sp.getBoolean(Key.Settings.ACCEPT_ANONYMOUS_QUESTION, false));
            }

            if (sp.contains(Key.Settings.NOTIFY_ABOUT_OTHER_DEVICES)) {
                settings.setNotifyAboutOtherDevices(sp.getBoolean(Key.Settings.NOTIFY_ABOUT_OTHER_DEVICES, false));
            }

            if (sp.contains(Key.Settings.PUSH_NOTIFICAION)) {
                settings.setPushNotification(sp.getBoolean(Key.Settings.PUSH_NOTIFICAION, false));
            }

            if (sp.contains(Key.Settings.SHOW_IN_GUEST_LIST)) {
                settings.setShowInGuestList(sp.getBoolean(Key.Settings.SHOW_IN_GUEST_LIST, false));
            }

            if (sp.contains(Key.Settings.LANGUAGE)) {
                settings.setLanguage((byte) sp.getInt(Key.Settings.LANGUAGE, AppSettingsModel.LANGUAGE.AZ));
            }

            if (sp.contains(Key.Settings.STATUS)) {
                settings.setStatus((byte) sp.getInt(Key.Settings.STATUS, AppSettingsModel.STATUS.STANDARD));
            }
        } catch (Exception exc) {
            // If error occurs we cancel loading.
            settings = null;
        }

        return settings;
    }

    public void saveSettings(AppSettingsModel settings) {
        SharedPreferences sp = context.getSharedPreferences(SP_USER_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        try {
            if (settings.getPrivateProfile() != null) {
                editor.putBoolean(Key.Settings.PRIVATE_PROFILE, settings.getPrivateProfile());
            }

            if (settings.getNotifyAboutSubscription() != null) {
                editor.putBoolean(Key.Settings.NOTIFY_ABOUT_SUBSCRIPTION, settings.getNotifyAboutSubscription());
            }

            if (settings.getAcceptQuestion() != null) {
                editor.putBoolean(Key.Settings.ACCEPT_QUESTION, settings.getAcceptQuestion());
            }

            if (settings.getAcceptAnonymousQuestion() != null) {
                editor.putBoolean(Key.Settings.ACCEPT_ANONYMOUS_QUESTION, settings.getAcceptAnonymousQuestion());
            }

            if (settings.getNotifyAboutOtherDevices() != null) {
                editor.putBoolean(Key.Settings.NOTIFY_ABOUT_OTHER_DEVICES, settings.getNotifyAboutOtherDevices());
            }

            if (settings.getPushNotification() != null) {
                editor.putBoolean(Key.Settings.PUSH_NOTIFICAION, settings.getPushNotification());
            }

            if (settings.getShowInGuestList() != null) {
                editor.putBoolean(Key.Settings.SHOW_IN_GUEST_LIST, settings.getShowInGuestList());
            }

            if (settings.getLanguage() != null) {
                editor.putInt(Key.Settings.LANGUAGE, settings.getLanguage());
            }

            if (settings.getStatus() != null) {
                editor.putInt(Key.Settings.STATUS, settings.getStatus());
            }
        } catch (Exception exc) {
            editor.clear();
        }

        // Closing Transaction
        editor.apply();
    }

    public void setUpdateNeeded(boolean isUpdateNeeded) {
        SharedPreferences sp = context.getSharedPreferences(SP_DATA_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        try {
            editor.putBoolean(Key.Data.IS_UPDATE_NEEDED, isUpdateNeeded);
        } catch (Exception exc) {}

        editor.apply();
    }

    public boolean isUpdateNeeded() {
        SharedPreferences sp = context.getSharedPreferences(SP_DATA_ID, Context.MODE_PRIVATE);

        try {
            return sp.getBoolean(Key.Data.IS_UPDATE_NEEDED, false);
        } catch (Exception exc) {}

        return false;
    }
}