package az.azgram.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import az.azgram.android.app.R;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.users.FollowEvent;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 2/11/2017.
 */

public class RecyclerAdapterUserMini extends RecyclerView.Adapter<RecyclerAdapterUserMini.UserHolder> {
    private Context context;
    private UserListModel data;
    private Controller controller;

    public RecyclerAdapterUserMini(Context context, UserListModel data, Controller controller) {
        this.context = context;
        this.data = data;
        this.controller = controller;

        BusProvider.getInstance().register(this);
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.recycler_item_user,
                parent,
                false
        );

        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        holder.bind(data.getUsers().get(position));
    }

    @Override
    public int getItemCount() {
        return data.getUsers().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageViewProfilePhoto, imageViewUserBadge;
        private TextView textView;
        private LinearLayout rootLayout;

        public UserHolder(View itemView) {
            super(itemView);

            imageViewProfilePhoto = (ImageView) itemView.findViewById(R.id.imageViewProfilePhotoUserItem);
            imageViewUserBadge = (ImageView) itemView.findViewById(R.id.imageViewUserBadgeUserItem);
            textView = (TextView) itemView.findViewById(R.id.textViewUserItem);
            rootLayout = (LinearLayout) itemView.findViewById(R.id.rootLayoutUserItem);

            rootLayout.setOnClickListener(this);
        }

        public void bind(UserModel user) {
            Utility.loadWithPicasso(
                    context,
                    user.getProfilePhoto().getSmallestPossible(),
                    imageViewProfilePhoto,
                    R.drawable.profile_photo_placeholder
            );

            imageViewUserBadge.setImageResource(
                    user.isOnline() ? R.drawable.user_online_badge :
                            R.drawable.user_offline_badge
            );

            if (user.getFullName() != null) {
                if (user.getFullName().length() > 20)
                    textView.setText(user.getUsername());
                else
                    textView.setText(user.getFullName());
            } else {
                textView.setText(user.getUsername());
            }
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                controller.onUserClicked(data.getUsers().get(position));
            }
        }
    }

    public interface Controller {
        void onUserClicked(UserModel user);
    }
}