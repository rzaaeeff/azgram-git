package az.azgram.android.app.services;

import android.app.IntentService;
import android.content.Intent;

import com.google.gson.Gson;

import java.io.IOException;

import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AppUpdateChecker extends IntentService {

    private static final String CHECK_URL = "http://api.azgram.az/app/check";

    public AppUpdateChecker() {
        super("AppUpdateChecker");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        boolean isUpdateNeeded = false;

        try {
            isUpdateNeeded = checkForUpdate();
        } catch (Exception exc) {}

        new Cache(this).setUpdateNeeded(isUpdateNeeded);
    }

    private boolean checkForUpdate() throws IOException {
        boolean isUpdateNeeded = false;

        CoreModel requestModel = new CoreModel();
        requestModel.setApiKey(Keys.API_KEY);

        String requestJson = new Gson().toJson(requestModel);

        RequestBody requestBody = RequestBody.create(
                MediaType.parse("application/json"),
                requestJson
        );

        Request request = new Request.Builder()
                .post(requestBody)
                .url(CHECK_URL)
                .build();

        Response response = new OkHttpClient().newCall(request).execute();

        String responseJson = response.body().string();

        CoreModel responseModel = new Gson().fromJson(responseJson, CoreModel.class);

        return responseModel.getMessageId() == NetworkOperations.App.APP_NEEDS_UPDATE;
    }
}
