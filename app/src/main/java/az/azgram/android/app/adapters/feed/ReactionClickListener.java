package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.content.Intent;

import az.azgram.android.app.activities.UserListActivity;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.response.UserListModel;

/**
 * Created by Rzaaeeff on 7/10/2017.
 */

public class ReactionClickListener {
    private Context context;
    private Intent intent;

    public ReactionClickListener(Context context) {
        this.context = context;

        intent = new Intent(context, UserListActivity.class);
    }

    public void onLikeCountClicked(PostModel post) {
        onReactionCountClicked(UserListActivity.Type.Like, post);
    }

    public void onDislikeCountClicked(PostModel post) {
        onReactionCountClicked(UserListActivity.Type.Dislike, post);
    }

    private void onReactionCountClicked(byte type, PostModel post) {
        PostModel minPost = new PostModel();
        minPost.setId(post.getId());

        intent.putExtra(UserListActivity.EXTRA_TYPE, type);
        intent.putExtra(PostModel.UID, minPost);
        context.startActivity(intent);
    }
}