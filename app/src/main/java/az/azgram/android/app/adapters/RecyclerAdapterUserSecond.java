package az.azgram.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import az.azgram.android.app.BuildConfig;
import az.azgram.android.app.R;
import az.azgram.android.app.activities.UserListActivity;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.users.FollowerHiddenEvent;
import az.azgram.android.app.events.users.FollowingHiddenEvent;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.UserListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.NetworkOperations;
import az.azgram.android.app.operations.UniversalListenerMethods;
import az.azgram.android.app.operations.Utility;
import com.squareup.otto.Subscribe;

/**
 * Created by Rzaaeeff on 2/11/2017.
 */

public class RecyclerAdapterUserSecond extends RecyclerView.Adapter<RecyclerAdapterUserSecond.UserHolder> {
    private Context context;
    private byte type;
    private UserListModel data;
    private static NetworkOperations service;
    private static UserModel request;

    public RecyclerAdapterUserSecond(Context context, UserListModel data, byte type) {
        this.context = context;
        this.data = data;
        this.type = type;

        BusProvider.getInstance().register(this);

        if (service == null) {
            service = new NetworkOperations(context, BusProvider.getInstance());
            service.getUsersEndpoint();
        }

        if (request == null) {
            request = new UserModel();
            request.setApiKey(Keys.API_KEY);
            request.setToken(new Cache(context).loadUser().getToken());
        }
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.recycler_item_user_second,
                parent,
                false
        );

        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        holder.bind(data.getUsers().get(position));
    }

    @Override
    public int getItemCount() {
        return data.getUsers().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Subscribe
    public void onAzGramServiceResponse(FollowerHiddenEvent event) {
        if (type == UserListActivity.Type.Follower)
            onEvent(event.getModel());
    }

    @Subscribe
    public void onAzGramServiceResponse(FollowingHiddenEvent event) {
        if (type == UserListActivity.Type.Following)
            onEvent(event.getModel());
    }

    private void onEvent(UserModel user) {
        if (user.getMessageId() == NetworkOperations.API_SUCCESS) {
            UserModel currentUser;
            for (int position = 0; position < data.getUsers().size(); position++) {
                if (data.getUsers().get(position).getUsername().equals(user.getUsername())) {
                    currentUser = data.getUsers().get(position);

                    currentUser.setHidden(!currentUser.isHidden());
                    notifyItemChanged(position);
                }
            }
        } else {
            Toast.makeText(context, context.getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }

    public class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageViewProfilePhoto, imageViewUserBadge;
        private TextView textView;
        private Button buttonShow, buttonHide;
        private LinearLayout rootLayout;

        public UserHolder(View itemView) {
            super(itemView);

            imageViewProfilePhoto = (ImageView) itemView.findViewById(R.id.imageViewProfilePhotoUserItem);
            imageViewUserBadge = (ImageView) itemView.findViewById(R.id.imageViewUserBadgeUserItem);
            textView = (TextView) itemView.findViewById(R.id.textViewUserItem);
            buttonShow = (Button) itemView.findViewById(R.id.buttonShowUserItem);
            buttonHide = (Button) itemView.findViewById(R.id.buttonHideUserItem);
            rootLayout = (LinearLayout) itemView.findViewById(R.id.rootLayoutUserItem);

            rootLayout.setOnClickListener(this);
            buttonHide.setOnClickListener(this);
            buttonShow.setOnClickListener(this);
        }

        public void bind(UserModel user) {
            Utility.loadWithPicasso(
                    context,
                    user.getProfilePhoto().getSmallestPossible(),
                    imageViewProfilePhoto,
                    R.drawable.profile_photo_placeholder
            );

            imageViewUserBadge.setImageResource(
                    user.isOnline() ? R.drawable.user_online_badge :
                            R.drawable.user_offline_badge
            );

            if (user.getFullName() != null) {
                if (user.getFullName().length() > 20)
                    textView.setText(user.getUsername());
                else
                    textView.setText(user.getFullName());
            } else {
                textView.setText(user.getUsername());
            }

            updateHidden(user);
        }

        private void updateHidden(UserModel user) {
            buttonShow.setVisibility(View.GONE);
            buttonHide.setVisibility(View.GONE);

            if (user.isHidden() != null) {
                if (user.isHidden())
                    buttonShow.setVisibility(View.VISIBLE);
                else
                    buttonHide.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                switch (view.getId()) {
                    case R.id.rootLayoutUserItem:
                        UniversalListenerMethods.onClickUser(
                                "@" + data.getUsers().get(position).getUsername(), context);
                        break;
                    case R.id.buttonShowUserItem:
                    case R.id.buttonHideUserItem:
                        request.setUsername(
                                data.getUsers().get(position).getUsername()
                        );

                        if (type == UserListActivity.Type.Follower)
                            service.getUsersEndpoint().hideFollower(request);
                        else if (type == UserListActivity.Type.Following)
                            service.getUsersEndpoint().hideFollowing(request);

                        break;
                }
            }
        }
    }
}