package az.azgram.android.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import az.azgram.android.app.R;
import az.azgram.android.app.activities.SinglePostActivity;
import az.azgram.android.app.custom.SquareImageView;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.response.FeedModel;
import az.azgram.android.app.operations.Utility;

/**
 * Created by Rzaaeeff on 12/12/2016.
 */

public class RecyclerAdapterPostsCollection extends RecyclerView.Adapter<RecyclerAdapterPostsCollection.PhotoViewHolder> {
    // Constants
    public static final int COLUMN_COUNT = 3;
    public static final int SPACING = 1;

    // Other elements
    private Context context;
    private FeedModel feed;

    public RecyclerAdapterPostsCollection(Context context, FeedModel feed) {
        this.context = context;
        this.feed = feed;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_post_collection, parent, false);

        return new PhotoViewHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, final int position) {
        // TODO Add code here :)
        Utility.loadWithPicasso(
                context,
                feed.getPosts().get(position).getPhoto().getSmallestPossible(),
                holder.imageView,
                R.drawable.post_photo_placeholder);

        holder.imageView.setClickable(true);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SinglePostActivity.class);
                intent.putExtra(PostModel.UID, feed.getPosts().get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return feed.getPosts().size();
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {
        SquareImageView imageView;

        public PhotoViewHolder(View itemView) {
            super(itemView);

            imageView = (SquareImageView) itemView.findViewById(R.id.draweeViewRecyclerItemGIFPostsCollection);
        }
    }
}
