package az.azgram.android.app.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import az.azgram.android.app.AzGram;
import az.azgram.android.app.R;
import az.azgram.android.app.adapters.ArrayAdapterWithIcon;
import az.azgram.android.app.adapters.PagerAdapterCoreActivity;
import az.azgram.android.app.constants.Constants;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.messages.MessageSentEvent;
import az.azgram.android.app.events.posts.PostDeletedEvent;
import az.azgram.android.app.events.posts.PostSetAsPPEvent;
import az.azgram.android.app.events.posts.ReportEvent;
import az.azgram.android.app.events.posts.ShareEvent;
import az.azgram.android.app.events.ui.OptionsButtonClickedEvent;
import az.azgram.android.app.fragments.BottomMenuDialog;
import az.azgram.android.app.fragments.HomeFragment;
import az.azgram.android.app.fragments.StatusDialog;
import az.azgram.android.app.models.ComplaintModel;
import az.azgram.android.app.models.ConversationModel;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.operations.BottomMenuController;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.DownloadManagerProvider;
import az.azgram.android.app.operations.Interaction;
import az.azgram.android.app.operations.LinkMechanism;
import az.azgram.android.app.operations.NetworkOperations;
import berlin.volders.badger.BadgeShape;
import berlin.volders.badger.Badger;
import berlin.volders.badger.CountBadge;
import cn.refactor.lib.colordialog.ColorDialog;
import im.ene.toro.Toro;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CoreActivity extends AppCompatActivity implements BottomMenuController {
    private static final String TAG = AzGram.TAG;

    public final static String EXTRA_MODE = "extra_mode";
    public final static byte MODE_DEFAULT = 0;
    public final static byte MODE_OPEN_NOTIFICATIONS = 1;

    // Widgets
    private ViewPager viewPager;

    // Other elements
    private FragmentPagerAdapter adapter;
    private BottomMenuDialog dialogFragment;
    private PostModel currentPost;
    public static UserModel user = null;
    private ImageButton imageButtonNotification;
    private CountBadge.Factory badgeFactory;
    private CountBadge.Factory badgeFactoryRed;
    private Cache cache;

    private Handler badgeRefreshHandler = new Handler();
    private Runnable badgeRefreshRunnable = new Runnable() {
        @Override
        public void run() {
            int notificationCount = cache.getUnseenNotificationCount();
            int messageCount = cache.getUnseenMessageCount();

            if (notificationCount > 0) {
                Badger.sett(imageButtonNotification, badgeFactory).setCount(notificationCount);
            } else {
                imageButtonNotification.setImageResource(R.drawable.ic_bell);
            }

            View view = viewPager.getFocusedChild();

            if (view != null) {
                ImageButton imageButtonChat = (ImageButton) view.findViewById(R.id.imageButton_chat);

                if (imageButtonChat != null) {
                    if (messageCount > 0) {
                        Badger.sett(imageButtonChat, badgeFactoryRed).setCount(messageCount);
                    } else {
                        imageButtonChat.setImageResource(R.drawable.ic_speech_bubble);
                    }
                }
            }

            badgeRefreshHandler.postDelayed(this, Constants.BADGE_REFRESH_INTERVAL);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_core);

        // Getting user from cache
        cache = new Cache(this);
        if (cache.hasUser()) {
            user = cache.loadUser();

            if (user == null) {
                // Couldn't load
                // TODO: redo login
            } else {
                if (user.getToken() != null && !user.getToken().trim().isEmpty()) {
                    // Loaded from Cache
                    // TODO: do the rest
                    Toro.attach(this);
                    init();

                    new NetworkOperations(this, BusProvider.getInstance()).getAppEndpoint().notifyOnline();
                } else {
                    // Couldn't get token
                    // TODO: redo login
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);

        badgeFactory = new CountBadge.Factory(
                CoreActivity.this,
                BadgeShape.circle(
                        0.5f,
                        Gravity.TOP | Gravity.END
                )
        );

        badgeFactoryRed = new CountBadge.Factory(
                BadgeShape.circle(
                        0.5f,
                        Gravity.TOP | Gravity.END
                ),
                Color.RED,
                Color.WHITE
        );

        imageButtonNotification = (ImageButton) findViewById(R.id.buttonNotificationCoreActivity);
        badgeRefreshHandler.postDelayed(badgeRefreshRunnable, Constants.BADGE_REFRESH_INTERVAL);
    }

    @Override
    protected void onStop() {
        BusProvider.getInstance().unregister(this);

        badgeRefreshHandler.removeCallbacks(badgeRefreshRunnable);

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        new NetworkOperations(this, BusProvider.getInstance()).getAppEndpoint().notifyOffline();
    }

    /**
     * Assigns IDs and does initialization...
     */
    private void init() {
        viewPager = (ViewPager) findViewById(R.id.viewPagerCoreActivity);

        // Initializing toolbar
        // setSupportActionBar(toolbar);
        // toolbar.setTitle("");

        // Initializing pager adapter
        viewPager.setOffscreenPageLimit(PagerAdapterCoreActivity.PAGE_COUNT - 1);
        adapter = new PagerAdapterCoreActivity(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        int page = PagerAdapterCoreActivity.HOME_PAGE;

        if (getIntent().hasExtra(EXTRA_MODE)) {
            byte mode = getIntent().getByteExtra(EXTRA_MODE, MODE_DEFAULT);

            switch (mode) {
                case MODE_OPEN_NOTIFICATIONS:
                    page = PagerAdapterCoreActivity.NOTIFICATIONS_PAGE;
                    break;
            }
        }

        viewPager.setCurrentItem(page);

        new LinkMechanism(this);
    }

    /**
     * Button click event
     *
     * @param view
     */
    public void navigationButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonHomeCoreActivity:
                changePage(PagerAdapterCoreActivity.HOME_PAGE);
                break;
            case R.id.buttonSearchCoreActivity:
                changePage(PagerAdapterCoreActivity.SEARCH_PAGE);
                break;
            case R.id.buttonPostCoreActivity:
                /**
                 * code from StackOverFlow.com
                 * link: http://stackoverflow.com/a/15453996/3497814
                 */
                final String[] items = new String[]{
                        getString(R.string.CoreActivityUploadPromptAnswerPhotoText),
                        getString(R.string.CoreActivityUploadPromptAnswerVideoText)};
                final Integer[] icons = new Integer[]{R.drawable.ic_photo_camera, R.drawable.ic_video_camera};
                ListAdapter adapter = new ArrayAdapterWithIcon(this, items, icons);

                new AlertDialog.Builder(this).setTitle(R.string.CoreActivityUploadPromptText)
                        .setAdapter(adapter, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                Intent intent = new Intent(CoreActivity.this,
                                        UploadControllerActivity.class);
                                intent.putExtra(UploadControllerActivity.EXTRA_CHOICE, item);
                                startActivity(intent);
                                dialog.dismiss();
                            }
                        }).show();
                break;
            case R.id.buttonNotificationCoreActivity:
                changePage(PagerAdapterCoreActivity.NOTIFICATIONS_PAGE);
                break;
            case R.id.buttonGuestsCoreActivity:
                changePage(PagerAdapterCoreActivity.GUESTS_PAGE);
                break;
            case R.id.buttonProfileCoreActivity:
                changePage(PagerAdapterCoreActivity.PROFILE_PAGE);
                break;
            default:
        }
    }

    /**
     * If position is not different from currently display page's position,
     * then we don't need to change the page.
     *
     * @param position
     */
    private void changePage(int position) {
        try {
            if (viewPager.getCurrentItem() != position) {
                viewPager.setCurrentItem(position);
            }
        } catch (Exception exc) {
            Log.e(AzGram.TAG, "changePage: ", exc);
        }
    }

    // To support fonts
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Subscribe
    public void openBottomMenu(OptionsButtonClickedEvent event) {
        if (dialogFragment != null) {
            dialogFragment.dismiss();
            dialogFragment = null;
        }

        currentPost = event.getModel();

        Bundle bundle = new Bundle();
        bundle.putBoolean(BottomMenuDialog.EXTRA_SHOULD_SHOW_AS_PP,
                currentPost.getType() == PostModel.Type.PHOTO);
        bundle.putBoolean(BottomMenuDialog.EXTRA_SHOULD_SHOW_DELETE,
                user.getUsername().equals(currentPost.getAuthor().getUsername()));


        dialogFragment = new BottomMenuDialog();
        dialogFragment.setController(this);
        dialogFragment.setArguments(bundle);
        dialogFragment.show(getSupportFragmentManager(), Constants.BOTTOM_MENU_TAG);
    }

    public void openSuggestedUsersList(View view) {
        Intent intent = new Intent(CoreActivity.this, UserListActivity.class);
        intent.putExtra(UserListActivity.EXTRA_TYPE, UserListActivity.Type.Suggested);
        startActivity(intent);
    }

    /**
     * BEGIN_BOTTOM_MENU_METHODS
     */
    @Override
    public void shareButtonClicked() {
        PostModel post = new PostModel();
        post.setToken(user.getToken());
        post.setApiKey(Keys.API_KEY);
        post.setId(currentPost.getId());
        post.setAuthor(new UserModel());
        post.getAuthor().setUsername(currentPost.getAuthor().getUsername());

        NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
        service.getPostsEndpoint().share(post);
    }

    @Override
    public void downloadButtonClicked() {
        if (currentPost.getType() != PostModel.Type.STATUS) {
            DownloadManagerProvider.getInstance().downloadFromUrl(
                    currentPost.getPhoto().getLargestPossible()
            );
        }
    }

    @Override
    public void searchResultClicked(CoreModel clickedItem) {
        MessageModel message = new MessageModel();
        message.setRecipients(new ArrayList<CoreModel>());

        // recipient can be either UserModel
        // or ConversationModel
        if (clickedItem instanceof UserModel) {
            // it's user
            UserModel user = new UserModel();
            user.setUsername(((UserModel) clickedItem).getUsername());
            message.getRecipients().add(user);
        } else {
            // it's conversation
            ConversationModel conversation = new ConversationModel();
            conversation.setId(clickedItem.getId());
            message.getRecipients().add(conversation);
        }

        message.setAuthor(new UserModel());
        message.getAuthor().setToken(user.getToken());
        message.setPost(new PostModel());
        message.getPost().setId(currentPost.getId());
        message.setType(MessageModel.Type.POST);
        message.setApiKey(Keys.API_KEY);

        NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
        service.getMessagesEndpoint().send(message);
    }

    @Override
    public void asPPButtonClicked() {
        PostModel post = new PostModel();
        post.setId(currentPost.getId());
        post.setToken(user.getToken());
        post.setApiKey(Keys.API_KEY);

        NetworkOperations service = new NetworkOperations(this, BusProvider.getInstance());
        service.getPostsEndpoint().asPP(post);
    }

    @Override
    public void reportButtonClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.CoreActivityBottomReportButtonText));

        // Set up input
        final AppCompatEditText editText = new AppCompatEditText(this);
        editText.setHint(getString(R.string.CoreActivityReportDialogHintText));
        // Specify the type of input expected;
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(editText);

        // Set up buttons
        final Context context = this;
        builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (editText.getText().toString().trim().length() > 0) {
                    ComplaintModel complaint = new ComplaintModel();

                    complaint.setTarget(new PostModel());
                    complaint.getTarget().setId(currentPost.getId());
                    complaint.getTarget().setAuthor(new UserModel());
                    complaint.getTarget().getAuthor().setUsername(currentPost.getAuthor().getUsername());

                    complaint.setText(editText.getText().toString());
                    complaint.setToken(user.getToken());
                    complaint.setApiKey(Keys.API_KEY);

                    NetworkOperations service = new NetworkOperations(context, BusProvider.getInstance());
                    service.getPostsEndpoint().report(complaint);
                } else {
                    Toast.makeText(context, getString(R.string.CoreActivityMessageFillEmptyField), Toast.LENGTH_SHORT).show();
                }
            }
        });

        builder.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.show();
    }

    @Override
    public void deleteButtonClicked() {
        Interaction.getInstance(this)
                .showPromptDialog(getString(R.string.are_you_sure))
                .setPositiveListener(R.string.Yes, new ColorDialog.OnPositiveListener() {
                    @Override
                    public void onClick(ColorDialog colorDialog) {
                        PostModel post = new PostModel();
                        post.setId(currentPost.getId());
                        post.setToken(user.getToken());
                        post.setApiKey(Keys.API_KEY);

                        NetworkOperations service = new NetworkOperations(CoreActivity.this,
                                BusProvider.getInstance());
                        service.getPostsEndpoint().delete(post);
                        colorDialog.dismiss();
                    }
                })
                .setNegativeListener(R.string.No, new ColorDialog.OnNegativeListener() {
                    @Override
                    public void onClick(ColorDialog colorDialog) {
                        colorDialog.dismiss();
                    }
                })
                .show();
    }

    /**
     * END_BOTTOM_MENU_METHODS
     */

    public void openStatusDialog(View view) {
        DialogFragment statusDialog = new StatusDialog();
        statusDialog.show(getSupportFragmentManager(), Constants.STATUS_DIALOG_TAG);
    }

    public void openMessagingActivity(View view) {
        Intent intent = new Intent(CoreActivity.this, MessagingActivity.class);
        startActivity(intent);
    }

    @Subscribe
    public void onAzGramServiceResponse(ShareEvent event) {
        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, getString(R.string.PostShared), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(ReportEvent event) {
        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, getString(R.string.PostReported), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.ErrorOccurred), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(MessageSentEvent event) {
        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, R.string.CoreActivityPostSent, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(PostSetAsPPEvent event) {
        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, R.string.CoreActivityPostSetAsPP, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(PostDeletedEvent event) {
        if (event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
            Toast.makeText(this, R.string.CoreActivityPostSetDeleted, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.UnknownError, Toast.LENGTH_SHORT).show();
        }
    }
}