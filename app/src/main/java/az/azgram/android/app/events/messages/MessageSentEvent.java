package az.azgram.android.app.events.messages;

import az.azgram.android.app.models.MessageModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class MessageSentEvent {
    private MessageModel model;

    public MessageSentEvent(MessageModel model) {
        this.model = model;
    }

    public MessageModel getModel() {
        return model;
    }

    public void setModel(MessageModel model) {
        this.model = model;
    }
}
