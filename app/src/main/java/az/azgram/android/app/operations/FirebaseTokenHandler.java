package az.azgram.android.app.operations;

import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.otto.Subscribe;

import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.users.SettingChangedEvent;
import az.azgram.android.app.models.UserSettingsModel;

/**
 * Created by Rzaaeeff on 6/9/2017.
 */

public class FirebaseTokenHandler {
    private Context context;
    private Cache cache;

    public FirebaseTokenHandler(Context context) {
        this.context = context;

        BusProvider.getInstance().register(this);
        cache = new Cache(context);
    }

    public void setTokenSent(boolean isTokenSent) {
        cache.setFirebaseTokenSent(isTokenSent);
    }

    public void sendToServer() {
        if (!cache.isFirebaseTokenSent()) {
            String firebaseToken = FirebaseInstanceId.getInstance().getToken();

            if (firebaseToken != null) {
                UserSettingsModel settings = new UserSettingsModel();
                settings.setChange("firebaseToken");
                settings.setFirebaseToken(firebaseToken);
                settings.setApiKey(Keys.API_KEY);
                settings.setToken(cache.loadUser().getToken());

                NetworkOperations service = new NetworkOperations(context, BusProvider.getInstance());
                service.getUsersEndpoint().save(settings);
            }
        }
    }

    @Subscribe
    public void onAzGramServiceResponse(SettingChangedEvent event) {
        if (event.getModel().getChange().equals("firebaseToken")) {
            if (event.getModel().getMessageId() != null &&
                    event.getModel().getMessageId() == NetworkOperations.API_SUCCESS) {
                // Token saved
                setTokenSent(true);
            }
        }
    }
}
