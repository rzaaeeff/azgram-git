package az.azgram.android.app.services;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.ArrayList;

import az.azgram.android.app.AzGram;
import az.azgram.android.app.activities.CoreActivity;
import az.azgram.android.app.activities.MessagingActivity;
import az.azgram.android.app.events.messages.MessageReceivedEvent;
import az.azgram.android.app.events.notifications.NotificationListReceivedEvent;
import az.azgram.android.app.events.notifications.NotificationReceivedEvent;
import az.azgram.android.app.fragments.NotificationFragment;
import az.azgram.android.app.models.MessageModel;
import az.azgram.android.app.models.NotificationModel;
import az.azgram.android.app.models.response.NotificationListModel;
import az.azgram.android.app.operations.BusProvider;
import az.azgram.android.app.operations.Cache;
import az.azgram.android.app.operations.MessagingUtils;
import az.azgram.android.app.operations.NotificationUtils;

public class FirebaseMessageReceiverService extends FirebaseMessagingService {
    public static final String TAG = AzGram.TAG;

    private static class Type {
        public static final String TYPE_NOTIFICATION = "0";
        public static final String TYPE_MESSAGE = "1";
    }

    public FirebaseMessageReceiverService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String type = remoteMessage.getData().get("type");

        if (type.equals(Type.TYPE_NOTIFICATION)) {
            NotificationModel notification = new Gson().fromJson(
                    remoteMessage.getData().get("notificationJson"), NotificationModel.class
            );

            new Cache(this).increaseUnseenNotificationCountBy(1);

            if (NotificationFragment.isVisibleToUser()) {
                broadcast(notification);
            } else {
                show(notification);
            }
        } else {
            MessageModel message = new Gson().fromJson(
                    remoteMessage.getData().get("messageJson"), MessageModel.class
            );

            new Cache(this).increaseUnseenMessageCountBy(1);

            if (MessagingActivity.isVisibleToUser()) {
                broadcast(message);
            } else {
                show(message);
            }
        }
    }

    private void broadcast(NotificationModel notification) {
        BusProvider.getInstance().post(new NotificationReceivedEvent(
                notification
        ));
    }

    private void broadcast(MessageModel message) {
        BusProvider.getInstance().post(new MessageReceivedEvent(
                message
        ));
    }

    private void show(NotificationModel notification) {
        NotificationUtils.showNotification(this, notification);
    }

    private void show(MessageModel message) {
        MessagingUtils.showNotification(this, message);
    }
}