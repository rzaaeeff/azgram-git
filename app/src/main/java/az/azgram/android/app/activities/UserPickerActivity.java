package az.azgram.android.app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import az.azgram.android.app.R;
import az.azgram.android.app.adapters.RecyclerAdapterUserMini;
import az.azgram.android.app.fragments.UserPickerFragment;
import az.azgram.android.app.models.UserModel;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class UserPickerActivity extends AppCompatActivity implements RecyclerAdapterUserMini.Controller {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_picker);

        UserPickerFragment fragment = new UserPickerFragment();
        fragment.addController(this);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout_UserPickerActivity, fragment)
                .commit();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onUserClicked(UserModel user) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(UserModel.UID, user);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }
}
