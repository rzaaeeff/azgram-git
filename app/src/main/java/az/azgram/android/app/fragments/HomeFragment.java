package az.azgram.android.app.fragments;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import az.azgram.android.app.AzGram;
import az.azgram.android.app.R;
import az.azgram.android.app.activities.CoreActivity;
import az.azgram.android.app.adapters.feed.RecyclerAdapterPostsList;
import az.azgram.android.app.constants.Keys;
import az.azgram.android.app.events.posts.FeedReceivedEvent;
import az.azgram.android.app.events.posts.PostDeletedEvent;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.models.UserModel;
import az.azgram.android.app.models.response.FeedModel;
import az.azgram.android.app.operations.NetworkOperations;
import com.squareup.otto.Bus;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import im.ene.toro.Toro;
import im.ene.toro.ToroAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    // Widgets
    private SuperRecyclerView recyclerViewContent;
    private View rootView;

    // Other elements
    private FeedModel feed;
    private ToroAdapter adapter;
    private LinearLayoutManager layoutManager;

    private Context context;
    public Bus bus = new Bus();
    private UserModel request;

    private boolean isEmpty = false;
    private boolean isLoading = false;
    private boolean isRefreshing = false;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        init();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        bus.register(this);
        Toro.register(recyclerViewContent.getRecyclerView());

        Log.d(AzGram.TAG, "onResume: HomeFragment");
    }

    @Override
    public void onPause() {
        bus.unregister(this);
        Toro.unregister(recyclerViewContent.getRecyclerView());

        Log.d(AzGram.TAG, "onPause: HomeFragment");
        super.onPause();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            if (recyclerViewContent != null) {
                Toro.register(recyclerViewContent.getRecyclerView());
            }
        } else {
            if (recyclerViewContent != null)
                Toro.unregister(recyclerViewContent.getRecyclerView());
        }
    }

    public void init() {
        context = getContext();
        UserModel user = CoreActivity.user;

        // RecyclerView
        recyclerViewContent = (SuperRecyclerView) rootView.findViewById(R.id.recyclerViewContentHomeFragment);
        recyclerViewContent.setRefreshingColor(
                Color.parseColor("#337ab7"),
                Color.parseColor("#3258b7"),
                Color.parseColor("#3732b7"),
                Color.parseColor("#7032b7")
        );

        recyclerViewContent.getRecyclerView().setHasFixedSize(true);
        recyclerViewContent.getRecyclerView().setItemViewCacheSize(30);
        recyclerViewContent.getRecyclerView().setDrawingCacheEnabled(true);
        recyclerViewContent.getRecyclerView().setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // Data for RecyclerView
        feed = new FeedModel();
        feed.setPosts(new ArrayList<PostModel>());
        feed.setPage(0);

        // Adapter
        adapter = new RecyclerAdapterPostsList(context, feed);
        adapter.setHasStableIds(true);
        recyclerViewContent.setAdapter(adapter);

        // Layout Manager
        layoutManager = new LinearLayoutManager(context);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerViewContent.setLayoutManager(layoutManager);

        // Listeners for RecyclerView
        recyclerViewContent.setOnMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                if (isLoading || isRefreshing)
                    return;

                loadMore();
            }
        });

        recyclerViewContent.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isRefreshing) {
                    isRefreshing = false;
                    return;
                }

                if (isLoading) {
                    recyclerViewContent.setRefreshing(false);
                    return;
                }

                refresh();
            }
        });

        // Creating request
        request = new UserModel();
        request.setUsername(user.getUsername());
        request.setApiKey(Keys.API_KEY);
        request.setToken(user.getToken());

        // First time loading
        loadMore();

        // show progress
        recyclerViewContent.setRefreshing(true);
    }

    private void loadMore() {
        if (isLoading || isRefreshing)
            return;

        if (feed.getPage() != null) {
            NetworkOperations service = new NetworkOperations(context, bus);
            request.setPage(feed.getPage() + 1);
            service.getPostsEndpoint().getFeed(request);
            startLoading();
        }
    }

    private void refresh() {
        isRefreshing = true;
        init();
    }

    private void startLoading() {
        isLoading = true;
        recyclerViewContent.getMoreProgressView().setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        isLoading = false;
        recyclerViewContent.getMoreProgressView().setVisibility(View.GONE);
    }

    @Produce
    @Subscribe
    public void onAzGramServiceResponse(FeedReceivedEvent event) {
        if (isLoading) stopLoading();
        if (isRefreshing) recyclerViewContent.setRefreshing(false); // = stopRefreshing()

        if (event.getModel() != null) {
            if (event.getModel().getPosts() != null) {
                feed.getPosts().addAll(event.getModel().getPosts());
                feed.setPage(event.getModel().getPage());

                if (event.getModel().getPage() == null)
                    recyclerViewContent.setOnMoreListener(null);

                adapter.notifyDataSetChanged();
            } else if (event.getModel().getMessageId() == NetworkOperations.Posts.EMPTY) {
                isEmpty = true;
            }
        }
    }

    @Produce
    @Subscribe
    public void onAzGramServiceResponse(PostDeletedEvent event) {
        CoreModel post = event.getModel();

        if (post.getMessageId() == NetworkOperations.API_SUCCESS) {
            for (int i = 0; i < feed.getPosts().size(); i++) {
                if (post.getId().equals(feed.getPosts().get(i).getId())) {
                    feed.getPosts().remove(i);
                    adapter.notifyItemRemoved(i);
                }
            }
        }
    }
}
