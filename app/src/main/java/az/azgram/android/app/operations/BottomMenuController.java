package az.azgram.android.app.operations;

import az.azgram.android.app.models.CoreModel;

/**
 * Created by Rzaaeeff on 2/2/2017.
 */

public interface BottomMenuController {
    void shareButtonClicked();
    void downloadButtonClicked();
//    void sendButtonClicked();
    void searchResultClicked(CoreModel clickedItem);
    void asPPButtonClicked();
    void reportButtonClicked();
    void deleteButtonClicked();
}
