package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import az.azgram.android.app.R;
import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.PostModel;
import az.azgram.android.app.operations.DateUtils;

/**
 * Created by Rzaaeeff on 8/10/2017.
 */

public class ExtendedPostHolder extends PostHolder {

    // Constants

    // Widgets
    ImageView imageViewPrivacy;
    TextView textViewTime;

    // Other elements

    public ExtendedPostHolder(View itemView, Context context) {
        super(itemView, context);

        imageViewPrivacy = (ImageView) itemView.findViewById(R.id.imageViewPrivacyPostsListItem);
        textViewTime = (TextView) itemView.findViewById(R.id.textViewTimePostsListItem);
    }

    @Override
    public void bind(RecyclerView.Adapter adapter, @Nullable Object object) {
        super.bind(adapter, object);

        if (post.getPrivacy() != null) {
            switch (post.getPrivacy()) {
                case PostModel.Privacy.FOLLOWERS:
                    imageViewPrivacy.setImageResource(R.drawable.ic_privacy_followers);
                    break;
                case PostModel.Privacy.PRIVATE:
                    imageViewPrivacy.setImageResource(R.drawable.ic_privacy_private);
                    break;
                default:
                    imageViewPrivacy.setImageResource(R.drawable.ic_privacy_public);
                    break;
            }
        }

        setTimeAgo(post.getDate());
    }

    private void setTimeAgo(String dateStr) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(CoreModel.DATE_TIME_FORMAT);
            Date date = df.parse(dateStr.trim());

            String agoStr = DateUtils.getTimeAgo(date, context);
            textViewTime.setText(agoStr);
        } catch (Exception exc) {
            // We couldn't parse it
            textViewTime.setText(dateStr);
        }
    }
}
