package az.azgram.android.app.adapters.feed;

import android.content.Context;
import android.view.View;

import az.azgram.android.app.R;

/**
 * Created by Rzaaeeff on 8/10/2017.
 */

public class StatusHolder extends ExtendedPostHolder {
    // Constants
    public static final int LAYOUT_RES = R.layout.recycler_item_status_post_list;

    // Widgets

    // Other Elements

    public StatusHolder(View itemView, Context context) {
        super(itemView, context);
    }
}
