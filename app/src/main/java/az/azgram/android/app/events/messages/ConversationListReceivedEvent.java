package az.azgram.android.app.events.messages;

import az.azgram.android.app.models.response.ConversationListModel;

/**
 * Created by Rzaaeeff on 12/6/2016.
 */

public class ConversationListReceivedEvent {
    private ConversationListModel model;

    public ConversationListReceivedEvent(ConversationListModel model) {
        this.model = model;
    }

    public ConversationListModel getModel() {
        return model;
    }

    public void setModel(ConversationListModel model) {
        this.model = model;
    }
}
