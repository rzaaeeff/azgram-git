package az.azgram.android.app.models.response;

import java.util.List;

import az.azgram.android.app.models.CoreModel;
import az.azgram.android.app.models.NotificationModel;

/**
 * Created by Rzaaeeff on 5/16/2017.
 */

public class NotificationListModel extends CoreModel {
    private List<NotificationModel> notifications;

    public NotificationListModel() {}

    public NotificationListModel(List<NotificationModel> notifications) {
        this.notifications = notifications;
    }

    public List<NotificationModel> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationModel> notifications) {
        this.notifications = notifications;
    }
}
